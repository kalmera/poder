#!/bin/zsh

RD=`pwd`
JAR=`ls $RD/target/scala-3.1.1/põder-1.0.jar`

typeset -a TESTFILES
typeset -a TESTRESULTS

warnings=""

compare_output(){
  reg_triple="[ \t]*(.*);(.*);(.*)"
  reg_quad="[ \t]*(.*);(.*);(.*);(.*)"
  to_remove=""
  file_ok=true
  
  # iterate over expected warnings
  while IFS= read -r line; do
    if [[ $line =~ $reg_triple ]] 
    then
      MSG=$match[1]
      CLASS=$match[2]
      LINENR=$match[3]
      CAND=`grep -n -E ".*;$CLASS;.*;$LINENR\$" $2 | cut -d';' -f1`
      if [[ -z $CAND ]]
      then
        if [[ $file_ok ]]
        then
          warnings+="Problem with '$1.java':\n"
        fi
        warnings+=" FAIL: expected '$MSG' ($CLASS.java:$LINENR)\n"
        file_ok=false
      else
        regex=".*${CAND#*:}.*"
        if [[ $MSG =~ $regex ]]
        then
          to_remove+="${CAND%%:*}d;"
        else
          if [[ $file_ok ]]
          then
            warnings+="Problem with '$1.java':\n"
          fi
          warnings+=" FAIL: expected '$MSG' but got '${CAND#*:}' ($CLASS.java:$LINENR)\n"
          file_ok=false
        fi
      fi
    fi
  done < $3

  # remove matched warnings from produced warnings
  sed -i.bak "$to_remove" $2

  # iterate over the rest of the produced warnings
  while IFS= read -r line; do
    if [[ $line =~ $reg_quad ]] 
    then
      MSG=$match[1]
      CLASS=$match[2]
      MTHD=$match[3]
      LINENR=$match[4]
      CAND=`grep -E ".*;$CLASS;$LINENR\$" $3 | cut -d';' -f1`
      if [[ -z $CAND ]]
      then
        if [[ $file_ok ]]
        then
          warnings+="Problem with '$1.java':\n"
        fi
        warnings+=" FAIL: Not expected '$MSG' ($CLASS.$MTHD:$LINENR)"
        file_ok=false
      fi
    fi
  done < $2
}

test_one_prepare(){
  ptmpfile=$(mktemp /tmp/p6der-output.XXXXXX)

  TESTFILES+=("$2")
  TESTRESULTS+=($ptmpfile)

  java -jar $JAR --regression --analysis $1 $1 $2 >"$ptmpfile" &
}

test_one_conclude(){
  jtmpfile=$(mktemp /tmp/p6der-goal.XXXXXX)

  grep -n "// Põder:\(.*\)" $1/$TESTFILES[$2].java | sed -E "s/([0-9]*):.*\/\/.*Põder:(.*)/\2;$TESTFILES[$2];\1/" >"$jtmpfile"

  compare_output $TESTFILES[$2] $TESTRESULTS[$2] $jtmpfile

  rm $TESTRESULTS[$2]
  rm "$jtmpfile"
}

main(){
pushd regression > /dev/null
all_ok=true
for A in `ls .` 
do
  TESTFILES=()
  TESTRESULTS=()

  # start analysis for all tests as background jobs
  for B in `ls $A/` 
  do
    regex="(.*)\.java"
    if [[ $B =~ $regex ]] 
    then
      test_one_prepare $A $match[1]
    fi
  done
  
  # wait for all bg jobs to finish
  wait
  
  # check results one-by-one
  for ((i = 1; i <= $#TESTFILES; i++)) 
  do
    test_one_conclude $A $i
  done
  
  if ! [[ -z $warnings ]]
  then
    all_ok=false
    echo "Analysis '$A':\n$warnings"
    warnings=""
  fi
done

if [[ $all_ok == true ]]
then
  echo "All ok!"
fi

popd > /dev/null
}

main