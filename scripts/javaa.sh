#!/bin/sh
ARGS=$@
PWD=`pwd`
P6DER="$PWD/target/scala-2.12/põder-1.0.jar"
PROFILER="-agentpath:/Users/kalmera/põder/async-profiler-1.5-macos-x64/build/libasyncProfiler.so=start,svg,file=profile.svg"
# PROFILER=""

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8


CLASSES=()
OUTDIR=`mktemp -d 2>/dev/null || mktemp -d -t 'p6der'`

run_in() {
  pushd $1
  echo $1
  echo "command:"
  echo $2
  $2
  LC_CTYPE=C LANG=C sed -i '' 's/Ã/6/g' profile.svg
  LC_CTYPE=C LANG=C sed -i '' 's/µ//g' profile.svg
  DIR='~/põder'
  echo "$DIR"
  mv ./profile.svg $DIR/profile.svg
  popd
}

compile(){
  for A in $@
  do
    echo "compiling: $A"
    regex="(.*)\/src\/(.*)"
    if [[ $A =~ $regex ]] 
    then
      BASE=${BASH_REMATCH[1]}
      FILE=${BASH_REMATCH[2]}
      
      run_in "$BASE/src" "javac -g -d $OUTDIR $FILE"
      CLASSES+=(${FILE%.java}.class)
    else
      run_in "." "javac -g -d $OUTDIR $A"
      CLASSES+=(${A%.java}.class)
    fi
    echo $A
  done
}

analyze() {
  for A in $CLASSES 
  do
    run_in "$OUTDIR" "java $PROFILER -jar $P6DER --nogui --analysis modular $A"
  done
}


compile $@
analyze

rm -rf $OUTDIR


# java --jar $ARGS