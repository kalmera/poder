package põder.dom.intervals

/**
  * A Num is an int, +infinity, or -infinity.
  */
sealed trait Num extends Ordered[Num]:
  def compare(that: Num): Int =
    (this, that) match
      case (x, y) if x == y => 0
      case (IntNum(a), IntNum(b)) => a - b
      case (MInf, _) => -1
      case (_, PInf) => -1
      case (PInf, _) => 1
      case (_, MInf) => 1

case class IntNum(i: Int) extends Num:
  override def toString = s"$i"

case object MInf extends Num:
  override def toString = "-inf"

case object PInf extends Num:
  override def toString = "+inf"

/**
  * Finds the maximum of the given set of Num values.
  */
def max(s: Set[Num]): Num =
  if s.isEmpty then MInf
  else
    s.reduce { (a, b) =>
      (a, b) match
        case (PInf, _) | (_, PInf) => PInf
        case (x, MInf) => x
        case (MInf, x) => x
        case (IntNum(x), IntNum(y)) => IntNum(math.max(x, y))
    }

/**
  * Finds the minimum of the given set of Num values.
  */
def min(s: Set[Num]): Num =
  if s.isEmpty then PInf
  else
    s.reduce { (a, b) =>
      (a, b) match
        case (PInf, x) => x
        case (x, PInf) => x
        case (MInf, _) | (_, MInf) => MInf
        case (IntNum(x), IntNum(y)) => IntNum(math.min(x, y))
    }

def add(a: Num, b: Num): Num = (a, b) match
  case (_, MInf) | (MInf, _) => MInf
  case (_, PInf) | (PInf, _) => PInf
  case (IntNum(x), IntNum(y)) => IntNum(x + y)

def add(a: Num, b: Int): Num = a match
  case MInf => MInf
  case PInf => PInf
  case IntNum(x) => IntNum(x + b)

case class Interval(lb: Num, ub: Num):
  override def toString = s"[$lb, $ub]"

def intToInterval(i: Int): Interval = Interval(IntNum(i), IntNum(i))

def add(a: Interval, b: Interval): Interval =
  Interval(add(a.lb, b.lb), add(a.ub, b.ub))

def mul(a: Interval, b: Interval): Interval = (a.lb, a.ub, b.lb, b.ub) match
  case (IntNum(lb1), IntNum(ub1), IntNum(lb2), IntNum(ub2)) =>
    val values = Set(lb1 * lb2, lb1 * ub2, ub1 * lb2, ub1 * ub2)
    Interval(IntNum(values.min), IntNum(values.max))
  case _ => throw new Exception(s"Undefined multiplication of $a and $b")

def div(a: Interval, b: Interval): Interval = (a.lb, a.ub, b.lb, b.ub) match
  case (IntNum(lb1), IntNum(ub1), IntNum(lb2), IntNum(ub2)) =>
    val values = Set(lb1 / lb2, lb1 / ub2, ub1 / lb2, ub1 / ub2)
    Interval(IntNum(values.min), IntNum(values.max))
  case _ => throw new Exception(s"Undefined division of $a and $b")

abstract class Result
case class RTrue() extends Result
case class RFalse() extends Result
case class Uncertain() extends Result

def compare_LE(a: Interval, b: Interval): Result = (a, b) match
  case (x, y) if x.ub <= y.lb => RTrue()
  case (x, y) if x.lb > y.ub => RFalse()
  case _ => Uncertain()

def compare_GE(a: Interval, b: Interval): Result = (a, b) match
  case (x, y) if x.lb >= y.ub => RTrue()
  case (x, y) if x.ub < y.lb => RFalse()
  case _ => Uncertain()

def compare_EQ(a: Interval, b: Interval): Result = (a, b) match
  case (x, y) if x.lb == x.ub && x.ub == y.lb && y.lb == y.ub => RTrue()
  case (x, y) if x.ub < y.lb || x.lb > y.ub => RFalse()
  case _ => Uncertain()

def compare_NE(a: Interval, b: Interval): Result = compare_EQ(a, b) match
  case RFalse() => RTrue()
  case RTrue() => RFalse()
  case _ => Uncertain()

def compare_LT(a: Interval, b: Interval): Result = compare_GE(a, b) match
  case RFalse() => RTrue()
  case RTrue() => RFalse()
  case _ => Uncertain()

def compare_GT(a: Interval, b: Interval): Result = compare_LE(a, b) match
  case RFalse() => RTrue()
  case RTrue() => RFalse()
  case _ => Uncertain()

def cmp(a: Interval, b: Interval): Interval =
  var results: List[Int] = List()
  compare_EQ(a, b) match
    case RTrue() => return intToInterval(0)
    case Uncertain() => results = 0 :: results
    case _ =>

  compare_GT(a, b) match
    case RTrue() => return intToInterval(1)
    case Uncertain() => results = 1 :: results
    case _ =>
  compare_LT(a, b) match
    case RTrue() => return intToInterval(-1)
    case Uncertain() => results = -1 :: results
    case _ =>
  Interval(IntNum(results.min), IntNum(results.max))

object Lattice:
  def join(x: Interval, y: Interval): Interval =
    Interval(min(Set(x.lb, y.lb)), max(Set(x.ub, y.ub)))

  def leq(x: Interval, y: Interval): Boolean =
    y.lb <= x.lb && x.ub <= y.ub

  def widen(x: Interval, y: Interval): Interval = (x, y) match
    case (Interval(PInf, MInf), i) => i
    case (i, Interval(PInf, MInf)) => i
    case _ =>
      val a: Num = if x.lb <= y.lb then x.lb else MInf
      val b: Num = if x.ub >= y.ub then x.ub else PInf
      Interval(a, b)

  def narrow(x: Interval, y: Interval): Interval = (x, y) match
    case (Interval(PInf, MInf), i) => i
    case (i, Interval(PInf, MInf)) => i
    case _ =>
      val a: Num = if x.lb == MInf then y.lb else x.lb
      val b: Num = if x.ub == PInf then y.ub else x.ub
      Interval(a, b)

implicit object IntervalOrdering extends Ordering[Interval] {
  def compare(a: Interval, b: Interval): Int =
    a.lb.compare(b.lb) match {
      case 0 => a.ub.compare(b.ub)
      case n => n
    }
}
