package põder.dom

import põder.dom.expressions._
import põder.framework.{Lattice, NarrowOpF, WidenOpF}
import põder.framework.cfg.{Instr, Node}

object ExpsLat extends Lattice[Exps] with WidenOpF[Exps] with NarrowOpF[Exps]:
  inline def bot: Exps = Set.empty

  inline def join(x: Exps, y: Exps): Exps =
    if x.contains(Unknw) || y.contains(Unknw) then
      top
    else
      x.union(y)

  def expString(x:Exp):String =
    x match {
      case Op(op) => op.toString
      case Op(op, ls@_*) => s"$op(${ls.map(expString).mkString(", ")})"
      case EVar(n, _, v) => s"${n.toString}.${v.str}"
      case Join(as) => s"join(${as.map(expString).mkString(", ")})"
      case Meet(as) => s"meet(${as.map(expString).mkString(", ")})"
      case IntExp(i) => i.toString
      case LFP(n, c, v, e) => s"lfp(\\ ${v.str}. ${expString(e)})"
      case Label(e, _, _) => expString(e)
    }

  def toJson[jv](x: Exps)(using j: põder.util.json.JsonF[jv]): jv =
    if x.size == 1 then
      j.Str(expString(x.head))
    else
      j.Arr(x.map((x:Exp) => j.Str(expString(x))).toSeq*)

  inline def leq(x: Exps, y: Exps): Boolean =
    y.contains(Unknw) || x.subsetOf(y)

  inline def top: Exps = Set(Unknw)

  inline def widen(oldv: Exps, newv: Exps): Exps = join(oldv, newv)


sealed abstract class ExpMap:
  def map(f: Map[Var,(Boolean,Exps)] => Map[Var,(Boolean,Exps)]):ExpMap
case object TopMap extends ExpMap:
  override def map(f: Map[Var,(Boolean,Exps)] => Map[Var,(Boolean,Exps)]):ExpMap = this
case object BotMap extends ExpMap:
  override def map(f: Map[Var,(Boolean,Exps)] => Map[Var,(Boolean,Exps)]):ExpMap = this
case class MapExps(m: Map[Var, (Boolean,Exps)]) extends ExpMap:
  override def map(f: Map[Var,(Boolean,Exps)] => Map[Var,(Boolean,Exps)]):MapExps = MapExps(f(m))

object ExpMapLat extends Lattice[ExpMap] with WidenOpF[ExpMap] with NarrowOpF[ExpMap]:
  inline def bot: ExpMap = BotMap
  inline def top: ExpMap = TopMap

  private inline def emptyMeet(e:Exp):Boolean = {
    e match
      case Meet(as) => as.isEmpty
      case _ => false
  }

  inline def merge(m1:Map[Var, (Boolean,Exps)], m2:Map[Var, (Boolean,Exps)]): Map[Var, (Boolean,Exps)] =
    //    println("merging "++m1.toString++" and "++m2.toString)
    val r = (m1.keySet ++ m2.keys).map{ i =>
      val (d1, m1x) = m1.getOrElse(i,(true,Set(Unknw)))
      val (d2, m2x) = m2.getOrElse(i,(true,Set(Unknw)))
      i -> (d1 || d2, (m1x ++ m2x).filterNot(emptyMeet(_)))
    }.toMap
    //    println("result "++r.toString)
    r

  inline def join(x: ExpMap, y: ExpMap): ExpMap =
    (x,y) match {
      case (MapExps(a), MapExps(b)) =>
        MapExps(merge(a,b))
      case (TopMap, _) | (_, TopMap) => TopMap
      case (BotMap, a) => a
      case (a, BotMap) => a
    }

  def toJson[jv](x: ExpMap)(using j: põder.util.json.JsonF[jv]): jv =
    x match {
      case MapExps(a) =>
        val xs = a.view.filter(_._2._1)
        val xsj = xs.map(x => x._1.str -> ExpsLat.toJson(x._2._2)).toSeq
        val ys = a.filterNot(_._2._1)
        if ys.nonEmpty then
          j.Obj(xsj :+ ("unchanged" -> j.Str(ys.keys.view.map(_.str).mkString(", ")))*)
        else
          j.Obj(xsj*)
      case TopMap =>
        j.Str("unknown")
      case BotMap =>
        j.Str("unreachable")
    }

  inline def leq(x: ExpMap, y: ExpMap): Boolean = {
    (x,y) match
      case (MapExps(a), MapExps(b)) =>
        a.forall{
          case (v,(d1,e1)) =>
            val (d2,e2) = b.getOrElse(v,(true, Set(Unknw)))
            d1 <= d2 && ExpsLat.leq(e1,e2)
        }
      case (BotMap, BotMap) | (TopMap, TopMap) | (BotMap, _) | (_, TopMap) => true
      case _ => false
  }

  override inline def equal(x: ExpMap, y: ExpMap): Boolean = x==y

  inline def widen(oldv: ExpMap, newv: ExpMap): ExpMap = join(oldv, newv)
