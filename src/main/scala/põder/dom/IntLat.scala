package põder.dom

import põder.dom.intervals._
import põder.framework.{Lattice, NarrowOpF, WidenOpF}
import põder.util.json.JsonF

import scala.collection.immutable.Map

trait Expr

case object Const extends Expr
case class IVar(i: Int) extends Expr
case class Arith(op: String, left: Expr, right: Expr) extends Expr
case object Unknown extends Expr

type IntervalDomain = (Option[List[Interval]], Option[Map[String, Interval]], Option[List[Expr]])

object IntLat extends Lattice[IntervalDomain] with WidenOpF[IntervalDomain] with NarrowOpF[IntervalDomain]:
  override def join(x: IntervalDomain, y: IntervalDomain): IntervalDomain =
    if x == top || y == top then return top
    else if x == bot then return y
    else if y == bot then return x
    else if x == y then return x

    // Stack
    val stack = (x._1, y._1) match
      case (None, None) => None
      case (Some(e), None) => Some(e)
      case (None, Some(e)) => Some(e)
      case (Some(a), Some(b)) =>
        Some(
          a.zipAll(b, Interval(PInf, MInf), Interval(PInf, MInf))
            .map(e => Lattice.join(e._1, e._2))
        )

    // LIVars
    val lIVars: Option[Map[String, Interval]] = (x._2, y._2) match
      case (None, None) => None
      case (Some(e), None) => Some(e)
      case (None, Some(e)) => Some(e)
      case (Some(a), Some(b)) =>
        Some(
          (a.toSeq ++ b.toSeq)
            .groupBy(_._1)
            .map{ case (x,y) => (x,y.map(_._2).reduce(Lattice.join))}
        )
    (stack, lIVars, x._3)

  override def leq(d1: IntervalDomain, d2: IntervalDomain): Boolean =
    if d1 == bot || d2 == top || d1 == d2 then true
    else if d1 == top || d2 == bot then false
    else
      val d1_stack = d1._1.getOrElse(List())
      val d1_locals = d1._2.getOrElse(Map())
      val d2_stack = d2._1.getOrElse(List())
      val d2_locals = d2._2.getOrElse(Map())

      if d1_stack.length != d2_stack.length || d1_locals.size != d2_locals.size then
        d1_stack.length <= d2_stack.length && d1_locals.size <= d2_locals.size
      else
        d1_stack.lazyZip(d2_stack).forall(Lattice.leq) &&
          (d1_locals.keys ++ d2_locals.keys)
            .forall(k => Lattice.leq(d1_locals(k), d2_locals(k)))

  override def top: IntervalDomain =
    (Some(List()), Some(Map()), Some(List()))

  override def bot: IntervalDomain =
    (None, None, None)

  override def toJson[jv](x: IntervalDomain)(using j: JsonF[jv]): jv =
    x match
      case (None, None, None) => j.Str("bot")
      case (Some(List()), t, Some(List())) if t == Some(Map()) => j.Str("top")
      case _ => j.Str(s"2\nStack variables: ${x._1.getOrElse(List())}\nLocal variables: ${Map(x._2.getOrElse(Map()).toSeq.sortBy(_._1)*)}\nStack details: ${x._3}\n")

  override def widen(oldv: IntervalDomain, newv: IntervalDomain): IntervalDomain =
    // Stack
    val stack: Option[List[Interval]] = (oldv._1, newv._1) match
      case (None, None) => None
      case (Some(e), None) => Some(e)
      case (None, Some(e)) => Some(e)
      case (Some(a), Some(b)) => Some(a.lazyZip(b).toList.map(e => Lattice.widen(e._1, e._2)))

    // LIVars
    val lIVars: Option[Map[String, Interval]] = (oldv._2, newv._2) match
      case (None, None) => None
      case (Some(e), None) => Some(e)
      case (None, Some(e)) => Some(e)
      case (Some(a), Some(b)) => Some((a.toSeq ++ b.toSeq).groupBy(_._1).map{case (x,y) => (x,y.map(_._2).reduce(Lattice.widen))})
    (stack, lIVars, newv._3)

  override def narrow(oldv: IntervalDomain, newv: IntervalDomain): IntervalDomain =
    if newv == bot then return bot

    // Stack
    val stack: Option[List[Interval]] = (oldv._1, newv._1) match
      case (None, None) => None
      case (Some(e), None) => Some(e)
      case (None, Some(e)) => Some(e)
      case (Some(a), Some(b)) => Some(a.lazyZip(b).toList.map(e => Lattice.narrow(e._1, e._2)))

    // LIVars
    val lIVars: Option[Map[String, Interval]] = (oldv._2, newv._2) match
      case (None, None) => None
      case (Some(e), None) => Some(e)
      case (None, Some(e)) => Some(e)
      case (Some(a), Some(b)) => Some((a.toSeq ++ b.toSeq).groupBy(_._1).map{ case (x,y) => (x,y.map(_._2).reduce(Lattice.narrow))})
    (stack, lIVars, newv._3)


