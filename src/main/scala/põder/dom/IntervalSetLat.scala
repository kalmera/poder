package põder.dom

import põder.dom.intervals._
import põder.framework.{Lattice, NarrowOpF, WidenOpF}

import scala.annotation.tailrec
import scala.collection.immutable.SortedSet

type IntervalSet = SortedSet[Interval]

object IntervalSetLat extends Lattice[IntervalSet] with WidenOpF[IntervalSet] with NarrowOpF[IntervalSet]:
  override def bot: IntervalSet = SortedSet.empty
  override def top: IntervalSet = SortedSet(Interval(MInf, PInf))
  def toJson [jv](x: IntervalSet)(using j: põder.util.json.JsonF[jv]): jv =
    j.Str(x.mkString("{",", ","}"))

  def foldNormal[A](is:IntervalSet, z:A, f: (A,Interval) => A): A =
    val (c,v) = is.foldLeft[(A,Option[Interval])]((z,None)){
      case ((c,None),y) => (c,Some(y))
      case ((c,Some(Interval(xlb,xub))),Interval(ylb,yub)) =>
        (xub.compare(add(ylb,1)) match {
          case n if n<=0 => (c,Some(Interval(xlb,yub)))
          case _ => (f(c,Interval(xlb,xub)),Some(Interval(ylb,yub)))
        })
    }
    v match {
      case Some(i) => f(c,i)
      case None => c
    }

  def normalize(is:IntervalSet): IntervalSet =
    val r = foldNormal[IntervalSet](is,SortedSet.empty,{
      case (is,i) => is.+(i)
    })
    //    println("n("++is.mkString("{",", ","}")++"="++r.mkString("{",", ","}"))
    r

  def isSingle(is:IntervalSet): Boolean = is.size==1 && is.head.lb==is.head.ub

  def zipWithNormal[A](is1:IntervalSet, is2:IntervalSet, z:A, f: (A,Interval,Boolean,Boolean,Boolean,Boolean) => A): A =
    @tailrec
    def loop(a:A, is1:IntervalSet, is2:IntervalSet): A =
      if is1.isEmpty then
        is2.foldLeft(a){ case (a,i) => f(a,i,false,true,false,false) }
      else if is2.isEmpty then
        is1.foldLeft(a){ case (a,i) => f(a,i,true,false,false,false) }
      else
        val x = is1.head
        val xs = is1.tail
        val y = is2.head
        val ys = is2.tail
        if x.lb == y.lb then
          if x.ub == y.ub then
          //  x:  |  |
          //  y:  |  |
            loop(f(a,x,true, true,false,false), xs, ys)
          else if x.ub < y.ub then
          //  x:  | |
          //  y:  |    |
            loop(f(a,x,true, true,false,true), xs, ys.+(Interval(x.ub,y.ub)))
          else /* x.ub >= y.ub */
          //  x:  |    |
          //  y:  |  |
            loop(f(a,y,true, true,false,true), xs.+(Interval(y.ub,x.ub)), ys)
        else if x.lb > y.lb then
          if y.ub < x.lb then
          //  x:       |   |
          //  y:  |  |
            loop(f(a,y,false, true,true,false), xs+x, ys)
          else
          //  x:     |     |
          //  y:  |     |
            loop(f(a,Interval(y.lb,x.lb),false, true,true,false), xs+x, ys.+(Interval(x.lb,y.ub)))
        else
          if y.lb <= x.ub then
          //  x:  |----|
          //  y:    |---
            loop(f(a,Interval(x.lb,y.lb), true, false,true,false), xs+Interval(y.lb,x.ub), ys+y)
          else /* y.lb > x.ub */
          //  x:  |   |
          //  y:        |  |
            loop(f(a,x, true, false,true,false), xs, ys+y)

    loop(z, normalize(is1), normalize(is2))

  def join(x: IntervalSet, y: IntervalSet): IntervalSet =
    normalize(zipWithNormal(x,y, SortedSet.empty,{ case (t, i, _, _,_,_) => t+i}))

  def meet(x: IntervalSet, y: IntervalSet): IntervalSet =
    normalize(zipWithNormal(x,y, SortedSet.empty,{
      case (t, i, true, true,_,_) => t+i
      case (t, i, a, b, c, d) => t
    }))

  def leq(x: IntervalSet, y: IntervalSet): Boolean =
    zipWithNormal(x,y,true,{ case (t, _, l, r,_,_) => t&&r})

  override
  def equal(x: IntervalSet, y: IntervalSet): Boolean =
    zipWithNormal(x,y,true,{ case (t, _, l, r,_,_) => t&&r&&l})

  def widen(oldv: IntervalSet, newv: IntervalSet): IntervalSet =
    val r = zipWithNormal(oldv,newv, SortedSet.empty,{
      case (t, Interval(lb,ub), _, _, true,_) => t+Interval(MInf, ub)
      case (t, Interval(lb,ub), _, _, false, true) => t+Interval(lb,PInf)
      case (t, i, _,_,_,_) => t+i
    })
    normalize(r)

  override def narrow(oldv: IntervalSet, newv: IntervalSet): IntervalSet = newv