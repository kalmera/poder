package põder.dom.expressions

import põder.dom.intervals.Interval
import põder.framework.cfg.{Instr, LNode, Node}

abstract class Var:
  def str: String
case class SVar(i:Int) extends Var:
  override val str: String = s"$$${i}"
case class LVar(i:Int) extends Var:
  var source_name: Option[String] = None
  override def str: String = source_name.getOrElse(s"£$i")

sealed abstract class Exp:
  val short: Boolean

type Ctx = Unit

case class Op(op:Instr, l:Exp*) extends Exp:
  override val short: Boolean = l.forall(!_.isInstanceOf[Op])
  override def toString: String = if l.isEmpty then s"Op($op)" else s"Op($op,${l.mkString(",")})"
case class EVar(n:Node, c:Ctx, v:Var) extends Exp:
  override val short = true

case class LFP(n:Node,c:Ctx,v:Var, e:Exp) extends Exp:
  override val short = false
case class Join(as:Set[Exp]) extends Exp:
  override val short = false
  override def toString: String = s"Join(${as.mkString(",")})"
case class Meet(as:Set[Exp]) extends Exp:
  override val short = false
  override def toString: String = s"Meet(${as.mkString(",")})"
case class IntExp(i:Interval) extends Exp:
  override val short = true
case class Label(e:Exp, source: String, line: Int) extends Exp:
  override val short: Boolean = e.short

val Unknw: Exp = Meet(Set.empty)

def freeVars(e:Exp):Set[(Node,Ctx,Var)] =
  e match {
    case Op(_, l@_*) => l.flatMap(freeVars).toSet
    case EVar(n, c, v) => Set((n, c, v))
    case LFP(n, c, v, e) => freeVars(e)-((n,c,v))
    case Join(as) => as.flatMap(freeVars)
    case Meet(as) => as.flatMap(freeVars)
    case Label(e, _, _) => freeVars(e)
    case _ => Set.empty
  }

type Exps = Set[Exp]

def distr(vs:Seq[Exps]): Set[Seq[Exp]] =
    if vs.isEmpty then
      Set()
    else if vs.size == 1 then
      vs.head.map(Seq(_))
    else
      val hd = vs.head
      val tl = distr(vs.tail)
      for { xs <- vs.head
            xss <- distr(vs.tail) }
      yield xs +: xss

def distrOp(op:Instr, vs:Exps*): Exps =
    for (xs <- distr(vs)) yield Op(op, xs*)

def subst(env: (Node, Ctx, Var) => Exp, seen:Set[(Node, Ctx, Var)])(exp: Exp): Exp =
    exp match
      case EVar(n, c, v) =>
        if seen contains(n, c, v) then
          EVar(n, c, v)
        else
          val e = subst(env, seen + ((n, c, v)))(env(n, c, v))
          if freeVars(e) contains ((n, c, v)) then LFP(n, c, v, e) else e
      case Op(op, l@_*) => Op(op, l.map(subst(env, seen))*)
      case LFP(n, c, v, e) => LFP(n, c, v, subst(env, seen + ((n, c, v)))(e))
      case Join(as) => Join(as.map(subst(env, seen)))
      case Meet(as) => Meet(as.map(subst(env, seen)))
      case Label(e,s,l) => Label(subst(env,seen)(e), s, l)
      case t => t

def optimize(exp: Exp): Exp =
    exp match {
      case Op(op, l@_*) => Op(op, l.map(optimize)*)
      case EVar(n, c, v) => EVar(n, c, v)
      case LFP(n,c,v, Join(as)) if as.size==2 && as.contains(EVar(n,c,v)) =>
        as.-(EVar(n,c,v)).head
      case LFP(n, c, v, e) => LFP(n, c, v, optimize(e))
      case Join(as) => Join(as.map(optimize))
      case Meet(as) => Meet(as.map(optimize))
      case Label(e,s,l) => Label(optimize(e), s, l)
      case t => t
    }

def deps(exp: Exp): Set[(Node, Ctx, Var)] =
  exp match
    case Op(op, l@_*) => l.toSet.flatMap(deps)
    case EVar(n, c, v) => Set((n,c,v))
    case LFP(n, c, v, e) => deps(e) - ((n,c,v))
    case Join(as) => as.flatMap(deps)
    case Meet(as) => as.flatMap(deps)
    case IntExp(i) => Set.empty
    case Label(e,s,l) => deps(e)
