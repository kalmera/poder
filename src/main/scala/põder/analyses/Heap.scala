package põder.analyses

import scala.language.implicitConversions
import põder.dom.{BotMap, ExpMap, ExpMapLat, ExpsLat, IntervalSetLat, MapExps, TopMap}
import põder.dom.expressions.*
import põder.dom.intervals.*
import põder.framework.*
import põder.framework.cfg.*
import põder.log.Log
import põder.util.CellConv
import põder.util.Cell
import põder.util.json.JsonF
import põder.util.typ.{MethodType, Parser, VoidT}

import scala.annotation.tailrec
import scala.math.Ordering.OptionOrdering
import scala.collection.immutable.{BitSet, Queue, SortedSet, TreeSet}

sealed abstract class HKey
case object HeapKey extends HKey
case object ResultKey extends HKey

type AVal = Top|Lifted[Set[String]]
type HSList = List[AVal]
object HeapStack extends ListLat(StringTopSetLat)
object HeapLat extends LiftBoth(ProductWN(HeapStack,HeapStack))

type heap = HeapLat.t

object HeapKeyJsonOp extends JsonOpF[HeapKey.type]:
  override def toJson[jv](x: HeapKey.type )(using j: JsonF[jv]): jv =
    j.Str("Heap")

trait Heap  extends JBCAnalysis:
  override type NCtx[N1, C1, K1, D1] = SolvCtx[N1, C1, K1, D1]
  override type ECtx[A,B,C,D] = JBCtx[A,B,C,D]

  override def forward: Boolean = true

  override type VarKey = HeapKey.type
  final lazy val VarKey: JsonOpF[VarKey] = HeapKeyJsonOp

  type VarCtx = Unit
  final lazy val VarCtx: JsonOpF[VarCtx] = UnitJsonOp

  final lazy val startCtxKey: Seq[(VarCtx,VarKey)] = Seq(((),HeapKey))

  override type Dl = HeapLat.t
  override given L: (Lattice[Dl] & WidenOpF[Dl] & NarrowOpF[Dl]) =
    HeapLat

  def update(n:Int, v:Top|Lifted[Set[String]], xs:List[Top|Lifted[Set[String]]]): List[Top|Lifted[Set[String]]] =
    if xs.isEmpty then
      List(v)
    else if n==0 then
      v :: xs.tail
    else
      xs.headOption.getOrElse(Lifted(Set.empty)) :: update(n-1,v,xs.tail)

  def get(n:Int, xs:List[Top|Lifted[Set[String]]]): Top|Lifted[Set[String]] =
    if n==0 then
      xs.headOption.getOrElse(Lifted(Set.empty))
    else
      xs match {
        case _ :: tail => get(n-1, tail)
        case Nil => Lifted(Set.empty)
      }

  def gVar(s:String, f:String): Node = GNode(s++"."++f)

  def setGlobal(ty:String, i:String, d:AVal)(using ctx: JBCtxT): Unit =
    val dl = Lifted((List(d),List.empty))
    Log.log(s"side effect: $ty.$i <- $d")
    ctx.set(gVar(ty, i), (), HeapKey, dl)

  def getGlobal(ty:String,i:String)(using ctx: JBCtxT): AVal =
    val g = ctx.get(gVar(ty,i), (),HeapKey)
    g match {
      case Bot => Lifted(Set.empty)
      case Lifted((x::xs,_)) => x
      case _ => Top
    }


  override inline def node(nd: Node, b: VarCtx, k: VarKey)(using ctx: SolvCtxT): Dl =
    nd match
      case n:LNode if n.is_start && n.methodInfo.mthd=="main" && project.mainClass==n.methodInfo.cls =>
        Lifted((List.empty, List(Top)))
      case _ => L.bot

  override inline def start(locals: Int, stack: Int, access: Int)(using ctx: JBCtxT): Dl =
    ctx.st(using HeapKey)

  inline def isThreadRun(a:Option[(String,String,String)]): Boolean =
    a match {
      case Some((cls,mth,ty)) =>
        cls=="java/lang/Thread" && mth=="run" // todo: check for subclasses
      case None =>
        false
    }

  override def isBuiltinFun(instr: MethodInstr, typ: MethodType, static: Boolean): Boolean =
    instr match {
      case INVOKESTATIC(a) if a._1 == "P6der" && a._2 == "check" => true
      case INVOKESTATIC(a) if a._1 == "P6der" && a._2 == "evalInt" => true
      case INVOKESTATIC(a) if a._1.startsWith("java/lang/") => true
      case INVOKESPECIAL(a) if a._1.startsWith("javalang/") => true
      case _ => isThreadRun(instr.a.get)
    }

  override def explain[jv](n: Node, c: VarCtx, k: VarKey, lod: Int)(using ctx: SolvCtxT, j: JsonF[jv]): jv =
    val d = ctx.get(n,c,k)
    (n,k,d) match
      case  (GNode(k),HeapKey,Lifted((v::_,_))) if lod==0 =>
        j.Obj(k -> StringTopSetLat.toJson(v))
      case _ => super.explain(n,c,k)

  // hargnemise üleminekufunktsioon
  override inline def branch(instr: JumpInstr, thn: Boolean)(using ctx: JBCtxT): Dl =
    val d: Dl = ctx.st(using HeapKey)
    (instr, d) match
      case (IF_ICMPx(x,a), Lifted((x1::x2::xs, ss))) =>
        Lifted((xs, ss))
      case (_,_) => d

  // todo: side-effect this to a global variable
  val top_pointer : collection.mutable.Set[String] = collection.mutable.Set.empty

  inline def normal_heap(instr: Instr)(using ctx: JBCtxT): heap =
    ctx.st(using HeapKey) match {
      case Lifted((ss, vs)) =>
        (instr,ss) match {
          case (NEW(cl), _) =>
            val p = cl.apply++"#"++ctx.from.toString
            top_pointer += p
            Lifted((Lifted(Set(p)) :: ss, vs))
          case (DUP, x::_) =>
            Lifted((x::ss, vs))
          case (c:ICONST, _) =>
            Lifted((Lifted(Set.empty)::ss, vs))
          case (BIPUSH(c), _) =>
            Lifted((Lifted(Set.empty)::ss, vs))
          case (SIPUSH(c), _) =>
            Lifted((Lifted(Set.empty)::ss, vs))
          case (ACONST_NULL, _) =>
            Lifted((Lifted(Set.empty)::ss, vs))
          case (PUTFIELD(f), _) =>
            ss match
              case v::Lifted(xs)::_ =>
                xs.foreach{setGlobal(_,f._2,v)}
              case _ => ()
            Lifted((ss.drop(2), vs))
          case (GETFIELD(f), _) =>
            ss match
              case Lifted(xs)::_ =>
                val d: Top|Lifted[Set[String]] = StringTopSetLat.Join(xs.map{getGlobal(_,f._2)}.toSeq)
                Lifted((d::ss.drop(1), vs))
              case _ =>
                Lifted((Top::ss.drop(1), vs))
          case (xLOAD(t,a), _) =>
            Lifted((get(a.toInt, vs) :: ss, vs))
          case (xSTORE(t,a), x::xs) =>
            Lifted((xs, update(a.toInt,x,vs)))
          case (INVOKESPECIAL(a), _) if a._1 == "java/lang/Object" && a._2 == "<init>" =>
            Lifted((ss.drop(1), vs))
          case (INVOKESTATIC(a), _) if a._1 == "java/lang/Boolean" && a._2 == "valueOf" =>
            Lifted((ss, vs))
          case _ =>
            Lifted((ss, vs))
        }
      case _ => HeapLat.top
    }

  // "tavaline" üleminekufunktsioon
  override inline def normal(instr: Instr)(using ctx: JBCtxT): Dl =
    normal_heap(instr)

  // erindi üleminekufunktsioon
  override inline def throws(instr: Instr, ex: Either[String, Set[String]])(using ctx: JBCtxT): Dl = L.bot

  // annotatsiooni ülemunekufunktsioon
  override inline def annot(ant: Annot)(using ctx: JBCtxT): Dl = ctx.st(using HeapKey)

  def enterMap(instr:MethodInstr, typ:MethodType, static: Boolean, st:HSList): HSList =
    val n = if static then typ.args.size else typ.args.size+1
    st.take(n)

  // need on meetodiväljakutsete jaoks
  override inline def enter(instr: MethodInstr, typ: MethodType, static: Boolean)(using ctx: JBCtxT): Seq[(VarCtx, VarKey, VarKey => Dl,Dl)] =
    ctx.st(using HeapKey) match {
      case Lifted((st,hp)) =>
        val entervars = enterMap(instr, typ, static, st)
        val startState = Lifted((List.empty, entervars))
        Seq(((), HeapKey, _ => Lifted(st,hp), startState))
      case _ => Seq.empty
    }


  override inline def combine(instr: MethodInstr, typ: MethodType, static: Boolean, result: VarKey => Dl)(using ctx: JBCtxT): Dl =
    (ctx.st(using HeapKey), result(HeapKey)) match {
      case (Lifted(st, hp), Lifted(str, hpr)) =>
        val n = if static then typ.args.size else typ.args.size+1
        if typ.ret == VoidT then
          Lifted(st.drop(n), hp)
        else
          Lifted(str.head :: st, hp)
      case _ => L.top
    }