package põder.analyses

import põder.framework.{AnalysisCSys, AnalysisTask, ConstrSys, JBCAnalysis, CompiledAnalysis}
import põder.framework.cfg.{Annot, Node, ProgramCFG, TFType}
import põder.util.Project

final class HeapCompiled    (val at: AnalysisTask) extends HeapTrans(at)   with CompiledAnalysis
final class DepCompiled     (val at: AnalysisTask) extends Dep             with CompiledAnalysis
final class ModSumCompiled  (val at: AnalysisTask) extends ModularSummary  with CompiledAnalysis
final class ValueCompiled   (val at: AnalysisTask) extends Value           with CompiledAnalysis
final class IntervalCompiled(val at: AnalysisTask) extends IntervalValue   with CompiledAnalysis
final class WPCompiled      (val at: AnalysisTask) extends WP              with CompiledAnalysis
final class ModularCompiled (val at: AnalysisTask) extends Modular         with CompiledAnalysis
final class CPACompiled     (val at: AnalysisTask) extends ConstProp       with CompiledAnalysis
final class SecCompiled     (val at: AnalysisTask) extends SecLabel        with CompiledAnalysis
final class ExprCompiled    (val at: AnalysisTask) extends OEscIntExpr(at)  with CompiledAnalysis
final class OEscCompiled    (val at: AnalysisTask) extends ObjectEscape    with CompiledAnalysis

object Registry:
  private var csys: List[(String, AnalysisTask => ConstrSys)] =
    List(
      ("expr"       , new ExprCompiled(_)    ),
      ("heap"       , new HeapCompiled(_)    ),
      ("dep"        , new DepCompiled(_)     ),
      ("modular-sum", new ModSumCompiled(_)  ),
      ("value"      , new ValueCompiled(_)   ),
      ("interval"   , new IntervalCompiled(_)),
      ("wp"         , new WPCompiled(_)      ),
      ("modular"    , new ModularCompiled(_) ),
      ("cpa"        , new CPACompiled(_)     ),
      ("sec"        , new SecCompiled(_)     ),
      ("oesc"       , new OEscCompiled(_)    ),
    )

  def getNames: Seq[String] = csys.map(_._1)

  def getCSys(name: String, at: AnalysisTask): ConstrSys =
    csys.find(_._1 == name) match
      case Some(value) => value._2(at)
      case None =>
        println(s"No analysis named: '$name'")
        sys.exit(-1)
