package põder.analyses

import scala.language.implicitConversions
import põder.dom.{BotMap, ExpMap, ExpMapLat, ExpsLat, IntervalSetLat, MapExps, TopMap}
import põder.dom.expressions.*
import põder.dom.intervals.*
import põder.framework.*
import põder.framework.cfg.*
import põder.log.Log
import põder.util.json.JsonF
import põder.util.*
import põder.util.typ.{MethodType, Parser}
import põder.util.CellConv

import scala.annotation.tailrec
import scala.math.Ordering.OptionOrdering
import scala.collection.immutable.{BitSet, SortedSet, TreeSet}

case class FieldGVar(ty:String, str:String,sig:String) extends Var

trait Expr extends JBCAnalysis:
  inline def forward: Boolean = true

  override type NCtx[N1, C1, K1, D1] = SolvCtx[N1, C1, K1, D1]
  override type ECtx[N1,C1,K1,D1] = IntQueryCtx[N1,C1,K1,D1] & EscapedVarCtx[N1,C1,K1,D1]

  given unit: Unit = ()

  type VarKey = Unit
  final lazy val VarKey: JsonOpF[VarKey] = UnitJsonOp

  type VarCtx = Unit
  final lazy val VarCtx: JsonOpF[VarCtx] = UnitJsonOp

  final lazy val startCtxKey: Seq[(VarCtx,VarKey)] = Seq(((),()))

  override type Dl = ExpMap
  override given L: (Lattice[Dl] & WidenOpF[Dl] & NarrowOpF[Dl]) =
    ExpMapLat

  extension (m:Map[Var,Exp])
    def replaceExp: Exp => Exp = {
      case Op(op, l@_*) => Op(op, l.map(replaceExp)*)
      case v: EVar => m.getOrElse(v.v,v)
      case r => r
    }

  extension (m:Map[Var,(Boolean,Exps)])
    def fetch(v:Var): Exps =
      val r = m.getOrElse(v,(true,Set(Unknw)))._2
      //      println(s"${v.str} -> ${r.toString()}")
      r

    def pushStack(j: Int):Map[Var,(Boolean,Exps)] =
      def g: ((Var, (Boolean,Exps))) => (Var,(Boolean,Exps)) = {
        case (SVar(i),(_,es)) => (SVar(i+j) -> (true,es))
        case (v,es) => (v -> es)
      }
      m.map(g)

    def popStack(j: Int):Map[Var,(Boolean,Exps)] =
      def g: ((Var, (Boolean,Exps))) => Seq[(Var,(Boolean,Exps))] = {
        case (SVar(i),(_,es)) if i>j => Seq(SVar(i-j) -> (true,es))
        case (SVar(i),(_,es)) => Seq()
        case (v,es) => Seq(v -> es)
      }
      m.flatMap(g)

    def update(us:(Var, Exps)*): Map[Var,(Boolean,Exps)] =
      m ++ us.map{case (v,e) => v -> (true, e)}

  def gVar(s:String): Node = GNode(s)

  def setGlobal(ty:String, i:String, sig:String, d:Exps)(using ctx: JBCtxT): Unit =
    val dl = MapExps(Map(FieldGVar(ty,i,sig) -> (true, d.filterNot(x => x.isInstanceOf[Meet] && x.asInstanceOf[Meet].as.isEmpty))))
    Log.log(s"side effect: $ty <- $d")
    ctx.set(gVar(ty),ctx.c,ctx.k,dl)

  def getGlobal(ty:String, i:String, sig:String)(using ctx: JBCtxT): Exps =
    Set(EVar(gVar(ty),ctx.c,FieldGVar(ty, i, sig)))

  override def isBuiltinFun(instr: MethodInstr, typ: MethodType, static: Boolean): Boolean =
    instr match {
      case INVOKESTATIC(a) if a._1 == "P6der" && a._2 == "check" => true
      case INVOKESTATIC(a) if a._1 == "P6der" && a._2 == "evalInt" => true
      case _ => false
    }


  def getId(from:Node, to:Node, c:VarCtx): Map[Var, (Boolean,Exps)] =
    (from,to) match
      case (ln:LNode,ln1:LNode) =>
        val svs = for (i <- 0 until ln.stackSize) yield SVar(i)
        val lvs = for (i <- 0 until ln.methodInfo.locals) yield LVar(i)
        for (v <- lvs) v.source_name = ln1.varNames.get(v.i)
        val r = (svs ++ lvs).map(v => v -> (false, Set[Exp](EVar(ln, c, v)))).toMap
        //    println(s"getId(${ctx.from.id}) = ${r.toString}")
        r
      case _ => Map.empty

  def getId(using ctx: JBCtxT): Map[Var, (Boolean,Exps)] = getId(ctx.from, ctx.to, ctx.c)

  override inline def node(nd: Node, b: VarCtx, k: VarKey)(using ctx: SolvCtxT): Dl =
    nd match
      case n:LNode if n.is_start && n.methodInfo.mthd=="main" && project.mainClass==n.methodInfo.cls =>
        MapExps(getId(n,n,b))
      case _ => L.bot

  override inline def start(locals: Int, stack: Int, access: Int)(using ctx: JBCtxT): Dl = getState

  def getState(using ctx: JBCtxT): Dl =
    val st = ctx.st
    if st == L.bot then L.bot else if ctx.from_start then MapExps(getId) else st

  def testeq(thn: Boolean, e: ExpMap)(using ctx: JBCtxT): ExpMap =
    (e,ctx.from) match
      case (BotMap,_) => BotMap
      case (MapExps(m),fln:LNode) =>
        val t = ctx.getInterval(fln,ctx.c,ctx.k,SVar(0))
        val u = ctx.getInterval(fln,ctx.c,ctx.k,SVar(1))
        if IntervalSetLat.isSingle(t) then
          if IntervalSetLat.equal(t, u) == thn then MapExps(m.popStack(2)) else BotMap
        else
          MapExps(m.popStack(2))
      case _ => TopMap

  // hargnemise üleminekufunktsioon
  override inline def branch(instr: JumpInstr, thn: Boolean)(using ctx: JBCtxT): Dl =
    def negate(o:Order): Order = {
      o match
        case EQ => NE
        case NE => EQ
        case LT => GE
        case GE => LT
        case GT => LE
        case LE => GT
    }
    val e = getState
    instr match {
      case IF_ICMPx(NE, a) => testeq(!thn, e)
      case IF_ICMPx(EQ, a) => testeq(thn, e)
      case IF_ICMPx(o, a) =>
        e.map{m =>
          val xs = m.fetch(SVar(0))
          val ys = m.fetch(SVar(1))
          val o1 = if !thn then negate(o) else o
          def qqq(k:Var,p:(Boolean,Exps)): (Var,(Boolean,Exps)) =
            val vs1: Exps = for {v <- p._2; x<-xs; y<-ys} yield Op(IF_ICMPx(o1,a),x,y,v)
            k -> (true, vs1)
          m.popStack(2).map(qqq)
        }
      case _ => e match
        case TopMap => TopMap
        case BotMap => BotMap
        case MapExps(m) => MapExps(m.popStack(2))
    }

  def loadExp(exp: Exp)(using ctx: JBCtxT): Exp =
    def find(n: Node, c: VarCtx, v: Var): Exp =
      ctx.get(n, c, ()) match
        case BotMap => Join(Set.empty)
        case MapExps(m) if m.contains(v) =>
          val t = m(v)._2
          if t.size == 1 then t.head else Join(t)
        case _ => Unknw
    subst(find, Set.empty)(exp)

  def loadExps(exps: Exps)(using ctx: JBCtxT): Exp =
    val rs = exps.map(loadExp)
    if rs.size == 1 then rs.head else Join(rs)

  override inline def normal(instr: Instr)(using ctx: JBCtxT): ExpMap =
    getState match
      case MapExps(c) =>
          instr match {
            // P6der.check method
            case INVOKESTATIC(a) if a.apply == ("P6der", "check", "(Z)V") =>
              val ts = ctx.getInterval(ctx.from.asInstanceOf[LNode],ctx.c,ctx.k,SVar(0))
              if IntervalSetLat.leq(ts, SortedSet(põder.dom.intervals.intToInterval(1))) then
                Log.logCommon("Check true")
                if inVerification then Log.logAssert("Check true", ctx.from)
              else if IntervalSetLat.leq(ts, SortedSet(intToInterval(0))) then
                Log.logCommon("Check false")
                if inVerification then Log.logAssert("Check false", ctx.from)
              else
                Log.logCommon("Check inconclusive")
                if inVerification then Log.logAssert("Check inconclusive", ctx.from)
              MapExps(c.popStack(1))
            // P6der.evalInt method
            case INVOKESTATIC(a) if a._1 == "P6der" && a._2 == "evalInt" =>
              MapExps(c)
            // iconst, istore, iload, return
            case BIPUSH(a) =>
              MapExps(c.pushStack(1).update(SVar(0) -> Set(Op(BIPUSH(a)))))
            case SIPUSH(a) =>
              MapExps(c.pushStack(1).update(SVar(0) -> Set(Op(SIPUSH(a)))))
            case i: ICONST =>
//              println(s"ICONST(${i.n}): $c")
              MapExps(c.pushStack(1).update(SVar(0) -> Set(Op(i))))
            case xSTORE(ty, a) =>
              val s0 = c.fetch(SVar(0))
              MapExps(c.popStack(1).update(LVar(a.toInt) -> s0))
            case xLOAD(ty, a) =>
//              println(s"xLOAD(${a.apply}): $c")
              val s0 = c.fetch(LVar(a.toInt))
//              println(s"xLOAD(${a.apply}) result: ${s0.map(ExpsLat.expString).mkString(", ")}")
//              println(s"$instr: fetch: ${s0.map(ExpsLat.expString).mkString(", ")}")
              MapExps(c.pushStack(1).update(SVar(0) -> s0))
            case xADD(ty) =>
//              println(s"xADD: $c")
              val s0 = c.fetch(SVar(0))
              val s1 = c.fetch(SVar(1))
//              println(s"xADD arg1: $s0")
//              println(s"xADD arg2: $s1")
              MapExps(c.popStack(1).update(SVar(0) -> distrOp(xADD(ty), s1, s0)))
            case xMUL(ty) =>
              val s0 = c.fetch(SVar(0))
              val s1 = c.fetch(SVar(1))
              MapExps(c.popStack(1).update(SVar(0) -> distrOp(xMUL(ty), s1, s0)))
            case xDIV(ty) =>
              val s0 = c.fetch(SVar(0))
              val s1 = c.fetch(SVar(1))
              MapExps(c.popStack(1).update(SVar(0) -> distrOp(xDIV(ty), s1, s0)))
            case POP =>
              MapExps(c.popStack(1))
            case POP2 =>
              MapExps(c.popStack(2))
            case PUTFIELD(a) =>
              val s0 = c.fetch(SVar(0))
              setGlobal(a._1,a._2,a._3,s0)
              val d = s0.flatMap(q => Set[Exp](Op(PUTFIELD(a), EVar(ctx.from, ctx.k, LVar(0)), q)))
              MapExps(c.popStack(2).update(LVar(0) -> d))
            case GETFIELD(a) =>
              val s0 = c.fetch(SVar(0))
              if s0.size==1 && s0.head.isInstanceOf[EVar] then
                val vr = s0.head.asInstanceOf[EVar]
                ctx.getPrivateVars(vr.n) match
                  case Some(pvs) if pvs contains LVar(0) =>
                    val d = Set[Exp](Op(GETFIELD(a), EVar(vr.n, (), LVar(0))))
                    MapExps(c.update(SVar(0) -> d))
                  case Some(gpv) =>
                    val d = getGlobal(a._1,a._2,a._3)
                    MapExps(c.update(SVar(0) -> d))
                  case None => BotMap
              else
                val d = getGlobal(a._1,a._2,a._3)
                MapExps(c.update(SVar(0) -> d))
            case RETURN =>
              MapExps(Map.empty)
            case _ => MapExps(c)
          }
      case BotMap => BotMap
      case TopMap => TopMap

  // erindi üleminekufunktsioon
  override inline def throws(instr: Instr, ex: Either[String, Set[String]])(using ctx: JBCtxT): Dl = L.bot

  // annotatsiooni ülemunekufunktsioon
  override inline def annot(ant: Annot)(using ctx: JBCtxT): Dl = ctx.st

  def enterMap(instr: MethodInstr, typ: MethodType, static: Boolean, st: ExpMap): ExpMap =
    st match {
      case TopMap => TopMap
      case BotMap => BotMap
      case MapExps(m) =>
//        val nm = for (n <- 0 to typ.args.length - 1) yield LVar(n) -> (true, m.fetch(SVar(n)))
//        MapExps(nm.toMap)
        MapExps(Map.empty)
    }

  // need on meetodiväljakutsete jaoks
  override inline def enter(instr: MethodInstr, typ: MethodType, static: Boolean)(using ctx: JBCtxT): Seq[(VarCtx, VarKey, VarKey => Dl, Dl)] =
    val c = getState
    def f(k: VarKey): Dl = c
    Seq(((), (), f, enterMap(instr, typ, static, c)))

  override inline def combine(instr: MethodInstr, typ: MethodType, static: Boolean, result: VarKey => Dl)(using ctx: JBCtxT): Dl =
    val _ = result(()) // sunnime väärtustama
    val n = if static then typ.args.size else typ.args.size+1
    ctx.st match
      case TopMap => TopMap
      case BotMap => BotMap
      case MapExps(m) =>
        val args = ((n-1).to(0,-1)).map(i => EVar(ctx.from,(),SVar(i)))
        val m1 = m.popStack(n).update(SVar(0) -> Set(Op(instr,args*)))
        MapExps(m1)