package põder.analyses

import scala.language.implicitConversions
import põder.framework.cfg.*
import põder.framework.{JsonOpF, Lattice, NarrowOpF, SimpleJBCAnalysis, WidenOpF}
import põder.log.Log
import põder.util.typ.{BoolT, IntT, MethodType, VoidT}
import põder.dom.*
import põder.dom.intervals.*
import põder.dom.{LVar, Val}
import põder.util.json.JsonF
import põder.util.CellConv

import scala.annotation.tailrec
import scala.collection.immutable.Queue

object UnitJsonOp extends JsonOpF[Unit]:
  override def toJson[jv](x: Unit)(using j: JsonF[jv]): jv = j.Null

@deprecated
trait IntervalValue extends SimpleJBCAnalysis:
  override type Dl = IntervalDomain
  override given L: (Lattice[Dl] & WidenOpF[Dl] & NarrowOpF[Dl]) = IntLat

  inline def pushStack(d: Dl, n: Int): Dl =
    pushStack(d, intToInterval(n), Const)

  inline def pushStack(d: Dl, n: Interval, lvar: Expr = Unknown): Dl =
    (Some(n :: d._1.getOrElse(List())), d._2, Some(lvar :: d._3.getOrElse(List())))

  inline def takeStacks(d: Dl, n: Int): List[Interval] =
    @tailrec
    def take[A](n:Int, xs:List[A], a:Queue[A]=Queue.empty): List[A] =
      xs match
        case x::xs if n != 0 => take(n-1, xs, a :+ x)
        case _ => a.toList
    d match
      case (Some(xs),_,_) => take(n,xs)
      case (None,ys,es) => List.empty

  inline def popStack(d: Dl): (Dl, Interval, Expr) =
    d match
      case (Some(x::xs),ys,Some(e::es)) => ((Some(xs),ys,Some(es)),x,e)
      case (Some(x::xs),ys,es) => ((Some(xs),ys,es),x,Unknown)
      case (_,ys,es) => ((None,ys,es),Interval(MInf,PInf),Unknown)


  def popStacks(d: Dl, n: Int): Dl =
    if n==0 then d else
      val (d1,_,_) = popStack(d)
      popStacks(d1, n-1)

  inline def peekStack(d: Dl): (Interval, Expr) =
    d match
      case (Some(x::_),_,Some(e::_)) => (x,e)
      case (Some(x::_),_,_) => (x, Unknown)
      case _ => (Interval(MInf,PInf), Unknown)

  def local(n: Node, start: Boolean = false)(i: Int): Var =
    val vl = LVar(i, start)
    if n.isInstanceOf[LNode] && (n.asInstanceOf[LNode].varNames contains i) then
      vl.alias = n.asInstanceOf[LNode].varNames(i)
    vl

  def load(i: Int, nd: Node, d: Dl): Interval =
    val v = local(nd)(i)
    d match
      case (_,Some(m),_) => m.getOrElse(v.str, Interval(MInf,PInf))
      case _ => Interval(MInf,PInf)

  override inline def node(nd: Node, b: VarCtx, k: VarKey)(using ctx: SolvCtxT): Dl =
    nd match {
      case n: LNode if n.is_start && project.startMethods.contains((n.methodInfo.mthd,n.methodInfo.mthdTyp)) =>
        val args = n.methodInfo.mthdTyp.args
        val d = args.zipWithIndex.foldRight(L.top)((x, y) => {
          x._1 match {
            case IntT => (y._1, Some(y._2.getOrElse(Map()) + (local(nd)(x._2).str -> Interval(MInf, PInf))), y._3)
            case _ => y
          }
        })
        d
      case _ => L.bot
    }

  override def isBuiltinFun(instr: MethodInstr, typ: MethodType, static: Boolean): Boolean =
    val (klass, mthd, typ) = instr.a.apply
    (klass == "P6der" && mthd == "check") || (klass == "java/lang/Math" && mthd == "random")

  // algseisund

  override inline def start(locals: Int, stack: Int, access: Int)(using ctx: JBCtxT): Dl = ctx.st

  // hargnemise üleminekufunktsioon
  override inline def branch(instr: JumpInstr, b: Boolean)(using ctx: JBCtxT): Dl =
    val d = ctx.st
    d match
      case (None, None, None) => d
      case _ =>
        instr match
          case IF_ICMPx(GT, a) =>
            val (d1, v1, i1) = popStack(d)
            val (d2, v2, i2) = popStack(d1)
            compare_GT(v2, v1) match
              case RTrue() => if b then d2 else L.bot
              case RFalse() => if !b then d2 else L.bot
              case Uncertain() => d2
          case IF_ICMPx(LT, a) =>
            val (d1, v1, i1) = popStack(d)
            val (d2, v2, i2) = popStack(d1)
            compare_LT(v2, v1) match
              case RTrue() => if b then d2 else L.bot
              case RFalse() => if !b then d2 else L.bot
              case Uncertain() => d2
          case IF_ICMPx(GE, a) =>
            val (d1, v1, i1) = popStack(d)
            val (d2, v2, i2) = popStack(d1)
            compare_GE(v2, v1) match
              case RTrue() => if b then d2 else L.bot
              case RFalse() => if !b then d2 else L.bot
              case Uncertain() =>
                i2 match
                  case IVar(i) =>
                    val localVarName = local(ctx.to)(i).str
                    if b then
                      (d2._1, Some(d2._2.getOrElse(Map()) + (localVarName -> Interval(v1.lb, d2._2.get(localVarName).ub))), d2._3)
                    else
                      (d2._1, Some(d2._2.getOrElse(Map()) + (localVarName -> Interval(d2._2.get(localVarName).lb, add(v1.ub, IntNum(-1))))), d2._3)
                  case _ => d2
          case IF_ICMPx(EQ, a) =>
            val (d1, v1, i1) = popStack(d)
            var (d2, v2, i2) = popStack(d1)
            compare_EQ(v2, v1) match
              case RTrue() => if b then d2 else L.bot
              case RFalse() => if !b then d2 else L.bot
              case Uncertain() => d2
          case IF_ICMPx(NE, a) =>
            val (d1, v1, i1) = popStack(d)
            var (d2, v2, i2) = popStack(d1)
            compare_NE(v2, v1) match
              case RTrue() => if b then d2 else L.bot
              case RFalse() => if !b then d2 else L.bot
              case Uncertain() => d2
          case IF_ICMPx(LE, a) =>
            val (d1, v1, i1) = popStack(d)
            val (d2, v2, i2) = popStack(d1)
            compare_LE(v2, v1) match
              case RTrue() => if b then d2 else L.bot
              case RFalse() => if !b then d2 else L.bot
              case Uncertain() => d2
          case IFx(order, a) =>
            val (d1, v1, i1) = popStack(d)
            val result = order match
              case GT => compare_GT(v1, intToInterval(0))
              case LT => compare_LT(v1, intToInterval(0))
              case NE => compare_NE(v1, intToInterval(0))
              case _ => Uncertain()
            result match
              case RTrue() => if b then d1 else L.bot
              case RFalse() => if !b then d1 else L.bot
              case Uncertain() => d1
          //      case IF_ACMPx(x, a) =>
          case GOTO(a) => d
          //      case IFNULL(a) =>
          //      case IFNONNULL(a) =>
          //      case TABLESWITCH(a) =>
          //      case LOOKUPSWITCH(a) =>
          case _ => d

  // "tavaline" üleminekufunktsioon
  override inline def normal(i: Instr)(using ctx: JBCtxT): Dl =
    val d = ctx.st
    Log.log(s"instruction: $i")
    Log.log(s"start state: $d")
    var r = L.bot
    d match
      case (None, None, None) => r = L.bot
      case _ => r = trans(ctx,i)
    Log.log(s"end state: $r")
    r


  // erindi üleminekufunktsioon
  override inline def throws(instr: Instr, ex: Either[String, Set[String]])(using ctx: JBCtxT): Dl = L.top

  // annotatsiooni ülemunekufunktsioon
  override inline def annot(ant: Annot)(using ctx: JBCtxT): Dl = ctx.st

  // need on meetodiväljakutsete jaoks
  override inline def enter(instr: MethodInstr, typ: MethodType, static: Boolean)(using ctx: JBCtxT): Seq[(Unit, Unit, Unit=>Dl, Dl)] =
    var dt = ctx.st
    Log.log(s"enter: $dt")
    val n = if static then typ.args.size else 1+typ.args.size
    val xs = takeStacks(dt,n)
    val nd = project.startVarsFun(instr.a._1, instr.a._2, typ)
    val x = xs.zipWithIndex.map(x => local(nd)(x._2).str -> x._1).toMap
    val domain = (Some(Nil), Some(x), Some(Nil))
    Log.log(s"enter new domain: $domain")
    enterWith(domain)


  override inline def combine(instr: MethodInstr, typ: MethodType, static: Boolean, result: VarKey => (Option[List[Interval]], Option[Map[String, Interval]], Option[List[Expr]]))(using ctx: JBCtxT): Dl =
    val n = if static then typ.args.size else 1+typ.args.size
    val caller = popStacks(ctx.st,n)
    val st = result(())
    val res = typ.ret match
      case VoidT => caller
      case _ =>
        val (_, v1, i) = popStack(st)
        pushStack(caller,v1,i)
    Log.log(s"combine:\n\td_prev: $caller\n\td_fun:  $st\n\td_res:  $res\n")
    res

  def gVar(s:String, f:String): Node = GNode(s++"."++f)

  def setGlobal(ty:String, i:String, d:Interval)(using ctx: JBCtxT): Unit =
    val dl = (Some(List(d)),None,None)
    Log.log(s"side effect: $ty.$i <- $d")
    ctx.set(gVar(ty, i),ctx.c,ctx.k,dl)

  def getGlobal(ty:String,i:String)(using ctx: JBCtxT): Option[(Interval,Expr)] =
    val g = ctx.get(gVar(ty,i),ctx.c,ctx.k)
    g match {
      case (None,_,_) => None
      case (Some(x::_),_,Some(e::_)) => Some((x,e))
      case (Some(x::_),_,None) => Some((x,Unknown))
      case _ => Some((Interval(MInf,PInf),Unknown))
    }

  override def explain[jv](n: Node, c: VarCtx, k: VarKey, lod: Int)(using ctx: SolvCtxT, j: JsonF[jv]): jv =
    val d = ctx.get(n,c,k)
    (n,d) match
      case  (GNode(k),(Some(x::_),_,_)) if lod==0 =>
        j.Obj(k -> j.Str(x.toString))
      case _ => super.explain(n,c,k)

  def trans(ctx: JBCtxT, i:Instr): Dl =
    val d = ctx.st
    i match
      case INVOKESPECIAL(a) if a._1 == "java/lang/Object" && a._2 == "<init>" =>
        val (d1,_,_) = popStack(d)
        d1
      case INVOKESTATIC(a) if a._1 == "P6der" && a._2 == "check" =>
        val (d1, v1, i1) = popStack(d)
        Log.log(s"push state: $d1")
        if inVerification then v1 match
          case Interval(IntNum(0), IntNum(0)) => println("Verification failed! Argument false.")
          case Interval(IntNum(1), IntNum(1)) => println("Verification passed! Argument true.")
          case _ => println(s"Verification unknown! Argument: $v1")
        d1
      case INVOKESTATIC(a) if a._1 == "java/lang/Math" && a._2 == "random" =>
        val d1 = pushStack(d, Interval(IntNum(0), IntNum(1)))
        Log.log(s"push state: $d1")
        d1
      case i: ICONST =>
        val d1 = pushStack(d, i.n)
        Log.log(s"push state: $d1")
        d1
      case i: DCONST =>
        val d1 = pushStack(d, i.n.toInt)
        Log.log(s"push state: $d1")
        d1
      case i: BIPUSH =>
        val d1 = pushStack(d, i.a)
        Log.log(s"push state: $d1")
        d1
      case i: SIPUSH =>
        val d1 = pushStack(d, i.a)
        Log.log(s"push state: $d1")
        d1
      case xSTORE(t, n) =>
        val (d1, v, _) = popStack(d)
        Log.log(local(ctx.to)(n.toInt).str + "=" + v)
        (d1._1, Some(d1._2.getOrElse(Map()) + (local(ctx.to)(n.toInt).str -> v)), d1._3)
      case xLOAD(t, a) =>
        val x = load(a.toInt,ctx.to,d)
        val d1 = pushStack(d, x, IVar(a.toInt))
        Log.log(s"push state: $d1")
        d1
      case xADD(t) =>
        val (d1, v1, i1) = popStack(d)
        val (d2, v2, i2) = popStack(d1)
        val d3 = pushStack(d2, add(v2, v1), Arith("+", i2, i1))
        d3
      case xMUL(t) =>
        val (d1, v1, i1) = popStack(d)
        val (d2, v2, i2) = popStack(d1)
        val d3 = pushStack(d2, mul(v2, v1), Arith("*", i2, i1))
        d3
      case xDIV(t) =>
        val (d1, v1, i1) = popStack(d)
        val (d2, v2, i2) = popStack(d1)
        val d3 = pushStack(d2, div(v2, v1), Arith("/", i2, i1))
        d3
      case i: LDC =>
        val d1 = pushStack(d, i.a.toString.toDouble.toInt)
        Log.log(s"push state: $d1")
        d1
      case xCMPG(t) =>
        val (d1, v1, i1) = popStack(d)
        val (d2, v2, i2) = popStack(d1)
        val d3 = pushStack(d2, cmp(v2, v1), Arith("=?", i2, i1))
        d3
      case ACONST_NULL =>
        pushStack(d,intToInterval(0),Const)
//      case (PUTFIELD(f), _) =>
//        ss match
//          case v::Lifted(xs)::_ =>
//            xs.foreach{setGlobal(_,f._2,v)}
//          case _ => ()
//        Lifted((ss.drop(2), vs))
//      case (GETFIELD(f), _) =>
//        ss match
//          case Lifted(xs)::_ =>
//            val d: Top|Lifted[Set[String]] = StringTopSetLat.Join(xs.map{getGlobal(_,f._2)}.toSeq)
//            Lifted((d::ss.drop(1), vs))
//          case _ =>
//            Lifted((Top::ss.drop(1), vs))
      case GETFIELD(a) =>
        val (d1,_,_) = popStack(d)
        getGlobal(a._1,a._2)(using ctx) match
          case Some((x,e)) => pushStack(d1,x,e)
          case None => L.bot
      case PUTFIELD(a) =>
        val (d1,i1,_) = popStack(d)
        val (d2,_,_) = popStack(d1)
        setGlobal(a._1,a._2,i1)(using ctx)
        d2
      case IINC(a) =>
        (d._1, Some(d._2.get + (local(ctx.to)(a._1).str -> add(d._2.get.get(local(ctx.to)(a._1).str).orNull, intToInterval(a._2)))), d._3)
      case NEW(a) =>
        pushStack(d, Interval(MInf, PInf))
      case DUP =>
        val (x,y) = peekStack(d)
        pushStack(d, x, y)
      case a => d
