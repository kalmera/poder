//package põder.analyses
//// TODO: re-implement using a transformation
//
//import põder.framework.{EqualOpF, JBCAnalysis, JBCtx, JoinOpF, JsonOpF, Lattice, NarrowOpF, NodeCtx, SolvCtx, TopOpF, WidenOpF}
//import põder.framework.cfg._
//import põder.util.Util.Cell
//import põder.util.json.JsonF
//import põder.util.typ.{MethodType, VoidT}
//
//object ClassLoader {
//  type d = Option[Set[String]]
//  object CL extends  Lattice[d] with WidenOpF[d] with NarrowOpF[d]{
//    override def widen(oldv: Option[Set[String]], newv: Option[Set[String]]): Option[Set[String]] = join(oldv,newv)
//    override def bot: Option[Set[String]] = None
//    override def top: Option[Set[String]] = Some(Set.empty)
//    override def leq(x: Option[Set[String]], y: Option[Set[String]]): Boolean = {
//      if (x==bot) true
//      else if (y==top) true
//      else if (y==bot) false
//      else y.get.subsetOf(x.get)
//    }
//
//    override def toJson[jv](x: Option[Set[String]])(using j: JsonF[jv]): jv = {
//      x match {
//        case Some(value) =>  j.Obj("loaded classes" -> j.Arr(value.map(j.Str)))
//        case None => j.Str("unreachable")
//      }
//    }
//
//    override def join(x: Option[Set[String]], y: Option[Set[String]]): Option[Set[String]] = (x,y) match {
//      case (None, x) => x
//      case (x,None) => x
//      case (Some(a), Some(b)) => Some(a intersect b)
//    }
//  }
//}
////
////final class ClassLoader(project: ProgramCFG, val c: JBCAnalysis)  extends JBCAnalysis{
////  import ClassLoader._
////  import c.{L => C}
////
////  override type Key = c.Key
////  override val Key: JsonOpF[c.Key] = c.Key
////  override val startKeys: Seq[c.Key] = c.startKeys
////
////  override type Ctx = c.Ctx
////  override val Ctx: JsonOpF[c.Ctx] = c.Ctx
////  override val startCtxs: Seq[c.Ctx] = c.startCtxs
////
////  override type Dl = (d, c.Dl)
////  override using val L: Lattice[Dl] with WidenOpF[Dl] with NarrowOpF[Dl] = new Lattice[Dl] with WidenOpF[Dl] with NarrowOpF[Dl] {
////    override def widen(oldv: Dl, newv: Dl): Dl =  (CL.widen(oldv._1,newv._1), C.widen(oldv._2,newv._2))
////    override def bot: Dl = (CL.bot, C.bot)
////    override def top: Dl = (CL.top, C.top)
////
////    override def leq(x: Dl, y: Dl): Boolean =
////      CL.leq(x._1,y._1) && C.leq(x._2,y._2)
////
////    override def toJson[jv](x: Dl)(using j: JsonF[jv]): jv =
////      j.Arr(CL.toJson(x._1),C.toJson(x._2))
////
////    override def join(x: Dl, y: Dl): Dl =
////      (CL.join(x._1,y._1),C.join(x._2,y._2))
////  }
////
////  private def ctxLift(ctx: SolvCtx[Node, c.Ctx, c.Key, (d, c.Dl)]): SolvCtx[Node, c.Ctx, c.Key, c.Dl] =
////    SolvCtx[Node, c.Ctx, c.Key, c.Dl] (
////      (n:Node, b:c.Ctx, k:c.Key) => ctx.get(n,b,k)._2,
////      (n:Node, b:c.Ctx, k:c.Key, v:c.Dl)=> ctx.set(n,b,k,(CL.bot,v))
////    )
////
////  private def ctxLift(ctx: NodeCtx[Node, c.Ctx, c.Key, (d, c.Dl)]): NodeCtx[Node, c.Ctx, c.Key, c.Dl] =
////    NodeCtx(ctxLift(ctx.sctx),ctx.from,ctx.from_start,ctx.to,ctx.to_final,ctx.k,ctx.c,(k:c.Key)=>ctx.st(k)._2)
////
////  private def ctxLift(ctx: JBCtx[Node, c.Ctx, c.Key, (d, c.Dl)]): JBCtx[Node, c.Ctx, c.Key, c.Dl] =
////    JBCtx[Node, c.Ctx, c.Key, c.Dl](ctxLift(ctx.sctx),ctx.stackSize,ctx.stackDiff)
////
////   def node(ctx: SolvCtx[Node, c.Ctx, c.Key, (d, c.Dl)], nd: Node, b: c.Ctx, k: c.Key): (d, c.Dl) = {
////    val cn = c.node(ctxLift(ctx),nd,b,k)
////    if (project.mainClass==nd.methodInfo.cls&&nd.is_start&&project.startMethods.contains((nd.methodInfo.mthd,nd.methodInfo.mthdTyp))) (CL.top,cn) else (CL.bot,cn)
////  }
////
////  override def isBuiltinFun(instr: MethodInstr, typ: MethodType, static: Boolean): Boolean =
////    c.isBuiltinFun(instr,typ,static)
////
////  def loadClassTF(ctx: JBCtx[Node, c.Ctx, c.Key, (d, c.Dl)], s:d, d:c.Dl): Dl = {
////    val cls = ctx.to.methodInfo.cls
////    if (s contains cls){
////      (s,d)
////    } else {
////      val mthType = MethodType(List(),VoidT)
////      val instr = INVOKESPECIAL(Cell((cls,"<clinit>","()V")))
////      val ctx1 = ctxLift(ctx)
////      val es = c.enter(ctx1,instr,mthType,static = true)
////      val snode = project.startVarsFun(cls,"<clinit>",MethodType(List(),VoidT))
////      val rnode = project.returnVarsFun(cls,"<clinit>",MethodType(List(),VoidT))
////      es.foreach{ case (b,k,d) => ctx.set(snode,b,k,(s,d)) }
////      val newc = c.combine(ctx1,instr,mthType,static = true, k=>ctx.st(k)._2)
////      (ctx.get(rnode,ctx.c,ctx.k)._1, newc)
////    }
////  }
////
////  def makeBot(st: Dl): Dl = st match {
////    case (s,d) if C.leq(d,C.bot) => (s,C.bot)
////    case (s,d) => (s,d)
////  }
////
////  override def start(ctx: JBCtx[Node, c.Ctx, c.Key, (d, c.Dl)], locals: Int, stack: Int, access: Int): (d, c.Dl) = {
////    val cs = c.start(ctxLift(ctx),locals,stack,access)
////    def nst(k:Key):Dl = (ctx.st(ctx.k)._1,cs)
////    makeBot(loadClassTF(get,set,fr,to,to.methodInfo.cls,nst)(k))
////  }
////
////
////  override def senter(get: Node => Key => Dl, set: Node => Key => Dl => Unit, fr: Node, fr_s: Boolean, to: Node, to_final: Boolean, st: Key => Dl)(k:Key, instr: MethodInstr, typ: MethodType, static: Boolean): Seq[Dl] = {
////    def get2(n:Node)(k:Key): c.Dl = get(n)(k)._2
////    def set2(n:Node)(k:Key)(d: c.Dl): Unit = set(n)(k)((st(k)._1,d))
////    val ce = c.senter(get2,set2,fr,fr_s,to,to_final,k=>st(k)._2)(k,instr,typ,static)
////    def nst(ce:c.Dl):Dl = (st(k)._1,ce)
////    ce.map(ce => makeBot(nst(ce)))
////  }
////
////  override def scombine(get: Node => Key => Dl, set: Node => Key => Dl => Unit, fr: Node, fr_s: Boolean,  to: Node, to_final: Boolean, st: Key => Dl)(k:Key, instr: MethodInstr, typ: MethodType, static: Boolean, caller: Key => Dl): Dl = {
////    def get2(n:Node)(k:Key): c.Dl = get(n)(k)._2
////    def set2(n:Node)(k:Key)(d: c.Dl): Unit = set(n)(k)((st(k)._1,d))
////    val cc = c.scombine(get2,set2,fr,fr_s,to,to_final,k=>st(k)._2)(k,instr,typ,static,k=>caller(k)._2)
////    makeBot((st(k)._1,cc))
////  }
////
////  override def sBranch(get: Node => Key => Dl, set: Node => Key => Dl => Unit, fr: Node, fr_s: Boolean,  to: Node, to_final: Boolean, st: Key => Dl)(k:Key, instr: JumpInstr, thn: Boolean): Dl = {
////    def get2(n:Node)(k:Key): c.Dl = get(n)(k)._2
////    def set2(n:Node)(k:Key)(d: c.Dl): Unit = set(n)(k)((st(k)._1,d))
////    val cb = c.sBranch(get2,set2,fr,fr_s,to,to_final,k=>st(k)._2)(k,instr,thn)
////    makeBot((st(k)._1,cb))
////  }
////
////  override def sNormal(get: Node => Key => Dl, set: Node => Key => Dl => Unit, fr: Node, fr_s: Boolean,  to: Node, to_final: Boolean, st: Key => Dl)(k:Key, instr: Instr): Dl = {
////    def get2(n:Node)(k:Key): c.Dl = get(n)(k)._2
////    def set2(n:Node)(k:Key)(d: c.Dl): Unit = set(n)(k)((st(k)._1,d))
////    val cn = c.sNormal(get2,set2,fr,fr_s,to,to_final,k=>st(k)._2)(k,instr)
////    instr match {
////      case GETSTATIC(a) =>
////        makeBot(loadClassTF(get,set,fr,to,a.x.get._1,k=>(st(k)._1,cn))(k))
////      case _ =>
////        makeBot((st(k)._1,cn))
////    }
////  }
////
////  override def sThrows(get: Node => Key => Dl, set: Node => Key => Dl => Unit, fr: Node, fr_s: Boolean,  to: Node, to_final: Boolean, st: Key => Dl)(k:Key, instr: Instr, ex: Either[String, Set[String]]): Dl = {
////    def get2(n:Node)(k:Key): c.Dl = get(n)(k)._2
////    def set2(n:Node)(k:Key)(d: c.Dl): Unit = set(n)(k)((st(k)._1,d))
////    val ct = c.sThrows(get2,set2,fr,fr_s,to,to_final,k=>st(k)._2)(k,instr,ex)
////    makeBot((st(k)._1,ct))
////  }
////
////  override def sAnnot(get: Node => Key => Dl, set: Node => Key => Dl => Unit, fr: Node, fr_s: Boolean,  to: Node, to_final: Boolean, st: Key => Dl)(k:Key, ant: Annot): Dl = {
////    def get2(n:Node)(k:Key): c.Dl = get(n)(k)._2
////    def set2(n:Node)(k:Key)(d: c.Dl): Unit = set(n)(k)((st(k)._1,d))
////    val ca = c.sAnnot(get2,set2,fr,fr_s,to,to_final,k=>st(k)._2)(k,ant)
////    makeBot((st(k)._1,ca))
////  }
//
////}
