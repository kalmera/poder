package põder.analyses

import scala.language.implicitConversions
import põder.dom.{BotMap, ExpMap, ExpMapLat, ExpsLat, IntervalSetLat, MapExps, TopMap}
import põder.dom.expressions.*
import põder.dom.intervals.*
import põder.framework.*
import põder.framework.cfg.*
import põder.log.Log
import põder.util.json.JsonF
import põder.util.*
import põder.util.typ.{MethodType, Parser}
import põder.util.CellConv

import scala.annotation.tailrec
import scala.math.Ordering.OptionOrdering
import scala.collection.immutable.{AbstractSet, BitSet, Queue, SortedSet, TreeSet}
import scala.collection.mutable

sealed abstract class ISetKey
case object IntervalKey extends ISetKey
case object ExplainKey extends ISetKey

trait IntQueryCtx[N,C,K,D] extends JBCtx[N,C,K,D]:
  def getInterval(n: Node, c: C, k:K, v: Var): IntervalSetLat.t

trait ExpQueryCtx[N,C,K,D] extends SolvCtx[N,C,K,D]:
  def getExps(n: Node, v: Var): Option[Exps]

trait EscapedVarCtx[N,C,K,D] extends JBCtx[N,C,K,D]:
  def getPrivateVars(n: N): Option[Set[Var]]

object ISetKeyJson extends JsonOpF[ISetKey]:
  override def toJson[jv](x: ISetKey)(using j: JsonF[jv]): jv =
    x match
      case IntervalKey => j.Str("intervals")
      case ExplainKey => j.Str("explanation")

class IntervalSet(val at:AnalysisTask) extends MetaJBCAnalysis:
  override type NCtx[A,B,C,D] = ExpQueryCtx[A,B,C,D]
  override type ECtx[A,B,C,D] = JBCtx[A,B,C,D]

  override lazy val project: ProgramCFG = at.pcfg
  override def forward: Boolean = true

  override type VarKey = ISetKey
  final lazy val VarKey: JsonOpF[VarKey] = ISetKeyJson

  type VarCtx = Unit
  final lazy val VarCtx: JsonOpF[VarCtx] = UnitJsonOp

  final lazy val startCtxKey: Seq[(VarCtx,VarKey)] = Seq(((),IntervalKey))

  given unit: Unit = ()

  override type Dl = põder.dom.IntervalSetLat.t
  override given L: (Lattice[Dl] & WidenOpF[Dl] & NarrowOpF[Dl]) =
    IntervalSetLat

  override def node(nd: Node, b: VarCtx, k: VarKey)(using ctx: SolvCtxT): Dl = L.bot

  type IntDom = IntervalSetLat.t

  inline def op_distr_bin(f: (Interval, Interval) => Interval)(xs: IntDom, ys: IntDom): IntDom =
    val r = xs.flatMap(x => ys.map(y => f(x, y)))
    val r2 = IntervalSetLat.normalize(r)
    r2

  def forget_ub(a:Int)(i:Interval): Interval =
    i.lb match
      case IntNum(i) => Interval(IntNum(i+a), PInf)
      case lb => Interval(lb, PInf)
  def forget_ub(a:Int)(i:IntDom): IntDom = IntervalSetLat.normalize(i.map(forget_ub(a)))

  def forget_lb(a:Int)(i:Interval): Interval =
    i.ub match
      case IntNum(i) => Interval(MInf, IntNum(i+a))
      case ub => Interval(MInf, ub)
  def forget_lb(a:Int)(i:IntDom): IntDom = IntervalSetLat.normalize(i.map(forget_lb(a)))

  def constValue(e:Exp):Boolean = {
    e match
      case Op(op, l@_*) => l.forall(constValue)
      case EVar(n, c, v) => false
      case LFP(n, c, v, e) => constValue(e)
      case Join(as) => as.forall(constValue)
      case Meet(as) => as.forall(constValue)
      case IntExp(i) => true
      case Label(e, _, _) => constValue(e)
  }

  def equalVarExp(a:Exp, b:Exp)(using m: Map[(Node,Var),Exp] = Map.empty):Boolean = {
    def f(x:Exp, acc:Set[(Node,Var)]=Set.empty):Set[(Node,Var)] = {
      x match
        case EVar(n, (), v) if acc contains ((n,v)) => acc
        case EVar(n, (), v) => if m.contains((n,v)) then f(m(n,v), acc+((n,v))) else acc
        case LFP(n, c, v, e) => f(e, acc + ((n,v)))
        case Join(as) => as.map(e => f(e,acc)).reduce(_.intersect(_))
        case Label(e, _, _) => f(e,acc)
        case _ => acc
    }
    f(a).intersect(f(b)).nonEmpty
  }

  def optimizeFieldsGet(e:Exp)(using gdata: (String, String, String)):Exp = {
    e match
      case Op(PUTFIELD(b),_,x) if gdata == b.apply => optimizeFields(x)
      case Op(PUTFIELD(_),th,_) => optimizeFieldsGet(th)
      case Op(GETFIELD(a),th) => optimizeFieldsGet(th)(using a.apply)
      case Op(op, l@_*) => Op(op, l.map(optimizeFieldsGet)*)
      case LFP(n, c, LVar(0), e) =>
        LFP(n, c, FieldGVar(gdata._1, gdata._2,gdata._3), optimizeFieldsGet(e))
      case LFP(n, c, v, e) =>
        LFP(n, c, v, optimizeFieldsGet(e))
      case Join(as) => Join(as.map(optimizeFieldsGet))
      case Meet(as) => Meet(as.map(optimizeFieldsGet))
      case EVar(n, c, v) => EVar(n, c, FieldGVar(gdata._1,gdata._2,gdata._3))
      case Label(e, source, line) => Label(optimizeFieldsGet(e),source,line)
      case _ => e
  }

  def optimizeFields(e:Exp):Exp = {
    e match
      case Op(GETFIELD(a),th) => optimizeFieldsGet(th)(using a.apply)
      case Op(PUTFIELD(_),th,_) => optimizeFields(th)
      case Op(op, l@_*) => Op(op, l.map(optimizeFields)*)
      case LFP(n, c, v, e) => LFP(n, c, v, optimizeFields(e))
      case Join(as) => Join(as.map(optimizeFields))
      case Meet(as) => Meet(as.map(optimizeFields))
      case Label(e, source, line) => Label(optimize(e),source, line)
      case t => t
  }

  def optimizeGuards1(e:Exp)(using m: Map[(Node,Var),Exp] = Map.empty):Exp = {
    e match
      case Op(IF_ICMPx(o,_),y,x,z) if constValue(x) && equalVarExp(y,z) =>
        val xv = evalExp(using (_,_,_) => IntervalSetLat.top, (_,_,_,_) => ())(x)
        val xl = getLocation(x)
        val cs = {
          o match
            case EQ => IntExp(xv.head)
            case NE => Meet(Set.empty) // negate xv ?
            case GE => IntExp(forget_ub(0)(xv.head))
            case GT => IntExp(forget_ub(1)(xv.head))
            case LT => IntExp(forget_lb(-1)(xv.head))
            case LE => IntExp(forget_lb(0)(xv.head))
        }
        val cs1 =
          xl match
            case Some((n,l)) => Label(cs,n,l)
            case None => cs
        Meet(Set(cs1,z))
      case Op(IF_ICMPx(o,_),x,y,z) if constValue(x) && equalVarExp(y,z) =>
        val xv = evalExp(using (_,_,_) => IntervalSetLat.top, (_,_,_,_) => ())(x)
        val xl = getLocation(x)
        val cs = {
          o match
            case EQ => IntExp(xv.head)
            case NE => Meet(Set.empty) // negate xv ?
            case GE => IntExp(forget_ub(0)(xv.head))
            case GT => IntExp(forget_ub(1)(xv.head))
            case LT => IntExp(forget_lb(-1)(xv.head))
            case LE => IntExp(forget_lb(0)(xv.head))
        }
        val cs1 =
          xl match
            case Some((n,l)) => Label(cs,n,l)
            case None => cs
        Meet(Set(cs1,optimizeGuards1(z)))
      case Op(IF_ICMPx(o,_),_,_,z) =>
//        println(s"optimizeGuards1: $e")
        optimizeGuards1(z)
      case Op(op, l@_*) => Op(op, l.map(optimizeGuards1)*)
      case LFP(n, c, v, e) => LFP(n,c,v,optimizeGuards1(e)(using m+((n,v) -> e)))
      case Label(e, source, line) => Label(optimizeGuards1(e), source, line)
      case Join(as) => Join(as.map(optimizeGuards1))
      case Meet(as) => Meet(as.map(optimizeGuards1))
      case _ => e
  }
  def optimizeGuards(e:Exp):Exp = {
//    println(s"optimizeGuards in: $e")
    val r = optimizeGuards1(e)
//    println(s"optimizeGuards out: $r")
    r
  }

  def dropFast(exp:Exp): Exp = {
    exp match
      case Op(op, l@_*) => Op(op,l.map(dropFast)*)
      case LFP(n, c, v, _) => EVar(n,c,v)
      case Join(as) => Join(as.map(dropFast))
      case Meet(as) => Meet(as.map(dropFast))
      case Label(e, source, line) => Label(optimizeGuards1(e), source, line)
      case _ => exp
  }

  def dropNonLin(exp:Exp): Exp = {
    exp match
      case Op(op, l@_*) => Op(op,l.map(dropFast)*)
      case EVar(_,_,_) => exp
      case LFP(n, c, v, e) =>
        EVar(n,c,v)
      case Join(as) =>
        val as1 = as.map(dropNonLin)
        val as2 = as1 - Join(Set.empty)
        if as2.size==1 then as2.head else Join(as2)
      case Meet(as) =>
        val as1 = as.map(dropNonLin)
        val as2 = as1 - Meet(Set.empty)
        if as2.size==1 then as2.head else Meet(as2)
      case Label(e, source, line) => Label(dropNonLin(e), source, line)
      case  _ => exp
  }


  def optimizeEmptyLoops(exp:Exp):Exp = {
    inline def label(n:Node, inline e:Exp) =
      n match
        case mn:MethodNode if mn.linenr.isDefined => Label(e,mn.methodInfo.sourceFile,mn.linenr.get)
        case _ => e
    def dropLFP(exp:Exp): Exp = {
      exp match
        case LFP(n, c, v, e) => dropLFP(e)  // not neccessary anymore?
        case Label(e, _, _) => dropLFP(e)
        case Join(as) =>
          val as1 = as.map(dropLFP)
          val as2 = as1 - Join(Set.empty)
          if as2.size==1 then as2.head else Join(as2)
        case Meet(as) =>
          val as1 = as.map(dropLFP)
          val as2 = as1 - Meet(Set.empty)
          if as2.size==1 then as2.head else Meet(as2)
        case  _ => exp
    }
    exp match
      case Op(op, l@_*) => Op(op,l.map(optimizeEmptyLoops)*)
      case LFP(n, c, v, e) =>
        val (ts, fs) = partition(freeVars(_) contains((n,(),v)), e)
        if ts.isEmpty then
          label(n,optimizeEmptyLoops(if fs.size==1 then fs.head else Join(fs)))
        else if  ts.map(dropLFP) == Set(EVar(n,c,v)) then
          val fs1 = fs - Join(Set.empty)
          optimizeEmptyLoops(if fs1.size==1 then fs1.head else Join(fs1))
        else
          LFP(n,c,v,optimizeEmptyLoops(e))
      case Join(as) =>
        val as1 = as.map(optimizeEmptyLoops)
        val as2 = as1 - Join(Set.empty)
        if as2.size==1 then as2.head else Join(as2)
      case Meet(as) =>
        val as1 = as.map(optimizeEmptyLoops)
        val as2 = as1 - Meet(Set.empty)
        if as2.size==1 then as2.head else Meet(as2)
      case Label(e,source,line) => Label(optimizeEmptyLoops(e),source,line)
      case _ => exp
  }


  def evalExp(using env: (Node, VarCtx, Var) => IntDom, side: (Node, VarCtx, Var, IntDom) => Unit)(e: Exp): IntDom =
    inline def ilit(i: Int): IntDom =
      val d = IntNum(i)
      SortedSet(Interval(d, d))

    e match {
      case Label(e, _, _) => evalExp(e)
      case Op(in) =>
        in match
          case BIPUSH(a) => ilit(a)
          case SIPUSH(a) => ilit(a)
          case i: ICONST => ilit(i.n)
          case _ => SortedSet(Interval(MInf, PInf))
      case Op(instr:MethodInstr, l@_*) =>
        val mt = Parser.mpar(instr.a._3)
        val v_ret = project.returnVarsFun(instr.a._1, instr.a._2, mt)
        val v_start = project.startVarsFun(instr.a._1, instr.a._2, mt)
        for (a,i) <- l.zipWithIndex do
          val d = evalExp(a)
          side(v_start,Right(()),LVar(i),d)
        env(v_ret,Right(()),SVar(0))
      case Op(op, x, y) =>
        op match {
          case xADD(t) => op_distr_bin(add)(evalExp(x), evalExp(y))
          case xMUL(t) => op_distr_bin(mul)(evalExp(x), evalExp(y))
          case xDIV(t) => op_distr_bin(div)(evalExp(x), evalExp(y))
          case _ => SortedSet(Interval(MInf, PInf))
        }
      case Op(IF_ICMPx(_,_), _, _, v) =>
        evalExp(v)
      case Op(op, l@_*) => SortedSet(Interval(MInf, PInf))
      case EVar(n, (), v) => env(n, Right(()), v)
      case LFP(n, (), v, e) =>
        def updated_env(d: IntDom)(n1: Node, c1: VarCtx, v1: Var): IntDom =
          if (n1 == n && c1 == () && v1 == v) then d else env(n1, c1, v1)

        def box(x: IntDom, y: IntDom): IntDom =
          if IntervalSetLat.leq(y,x) then IntervalSetLat.narrow(x,y) else IntervalSetLat.widen(x,y)

        @tailrec
        def lfp(ol: IntDom): IntDom =
          val nw = evalExp(using updated_env(ol))(e)
          val test = IntervalSetLat.equal(ol, nw)
          if test then ol else lfp(box(ol, nw))

        lfp(evalExp(using updated_env(IntervalSetLat.bot))(e))
      case Join(as) =>
        if as.isEmpty then
          SortedSet.empty
        else
          as.tail.foldLeft(evalExp(as.head)) {
            case (b, a) =>
              val q = evalExp(a)
              val r = IntervalSetLat.join(b, q)
              //              println(s"join(${b.mkString("{",",","}")},${q.mkString("{",",","}")}) = ${r.mkString("{",",","}")}" )
              r
          }
      case Meet(as) =>
        if as.isEmpty then
          SortedSet(Interval(MInf, PInf))
        else
          as.tail.foldLeft(evalExp(as.head)) {
            case (b, a) =>
              val q = evalExp(a)
              val r = IntervalSetLat.meet(b, q)
              r
          }
      case IntExp(i) => SortedSet(i)
    }

  def evalExps(using env: (Node, VarCtx, Var) => IntDom, side: (Node, VarCtx, Var, IntDom) => Unit)(e: Exps): IntDom =
    val s = IntervalSetLat.normalize(e.foldLeft(SortedSet.empty) {
      case (s, r) => evalExp(r) ++ s
    })
    s

  def loadFullExp(exp: Exp)(using seen:Set[(Node, Var)] = Set.empty, ctx: SolvCtxT): Exp =
    inline def label(n:Node, inline e:Exp) =
      n match
        case mn:MethodNode if mn.linenr.isDefined => Label(e,mn.methodInfo.sourceFile,mn.linenr.get)
        case _ => e
    def opt(e:Exp):Exp =
      e match
        case Join(as) =>
          val as1 = (as -- seen.map((ln,v) => EVar(ln,(),v))) - Join(Set.empty)
          if as1.size == 1 then as1.head else Join(as1)
        case _ => e
    def find(n: Node, v: Var): Exp =
      ctx.getExps(n, v) match
        case Some(s) => if s.size==1 then s.head else Join(s)
        case None => Unknw
    exp match
      case Label(e, source, line) => Label(loadFullExp(e),source,line)
      case EVar(n, (), v) =>
        if n.isInstanceOf[MethodNode] && n.asInstanceOf[MethodNode].is_start then
          exp
        else if seen contains((n, v)) then
          EVar(n, (), v)
        else
          val er = loadFullExp(find(n,v))(using seen+((n,v)))
          if freeVars(er) contains (n,(),v) then
            LFP(n,(),v,er)
          else
            label(n,er)
      case Join(xs) =>
        val xs1 = xs.map(loadFullExp)
        val xs2 = xs1 - Join(Set.empty)
        if xs2.size == 1 then xs2.head else Join(xs2)
      case Meet(xs) =>
        val xs1 = xs.map(loadFullExp)
        val xs2 = xs1 - Meet(Set.empty)
        if xs2.size == 1 then xs2.head else Meet(xs2)
      case Op(op,xs@_*) => Op(op,xs.map(loadFullExp)*)
      case LFP(n1,(),v,e) => LFP(n1,(),v,loadFullExp(e)(using seen+((n1,v))))
      case e => e

  def partition(p:Exp=>Boolean, e:Exp): (Exps,Exps) = {
    e match
      case LFP(n, c, v, e) =>
        val (tp,fp) = partition(p,e)
        (tp.map(LFP(n,c,v,_)),fp.map(LFP(n,c,v,_)))
      case Join(as) =>
        as.foldLeft((Set.empty,Set.empty)){
          case ((ts,fs), e) =>
            val(te,fe) = partition(p,e)
            (ts++te,fs++fe)
        }
      case Label(e, c, l) =>
        val (tp,fp) = partition(p,e)
        (tp.map(Label(_,c,l)),fp.map(Label(_,c,l)))
      case _ => if p(e) then (Set(e),Set.empty) else (Set.empty, Set(e))
  }

  def niceShort(d:IntervalSetLat.t): String =
    if d.isEmpty then
      "dead code"
    else if d.size==1 && d.head.ub==d.head.lb then
      s"value ${d.head.ub}"
    else if d.size==1 then
      s"range ${d.head}"
    else
      s"ranges ${d.mkString(",")}"

  def niceConstraint(d:IntervalSetLat.t): String = {
    if d.isEmpty then
      "dead code"
    else if d.size==1 then
      d.head match
        case Interval(MInf,PInf) => "any"
        case Interval(MInf, IntNum(n)) => s"at most ${n}"
        case Interval(IntNum(n), PInf) => s"at least ${n}"
        case Interval(IntNum(n), IntNum(m)) => s"between $n and $m"
        case _ => s"in range ${d.head}"
    else
      s"in ranges ${d.mkString(", or")}"
  }

  def niceVar(v:Var): String =
    def f(i:Int): String =
      val e =
        if (i%10==1 && i%100!=11) then "st"
        else if (i%10==2 && i%100!=12) then "nd"
        else if (i%10==3 && i%100!=13) then "rd"
        else "th"
      s"$i$e"
    v match
      case SVar(i) => s"${f(i+1)} operand"
      case LVar(i) => s"variable ${v.str}"
      case FieldGVar(_,s,_) => s"field $s"

  def niceNode(ln:Node):String =
    ln match
      case ln: MethodNode =>
        ln.linenr match
          case Some(value) => s"on line ${value}"
          case None => ln.near_linenr match
            case Some(value) => s"near line ${value}"
            case None => s"somewhere in ${ln.methodInfo.sourceFile}"
      case ln: GNode =>
        s"in class ${ln.ty}"
      case _ => ln.toString

  def niceLocation(l:Option[(String,Int)]): String =
    l match
      case Some((src,ln)) => s"on line ${ln}"
      case None => ""

  def locFromNode(n:Node): Option[(String,Int)] =
    n match
      case n: MethodNode => n.linenr.map((n.methodInfo.sourceFile,_))
      case _ => None

  def getLocation(exp:Exp):Option[(String,Int)] =
    exp match
      case EVar(n, (), v) => locFromNode(n)
      case LFP(n:Node, _, v, e) =>
        getLocation(e) match
          case None => locFromNode(n)
          case e => e
      case Label(e, source, line) =>
        getLocation(e) match
          case None => Some((source, line))
          case e => e
      case _ => None

  def isDead(e:Exp): Boolean = {
    e match
      case LFP(_,_,_,e) => isDead(e)
      case Join(as) => as.isEmpty
      case Label(e,_,_) => isDead(e)
      case _ => false
  }

  def exp2json[jv](e:Exp)(using loc:Option[(String,Int)], v:Var, ctx: SolvCtxT, j: JsonF[jv]): jv =
    inline def label(inline q:jv)(using loc:Option[(String,Int)]) =
      loc match
        case Some((s,l)) => j.Label(s,l,q)
        case None => q
    var args = List.empty[(String,jv)]
    def setInt(n:Node, c:VarCtx, k:Var, d:IntDom): Unit =
      args = (k.str, L.toJson(d)) :: args
    def getInt(n:Node, c:VarCtx, k:Var): IntDom =
      n match
        case ln: LNode => ctx.get(NodeVar(ln,k),(),IntervalKey)
        case _ => ctx.get(NodeGVar(n,k),(),IntervalKey)
    val vl = evalExp(using getInt, setInt)(e)
    e match
      case Label(e, source, line) => exp2json(e)(using loc=Some((source,line)))
      case IntExp(i) =>
        label(j.Str(s"${niceShort(vl)} due to constant ${niceLocation(loc)}"))
      case Op(_) =>
        label(j.Str(s"${niceShort(vl)} due to constant ${niceLocation(loc)}"))
      case Op(instr:MethodInstr, l@_*) =>
        val v_ret = project.returnVarsFun(instr.a._1, instr.a._2, Parser.mpar(instr.a._3))
        val static = if instr.isStatic then " static" else ""
        val loc_vret = v_ret.linenr.map((v_ret.methodInfo.sourceFile, _))
        label(j.Obj(s"${niceShort(vl)} due to post-condition of$static method ${instr.a._1}.${instr.a._2}" -> j.Arr(
          label(j.Str(s"all calls are merged ${niceNode(v_ret)}"))(using loc_vret) +:
          l.zipWithIndex.map{
          case (a,b) =>
            j.Obj( s"parameter ${b+1}" -> exp2json(a))
        })))
      case Op(op, l@_*) => label(j.Obj(s"${niceShort(vl)} due to operation $op" -> j.Arr(l.zipWithIndex.map{
          case (a,b) =>
            j.Obj( s"parameter ${b+1}" -> exp2json(a))
        })))
      case EVar(n, (), v) =>
        n match
          case ln: LNode if ln.is_start =>
            given Option[((String,Int))] = ln.linenr.map((ln.methodInfo.sourceFile, _))
            label(j.Str(s"${niceShort(vl)} due to ${niceVar(v)} at the start of ${ln.methodInfo.cls}.${ln.methodInfo.mthd}"))
          case _ =>
            label(j.Str(s"${niceShort(vl)} due to ${niceVar(v)} at the head of the loop ${niceNode(n)}"))
      case LFP(n, (), v1, e) =>
        given Node = n
        given Option[(String,Int)] = locFromNode(n)
        val (ts, fs) = partition(freeVars(_) contains((n,(),v1)), e)
        if ts.isEmpty && (v==v1 || v1.isInstanceOf[FieldGVar]) then
          exp2json(e)
        else if ts.isEmpty then
          given Var = v1
          given Option[(String, Int)] = locFromNode(n)
          val var_text = s"${niceShort(vl)} due to ${niceVar(v1)} ${niceNode(n)}"
          label(j.Obj(var_text -> exp2json(e)))
        else
          val sw = if fs.size == 1 then exp2json(fs.head) else j.Arr(fs.map(exp2json))
          val cw = if ts.size == 1 then exp2json(ts.head) else j.Arr(ts.map(exp2json))
          label(j.Obj(s"${niceShort(vl)} due to a loop on ${niceVar(v1)} ${niceNode(n)}" ->
            j.Obj("starting with" -> sw,
                       "cycle with" -> cw
            )))
      case Meet(as) if as.isEmpty => j.Str("unknown value")
      case Meet(as) if as.size==1 => exp2json(as.head)
      case Meet(as) =>
        val (cs,nc) = as.partition(constValue)
        if (cs.isEmpty) then
          j.Obj(s"${niceShort(vl)} due to intersection of the following" -> j.Arr(as.map(exp2json)))
        else
          val c = cs.foldLeft(SortedSet.empty){
            case (c,e) => IntervalSetLat.join(c,evalExp(using (_,_,_) => IntervalSetLat.top, (_,_,_,_) => ())(e))
          }
          val n = cs.foldLeft[Option[(String,Int)]](None){case (l,e) => if l.isEmpty then getLocation(e) else l }
          val q = if nc.size==1 then exp2json(nc.head) else j.Arr(nc.map(exp2json))
          label(j.Obj(s"${niceShort(vl)} due to condition \"${niceConstraint(c)}\" ${niceLocation(n)}" -> q))(using loc=n)
      case Join(as) =>
        val as1 = as.filterNot(isDead)
        if as1.isEmpty then j.Str("dead code")
        else if  as1.size==1 then exp2json(as1.head)
        else j.Obj(s"${niceShort(vl)} due to any of the following" -> j.Arr(as1.map(exp2json)))

  def explainNicely[jv](n:Node, v:Var)(using ctx: SolvCtxT, j: JsonF[jv]): jv =
    ctx.getExps(n,v) match
      case Some(vs) =>
        val e1 = if vs.size==1 then vs.head else Join(vs)
        val e2 = loadFullExp(e1)
        val e3 = optimizeFields(e2)
        val e4 = optimizeGuards(e3)
        val e = optimizeEmptyLoops(e4)
//        val d = evalExp(env)(e)
        given Var = v
        given Node = n
        given Option[(String, Int)] = None
//        j.Obj(s"explanation of value" -> exp2json(e))
        exp2json(e)
      case _ => j.Str("could not load expression data")

  override def explain[jv](n: Node, c: Unit, k: ISetKey, lod: Int)(using ctx: SolvCtxT, j: JsonF[jv]): jv =
    (n,k) match
      case (NodeVar(ln,v), ExplainKey) if lod>=1 =>
        explainNicely(ln,v)
      case (NodeGVar(n,v), ExplainKey) if lod>=1 =>
        explainNicely(n,v)
      case (NodeVar(ln,v),IntervalKey) =>
        val d = ctx.get(n,c,k)
        j.Obj(s"${niceVar(v)} ${niceNode(ln)}" -> L.toJson(d))
      case (NodeGVar(ln,v),IntervalKey) =>
        val d = ctx.get(n,c,k)
        j.Obj(s"${niceVar(v)} ${niceNode(ln)}" -> L.toJson(d))
      case _ =>
        super.explain(n, c, k, lod)(using ctx, j)

case class NodeGVar(ln:Node, v:Var) extends Node:
  override def toString: String = ln.toString++"."++v.str

case class NodeVar(ln:LNode, v:Var) extends MethodNode:
  val id: Int = ln.id
  val methodInfo: MethodInfo = ln.methodInfo
  val labels: mutable.Set[String] = ln.labels
  var lastLabel: Option[String] = ln.lastLabel
  var is_start: Boolean = ln.is_start
  var is_return: Boolean = ln.is_return
  var is_exn: Boolean = ln.is_exn
  var offset: Option[Int] = ln.offset
  var linenr: Option[Int] = ln.linenr
  var near_linenr: Option[Int] = ln.near_linenr
  var varNames: Map[Int, String] = ln.varNames
  var annot: List[AnyRef] = ln.annot
  var stackSize: Int = ln.stackSize
  var str: String = ln.id.toString ++ "." ++ v.str
  override def toString: String = str

case class ExprAnnot(expr: Exp) extends Annot:
  override def toString: String = ExpsLat.expString(expr)

abstract class IntSetExpr(at1: AnalysisTask) extends Transform[Expr,IntervalSet]:
  override val next: IntervalSet = IntervalSet(at1)

  override val base: Expr = new Expr {
    override val at: AnalysisTask = at1
    override lazy val project: ProgramCFG = at.pcfg

    override def explain[jv](n: Node, c: VarCtx, k: VarKey, lod: Int)(using ctx: SolvCtxT, j: JsonF[jv]): jv =
      n match
        case ln: LNode =>
          val d = ctx.get(n,c,k)
          j.Obj(s"expressions ${next.niceNode(ln)}" -> L.toJson(d))
        case _ => super.explain(n,c,k,lod)
  }

  override def setVerificationMode(b: Boolean): Unit =
    super.setVerificationMode(b)
    base.setVerificationMode(b)
    next.setVerificationMode(b)

  override def nextNCtx(using ctx: SolvCtxT): ExpQueryCtx[Node, next.VarCtx, next.VarKey, next.Dl] =
    new nextCtx with ExpQueryCtx[Node, next.VarCtx, next.VarKey, next.Dl] {
      override def getExps(n: Node, v: Var): Option[Exps] =
        ctx.get(n, Left(()), Left(())) match
          case Base(BotMap, _) => Some(Set.empty)
          case Base(MapExps(m), _) => m.get(v).map(_._2)
          case _ => None
    }

  override type ECtx[N1, C1, K1, D1] = EscapedVarCtx[N1, C1, K1, D1]

  override def nextECtx(using ctx: JBCtxT): next.ECtx[Node, next.VarCtx, next.VarKey, next.Dl] = new nextJBCtx

  override def baseNCtx(using ctx: SolvCtxT): base.NCtx[Node, base.VarCtx, base.VarKey, base.Dl] = new baseCtx

  override def baseECtx(using ctx: JBCtxT): IntQueryCtx[Node, base.VarCtx, base.VarKey, base.Dl] & EscapedVarCtx[Node, base.VarCtx, base.VarKey, base.Dl] =
    val baseNC = new baseNodeCtx
    val stSize = ctx.stackSize
    val stDiff = ctx.stackDiff
    new JBC(baseNC)(stSize,stDiff) with IntQueryCtx[Node, base.VarCtx, base.VarKey, base.Dl] with EscapedVarCtx[Node, base.VarCtx, base.VarKey, base.Dl]  {
      override def getInterval(n: Node, c: base.VarCtx, k:base.VarKey, v: Var): IntervalSetLat.t =
        n match
          case ln:LNode =>
            ctx.get(NodeVar(ln, v), Right(()), Right(IntervalKey)) match
              case Base(_, _) => IntervalSetLat.bot
              case Next(t) => t
          case _ =>
            ctx.get(NodeGVar(n, v), Right(()), Right(IntervalKey)) match
              case Base(_, _) => IntervalSetLat.bot
              case Next(t) => t

      override def getPrivateVars(n: Node): Option[Set[Var]] =
        ctx.getPrivateVars(n)
    }

  type IntDom = IntervalSetLat.t

  override inline def normal(instr: Instr)(using ctx: JBCtxT): Dl =
    instr match {
      case INVOKESTATIC(a) if a._1 == "P6der" && a._2 == "evalInt" =>
        ctx.from match {
          case ln: LNode =>
            val _ = ctx.get(NodeVar(ln, SVar(0)), Right(()), Right(IntervalKey)) // force evaluation
            val _ = ctx.get(NodeVar(ln, SVar(0)), Right(()), Right(ExplainKey))  // show explain node
          case gn: GNode =>
            val _ = ctx.get(NodeGVar(gn, SVar(0)), Right(()), Right(IntervalKey)) // force evaluation
            val _ = ctx.get(NodeGVar(gn, SVar(0)), Right(()), Right(ExplainKey))  // show explain node
          case _ => ()
        }
        normal_def(instr)
      case _ => normal_def(instr)
    }

  override inline def annot(ant: Annot)(using ctx: JBCtxT): Dl =
    def sideVar(n: Node, c: next.VarCtx, v: Var, d:IntDom): Unit =
      n match
        case lv:LNode =>
          ctx.set(NodeVar(lv,v),Right(()),Right(IntervalKey),Next(d))
        case _ =>
          ctx.set(NodeGVar(n,v),Right(()),Right(IntervalKey),Next(d))
    def evalVar(n: Node, c: next.VarCtx, v: Var): IntDom =
      n match
        case n:LNode =>
          if n.is_return then
            ctx.get(NodeVar(n,v), Right(c), Right(ExplainKey))
          ctx.get(NodeVar(n,v), Right(c), Right(IntervalKey)) match
            case Base(_, _) => SortedSet()
            case Next(i) => i
        case _ =>
          ctx.get(NodeGVar(n,v), Right(c), Right(IntervalKey)) match
            case Base(_, _) => SortedSet()
            case Next(i) => i
    (ctx.k, ctx.c, ctx.to, ant) match
      case (Right(IntervalKey), Right(()), NodeVar(ln,v), ExprAnnot(e)) =>
        Next(next.evalExps(using evalVar, sideVar)(Set(e)))
      case (Right(IntervalKey), Right(()), NodeGVar(ln,v), ExprAnnot(e)) =>
        Next(next.evalExps(using evalVar, sideVar)(Set(e)))
      case _ => ctx.st(using ctx.k)

  override def metaNode(node: Node, c: VarCtx, key: VarKey)(using ctx: SolvCtxT): Option[Dl] =
    (key, c) match
      case (Right(IntervalKey), Right(())) => None
      case (Left(k),Left(c)) =>
        base.metaNode(node,c,k)(using baseNCtx).map(Base(_,ExpsLat.bot))
      case _ => Some(L.bot)

  def expToEdges2(to:NodeVar|NodeGVar,e:Exp)(using ctx: SolvCtxT):Seq[(Edge,Node)] =
    def toNodeVar(n:Node, c:Ctx, v:Var): Node = {
      n match
        case ln:LNode => NodeVar(ln,v)
        case _ => NodeGVar(n,v)
    }
    def getFrom(e:Exp): Seq[Node] =
      val ds = deps(e)
      if ds.isEmpty then Seq(to)
      else ds.view.map(toNodeVar).toSeq

    val er1 = next.loadFullExp(e)(using Set.empty, nextNCtx)
    val er2 = next.optimizeFields(er1)
    val er3 = next.optimizeGuards(er2)
    val er4 = next.optimizeEmptyLoops(er3)
    val er = next.dropNonLin(er4)
    if er == Unknw then
      Seq((BasicBlock(List(Right(ExprAnnot(Unknw)))),to))
    else
      to match
        case NodeVar(ln, v) if er == EVar(ln,(),v) => Seq.empty
        case NodeGVar(ln, v) if er == EVar(ln,(),v) => Seq.empty
        case _ =>
          for d <- getFrom(er)
            yield (BasicBlock(List(Right(ExprAnnot(er)))), d)


  override def genEdges(node: Node, ct: VarCtx, key: VarKey)(using ctx: SolvCtxT): Set[(Edge, (Node, VarKey))] =
    (node,ct,key) match
      case (nv:NodeVar,Right(()),Right(IntervalKey)) =>
        nv.v match
          case FieldGVar(ty,g,sig) =>
            ctx.get(nv.ln, Left(()), Left(())) match {
              case Base(MapExps(cs), _) =>
                for c <- cs.getOrElse(LVar(0), (true,Set.empty))._2
                    (e,from) <- expToEdges2(nv, Op(GETFIELD(Cell((ty,g,sig))),c))
                yield (e,(from,Right(IntervalKey)))
              case _ => Set.empty
            }
          case nvv =>
            ctx.get(nv.ln, Left(()), Left(())) match {
              case Base(MapExps(cs), _) =>
                for c <- cs.getOrElse(nvv, (true,Set.empty))._2
                    (e,from) <- expToEdges2(nv, c)
                yield (e,(from,Right(IntervalKey)))
              case _ => Set.empty
            }
      case (nv:NodeGVar,Right(()),Right(IntervalKey)) =>
        ctx.get(nv.ln, Left(()), Left(())) match {
          case Base(MapExps(cs), _) =>
            for c <- cs.getOrElse(nv.v, (true,Set.empty))._2
                (e,from) <- expToEdges2(nv, c)
            yield (e,(from,Right(IntervalKey)))
          case _ => Set.empty
        }
      case _ => Set.empty


abstract class OEscIntExpr(at1: AnalysisTask) extends Transform[ObjectEscape,IntSetExpr]:
  override val next: IntSetExpr = new IntSetExpr(at1) {
    override val at: AnalysisTask = at1
    override lazy val project: ProgramCFG = at.pcfg
  }
  override val base: ObjectEscape = new ObjectEscape {
    override val at: AnalysisTask = at1
    override lazy val project: ProgramCFG = at.pcfg
  }

  override def setVerificationMode(b: Boolean): Unit =
    super.setVerificationMode(b)
    base.setVerificationMode(b)
    next.setVerificationMode(b)

  override def baseNCtx(using ctx: SolvCtxT): SolvCtx[Node, Unit, Unit, OEDomain] = new baseCtx

  override def baseECtx(using ctx: JBCtxT): JBCtx[Node, Unit, Unit, OEDomain] = new baseJBCtx

  override def nextNCtx(using ctx: SolvCtxT): SolvCtx[Node, next.VarCtx, next.VarKey, next.Dl] = new nextCtx

  override def nextECtx(using ctx: JBCtxT): EscapedVarCtx[Node, next.VarCtx, next.VarKey, next.Dl] =
    val nextNC = new nextNodeCtx
    val stSize = ctx.stackSize
    val stDiff = ctx.stackDiff
    new JBC(nextNC)(stSize,stDiff) with EscapedVarCtx[Node, next.VarCtx, next.VarKey, next.Dl]  {
      override def getPrivateVars(n: Node): Option[Set[Var]] =
        val vl = ctx.get(n, Left(()), Left(()))
        vl match
          case Base(None, _) => None
          case Base(Some((vs,_)), _) => Some(vs)
          case _ => Some(Set.empty)
    }

  override type ECtx[N1, C1, K1, D1] = JBCtx[N1, C1, K1, D1]

  override def normal(instr: Instr)(using ctx: JBCtx[Node, VarCtx, VarKey, Dl]): Dl =
    instr match
      case INVOKESTATIC(a) if a._1 == "P6der" && a._2 == "evalInt" =>
        ctx.from match {
          case ln: LNode =>
            val _ = ctx.get(NodeVar(ln, SVar(0)), Right(Right(())), Right(Right(IntervalKey))) // force evaluation
            val _ = ctx.get(NodeVar(ln, SVar(0)), Right(Right(())), Right(Right(ExplainKey)))  // show explain node
          case _ => ()
        }
        normal_def(instr)
      case _ =>
        normal_def(instr)

  override def keyStage(k: Either[Unit, Either[Unit, ISetKey]]): Int =
    k match
      case Left(value) => 0
      case Right(value) => next.keyStage(value)

  override def genEdges(n: Node, ct: VarCtx, key: VarKey)(using ctx: SolvCtxT): Set[(Edge, (Node, VarKey))] =
    (ct,key) match
      case (Left(c), Left(k)) => base.genEdges(n,c,k)(using baseNCtx).map{case (e,(n,k)) => (e,(n,Left(k)))}
      case (Right(c), Right(k)) => next.genEdges(n,c,k)(using nextNCtx).map{case (e,(n,k)) => (e,(n,Right(k)))}
      case _ => Set.empty

  override def metaNode(n: Node, ct: VarCtx, key: VarKey)(using ctx: SolvCtxT): Option[Dl] =
    (ct,key) match
      case (Left(c), Left(k)) => base.metaNode(n,c,k)(using baseNCtx).map(d => Base(d, Set.empty))
      case (Right(c), Right(k)) => next.metaNode(n,c,k)(using nextNCtx).map(d => Next(d))
      case _ => None

  override def annot(ant: Annot)(using ctx: JBCtxT): Dl =
    (ctx.c, ctx.k) match
      case (Left(c), Left(k)) => Base(base.annot(ant)(using baseECtx), Set.empty)
      case (Right(c), Right(k)) => Next(next.annot(ant)(using nextECtx))
      case _ => L.bot

