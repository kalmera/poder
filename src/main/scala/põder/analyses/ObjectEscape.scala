package põder.analyses

import scala.language.implicitConversions
import põder.framework.cfg.*
import põder.framework.{JsonOpF, Lattice, NarrowOpF, SimpleJBCAnalysis, WidenOpF}
import põder.log.Log
import põder.util.typ.{BoolT, IntT, MethodType, VoidT}
import põder.dom.*
import põder.dom.intervals.*
import põder.dom.expressions.{Var,LVar,SVar}
import põder.util.json.JsonF
import põder.util.CellConv

import scala.annotation.tailrec
import scala.collection.immutable.Queue

type OEDomain = Option[(Set[Var],List[Set[Var]])]

object OELat extends Lattice[OEDomain] with WidenOpF[OEDomain] with NarrowOpF[OEDomain]:
  override def top: OEDomain = Some((Set.empty, List.empty))
  override def bot: OEDomain = None
  override def widen(oldv: OEDomain, newv: OEDomain): OEDomain = newv

  override def toJson[jv](x: OEDomain)(using j: JsonF[jv]): jv =
    x match
      case Some((vs,es)) =>
        val copies = es.zipWithIndex.map{case (xs,n) => n.toString -> j.Arr(xs.map(x => j.Str(x.str)).toSeq*)}
        j.Obj("private" -> j.Arr(vs.map(x => j.Str(x.str)).toSeq*), "copies" -> j.Obj(copies*))
      case None => j.Str("dead code")

  override def join(x: OEDomain, y: OEDomain): OEDomain =
    (x,y) match
      case (Some((vs1, es1)), Some((vs2,es2))) => Some((vs1.intersect(vs2),es1.lazyZip(es2).map(_.union(_))))
      case (None, y) => y
      case (x, None) => x

  override def leq(x: OEDomain, y: OEDomain): Boolean =
    (x,y) match
      case (Some((vs1, es1)), Some((vs2,es2))) =>
        vs2.subsetOf(vs1) && es1.lazyZip(es2).forall(_.subsetOf(_))
      case (None, _) => true
      case _ => false


trait ObjectEscape extends SimpleJBCAnalysis:
  override type Dl = OEDomain
  override given L: (Lattice[Dl] & WidenOpF[Dl] & NarrowOpF[Dl]) = OELat

  def move(n:Int)(xs: Var):Set[Var] =
    xs match
      case SVar(i) if i+n<0 => Set.empty
      case SVar(i) => Set(SVar(i+n))
      case LVar(i) => Set(LVar(i))

  def popSafe(d:Dl):Dl =
    d match
      case Some((vs,es)) =>
        if es.nonEmpty then
          Some((vs.flatMap(move(-1)),es.tail.map(_.flatMap(move(-1)))))
        else
          Some((vs.flatMap(move(-1)), es))
      case _ => d

  def popUnSafe(d:Dl):Dl =
    d match
      case Some((vs,es)) =>
        if es.nonEmpty then
          Some(((vs--(es.head)).flatMap(move(-1)),es.tail.map(_.flatMap(move(-1)))))
        else
          Some((vs.flatMap(move(-1)), es))
      case _ => d

  def push(d:Dl, s:Set[Var]):Dl =
    d match
      case Some((vs,es)) =>
        if (s.intersect(vs)).nonEmpty then
          Some((vs.flatMap(move(1))+SVar(0),s.intersect(vs).flatMap(move(1))::es.map(_.flatMap(move(1)))))
        else
          Some((vs.flatMap(move(1)),s.intersect(vs).flatMap(move(1))::es.map(_.flatMap(move(1)))))
      case _ => d

  def loadVar(d:Dl, n:Int):Dl =
    push(d,Set(LVar(n)))

  def storeVar(d:Dl, n:Int):Dl =
    d match
      case Some((vs,es)) =>
        if vs contains SVar(0) then
          val vs1 = (vs-SVar(0)+LVar(n)).flatMap(move(-1))
          popSafe(Some((vs1,es)))
        else
          popSafe(Some((vs,es)))
      case _ => d

  def peek(d:Dl):Set[Var] =
    d match
      case Some((_,es)) if es.nonEmpty => es.head
      case _ => Set.empty

  override inline def normal(i: Instr)(using ctx: JBCtxT): Dl =
    val d = ctx.st
    i match
      case NEW(_) =>
        d match
          case Some((vs,es)) =>
            Some((vs.flatMap(move(1))+SVar(0),Set(SVar(0))::es.map(_.flatMap(move(1)))))
          case d => d
      case _:ICONST => push(d, Set.empty)
      case BIPUSH(_) => push(d, Set.empty)
      case xSTORE(_,a) => storeVar(d, a.toInt)
      case xLOAD(_,a) => loadVar(d, a.toInt)
      case PUTFIELD(_) => popSafe(popUnSafe(d))
      case PUTSTATIC(_) => popUnSafe(d)
      case GETFIELD(_) => push(popSafe(d), Set.empty)
      case xADD(_) => push(popSafe(popSafe(d)), Set.empty)
      case xMUL(_) => push(popSafe(popSafe(d)), Set.empty)
      case xDIV(_) => push(popSafe(popSafe(d)), Set.empty)
      case xSUB(_) => push(popSafe(popSafe(d)), Set.empty)
      case xAND(_) => push(popSafe(popSafe(d)), Set.empty)
      case xOR(_) => push(popSafe(popSafe(d)), Set.empty)
      case INVOKESTATIC(a) if a._1 == "P6der" && a._2 == "evalInt" => d
      case POP => popSafe(d)
      case DUP =>
        val t = peek(d)
        push(d,t)
      case RETURN =>
        ctx.from match
          case n:MethodNode =>
            1.to(n.stackSize).foldLeft(d){case (d,_) => popSafe(d) }
          case _ => L.top
      case INVOKESPECIAL(a) if a._1 == "java/lang/Object" && a._2 == "<init>" =>
        popSafe(d)
      case i =>
        i.stackDiff match
          case Relative(0) => d
          case Relative(n) if n<0 => 1.to(-n).foldLeft(d){case (d,_) => popUnSafe(d) }
          case Relative(n) if n>0 => 1.to(n).foldLeft(d){case (d,_) => push(d,Set.empty) }
          case _ => L.top

  override inline def branch(instr: JumpInstr, thn: Boolean)(using ctx: JBCtxT): Dl =
    instr match
      case IF_ICMPx(o,_) => popSafe(popSafe(ctx.st))
      case _ =>
        instr.stackDiff match
          case Relative(0) => ctx.st
          case Relative(n) if n<0 => 1.to(-n).foldLeft(ctx.st){case (d,_) => popUnSafe(d) }
          case Relative(n) if n>0 => 1.to(n).foldLeft(ctx.st){case (d,_) => push(d,Set.empty) }
          case _ => L.top

  override inline def node(nd: Node, b: VarCtx, k: VarKey)(using ctx: SolvCtxT): Dl =
    nd match {
      case n: LNode if n.is_start && project.startMethods.contains((n.methodInfo.mthd,n.methodInfo.mthdTyp)) =>
        Some((Set.empty, List.empty))
      case _ => L.bot
    }

  override inline def throws(instr: Instr, ex: Either[String, Set[String]])(using ctx: JBCtxT): Dl = L.bot

  override inline def annot(ant: Annot)(using ctx: JBCtxT): Dl = L.bot

  override inline def start(locals: Int, stack: Int, access: Int)(using ctx: JBCtxT): Dl = ctx.st

  override inline def enter(instr: MethodInstr, typ: MethodType, static: Boolean)(using ctx: JBCtxT): Seq[(VarCtx, VarKey, VarKey => Dl, Dl)] =
    ctx.st match
      case None => Seq.empty
      case Some((_,es:List[Set[Var]])) =>
        val n = if static then typ.args.size else 1+typ.args.size
        val qs = es.take(n).zipWithIndex.toSet.flatMap{case (e,i) => if e.isEmpty then Set.empty else Set[Var](LVar(i))}
        enterWith(Some((qs,List.empty)))

  override inline def combine(instr: MethodInstr, typ: MethodType, static: Boolean, result: VarKey => Dl)(using ctx: JBCtxT): Dl =
    val n = if static then typ.args.size else 1+typ.args.size
    result(()) match
      case None => None
      case Some(vsr,_) =>
        (0.until(n)).foldLeft(ctx.st){
          case (d1,i) =>
            if vsr contains LVar(i) then
              popSafe(d1)
            else
              popUnSafe(d1)
        }

  override def isBuiltinFun(instr: MethodInstr, typ: MethodType, static: Boolean): Boolean =
    val (klass, mthd, typ) = instr.a.apply
    (klass == "java/lang/Object" && mthd == "<init>") || (klass == "P6der" && mthd == "evalInt")

  override def explain[jv](n: Node, c: VarCtx, k: VarKey, lod: Int)(using ctx: SolvCtxT, j: JsonF[jv]): jv =
    val v = ctx.get(n,c,k)
    j.Obj(s"object escape information for node ${n.toString}" -> L.toJson(v))
