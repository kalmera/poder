package põder.analyses

import scala.language.implicitConversions
import põder.dom
import põder.dom.{BotMap, ExpMap, ExpMapLat, ExpsLat, IntervalSetLat, MapExps, TopMap}
import põder.dom.expressions.*
import põder.dom.intervals.*
import põder.framework.*
import põder.framework.cfg.*
import põder.log.Log
import põder.util.CellConv
import põder.util.Cell
import põder.util.json.JsonF
import põder.util.typ.{MethodType, Parser, VoidT}

import scala.annotation.tailrec
import scala.math.Ordering.OptionOrdering
import scala.collection.immutable.{BitSet, Queue, SortedSet, TreeSet}

final class HeapClass(val at: AnalysisTask) extends Heap

final class IntervalValueClass(val at: AnalysisTask) extends IntervalValue

abstract class HeapTrans(at: AnalysisTask) extends Transform[HeapClass, IntervalValueClass]:
  val base: HeapClass = HeapClass(at)
  val next: IntervalValueClass = IntervalValueClass(at)

  override def nextNCtx(using ctx: SolvCtxT): SolvCtx[Node, next.VarCtx, next.VarKey, next.Dl] = new nextCtx

  override def nextECtx(using ctx: JBCtxT): JBCtx[Node, next.VarCtx, next.VarKey, next.Dl] =
    new nextJBCtx

  override def baseNCtx(using ctx: SolvCtxT): base.NCtx[Node, base.VarCtx, base.VarKey, base.Dl] = new baseCtx

  override def baseECtx(using ctx: JBCtxT): base.ECtx[Node, base.VarCtx, base.VarKey, base.Dl] =
    new baseJBCtx

  override inline def normal(instr: Instr)(using ctx: JBCtxT): Dl =
    (ctx.k,instr,ctx.st(using ctx.k)) match
      case (Left(k), PUTFIELD(a), Base(Lifted((_::Lifted(vs:Set[String])::_,_)),_)) =>
        val t: base.Dl = base.normal(instr)(using new baseJBCtx)
        val is = for v <- vs yield PUTFIELD(Cell((v,a._2,a._3)))
        val c = normal_exps(is)
        Base(t, c)
      case (Left(k), PUTFIELD(a), Base(Lifted((_::Top::_,_)),_)) =>
        val t: base.Dl = base.normal(instr)(using new baseJBCtx)
        val is = for v <- base.top_pointer yield PUTFIELD(Cell((v,a._2,a._3)))
        val c = normal_exps(is)
        Base(t, c)
      case (Left(k), GETFIELD(a), Base(Lifted((Top::_,_)),_)) =>
        val t: base.Dl = base.normal(instr)(using new baseJBCtx)
        val is = for v <- base.top_pointer yield GETFIELD(Cell((v,a._2,a._3)))
        val c = normal_exps(is)
        Base(t, c)
      case (Left(k), GETFIELD(a), Base(Lifted((Lifted(vs:Set[String])::_,_)),_)) =>
        val t: base.Dl = base.normal(instr)(using new baseJBCtx)
        val is = for v <- vs yield GETFIELD(Cell((v,a._2,a._3)))
        val c = normal_exps(is)
        Base(t, c)
      case (Left(k),_,_) =>
        val t: base.Dl = base.normal(instr)(using new baseJBCtx)
        val c = normal_exps(List(instr))
        Base(t, c)
      case (Right(k),_,_) =>
        Next(next.normal(instr)(using new nextJBCtx))

  // todo: exceptions
  def expToEdges(to:Node, key:VarKey, e:Exp, es:Queue[Instr] = Queue.empty):Seq[(Edge,(Node,VarKey))] =
    e match
      case Unknw => Seq.empty
      case Op(op:MethodInstr, EVar(n, c, _)) =>
        Seq((Returns(op), (n, key)))
      case Op(op:(JumpInstr & LabelArg), EVar(n, c, _)) =>
        to match {
          case m: LNode if m.labels.contains(op.a) => Seq((Fall(op), (n, key)))
          case _ => Seq((Jump(op), (n, key)))
        }
      case Op(op, p) => expToEdges(to,key,p,es:+op)
      case EVar(n, c, v) if es.isEmpty && n==to => Seq.empty
      case EVar(n, c, v) if es.isEmpty => Seq((BasicBlock(List(Left(NOP))),(n,key)))
      case EVar(n, c, v) => Seq((BasicBlock(es.reverse.map(Left.apply).toList),(n,key)))
      case _ => Seq.empty

  override def genEdges(node: Node, ct: VarCtx, key: VarKey)(using ctx: SolvCtxT): Set[(Edge, (Node, VarKey))] =
    (node,ct,key) match
      case (LNode(mi, id), _, Right(k)) if id==1 =>
        Set((Start(mi.locals,mi.stackSize,mi.access),(project.startVarsFun(mi.cls,mi.mthd,mi.mthdTyp),Right(k))))
      case (_,Right(()),Right(k)) =>
            ctx.get(node, Left(()), Left(HeapKey)) match {
              case Base(t, cs) =>
                for c <- cs
                    e <- expToEdges(node, Right(k), c)
                yield e
              case _ => Set.empty
            }
      case _ => Set.empty
