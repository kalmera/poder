package põder.analyses

import scala.language.implicitConversions
import põder.dom.LogicHelper.*
import põder.dom.{IProp, LVar, LogLat, Logic, SVar, Val}
import põder.framework.{Lattice, NarrowOpF, SimpleJBCAnalysis, WidenOpF}
import põder.framework.cfg.{Annot, GETSTATIC, IF_ACMPx, IF_ICMPx, IFx, INVOKEDYNAMIC, INVOKESTATIC, Instr, JumpInstr, LNode, MethodInstr, Node, ProgramCFG, xLOAD, xSTORE}
import põder.log.Log
import põder.util.typ.{MethodType, Parser}
import põder.util.CellConv

trait SecLabel extends SimpleJBCAnalysis:
  override type Dl = Logic
  override given L: (Lattice[Dl] & WidenOpF[Dl] & NarrowOpF[Dl]) = LogLat

  private val builtins = Map("P6der" -> List("getDirtyInput", "sanitize", "runQuery", "check","grantPermission","revokePermission"))
  override def isBuiltinFun(instr: MethodInstr, typ: MethodType, static: Boolean): Boolean =
    val (pkg,mthd,_) = instr.a.apply
    builtins.contains(pkg) && builtins(pkg).contains(mthd)

  def local(n:Node, start:Boolean = false)(i:Int): Val =
    val vl =  LVar(i, start)
    if n.isInstanceOf[LNode] && (n.asInstanceOf[LNode].varNames contains i) then
      vl.alias = n.asInstanceOf[LNode].varNames(i)
    vl

  private val dirty : IProp = IProp("dirty")

  override inline def node(nd: Node, b: VarCtx, k: VarKey)(using ctx: SolvCtxT): Logic =
    if nd.isInstanceOf[LNode] && nd.asInstanceOf[LNode].is_start then L.top else L.bot

  override inline def start(locals: Int, stack: Int, access: Int)(using ctx: JBCtxT): Logic = ctx.st
  override inline def enter(instr: MethodInstr, typ: MethodType, static: Boolean)(using ctx: JBCtxT): Seq[(VarCtx, VarKey, VarKey => Logic, Logic)] = enterWith(ctx.st)
  override inline def combine(instr: MethodInstr, typ: MethodType, static: Boolean, result: VarKey => Logic)(using ctx: JBCtxT): Logic = result(())

  override inline def branch(instr: JumpInstr, thn: Boolean)(using ctx: JBCtxT): Logic =
    instr match
      case IFx(x, a) => ctx.st.pop()
      case IF_ICMPx(x, a) => ctx.st.pop(2)
      case IF_ACMPx(x, a) => ctx.st.pop(2)
      case _ => ctx.st

  override inline def normal(instr: Instr)(using ctx: JBCtxT): Logic =
    val st = ctx.st
    instr match
      case xSTORE(_, n) =>
        val v = local(ctx.to)(n.toInt)
        st.eqForget(v).pop(1, Seq(v())).simpl
      case xLOAD(_, n) =>
        val v = local(ctx.to)(n.toInt)
        if st.query(dirty(v)) contains false then
          st.push
        else
          st.push && dirty(SVar(0))
      case GETSTATIC(_) =>
        st.push
      case INVOKESTATIC(a) if a._1 == "P6der" && a._2 == "getDirtyInput" =>
        st.push && dirty(SVar(0))
      case INVOKESTATIC(a) if a._1 == "P6der" && a._2 == "sanitize" =>
        st.pop().push
      case INVOKESTATIC(a) if a._1 == "P6der" && a._2 == "runQuery" =>
        if st.query(dirty(SVar(0))) contains true then
          Log.logCommon("Doom!")
        else
          Log.logCommon("All is well!")
        st.pop()
      case INVOKEDYNAMIC(a, b) if b._2 == "makeConcatWithConstants" =>
        val typ = Parser.mpar(a._3)
        if typ.args.indices.exists(i => st.query(dirty(SVar(i))) contains true) then
          st.pop(typ.args.length).push && dirty(SVar(0))
        else
          st.pop(typ.args.length).push
      case _ =>
        st

  override inline def throws(instr: Instr, ex: Either[String, Set[String]])(using ctx: JBCtxT): Logic = ctx.st
  override inline def annot(ant: Annot)(using ctx: JBCtxT): Logic = ctx.st
