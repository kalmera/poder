package põder.analyses

import scala.language.implicitConversions
import põder.framework._
import põder.framework.cfg._
import põder.util.json.JsonF
import põder.util.typ.MethodType
import põder.util.CellConv

case class Method(val cls: String, val mthd: String)

object MSet extends TopSet[Method]:
  def TtoJson[jv](x: Method)(using j: JsonF[jv]): jv =
    j.Str(x.cls ++ "." ++ x.mthd)

object ESet extends TopSet[String]:
  def TtoJson[jv](x: String)(using j: JsonF[jv]): jv =
    j.Str(x)

object SPrd extends Product(MSet ,ESet) :
  override def toJson[jv](x: (Top.type|Lifted[Set[Method]], Top.type|Lifted[Set[String]]))(using j: JsonF[jv]): jv =
    import j._
    Obj("methods" -> MSet.toJson(x._1), "exceptions" -> ESet.toJson(x._2))

object DepD extends BotLift(SPrd)

trait Dep extends SimpleJBCAnalysis:
  override type Dl = Bot.type | Lifted[(Top.type|Lifted[Set[Method]], Top.type|Lifted[Set[String]])]
  override given L: (Lattice[DepD.t] & WidenOpF[DepD.t] & NarrowOpF[DepD.t]) = DepD

  val empty:Dl = Lifted((Lifted(Set()),Lifted(Set())))

  override inline def node(nd: Node, b: VarCtx, k: VarKey)(using ctx: SolvCtxT): DepD.t =
    nd match
      case n: LNode if n.is_start => empty
      case _ => L.bot

  override inline def isBuiltinFun(instr: MethodInstr, typ:MethodType, static:Boolean): Boolean =
    val (cls, mthd, _) = instr.a.apply
    cls == "P6der" && mthd == "check"

  // algseisund
  override inline def start(locals: Int, stack: Int, access: Int)(using ctx: JBCtxT): Dl = ctx.st

  // hargnemise üleminekufunktsioon
  override inline def branch(instr: JumpInstr, thn: Boolean)(using ctx: JBCtxT): DepD.t = ctx.st

  def printDep(node:Node, d:Dl): Unit =
    (d, node) match
      case (Lifted((Lifted(a),y)), LNode(mi,_)) =>
        printf("%s: %s\n", mi.cls ++ "." ++ mi.mthd, a.map(x=>x.cls++"."++x.mthd).mkString(", "))
      case _ => ()

  // "tavaline" üleminekufunktsioon
  override inline def normal(instr: Instr)(using ctx: JBCtxT): Dl = instr match
    case RETURN  => if inVerification then printDep(ctx.to,ctx.st) ; ctx.st
    case xRETURN(_) => printDep(ctx.to,ctx.st) ; ctx.st
    case i => ctx.st

  // erindi üleminekufunktsioon
  override inline def throws(instr: Instr, ex: Either[String, Set[String]])(using ctx: JBCtxT): Dl = ex match
    case Left(exception) =>
      ctx.st match
        case Lifted((m, e)) =>
          val methods: Top.type|Lifted[Set[Method]] = m.asInstanceOf[Top.type|Lifted[Set[Method]]]
          val exceptions: Top.type|Lifted[Set[String]] = e.asInstanceOf[Top.type|Lifted[Set[String]]]
          Lifted((methods, ESet.join(exceptions, Lifted(Set(exception)))))
        case _ => ctx.st

    case Right(exceptionSet) => ctx.st

  // annotatsiooni üleminekufunktsioon
  override inline def annot(ant: Annot)(using ctx: JBCtxT): Dl = ctx.st

  // need on meetodiväljakutsete jaoks
  override inline def enter(instr: MethodInstr, typ: MethodType, static: Boolean)(using ctx: JBCtxT): Seq[(VarCtx, VarKey, VarKey => DepD.t, DepD.t)] =
    enterWith(empty)

  override inline def combine(instr: MethodInstr, typ: MethodType, static: Boolean, result: VarKey => _root_.põder.analyses.DepD.t)(using ctx: JBCtxT): Dl = ctx.st match
    case Lifted((m, e)) =>
      val d1m: Lifted[Set[Method]]|Top.type = m.asInstanceOf
      val d1t: Lifted[Set[String]]|Top.type = e.asInstanceOf
      result(()) match
        case Bot => Bot
        case Lifted((m, e)) =>
          val dm: Lifted[Set[Method]]|Top.type = m.asInstanceOf
          val dt: Lifted[Set[String]]|Top.type = e.asInstanceOf
          val mn = instr match
            case INVOKEVIRTUAL(a) => a
            case INVOKESPECIAL(a) => a
            case INVOKESTATIC(a) => a
            case INVOKEINTERFACE(a) => a
            case INVOKEDYNAMIC(a,_) => a
          val mmn = Method(mn._1, mn._2)
          Lifted(MSet.join(MSet.join(d1m, dm),Lifted(Set(mmn))), ESet.join(d1t, dt))
    case Bot => Bot
