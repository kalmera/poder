package põder.analyses

import scala.language.implicitConversions
import põder.dom._
import põder.dom.Logic._
import põder.framework.cfg._
import põder.framework._
import põder.util.typ.{MethodType, VoidT}
import põder.util.CellConv
import smtlib.trees.Terms
import smtlib.trees.Terms.Term

trait WP extends SimpleJBCAnalysis:
  val D: LogLat.type = LogLat
  type node = Node

  override type Dl = D.t
  override given L: (Lattice[D.t] & WidenOpF[D.t] & NarrowOpF[D.t]) = D

  def simplify(d:Dl): Dl =
    def not_op(s:String): String = s match
      case "!=" => "="
      case "=" => "!="
      case ">=" => "<"
      case "<=" => ">"
      case "<" => ">="
      case ">" => "<="
    def is_top(x:Dl): Boolean = D.leq(D.top, x)
    def is_bot(x:Dl): Boolean = D.leq(x, D.bot)
    if is_top(d) then D.top else
    if is_bot(d) then D.bot else
      d match
        case And(xs@_*) =>
          val ys = xs.map(simplify).filterNot(is_top)
          if ys.isEmpty then D.top else And(ys*)
        case Or(xs@_*) =>
          val ys = xs.map(simplify).filterNot(is_bot)
          if ys.isEmpty then D.bot else Or(ys*)
        case Not(App(Fun(op,t), x,y)) => App(Fun(not_op(op),t), x, y)
        case Not(x) => Not(simplify(x))
        case ForallL(t, x) => ForallL(t,simplify(x))
        case ExistsL(t, x) => ExistsL(t,simplify(x))
        case _ => d

  override def isBuiltinFun(i: MethodInstr, t:MethodType, b:Boolean): Boolean =
    val (klass, mthd, _) = i.a.apply
    klass == "P6der" && mthd == "check"

  override inline def node(nd: node, b: VarCtx, k: VarKey)(using ctx: SolvCtxT): D.t = nd match
    case n:LNode if n.is_return => L.top
    case _ => D.bot

  override inline def branch(instr: JumpInstr, thn: Boolean)(using ctx: JBCtxT): D.t = instr match
    case IF_ICMPx(x, _) =>
      val a = x match
        case EQ => app(BinIntProp("="), SVar(1), SVar(0))
        case NE => app(BinIntProp("!="), SVar(1), SVar(0))
        case LT => app(BinIntProp("<"), SVar(1), SVar(0))
        case GE => app(BinIntProp(">="), SVar(1), SVar(0))
        case GT => app(BinIntProp(">"), SVar(1), SVar(0))
        case LE => app(BinIntProp("<="), SVar(1), SVar(0))

      val aa = if thn then
        a
      else
        not(a)

      simplify(and(
        aa,
        vS(vS(ctx.st))
      ))

    case _ => ctx.st


  // tavaline "üleminekurelatsioon", põhitegevuse saadab aNormal_2-e
  // vajadusel laseb invariant funktsioonil invariantidega tegelda
  override inline def normal(instr: Instr)(using ctx: JBCtxT): D.t =
    ctx.to match
      case n:LNode =>
        val a = n.annot
        val dprim = sNormal_2(instr)(ctx.st)
        a match
          case List(s: String) if ctx.to_final => invariant(s, dprim, n.varNames)
          case _ => dprim
      case _ => D.bot

  // tsükliinvariandid
  def invariant(s: String, dPrim: Dl, varNames : Map[Int, String]): Dl =
    val term = parseTerm(s)
    val d = term2avaldis(term, varNames)

    if !D.leq(dPrim, d) then
      println("Hoiatus: Ei suuda tõestada invariandi kehtimist.")

    d

  // "tavaline" üleminekufunktsioon
  val sNormal_2: PartialFunction[Instr, Dl => Dl] = {
    case xLOAD(_, i) => (d: Dl) => {
      vS(repl(Map[Val, Dl](SVar(0) -> app(LVar(i.toInt))))(d))
    }
    case xSTORE(_, i) => d => {
      repl(Map[Val, Dl](LVar(i.toInt) -> app(SVar(0))))(sS(d))
    }

    case INVOKESTATIC(a) if a._1 == "P6der" && a._2 == "check" => d => {
      and(app(BinIntProp("="), SVar(0), IVal(1)), sS(d))
    }

    case xRETURN(_) => d => {
      or(app(BinIntProp("="), RetVar, SVar(0)), d)
    }

    case RETURN => d => {
      d
    }

    case i: ICONST => d => {
      simplify(vS(repl(Map[Val, Dl](SVar(0) -> app(IVal(i.n))))(d)))
    }

    case BIPUSH(n) => d => {
      simplify(vS(repl(Map[Val, Dl](SVar(0) -> app(IVal(n))))(d)))
    }
    case SIPUSH(n) => d => {
      simplify(vS(repl(Map[Val, Dl](SVar(0) -> app(IVal(n))))(d)))
    }
    case LDC(s,_) => d => {
      vS(repl(Map[Val, Dl](SVar(0) -> app(IVal(3))))(d))
    }

    case IINC(la) => d => {
      val l = la._1
      val a = la._2
      repl(Map[Val, Dl](LVar(l) -> app(BinIntOp("-"), LVar(l), IVal(a))))(d)
    }

    // aritmeetika
    case xADD(x) => d => {
      repl(Map[Val, Dl](SVar(1) -> app(BinIntOp("+"), SVar(1), SVar(0))))(sS(d))
    }
    case xSUB(x) => d => {
      repl(Map[Val, Dl](SVar(1) -> app(BinIntOp("-"), SVar(1), SVar(0))))(sS(d))
    }
    case xMUL(x) => d => {
      repl(Map[Val, Dl](SVar(1) -> app(BinIntOp("*"), SVar(1), SVar(0))))(sS(d))
    }
    case xDIV(x) => d => {
      repl(Map[Val, Dl](SVar(1) -> app(BinIntOp("/"), SVar(1), SVar(0))))(sS(d))
    }
    case xREM(x) => d => {
      repl(Map[Val, Dl](SVar(1) -> app(BinIntOp("%"), SVar(1), SVar(0))))(sS(d))
    }
    case xNEG(x) => d => {
      repl(Map[Val, Dl](SVar(1) -> app(BinIntOp("-"), SVar(0))))(sS(d))
    }


    // loogika
    case xSHL(_) => d => {
      repl(Map[Val, Dl](SVar(1) -> app(BinIntOp("<<"), SVar(1))))(sS(d))
    }
    case xSHR(_) => d => {
      repl(Map[Val, Dl](SVar(1) -> app(BinIntOp(">>"), SVar(1))))(sS(d))
    }
    case xUSHR(_) => d => {
      repl(Map[Val, Dl](SVar(1) -> app(BinIntOp("U"), SVar(1), SVar(0))))(sS(d))
    }
    case xAND(_) => d => {
      repl(Map[Val, Dl](SVar(1) -> app(BinIntOp("&"), SVar(1), SVar(0))))(sS(d))
    }
    case xOR(_) => d => {
      repl(Map[Val, Dl](SVar(1) -> app(BinIntOp("|"), SVar(1), SVar(0))))(sS(d))
    }

    case i => d => {
      print("tundmatu: ")
      println(i)
      d
    }
  }

  // annotatsiooni ülemunekufunktsioon
  override inline def annot(ant: Annot)(using ctx: JBCtxT): D.t = ctx.st

  // meetodiväljakutsesse sisenemine "altpoolt"
  override inline def enter(instr: MethodInstr, typ: MethodType, static: Boolean)(using ctx: JBCtxT): Seq[(VarCtx, VarKey, VarKey => D.t, D.t)] =
      if getAtoms(ctx.st).contains(SVar(0)) then
        enterWith(False)
      else
        enterWith(True)

  // meetodisisu ja väliste reeglite ühendamine
  override inline def combine(instr: MethodInstr, typ: MethodType, static: Boolean, result: VarKey => D.t)(using ctx: JBCtxT): D.t =
    val argumendiVahetused: Map[Val, Dl] =
      LazyList.from(0).take(typ.args.length).map(LVar(_)).zip(LazyList.from(0).map(a => app(SVar(a)))).toMap
    val vahetatud_args = repl(argumendiVahetused)(result(()))

    val vana_d_suurendatud_argumentideArv = argumendiVahetused.foldLeft(ctx.st)((reeglid, _) => sS(reeglid))
    val vana_d_suurendatud_argumentideArv_ja_vahendatud_ret = if typ.ret == VoidT then
      vana_d_suurendatud_argumentideArv
    else
      vS(vana_d_suurendatud_argumentideArv)

    abiCombine(vahetatud_args, vana_d_suurendatud_argumentideArv_ja_vahendatud_ret)


  def abiCombine(funReeglid: Dl, enneReeglid: Dl): Dl =
    funReeglid match

      case Or(xs@_*) => Or(xs.map(abiCombine(_, enneReeglid))*)
      case And(xs@_*) => And(xs.map(abiCombine(_, enneReeglid))*)
      case Not(x) => Not(abiCombine(x, enneReeglid))
      case App(f, xs@_*) if f == BinIntOp("=") && xs.head == app(RetVar) => {
        repl(Map[Val, Dl](SVar(0) -> xs(1)))(enneReeglid)
      }
      case App(f, xs@_*) if f == BinIntOp("=") && xs(1) == app(RetVar) => {
        repl(Map[Val, Dl](SVar(0) -> xs.head))(enneReeglid)
      }
      case App(f, xs@_*) => App(f, xs.map(abiCombine(_, enneReeglid))*)
      case a => {
        println("Midagi muud")
        println(a)
        a
      }


  // pinuindeksite vähendaja
  def vS(a: Dl): Dl =

    def vs(s: Seq[Val]): List[(Val, Dl)] = s match
      case Seq() => List()
      case SVar(n,t) +: xs => (SVar(n,t), app(SVar(n - 1,t))) :: vs(xs)
      case x +: xs => vs(xs)

    repl(vs(getAtoms(a).toSeq).toMap)(a)

  // pinuindeksite suurendaja
  def sS(a: Dl): Dl =

    def ss(s: Seq[Val]): List[(Val, Dl)] = s match
      case Seq() => List()
      case SVar(n,t) +: xs => (SVar(n), app(SVar(n + 1, t))) :: ss(xs)
      case x +: xs => ss(xs)

    repl(ss(getAtoms(a).toSeq).toMap)(a)


  def parseTerm(s: String): Term =
    val is = new java.io.StringReader(s)
    val lexer = new smtlib.lexer.Lexer(is)
    val parser = new smtlib.parser.Parser(lexer)
    parser.parseTerm

  def term2avaldis(t : Term, varNames : Map[Int, String]) : Dl =

    val m : Map[String, Int] = varNames.toList.foldLeft(Nil: List[(String, Int)])((list, p) => (p._2, p._1) :: list).toMap
    t match
      case Terms.FunctionApplication(fun, terms) =>
        val tehe = Fun(fun.id.symbol.name,AnyType)
        val vp = terms.head
        val pp = terms(1)

        val vVal : Val = vp match
          case n : Terms.SNumeral               => IVal(n.value.bigInteger.intValue())
          case Terms.QualifiedIdentifier(id, _) => LVar(m(id.symbol.name))
          case _ => return True
        val pVal : Val = pp match
          case n : Terms.SNumeral               => IVal(n.value.bigInteger.intValue())
          case Terms.QualifiedIdentifier(id, _) => LVar(m(id.symbol.name))
          case _ => return True

        app(tehe, vVal, pVal)
      case a =>
        print("Tundmatu invariant: ")
        println(a)
        True

  override inline def throws(instr: Instr, ex: Either[String, Set[String]])(using ctx: JBCtxT): D.t = D.top
  override inline def start(locals: Int, stack: Int, access: Int)(using ctx: JBCtxT): D.t = ctx.st

