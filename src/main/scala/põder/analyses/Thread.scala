package põder.analyses

import scala.language.implicitConversions
import põder.dom.{BotMap, ExpMap, ExpMapLat, ExpsLat, IntervalSetLat, MapExps, TopMap}
import põder.dom.expressions.*
import põder.dom.intervals.*
import põder.framework.*
import põder.framework.cfg.*
import põder.log.Log
import põder.util.json.JsonF
import põder.util.*
import põder.util.typ.{MethodType, Parser}
import põder.util.CellConv

import scala.annotation.tailrec
import scala.math.Ordering.OptionOrdering
import scala.collection.immutable.{BitSet, SortedSet, TreeSet}

case object ThreadKey

type thread = Option[(Option[Node], Set[Option[Node]])]

object ThreadLat extends Lattice[thread] with WidenOpF[thread] with NarrowOpF[thread]:
  inline def bot: thread = Some(None,Set.empty)
  inline def top: thread = None

  inline def join(x: thread, y: thread): thread =
    (x,y) match {
      case (Some((c1,s1)), Some((c2,s2))) =>
        if c2==c2 then
          Some((c1,s1.union(s2)))
        else
          None
      case (Some(_),_) => x
      case _ => y
    }

  inline def widen(oldv: thread, newv: thread): thread = join(oldv, newv)

  def str(q: Option[Node]): String =
    q match {
      case Some(LNode(methodInfo, id)) => s"${methodInfo.cls}:${methodInfo.mthd}#${id.toString}"
      case Some(a) => a.toString
      case None => "main"
    }

  def toJson[jv](x: thread)(using j: põder.util.json.JsonF[jv]): jv =
    x match {
      case Some((c,s)) =>
        j.Obj("id" -> j.Str(str(c)),
              "started" -> j.Arr(s.map(v => j.Str(str(v))).toSeq*))
      case None =>
        j.Str("top")
    }

  inline def leq(x: thread, y: thread): Boolean =
    y match {
      case Some((c2,s2)) =>
        x match {
          case None => false
          case Some((c1,s1)) => c1==c2 && s1.subsetOf(s2)
        }
      case None => true
    }

  override inline def equal(x: thread, y: thread): Boolean = x==y

given threadKey: ThreadKey.type = ThreadKey

object ThreadKeyJsonOp extends JsonOpF[ThreadKey.type]:
  override def toJson[jv](x: ThreadKey.type)(using j: JsonF[jv]): jv =
    j.Str("Thread")

trait Thread extends SimpleJBCAnalysis:
  override type Dl = thread
  override given L: (Lattice[Dl] & WidenOpF[Dl] & NarrowOpF[Dl]) =
    ThreadLat

  override inline def node(nd: Node, b: VarCtx, k: VarKey)(using ctx: SolvCtxT): Dl =
    nd match
      case n:LNode if n.is_start && n.methodInfo.mthd=="main" && project.mainClass==n.methodInfo.cls =>
        Some((None, Set(None)))
      case _ => L.bot

  override inline def start(locals: Int, stack: Int, access: Int)(using ctx: JBCtxT): Dl =
    ctx.st

  inline def isThreadRun(a: Option[(String, String, String)]): Boolean =
    a match {
      case Some((cls, mth, ty)) =>
        cls == "java.lang.Thread" && mth == "run" // todo: check for subclasses
      case None =>
        false
    }

  override def isBuiltinFun(instr: MethodInstr, typ: MethodType, static: Boolean): Boolean =
    instr match {
      case INVOKESTATIC(a) if a._1 == "P6der" && a._2 == "check" => true
      case INVOKESTATIC(a) if a._1 == "P6der" && a._2 == "evalInt" => true
      case _ => isThreadRun(instr.a.get)
    }

  override inline def branch(instr: JumpInstr, thn: Boolean)(using ctx: JBCtxT): Dl =
    ctx.st

  inline def normal(instr: Instr)(using ctx: JBCtxT): Dl =
    val t = ctx.st
    instr match
      case INVOKEVIRTUAL(a) if isThreadRun(a.get) =>
        t match {
          case Some((q, s)) =>
            val (z, m, t) = a.apply
            val node = project.startVarsFun(z, m, Parser.mpar(t))
            val newTh = Some(ctx.from)
            ctx.set(node, ctx.c, ctx.k, Some(newTh, s + None))
            Some(q, s + newTh)
          case _ => ThreadLat.top
        }
      case _ => t

  // erindi üleminekufunktsioon
  override inline def throws(instr: Instr, ex: Either[String, Set[String]])(using ctx: JBCtxT): Dl = L.bot

  // annotatsiooni ülemunekufunktsioon
  override inline def annot(ant: Annot)(using ctx: JBCtxT): Dl = ctx.st

  // need on meetodiväljakutsete jaoks
  override inline def enter(instr: MethodInstr, typ: MethodType, static: Boolean)(using ctx: JBCtxT): Seq[(VarCtx, VarKey, VarKey => Dl, Dl)] =
    val t = ctx.st
    def f(k: VarKey): Dl = t
    Seq(((), (), f, t))

  override inline def combine(instr: MethodInstr, typ: MethodType, static: Boolean, result: VarKey => Dl)(using ctx: JBCtxT): Dl =
    result(())