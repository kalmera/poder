package põder.analyses

import scala.language.implicitConversions
import põder.util.typ.{MethodType, Parser, Type, VoidT}
import põder.dom.LogicHelper._
import põder.dom.Logic._
import põder.dom._
import põder.framework._
import põder.framework.cfg._
import põder.log.Log
import põder.util.json.JsonF
import põder.util.CellConv

import scala.language.postfixOps


object D extends Product(LogLat,LogLat) with WidenOpF[(LogLat.t,LogLat.t)] with NarrowOpF[(LogLat.t,LogLat.t)]:
  override def widen(oldv: (LogLat.t, LogLat.t), newv: (LogLat.t, LogLat.t)): (LogLat.t, LogLat.t) =
    (LogLat.widen(oldv._1,newv._1), LogLat.widen(oldv._2,newv._2))

  override def toJson[jv](x: (LogLat.t, LogLat.t))(using j: JsonF[jv]): jv =
    import j._
    Obj("kokkuvõte" -> LogLat.toJson(x._1), "saavutatav" -> LogLat.toJson(x._2))

case class FieldVar(cls: String, fld: String, override val typ:SimpType) extends Var(typ):
  override def str: String = cls++"."++fld

trait ModularLogic extends SimpleJBCAnalysis:
  type node = Node

  type l = LogLat.t
  type el = (node => l) => (node => l => Unit) => (node,Boolean) => l => l

  val inReachable = false

  def havoc(x:Val, o:Boolean = true): Logic => Logic = inForall{
    case Not(y) => not(havoc(x,!o)(y))
    case Or(xs@_*) =>  or(xs.map(havoc(x,o))*)
    case And(xs@_*) =>
      def eqProp: Logic => Seq[Logic] = {
        case App(BinIntProp("="), App(y), App(z)) if x == y && x == z => Seq()
        case App(BinIntProp("="), App(y), e) if x == y && !getAtoms(e).contains(y) => Seq(e)
        case App(BinIntProp("="), e, App(y)) if x == y && !getAtoms(e).contains(y) => Seq(e)
        case And(xs@_*) => xs.flatMap(eqProp)
        case _ => Seq()
      }
      val v: Option[Logic] = xs.flatMap(eqProp).headOption
      v match
        case Some(value) => and(xs.map(rewriteExp(_,x,value)).map(simplify)*)
        case _ => and(xs.map(havoc(x,o))*)
    case True => True
    case False => False
    case App(f, y@_*) => forgetE(x,o)(App(f,y*))
    case ExistsL(t, y) if t==x => ExistsL(t, y)
    case ExistsL(t, y) => ExistsL(t, havoc(x,o)(y))
    case ForallL(t, x) => ForallL(t, x)
  }

  def havoc1(x:Val): Logic => Logic = z => {
    val r = havoc(x)(z)
//    printf("havoc(%s,%s):\n%s\n", x.str, clearString(z), clearString(r))
    r
  }


  def args(n:Node,t:Seq[Type], static:Boolean): Seq[(Int,LVar)] =
    val m = (if static then 0 else 1) + t.length - 1
    (0 to m).map(i => (i,local(n,start = true)(i)))

  def ent(n:Node)(t:Seq[Type],static: Boolean): LogLat.t =
    val as = args(n,t,static)
    val bs = as.foldRight(LogLat.top) {
      case ((i,v), b) => and(b,app(BinIntProp("="),local(n)(i),v))
    }
    bs

  def local(n:Node, start:Boolean = false)(i:Int): LVar =
    val vl =  LVar(i, start)
    if n.isInstanceOf[LNode] && (n.asInstanceOf[LNode].varNames contains i) then
      vl.alias = n.asInstanceOf[LNode].varNames(i)
    vl

  override def isBuiltinFun(instr: MethodInstr, typ: MethodType, static: Boolean): Boolean =
    instr.a.apply match
      case ("P6der", "check", _) | ("java/lang/Math", "random", _) | (_,"<init>", _) | ("java/util/Random",_,_)  => true
      case _ => false

  def forallInForall(f: LogLat.t => LogLat.t): LogLat.t => LogLat.t =
    case ForallL(t, x) => ForallL(t, forallInForall(f)(x))
    case x => f(x)

  // compare with zero
  def branchIFx(x:Order, d: LogLat.t, b: Boolean): LogLat.t =
    def ordTrue = x match
      case EQ => app(BinIntProp("="), SVar(0), IVal(0))
      case NE => not(app(BinIntProp("="), SVar(0), IVal(0)))
      case LT => app(BinIntProp("<"), SVar(0), IVal(0))
      case GE => app(BinIntProp(">="), SVar(0), IVal(0))
      case GT => app(BinIntProp(">"), SVar(0), IVal(0))
      case LE => app(BinIntProp("<="), SVar(0), IVal(0))
    def ordFalse = x match
      case EQ => not(app(BinIntProp("="), SVar(0), IVal(0)))
      case NE => app(BinIntProp("="), SVar(0), IVal(0))
      case LT => app(BinIntProp(">="), SVar(0), IVal(0))
      case GE => app(BinIntProp("<"), SVar(0), IVal(0))
      case GT => app(BinIntProp("<="), SVar(0), IVal(0))
      case LE => app(BinIntProp(">"), SVar(0), IVal(0))
    inForall{ q =>
      val p = if b then ordTrue else ordFalse
      val r = popStack(and(q, p), 1)
      Log.log(s"result: ${clearString(r)}")
      r
    }(d)

  // compare two numbers
  def branchIF_ICMPx(x:Order, d: LogLat.t, b: Boolean): LogLat.t =
    def filterExp(b: Boolean)(e: l): l = if b then e else not(e)
    def ord = x match
      case EQ => "="
      case NE => "="
      case LT => "<"
      case GE => ">="
      case GT => ">"
      case LE => "<="
    inForall { q =>
      val a =
        if x==NE then
          filterExp(b)(not(app(BinIntProp(ord), SVar(1), SVar(0))  ))
        else
          filterExp(b)(app(BinIntProp(ord), SVar(1), SVar(0)))
      Log.log(s"filter: ${clearString(a)}")
      val c = and(a, q)
      Log.log(s"cut: ${clearString(c)}")
      //      val d = dnf(c)
//            Log.log(s"normalization: ${clearString(d)}")
      val e = simplify(c)
      //      Log.log(s"filter: ${clearString(e)}")
      val r = simplify(popStack(e, 2))
      r
    }(d)


  def sBranchS(st:LogLat.t, instr: JumpInstr, thn: Boolean): l =
    instr match
      case IF_ICMPx(x, a) => branchIF_ICMPx(x, st, thn)
      case IFx(x, a) => branchIFx(x, st, thn)
      //      case IF_ACMPx(x, a) =>
      case GOTO(a) => st
      //      case IFNULL(a) =>
      //      case IFNONNULL(a) =>
      //      case TABLESWITCH(a) =>
      //      case LOOKUPSWITCH(a) =>
      case _ => st

  // keeps the top element
  def clearStack(d: l): l =
    val vs = getAtoms(d).filter(_.isInstanceOf[SVar]).map(_.asInstanceOf[SVar])
    if vs nonEmpty then
      val n = vs.map(_.n).max
      val d1 = (2 to n).foldLeft(d) { (v, i) => eqForget(SVar(i - 1))(v) }
      def vs2 = vs.map {
        case SVar(m,t) => SVar(m,t) -> app(SVar(m - n,t))
      }.toMap[Val, l]

      simplify(repl(vs2)(d1))
    else
      d

  def clearLocals(d: l): l =
    def isNormalLocal(v:Val):Boolean = v.isInstanceOf[LVar] && !v.asInstanceOf[LVar].start
    val vs = getAtoms(d).filter(isNormalLocal)
    val qq1 = vs.foldLeft(d){ case (d,v) =>
      val r = havoc1(v)(d)
//      Log.logCommon(s"havoc(${v.str},${clearString(d)}):\n${clearString(r)}");
      r
    }
    if LogLat.leq(LogLat.top, qq1) then
      LogLat.top
    else if LogLat.leq(qq1,LogLat.bot) then
      LogLat.bot
    else
      qq1

  def forgetLocals(d: l): l =
    val vs = getAtoms(d).filter(_.isInstanceOf[LVar]).map(_.asInstanceOf[LVar]).filter(_.n>=0)
    vs.foldLeft(d)((d1,v) => eqForget(v)(d1))

  def trans(summary:Boolean,instr: Instr,ctxT: JBCtxT): l => l = instr match
    case xCMPG(a) => inForall { d =>
      // not implemented
      pushStack(popStack(d, 2))
    }
    case xCMPL(a) => inForall { d =>
      // not implemented
      pushStack(popStack(d, 2))
    }
    case a: FCONST => inForall { d =>
      // not implemented
      pushStack(d)
    }
    case a: LCONST => inForall { d =>
      // not implemented
      pushStack(d)
    }
    case a: DCONST => inForall { d =>
      // not implemented
      pushStack(d)
    }
    case DUP_X1 => inForall { d =>
      // not implemented
      pushStack(d)
    }
    case ANEWARRAY(a) => inForall { d =>
      // not implemented
      pushStack(d)
    }
    case INSTANCEOF(a) => inForall { d =>
      // not implemented
      pushStack(popStack(d, 2))
    }
    case CHECKCAST(a) => inForall { d =>
      // not implemented
      d
    }
    case xXOR(_) => inForall { d =>
      // not implemented
      pushStack(popStack(d, 2))
    }
    case LCMP => inForall { d =>
      // not implemented
      pushStack(popStack(d, 2))
    }
    case ACONST_NULL => inForall { d =>
      // not implemented
      pushStack(d)
    }
    case xUSHR(a) => inForall { d =>
      // not implemented
      pushStack(popStack(d, 2))
    }
    case xOR(a) => inForall { d =>
      // not implemented
      pushStack(popStack(d, 2))
    }
    case xAND(a) => inForall { d =>
      // not implemented
      pushStack(popStack(d, 2))
    }
    case LDC(a,_) => inForall { d =>
      // not implemented
      a.get match
        case Some(x: Int) =>
          val d1 = pushStack(d)
          Log.log(s"push state: ${clearString(d1)}")
          val s0eqi = app(BinIntProp("="), app(SVar(0)), app(IVal(x)))
          and(d1, s0eqi)
        case _ => pushStack(d)
    }
    case ATHROW => _ => False
    case MONITORENTER => inForall { d =>
      // not implemented
      popStack(d, 1)
    }
    case MONITOREXIT => inForall { d =>
      // not implemented
      popStack(d, 1)
    }
    case xSHL(_) => inForall { d =>
      // not implemented
      pushStack(popStack(d, 2))
    }
    case xSHR(_) => inForall { d =>
      // not implemented
      pushStack(popStack(d, 2))
    }
    case ARRAYLENGTH => inForall { d =>
      // not implemented
      pushStack(popStack(d, 1))
    }
    case x2y(a, b) => inForall { d =>
      // not implemented
      d
    }
    case NEWARRAY(a) => inForall { d =>
      // not implemented
      pushStack(popStack(d, 1))
    }
    case GETFIELD(a) => inForall { d =>
      // not implemented
      pushStack(popStack(d, 1))
    }
    case xASTORE(t) => inForall { d =>
      // not implemented
      popStack(d, 3)
    }
    case xALOAD(t) => inForall { d =>
      // not implemented
      pushStack(popStack(d, 2))
    }
    case SIPUSH(i) => inForall { d =>
      val d1 = pushStack(d)
      val s0eqi = app(BinIntProp("="), app(SVar(0)), app(IVal(i.apply)))
      and(d1, s0eqi)
    }
    case INVOKESPECIAL(c) if c.apply._2 == "<init>" => inForall { d =>
      popStack(d, 1)
    }
    case INVOKEVIRTUAL(c) if c.apply == ("java/util/Random", "nextInt", "()I") => inForall { d =>
      popStack(d, 1)
    }
    case INVOKESTATIC(c) if c.apply == ("P6der", "check", "(Z)V") => inForall { d =>
      import ctxT._
      if LogLat.leq(d, app(BinIntProp("="), SVar(0), IVal(1))) then
        Log.log("Check true")
        if inVerification&&(!summary) then Log.logAssert("Check true", from)
      else if LogLat.leq(d, app(BinIntProp("="), app(SVar(0)), app(IVal(0)))) then
        Log.log("Check false")
        if inVerification&&(!summary) then Log.logAssert("Check false", from)
      else
        Log.log("Check inconclusive")
        if inVerification&&(!summary) then Log.logAssert("Check inconclusive", from)
      popStack(d, 1)
    }
    case INVOKEDYNAMIC(a,b) if b._2=="makeConcatWithConstants" => st =>
      val typ = Parser.mpar(a._3)
      st.pop(typ.args.length).push
    case IINC(a) => inForall { d =>
      import ctxT._
      val r = repl(Map[Val, l](local(to)(a._1) -> app(TempVar)))(d)
      Log.log(s"replace state: ${clearString(r)}")
      val q = and(app(BinIntProp("="), app(local(to)(a._1)), app(BinIntOp("+"), app(TempVar), app(IVal(a._2)))), r)
      simplify(eqForget(TempVar)(q))
    }
    case xADD(t) => inForall { d =>
      val w = and(app(BinIntProp("="), app(SVar(2)), app(BinIntOp("+"), app(SVar(0)), app(SVar(1)))), d)
      simplify(popStack(eqForget(SVar(0))(eqForget(SVar(1))(w)), 2))
    }
    case xSUB(t) => inForall { d =>
      val w = and(app(BinIntProp("="), app(SVar(2)), app(BinIntOp("-"), app(SVar(0)), app(SVar(1)))), d)
      simplify(popStack(eqForget(SVar(0))(eqForget(SVar(1))(w)), 2))
    }
    case xMUL(t) => inForall { d =>
      val w = and(app(BinIntProp("="), app(SVar(2)), app(BinIntOp("*"), app(SVar(0)), app(SVar(1)))), d)
      simplify(popStack(eqForget(SVar(0))(eqForget(SVar(1))(w)), 2))
    }
    case xDIV(t) => inForall { d =>
      val w = and(app(BinIntProp("="), app(SVar(2)), app(BinIntOp("*"), app(SVar(0)), app(SVar(1)))), d)
      simplify(popStack(eqForget(SVar(0))(eqForget(SVar(1))(w)), 2))
    }
    case xNEG(t) => inForall { d =>
      val r = repl(Map[Val, l](SVar(0) -> app(BinIntOp("-"), SVar(0))))(d)
      r
    }
    case xLOAD(t, a) => inForall { d =>
      import ctxT._
      val d1 = push(d)
      Log.log(s"push state: ${clearString(d1)}")
      val s0eqi = app(BinIntProp("="), app(SVar(0)), app(local(to)(a.toInt)))
      and(d1, s0eqi)
    }
    case POP => inForall { d =>
      simplify(popStack(d, 1))
    }
    case i: ICONST => inForall {
      d =>
        val d1 = push(d)
        Log.log(s"push state: ${clearString(d1)}")
        val s0eqi = app(BinIntProp("="), app(SVar(0)), app(IVal(i.n)))
        and(d1, s0eqi)
    }
    case i: BIPUSH => inForall {
      d =>
        val d1 = push(d)
        Log.log(s"push state: ${clearString(d1)}")
        val s0eqi = app(BinIntProp("="), app(SVar(0)), app(IVal(i.a)))
        val r = and(d1, s0eqi)
        r
    }
    case xSTORE(t, n) => inForall {
      d =>
        import ctxT._
        val d1 = eqForget(local(to)(n.toInt))(d)
        Log.log(s"forget state: ${clearString(d1)}")
        simplify(popStack(d1, 1, Seq(app(local(to)(n.toInt)))))
    }
    case NEW(s) => inForall {
      d =>
        val d1 = push(d)
        Log.log(s"push state: ${clearString(d1)}")
        val s0eqi = app(Fun("type", FunType(ClassType, FunType(StringType, PropType))), SVar(0), SVal(s))
        and(d1, s0eqi)
    }
    case DUP => inForall {
      d =>
        val d1 = push(d)
        Log.log(s"push state: ${clearString(d1)}")
        val s0eqi = app(BinIntProp("="), app(SVar(0)), app(SVar(1)))
        and(d1, s0eqi)
    }
    case PUTFIELD(a) => inForall {
      d =>
        val d1 = and(app(Fun("field", FunType(ClassType, FunType(StringType, FunType(AnyType, PropType)))), app(SVar(0)), SVal(a._2), SVar(1)), d)
        Log.log(s"before simplify: ${clearString(d1)}")
        simplify(popStack(d1, 2))
    }
    case PUTSTATIC(a) => inForall {
      d =>
        val fld = FieldVar(a._1,a._2,IntType)
        d.eqForget(fld).pop(1,Seq(app(fld))).simpl
    }
    case GETSTATIC(a) => inForall {
      d =>
        val fld = FieldVar(a._1,a._2,IntType)
        val eq = BinIntProp("=")
        d.push && eq(SVar(0),fld)
    }
    case xRETURN(t: SimpleType) => inForall {
      d =>
        //        Log.logCommon("before return:\n"++clearString(d))
        simplify(clearLocals(clearStack(d)))
    }
    case RETURN => inForall {
      d => simplify(clearLocals(popStack(clearStack(d), 1)))
    }
    case a => st =>
      Log.logCommon(s"Ignoring unknown instruction: $a")
      st

  def sThrowsS(get: node => l, set: node => l => Unit, to: node, to_final: Boolean,instr: Instr, ex: Either[String, Set[String]]): l => l = instr match
    case ATHROW => inForall {
      d => clearStack(d)
    }
    case _ => st => st

  def sAnnotS(get: node => l, set: node => l => Unit, to: node, to_final: Boolean, st: l,ant: Annot): l =
    st
//    ant match
//    case LocalVar(i,n,t) => st


  def compose(args:Seq[(Int,LVar)], f:l): l => l = inForall{
    x =>
      Log.log(s"compose:\n\td_prev: ${clearString(x)}\n\td_fun: ${clearString(f)}\n")
      val retVar =  RetVar
      val f1 = repl(Map[Val,l](SVar(0) -> and(retVar)))(f)
      def r(x: l): l =
        args.foldRight(x) {
          case ((i,v),b) => repl(Map[Val,l](v -> and(SVar(args.length-1-i))))(b)
        }

      val q1 = r(f1)
//      Log.log(s"combine rename: ${clearString(q1)}")
      val q2 = forgetLocals(q1)
//      Log.log(s"combine forget-locals: ${clearString(q1)}")
      val q = popStack(and(x, q2), args.length)
//      Log.log(s"combine and-pop: ${clearString(q)}")
      val w = simplify(repl(Map[Val,l](retVar -> and(SVar(0))))(pushStack(q)))
      Log.log(s"combine result: ${clearString(w)}")
      w
  }

trait Modular extends ModularLogic:

  override type Dl = D.t
  override given L: (Lattice[D.t] & WidenOpF[D.t] & NarrowOpF[D.t]) = D

  override inline def node(nd: node, b: VarCtx, k: VarKey)(using ctx: SolvCtxT): Dl =
    nd match
      case n: LNode if n.is_start && n.methodInfo.mthd=="main" && project.mainClass==n.methodInfo.cls =>
        (ent(nd)(n.methodInfo.mthdTyp.args, n.methodInfo.is_static), LogLat.top)
      case _ => L.bot

  override inline def start(locals: Int, stack: Int, access: Int)(using ctx: JBCtxT): Dl = ctx.st

  override inline def branch(instr: JumpInstr, thn: Boolean)(using ctx: JBCtxT): Dl =
    (sBranchS(ctx.st._1, instr, thn), sBranchS(ctx.st._2, instr, thn))

  override inline def normal(instr: Instr)(using ctx: JBCtxT): Dl =
    val (ds: LogLat.t, dr: LogLat.t) = ctx.st
    Log.log(s"instruction: $instr")
    Log.log(s"start summary state: ${clearString(ds)}")
    Log.log(s"start state: ${clearString(dr)}")

    def getS(v: node): l = ctx.get(v,ctx.k,ctx.k)._1
    def setS(v: node)(d: l): Unit = ctx.set(v,ctx.k,ctx.k,(d, LogLat.bot))
    def getR(v: node): l = ctx.get(v,ctx.k,ctx.k)._2
    def setR(v: node)(d: l): Unit = ctx.set(v,ctx.k,ctx.k,(LogLat.bot, d))

    val rs = trans(true,instr,ctx)(ds)
    val rr = trans(false,instr,ctx)(dr)
    Log.log(s"end summary state: ${clearString(rs)}")
    Log.log(s"end state: ${clearString(rr)}")
    (rs, rr)


  override inline def throws(instr: Instr, ex: Either[String, Set[String]])(using ctx: JBCtxT): Dl =
    def getS(v: node): l = ctx.get(v,ctx.k,ctx.k)._1
    def setS(v: node)(d: l): Unit = ctx.set(v,ctx.k,ctx.k,(d, LogLat.bot))
    def getR(v: node): l = ctx.get(v,ctx.k,ctx.k)._2
    def setR(v: node)(d: l): Unit = ctx.set(v,ctx.k,ctx.k,(LogLat.bot, d))

    ( sThrowsS(getS,setS,ctx.to,ctx.to_final,instr,ex)(ctx.st._1),
      sThrowsS(getR,setR,ctx.to,ctx.to_final,instr,ex)(ctx.st._2))

  override inline def annot(ant: Annot)(using ctx: JBCtxT): Dl =
    def getS(v: node): l = ctx.get(v,ctx.k,ctx.k)._1
    def setS(v: node)(d: l): Unit = ctx.set(v,ctx.k,ctx.k,(d, LogLat.bot))
    def getR(v: node): l = ctx.get(v,ctx.k,ctx.k)._2
    def setR(v: node)(d: l): Unit = ctx.set(v,ctx.k,ctx.k,(LogLat.bot, d))

    ( sAnnotS(getS,setS,ctx.to,ctx.to_final,ctx.st._1,ant),
      sAnnotS(getR,setR,ctx.to,ctx.to_final,ctx.st._2,ant))

  override inline def enter(instr: MethodInstr, typ: MethodType, static: Boolean)(using ctx: JBCtxT): Seq[(VarCtx, VarKey, VarKey=>Dl, Dl)] =
    val (cls, mthd, atyp) = instr.a.apply
    val startn = project.startVarsFun(cls, mthd, Parser.mpar(atyp))
    val as = args(startn,typ.args,static)
    Log.log(s"senter reachable start: ${clearString(ctx.st._2)}")
    val r = as.foldRight(forgetLocals(ctx.st._2)) {
      case ((i, x), b) =>
        repl(Map[Val, l](SVar(i) -> and(local(startn)(i))))(b)
    }
    val q = popStack(clearStack(r), 1)
    Log.log(s"senter reachable result: ${clearString(q)}")
    enterWith((ent(startn)(typ.args, static),q))

  override inline def combine(instr: MethodInstr, typ: MethodType, static: Boolean, result: VarKey => (LogLat.t, LogLat.t))(using ctx: JBCtxT): Dl =
    val caller = ctx.st
    val st = result(())
    if typ.ret == VoidT then
      val n = typ.args.length
      ((caller._1.pop(n) && st._1.pop()).simpl, (caller._2.pop(n) && st._2.pop()).simpl)
    else
      val (cls, mthd, atyp) = instr.a.apply
      val startn = project.startVarsFun(cls, mthd, Parser.mpar(atyp))
      ( compose(args(startn,typ.args,static),st._1)(caller._1).simpl,
        compose(args(startn,typ.args,static),st._1)(caller._2).simpl )

  override def metaNode(node: node, c: VarCtx, key: VarKey)(using ctx: SolvCtxT): Option[(LogLat.t, LogLat.t)] = None


trait ModularSummary extends ModularLogic:
  override type Dl = LogLat.t
  override given L: (Lattice[Dl] & WidenOpF[Dl] & NarrowOpF[Dl]) = LogLat

  override inline def node(nd: node, b: VarCtx, k: VarKey)(using ctx: SolvCtxT): Dl =
    nd match
      case n: LNode if n.is_start && n.methodInfo.mthd=="main" && (project.mainClass==n.methodInfo.cls) =>
        ent(n)(n.methodInfo.mthdTyp.args, n.methodInfo.is_static)
      case _ => L.bot

  override inline def branch(instr: JumpInstr, thn: Boolean)(using ctx: JBCtxT): LogLat.t = sBranchS(ctx.st,instr,thn)
  override inline def normal(instr: Instr)(using ctx: JBCtxT): LogLat.t = trans(true,instr,ctx)(ctx.st)
  override inline def throws(instr: Instr, ex: Either[String, Set[String]])(using ctx: JBCtxT): LogLat.t = sThrowsS(ctx.get(_,ctx.c,ctx.k), (n:Node)=> ctx.set(n,ctx.k,ctx.k,_),ctx.to,ctx.to_final,instr,ex)(ctx.st)
  override inline def annot(ant: Annot)(using ctx: JBCtxT): _root_.põder.dom.LogLat.t = sAnnotS(ctx.get(_,ctx.c,ctx.k), (n:Node)=> ctx.set(n,ctx.c,ctx.k,_),ctx.to,ctx.to_final,ctx.st,ant)
  override inline def start(locals: Int, stack: Int, access: Int)(using ctx: JBCtxT): _root_.põder.dom.LogLat.t = ctx.st

  override inline def enter(instr: MethodInstr, typ: MethodType, static: Boolean)(using ctx: JBCtxT): Seq[(VarCtx, VarKey, VarKey=>Dl, Dl)] =
    enterWith(ent(ctx.to)(typ.args, static))

  override inline def combine(instr: MethodInstr, typ: MethodType, static: Boolean, result: VarKey => _root_.põder.dom.LogLat.t)(using ctx: JBCtxT): Dl =
    val caller = ctx.st
    val st = result(())
    if typ.ret == VoidT then
      def r(x: l): Int =
        x match
          case ForallL(t, y) => 1 + r(y)
          case y => 0

      popStack(caller, r(st))
    else
      val (cls, mthd, atyp) = instr.a.apply
      val startn = project.startVarsFun(cls, mthd, Parser.mpar(atyp))
      compose(args(startn,typ.args,static),st)(caller).simpl
