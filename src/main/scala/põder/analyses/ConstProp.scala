package põder.analyses

import põder.analyses.ConstProp.l
import põder.framework.{EqualOpF, SimpleJBCAnalysis, JoinOpF, JsonOpF, Lattice, NarrowOpF, TopOpF, WidenOpF}
import põder.framework.cfg._
import põder.util.json.JsonF
import põder.util.typ.{MethodType}

/*
  This is an exercise code to help understand Põder.
  Understaning of static program analysis and constant propagation analysis is assumed.
  Do not check in the solution!

  See Value.scala for a template of a simple analysis.
*/

/* Test program (save as CPA.java in directory test) below, run using arguments "--analysis cpa --gui test CPA"
class P6der {
  public static void check(boolean b) {}
}

class CPA {
  static int viis(){
    return 5;
  }

  public static void main(String[] args) {
    P6der.check(false); // test
    int x = 1;
    int y = 2;
    int z = x + y;
    P6der.check(z==3);
    int q = viis();
    P6der.check(q==5);
    if (x < z)
      z = 100;
    else
      z = 200;
    P6der.check(z==100);
  }
}
*/

/*
   * Implement top, toJson, leq, and join.
     ** CPValue is meant to be either an integer constant or unknown value. You may add constants of other types.
   * In the end you need to add code to all places marked with ???.
   * Run/debug the program and implement instructions as needed.
     ** At first write default cases that keeps the (abstract) state the same.
     ** Implement your own helper functions as needed.
     ** Use breakpoints or printf-s to see what is happening.
 */


object ConstProp:
  sealed abstract class CPValue
  case object Unknown extends CPValue
  case class Cons(i:Int) extends CPValue

  type vl = CPValue
  val VL: TopOpF[vl] & JoinOpF[vl] & EqualOpF[vl] & JsonOpF[vl] & WidenOpF[vl] & NarrowOpF[vl] =
    new TopOpF[vl] with JoinOpF[vl] with EqualOpF[vl] with JsonOpF[vl] with WidenOpF[vl] with NarrowOpF[vl] {
      override def top: vl = ???

      override def toJson[jv](x: vl)(using j: JsonF[jv]): jv = ???

      override def leq(x: vl, y: vl): Boolean = ???
      override def join(x: vl, y: vl): vl = ???
      override def widen(oldv: vl, newv: vl): vl = join(oldv,newv)

      override def equal(x: vl, y: vl): Boolean = x==y
  }

  // this code adds Node as dead code and Some(st) as stacks st of CPValue values
  type l = Option[(List[vl],List[vl])]
  val L: Lattice[l] & WidenOpF[l] & NarrowOpF[l] = new Lattice[l] with WidenOpF[l] with NarrowOpF[l] {
    override def bot: l = None
    override def top: l = Some((List(),List()))

    override def toJson[jv](x: l)(using j: JsonF[jv]): jv = x match
      case Some((st,ar)) => j.Obj("stack" -> j.Arr(st.map(VL.toJson(_))), "locals" -> j.Arr(ar.map(VL.toJson(_))))
      case None => j.Str("unreachable")

    override def leq(x: l, y: l): Boolean =
      if x.isEmpty then
        true
      else if y.isEmpty then
        false
      else
        val (xs, xa) = x.get
        val (ys, ya) = y.get
        xs.lazyZip(ys).forall(VL.leq) && xa.lazyZip(ya).forall(VL.leq)

    override def join(x: l, y: l): l =
      if x.isEmpty then
        y
      else if y.isEmpty then
        x
      else
        val (xs, xa) = x.get
        val (ys, ya) = y.get
        Some(xs.lazyZip(ys).map(VL.join), xa.lazyZip(ya).map(VL.join))

    override def widen(oldv: l, newv: l): l = newv

    @scala.annotation.tailrec
    def eqList(xs:List[vl], ys:List[vl]): Boolean = (xs,ys) match
      case (Nil, Nil)     => true
      case (Nil, x::xs)   => x==VL.top && eqList(Nil,xs)
      case (x::xs, Nil)   => x==VL.top && eqList(Nil,xs)
      case (x::xs, y::ys) => x==y && eqList(xs,ys)

    override def equal(x: l, y: l): Boolean = (x,y) match
      case (None, None) => true
      case (None, _) => false
      case (_, None) => false
      case (Some((xs,xa)),Some((ys,ya))) => eqList(xs,ys) && eqList(xa,ya)
  }

trait ConstProp extends SimpleJBCAnalysis:
  override type Dl = ConstProp.l
  override given L: (Lattice[Dl] & WidenOpF[Dl] & NarrowOpF[Dl]) = ConstProp.L

  override inline def node(nd: Node, b: VarCtx, k: VarKey)(using ctx: SolvCtxT): l =
    // starting state of main is L.top, otherwise L.bot
    nd match {
      case n: LNode if n.is_start && n.methodInfo.mthd == "main" => L.top
      case _ => L.bot
    }

  override inline def isBuiltinFun(instr: MethodInstr, typ: MethodType, static: Boolean): Boolean =
    val (pkg,mthd,_) = instr.a.apply
    // P6der.check method calls are handeled as atomic instructions
    pkg=="P6der"&&mthd=="check"

  override inline def start(locals: Int, stack: Int, access: Int)(using ctx: JBCtxT): l =
    // currently, start edges do not change the state
    ctx.st

  override inline def enter(instr: MethodInstr, typ: MethodType, static: Boolean)(using ctx: JBCtxT): Seq[(VarCtx, VarKey, VarKey => l, l)] =
    if ctx.st.isEmpty then
      Seq() // for dead code we do not call any method
    else
      // else, compute the starting state of the called method
      val stateAtStartOfCalledMethod: l = ???
      enterWith(stateAtStartOfCalledMethod)

  override inline def combine(instr: MethodInstr, typ: MethodType, static: Boolean, result: VarKey => l)(using ctx: JBCtxT): l =
    val stateBeforeCall: l = ctx.st
    val stateAtMethodReturn: l = result(())
    if ctx.st.isEmpty || stateAtMethodReturn.isEmpty then
      L.bot
    else
      // based on available data, return the state after the call
      ???

  override inline def branch(instr: JumpInstr, thn: Boolean)(using ctx: JBCtxT): l =
    // if state before call is None (same as L.bot), return L.bot
    if ctx.st.isEmpty then
      L.bot
    else
      // .. otherwise we have list of values for stack and locals
      val (sts, sta) = ctx.st.get

      // we need to compute the effect of each instruction (from the example code)
      instr match
        // ???
        case _ => L.top

  override inline def normal(instr: Instr)(using ctx: JBCtxT): l =
    // if state before call is None (same as L.bot), return L.bot
    if ctx.st.isEmpty then
      L.bot
    else
      // .. otherwise we have list of values for stack and locals
      val (sts, sta) = ctx.st.get

      // we need to compute the effect of each instruction (from the example code)
      instr match
        // ???
        case d => Some((sts,sta))

  // exceptions and annotations currently not handled
  override inline def throws(instr: Instr, ex: Either[String, Set[String]])(using ctx: JBCtxT): l = ctx.st
  override inline def annot(ant: Annot)(using ctx: JBCtxT): l = ctx.st
