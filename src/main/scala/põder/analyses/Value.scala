package põder.analyses

import põder.dom.intervals.Interval
import põder.dom._
import põder.framework._
import põder.framework.cfg._
import põder.util.typ.MethodType

// This file is a template for a simple analysis.

// See IntervalValue.scala for an example of interval analysis.

// See ConstProp.scala for a tutorial on how to implement a constant propagation.

// See Registry.scala for how analysis specifications (the Value trait here)
// is found by the framework.

trait Value  extends SimpleJBCAnalysis:
  // domain: the state to be stored for each CFG node (with some context)
  override type Dl = IntervalDomain  // hint: instead a single value, modify to use stacks of values
  override given L: (Lattice[Dl] & WidenOpF[Dl] & NarrowOpF[Dl]) = IntLat

  // set the starting value for any cfg node -- L.bot if in doubt
  override inline def node(nd: Node, b: VarCtx, k: VarKey)(using ctx: SolvCtxT): Dl = ???


  // start --- local variables, stack size and visibility infrormation
  override inline def start(locals: Int, stack: Int, access: Int)(using ctx: JBCtxT): Dl = ???


  // transfer functions for: branching instructions
  override inline def branch(instr: JumpInstr, thn: Boolean)(using ctx: JBCtxT): Dl = ???

  // transfer functions for: "normal" instructions,
  override inline def normal(instr: Instr)(using ctx: JBCtxT): Dl = ???

  // transfer functions for: the case that an exception is thrown
  override inline def throws(instr: Instr, ex: Either[String, Set[String]])(using ctx: JBCtxT): Dl = ???

  // transfer functions for: annotations (only used after transformations, not present in normal code)
  override inline def annot(ant: Annot)(using ctx: JBCtxT): Dl = ???


  // transfer functions for: method entry
  override inline def enter(instr: MethodInstr, typ: MethodType, static: Boolean)(using ctx: JBCtxT): Seq[(VarCtx, VarKey, VarKey => Dl,Dl)] = ???
  // transfer functions for: method return + combining with pre-entry state
  override inline def combine(instr: MethodInstr, typ: MethodType, static: Boolean, result: VarKey => Dl)(using ctx: JBCtxT): Dl = ???
