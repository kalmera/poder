package põder.framework

import põder.util.json.JsonF

trait TypedF[T]:
  type t = T

trait TopOpF[T] extends TypedF[T]:
  def top: T
trait BotOpF[T] extends TypedF[T]:
  def bot: T
trait JoinOpF[T] extends TypedF[T]:
  def join(x:T, y:T): T
trait MeetOpF[T] extends TypedF[T]:
  def meet(x:T, y:T): T
trait LeqOpF[T] extends TypedF[T]:
  def leq(x:T, y:T): Boolean
trait EqualOpF[T] extends LeqOpF[T]:
  def equal(x:T, y:T): Boolean = leq(x,y) && leq(y, x)
trait JsonOpF[T] extends TypedF[T]:
  def toJson[jv](x: T)(using j: JsonF[jv]): jv
trait WidenOpF[T] extends TypedF[T]:
  def widen(oldv: T, newv:T): T
trait NarrowOpF[T] extends TypedF[T]:
  def narrow(oldv: T, newv:T): T = oldv


trait Lattice[T]
  extends TopOpF[T]
     with BotOpF[T]
     with JoinOpF[T]
     with EqualOpF[T]
     with JsonOpF[T]
:
  def Join(xs: Seq[T]): T =
    if xs.isEmpty then
      bot
    else
      xs.tail.foldLeft(xs.head)(join)

case object BotException extends Exception

trait ExBotLattice[T] extends Lattice[T]:
  final override def bot = throw BotException

abstract class ReducedBotLattice[T] extends Lattice[T]:
  final def lift(x: => T): T =
    try
      x
    catch
      case BotException => bot

object MaxInt extends Lattice[Int]:
  def top = Int.MaxValue
  def bot = Int.MinValue

  def join(x:Int, y: Int): Int = math.max(x, y)

  override def leq(x: Int, y: Int): Boolean = x <= y

  override def toJson[jv](x: Int)(using j: JsonF[jv]): jv = j.Num(BigDecimal(x))

class Const[T](x: T) extends Lattice[T]:
  override def top: T = x
  override def bot: T = x
  override def join(x: T, y: T): T = x

  override def leq(x: T, y: T): Boolean = true

  override def toJson[jv](x: T)(using j: JsonF[jv]): jv =
    j.fromVal(x)

case object UnitLat extends Const[Unit](()) with Lattice[Unit]

class Product[T,R](To: Lattice[T] & WidenOpF[T] & NarrowOpF[T], Ro: Lattice[R] & WidenOpF[R] & NarrowOpF[R])
  extends Lattice[(T,R)] with WidenOpF[(T,R)] with NarrowOpF[(T,R)]:
  override def top: (T,R) = (To.top, Ro.top)
  override def bot: (T,R) = (To.bot, Ro.bot)
  override def join(x: (T,R), y: (T,R)): (T,R) = (x,y) match
    case ((x1,x2),(y1,y2)) => (To.join(x1,y1), Ro.join(x2,y2))

  override def widen(x: (T,R), y: (T,R)): (T,R) = (x,y) match
    case ((x1,x2),(y1,y2)) => (To.widen(x1,y1), Ro.widen(x2,y2))

  override def narrow(x: (T,R), y: (T,R)): (T,R) = (x,y) match
    case ((x1,x2),(y1,y2)) => (To.narrow(x1,y1), Ro.narrow(x2,y2))

  override def leq(x: (T, R), y: (T, R)): Boolean =
   To.leq(x._1, y._1) && Ro.leq(x._2, y._2)

  override def equal(x: (T, R), y: (T, R)): Boolean =
    To.equal(x._1, y._1) && Ro.equal(x._2, y._2)

  override def toJson[jv](x: (T, R))(using j: JsonF[jv]): jv =
    import j._
    Obj("left" -> To.toJson(x._1), "right" -> Ro.toJson(x._2))

class ProductWN[T,R](To: Lattice[T] & NarrowOpF[T] & WidenOpF[T],
                   Ro: Lattice[R] & NarrowOpF[R] & WidenOpF[R])
  extends Product[T,R](To, Ro) with NarrowOpF[(T,R)] with WidenOpF[(T,R)]:
  override def widen(oldv: (T, R), newv: (T, R)): (T, R) =
    (To.widen(oldv._1, newv._1), Ro.widen(oldv._2, newv._2))

  override def narrow(oldv: (T, R), newv: (T, R)): (T, R) =
    (To.narrow(oldv._1, newv._1), Ro.narrow(oldv._2, newv._2))

class ProductExBot[T,R](To: ExBotLattice[T], Ro: ExBotLattice[R]) extends ExBotLattice[(T,R)]:
  override def top: (T,R) = (To.top, Ro.top)
  override def join(x: (T,R), y: (T,R)): (T,R) = (x,y) match
    case ((x1,x2),(y1,y2)) => (To.join(x1,y1), Ro.join(x2,y2))

  override def leq(x: (T, R), y: (T, R)): Boolean =
    To.leq(x._1, y._1) && Ro.leq(x._2, y._2)

  override def toJson[jv](x: (T, R))(using j: JsonF[jv]): jv =
    import j._
    Obj("left" -> To.toJson(x._1), "right" -> Ro.toJson(x._2))

class ProductExtraExBot[T,R](To: ExBotLattice[T]) extends ExBotLattice[(T,Option[R])]:
  override def top: (T,Option[R]) = (To.top, None)
  override def join(x: (T,Option[R]), y: (T,Option[R])): (T,Option[R]) = (x,y) match
    case ((x1,x2),(y1,y2)) =>
      val ei = if x2.isEmpty then y2 else x2
      (To.join(x1,y1), ei)

  override def leq(x: (T, Option[R]), y: (T, Option[R])): Boolean =
    To.leq(x._1, y._1)

  override def toJson[jv](x: (T, Option[R]))(using j: JsonF[jv]): jv =
    import j._
    x match
      case (a, Some(b)) =>
        Obj("data" -> To.toJson(a), "info" -> j.fromVal(b))
      case (a,_) =>
        To.toJson(a)


class ListLat[T](Ops: Lattice[T]) extends Lattice[List[T]] with NarrowOpF[List[T]] with WidenOpF[List[T]]:
  override def top: List[T] = throw Exception()
  override def bot: List[T] = List()

  override def join(x: List[T], y: List[T]): List[T] =
    (x,y) match {
      case (x::xs,y::ys) => Ops.join(x,y) :: join(xs,ys)
      case (xs, List()) => xs
      case (List(), ys) => ys
    }

  override def widen(x: List[T], y: List[T]): List[T] =
    (x,y) match {
      case (x::xs,y::ys) => Ops.join(x,y) :: widen(xs,ys)
      case (xs, List()) => xs
      case (List(), ys) => ys
    }

  override def leq(x: List[T], y: List[T]): Boolean =
    (x,y) match
      case (x::xs, y::ys) => Ops.leq(x,y) && leq(xs,ys)
      case (List(),_) => true
      case (xs, List()) => false

  override def toJson[jv](x: List[T])(using j: JsonF[jv]): jv =
    if x.isEmpty then j.Str("bot") else
      j.Obj(x.zipWithIndex.map((x,i) => (i.toString, Ops.toJson(x))))
//      j.Arr(x.map(Ops.toJson))

class ExBotStacks[T](Ops: ExBotLattice[T]) extends ExBotLattice[Option[List[T]]]:
  override def top: Option[List[T]] = None

  override def join(x: Option[List[T]], y: Option[List[T]]): Option[List[T]] =
    for xv <- x; yv <- y yield xv.lazyZip(yv).map(Ops.join)

  override def leq(x: Option[List[T]], y: Option[List[T]]): Boolean =
    y.isEmpty || (x.isDefined && (x.get.length==y.get.length) && x.get.lazyZip(y.get).forall(Ops.leq))

  override def toJson[jv](x: Option[List[T]])(using j: JsonF[jv]): jv =
    import j._
    x match
      case Some(y) =>
        Arr(y.map(Ops.toJson(_)))
      case None =>
        Str("⏉")

sealed trait TopBotLifted[+T]:
  def map[U](f: T=>U): TopBotLifted[U]
sealed trait TopLifted[+T] extends TopBotLifted[T]:
  override def map[U](f: T=>U): TopLifted[U]
sealed trait BotLifted[+T] extends TopBotLifted[T]:
  override def map[U](f: T=>U):BotLifted[U]

case object Top extends TopLifted[Nothing]:
  override def map[U](f: Nothing=>U):this.type = this
type Top = Top.type

case object Bot extends BotLifted[Nothing]:
  override def map[U](f: Nothing=>U):this.type = this
type Bot = Bot.type

sealed case class Lifted[+T](val get:T) extends TopLifted[T] with BotLifted[T]:
  def isEmpty: Boolean = false
  override def map[U](f: T=>U):Lifted[U] = Lifted(f(get))

abstract class TopSet[T] extends Lattice[Top|Lifted[Set[T]]] with WidenOpF[Top|Lifted[Set[T]]] with NarrowOpF[Top|Lifted[Set[T]]]:
  override def top: Top = Top
  override def bot: Lifted[Set[T]] = Lifted(Set.empty)

  def TtoJson[jv](x: T)(using j: JsonF[jv]): jv

  override def toJson[jv](x: Top|Lifted[Set[T]])(using j: JsonF[jv]): jv =
    x match
      case Top => j.Str("top")
      case Lifted(value) => j.Arr(value.map(x => TtoJson(x)))

  override def leq(x: Top|Lifted[Set[T]], y: Top|Lifted[Set[T]]): Boolean =
    (x,y) match
      case (Lifted(xs), Lifted(ys)) => xs subsetOf ys
      case (Top, Top)     => true
      case (Top, Lifted(_)) => false
      case (Lifted(_), Top) => true

  override def join(x: Top|Lifted[Set[T]], y: Top|Lifted[Set[T]]): Top|Lifted[Set[T]] =
    (x,y) match
      case (Lifted(xs), Lifted(ys)) => Lifted(xs union ys)
      case _ => Top

  override def widen(oldv: Top|Lifted[Set[T]], newv: Top|Lifted[Set[T]]): Top|Lifted[Set[T]] =
    (oldv, newv) match
      case (Lifted(x), Lifted(y)) if x==y => Lifted(x)
      case _ => Top

object StringTopSetLat extends TopSet[String]:
  def TtoJson[jv](x: String)(using j: JsonF[jv]): jv = j.Str(x)

class LiftBoth[T](l:Lattice[T] & WidenOpF[T] & NarrowOpF[T])
  extends Lattice[Top|Lifted[T]|Bot] with WidenOpF[Top|Lifted[T]|Bot] with NarrowOpF[Top|Lifted[T]|Bot]:
  override def top: Top = Top
  override def bot: Bot = Bot

  override def widen(oldv: Top|Lifted[T]|Bot, newv: Top|Lifted[T]|Bot): Top|Lifted[T]|Bot =
    (oldv, newv) match
      case (Bot,_) => newv
      case (Lifted(x), Lifted(y)) => Lifted(l.widen(x,y))
      case _ => Top

  override def join(oldv: Top|Lifted[T]|Bot, newv: Top|Lifted[T]|Bot): Top|Lifted[T]|Bot =
    (oldv, newv) match
      case (Bot,_) => newv
      case (Lifted(x), Lifted(y)) => Lifted(l.join(x,y))
      case _ => Top

  override def leq(x: Top|Lifted[T]|Bot, y: Top|Lifted[T]|Bot): Boolean =
    (x,y) match
      case (Lifted(x),Lifted(y)) => l.leq(x,y)
      case (Bot, Bot) | (Top, Top) => true
      case (_, Bot) | (Top, _) => false
      case _ => true

  override def toJson[jv](x: Top|Lifted[T]|Bot)(using j: JsonF[jv]): jv =
    x match
      case Lifted(value) => l.toJson(value)
      case Bot => j.Str("bot")
      case Top => j.Str("top")


class BotLift[T](l:Lattice[T] & WidenOpF[T] & NarrowOpF[T])
  extends Lattice [Lifted[T]|Bot] with WidenOpF[Lifted[T]|Bot] with NarrowOpF[Lifted[T]|Bot]:
  override def top: Lifted[T] = Lifted(l.top)
  override def bot: Bot = Bot

  override def toJson[jv](x: Lifted[T]|Bot)(using j: JsonF[jv]): jv =
    x match
      case Lifted(value) => l.toJson(value)
      case Bot =>j.Str("bot")

  override def leq(x: Lifted[T]|Bot, y: Lifted[T]|Bot): Boolean =
    (x,y) match
      case (Lifted(_), Bot) => false
      case (Bot,_) => true
      case (Lifted(x),Lifted(y)) => l.leq(x,y)

  override def join(x: Lifted[T]|Bot, y: Lifted[T]|Bot): Lifted[T]|Bot =
    (x,y) match
      case (Bot, Bot) => Bot
      case (Lifted(x), Bot) => Lifted(x)
      case (Bot,(Lifted(y))) => Lifted(y)
      case (Lifted(x),Lifted(y)) => Lifted(l.join(x,y))

  override def widen(oldv: Lifted[T]|Bot, newv: Lifted[T]|Bot): Lifted[T]|Bot =
    (oldv, newv) match
      case (Bot, Bot) => Bot
      case (Lifted(x),Bot) => Lifted(x)
      case (Bot, Lifted(y)) => Lifted(y)
      case (Lifted(x),Lifted(y)) => Lifted(l.widen(x,y))
