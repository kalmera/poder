package põder.framework


import põder.dom.{BotMap, ExpMap, ExpMapLat, ExpsLat, IntervalSetLat, MapExps, TopMap}
import põder.dom.expressions.*
import põder.dom.intervals.*
import põder.framework.*
import põder.framework.cfg.*
import põder.log.Log
import põder.util.CellConv
import põder.util.Cell
import põder.util.json.JsonF
import põder.util.typ.{MethodType, Parser, VoidT}

import scala.annotation.tailrec
import scala.math.Ordering.OptionOrdering
import scala.collection.immutable.{BitSet, Queue, SortedSet, TreeSet}


class EitherJsonOp[A,B](a:JsonOpF[A], b:JsonOpF[B]) extends JsonOpF[Either[A,B]]:
  override def toJson[jv](x: Either[A,B])(using j: JsonF[jv]): jv =
    x match {
      case Left(x) => a.toJson(x)
      case Right(y) => b.toJson(y)
    }


sealed abstract class TSumDom[+A,+B]
case class Base[A](t:A,e:Exps) extends TSumDom[A,Nothing]
case class Next[B](t:B) extends TSumDom[Nothing,B]


class TSumDomLat[A,B](a: Lattice[A] & WidenOpF[A] & NarrowOpF[A],
                      b: Lattice[B] & WidenOpF[B] & NarrowOpF[B])
  extends Lattice[TSumDom[A,B]] with WidenOpF[TSumDom[A,B]] with NarrowOpF[TSumDom[A,B]]:

  override def bot: TSumDom[A,B] = Base(a.bot, ExpsLat.bot)
  override def top: TSumDom[A,B] = Next(b.top)

  def expToJson [jv](x: Exp)(using j: JsonF[jv]): jv =
    def lin(e:Exp): Queue[jv] =
      e match
        case Op(i,x) => lin(x) :+ j.Str(i.toString)
        case EVar(n,_,SVar(0)) => Queue(j.Str(s"[$n]"))
        case _ => Queue(ExpsLat.toJson(Set(e)))
    j.Arr(lin(x))

  def expsToJson [jv](xs: Exps)(using j: JsonF[jv]): jv =
    if xs.size==1 then
      expToJson(xs.head)
    else
      j.Obj(for (x,i) <- xs.zipWithIndex yield i.toString -> expToJson(x))

  def toJson [jv](x: TSumDom[A,B])(using j: JsonF[jv]): jv =
    x match {
      case Base(v,e) if v==a.bot && e==ExpMapLat.bot => j.Str("bot")
      case Base(v,e) => j.Obj("base" -> a.toJson(v), "exps" -> expsToJson(e))
      case Next(v) => b.toJson(v)
    }
  def join(x: TSumDom[A,B], y: TSumDom[A,B]): TSumDom[A,B] =
    (x,y) match {
      case (Base(v,e),Base(u,r)) => Base(a.join(v,u),ExpsLat.join(e,r))
      case (Next(v), Next(u)) => Next(b.join(v,u))
      case (Base(_,_), y) => y
      case (x, Base(_,_)) => x
    }
  def leq(x: TSumDom[A,B], y: TSumDom[A,B]): Boolean =
    (x,y) match {
      case (Base(v,e),Base(u,r))=> a.leq(v,u) && ExpsLat.leq(e,r)
      case (Next(v), Next(u))   => b.leq(v,u)
      case (Base(_,_), y) => true
      case (x, Base(_,_)) => false
    }

  override def equal(x: TSumDom[A,B], y: TSumDom[A,B]): Boolean =
    (x,y) match {
      case (Base(v,e),Base(u,r)) => a.equal(v,u) && ExpsLat.equal(e,r)
      case (Next(v), Next(u))  => b.equal(v,u)
      case _ => false
    }

  def widen(x: TSumDom[A,B], y: TSumDom[A,B]): TSumDom[A,B] =
    (x,y) match {
      case (Base(v,e),Base(u,r))=> Base(a.widen(v,u),ExpsLat.widen(e,r))
      case (Next(v), Next(u))   => Next(b.widen(v,u))
      case (Base(_,_), y) => y
      case (x, Base(_,_)) => x
    }
  override def narrow(x: TSumDom[A,B], y: TSumDom[A,B]): TSumDom[A,B] =
    (x,y) match {
      case (Base(v,e),Base(u,r))=> Base(a.narrow(v,u),ExpsLat.narrow(e,r))
      case (Next(v), Next(u))   => Next(b.narrow(v,u))
      case (Base(x,e), _) => Base(x,e)
      case (x, Base(y,e)) => Base(y,e)
    }

abstract class Transform[A1 <: JBCAnalysis, A2 <: JBCAnalysis] extends JBCAnalysis:
  val base: A1
  val next: A2

  override type NCtx[N1, C1, K1, D1] = SolvCtx[N1, C1, K1, D1]

  override def forward: Boolean = true

  override type VarKey = Either[base.VarKey,next.VarKey]
  final lazy val VarKey: JsonOpF[VarKey] = EitherJsonOp(base.VarKey, next.VarKey)

  type VarCtx = Either[base.VarCtx, next.VarCtx]
  final lazy val VarCtx: JsonOpF[VarCtx] = EitherJsonOp(base.VarCtx, next.VarCtx)

  final lazy val startCtxKey: Seq[(VarCtx,VarKey)] =
    (for ((c,k) <- base.startCtxKey) yield (Left(c),Left(k))) ++
    (for ((c,k) <- next.startCtxKey) yield (Right(c),Right(k)))

  override type Dl = TSumDom[base.Dl, next.Dl]
  override given L: (Lattice[Dl] & WidenOpF[Dl] & NarrowOpF[Dl]) =
    TSumDomLat(base.L, next.L)

  class baseCtx(using ctx: SolvCtxT) extends SolvCtx[Node,base.VarCtx,base.VarKey,base.Dl]:
    override def get(n: Node, c: base.VarCtx, k: base.VarKey): base.Dl =
      ctx.get(n,Left(c),Left(k)) match
        case Base(c,es) => c
        case Next(_) => base.L.top
    override def set(n: Node, c: base.VarCtx, k: base.VarKey, d: base.Dl): Unit =
      ctx.set(n,Left(c),Left(k),Base(d,ExpsLat.bot))

  object fakeBaseCtx extends SolvCtx[Node,base.VarCtx,base.VarKey,base.Dl]:
    override def get(n: Node, c: base.VarCtx, k: base.VarKey): base.Dl = base.L.bot
    override def set(n: Node, c: base.VarCtx, k: base.VarKey, d: base.Dl): Unit = ()

  class baseNodeCtx(using ctx: JBCtxT) extends NodeCtx[Node,base.VarCtx,base.VarKey,base.Dl]:
    override def get(n: Node, c: base.VarCtx, k: base.VarKey): base.Dl =
      ctx.get(n,Left(c),Left(k)) match
        case Base(c,_) => c
        case Next(_) => base.L.top
    override def set(n: Node, c: base.VarCtx, k: base.VarKey, d: base.Dl): Unit =
      ctx.set(n,Left(c),Left(k),Base(d,ExpsLat.bot))
    override val from: Node = ctx.from
    override val from_start: Boolean = ctx.from_start
    override val to: Node = ctx.to
    override val to_final: Boolean = ctx.to_final
    override val c: base.VarCtx = ctx.c match
      case Left(x) => x
      case Right(_) => base.startCtxKey.head._1
    override val k: base.VarKey = ctx.k match
      case Left(x) => x
      case Right(_) => base.startCtxKey.head._2
    override def st(using k: base.VarKey): base.Dl =
      (ctx.c,ctx.k) match
        case (Left(_), Left(k1)) =>
          ctx.st(using Left(k1)) match
            case Base(c,_) => c
            case Next(_) => base.L.top
        case _ => base.L.bot


  class baseJBCtx(using ctx: JBCtxT) extends JBC(new baseNodeCtx)(ctx.stackSize, ctx.stackDiff)

  def baseNCtx(using ctx: SolvCtxT): base.NCtx[Node,base.VarCtx,base.VarKey,base.Dl]
  def baseECtx(using ctx: JBCtxT): base.ECtx[Node,base.VarCtx,base.VarKey,base.Dl]

  class nextCtx(using ctx: SolvCtxT) extends SolvCtx[Node,next.VarCtx,next.VarKey,next.Dl]:
    override def get(n: Node, c: next.VarCtx, k: next.VarKey): next.Dl =
      ctx.get(n,Right(c),Right(k)) match
        case Base(c,es) if c==base.L.bot => next.L.bot
        case Next(c) => c
        case Base(_,_) => next.L.top
    override def set(n: Node, c: next.VarCtx, k: next.VarKey, d: next.Dl): Unit =
      ctx.set(n,Right(c),Right(k),Next(d))

  object fakeNextCtx extends SolvCtx[Node,next.VarCtx,next.VarKey,next.Dl]:
    override def get(n: Node, c: next.VarCtx, k: next.VarKey): next.Dl = next.L.bot
    override def set(n: Node, c: next.VarCtx, k: next.VarKey, d: next.Dl): Unit = ()

  class nextNodeCtx(using ctx: JBCtxT) extends NodeCtx[Node,next.VarCtx,next.VarKey,next.Dl]:
    override def get(n: Node, c: next.VarCtx, k: next.VarKey): next.Dl =
      ctx.get(n,Right(c),Right(k)) match
        case Base(_,_) => next.L.bot
        case Next(c) => c
    override def set(n: Node, c: next.VarCtx, k: next.VarKey, d: next.Dl): Unit =
      ctx.set(n,Right(c),Right(k),Next(d))
    override val from: Node = ctx.from
    override val from_start: Boolean = ctx.from_start
    override val to: Node = ctx.to
    override val to_final: Boolean = ctx.to_final
    override val c: next.VarCtx = ctx.c match
      case Left(_) => next.startCtxKey.head._1
      case Right(x) => x
    override val k: next.VarKey = ctx.k match
      case Left(_) => next.startCtxKey.head._2
      case Right(x) => x
    override def st(using k: next.VarKey): next.Dl =
      (ctx.c,ctx.k) match
        case (Right(_), Right(k1)) =>
          ctx.st(using Right(k1)) match
            case Base(_,_) => next.L.bot
            case Next(c) => c
        case _ => next.L.bot

  class nextJBCtx(using ctx: JBCtxT) extends JBC(new nextNodeCtx)(ctx.stackSize, ctx.stackDiff)

  def nextNCtx(using ctx: SolvCtxT): next.NCtx[Node,next.VarCtx,next.VarKey,next.Dl]
  def nextECtx(using ctx: JBCtxT): next.ECtx[Node,next.VarCtx,next.VarKey,next.Dl]

  def getState(using ctx: JBCtxT): Dl =
    (ctx.from, ctx.from_start) match
      case (ln:Node, true) =>
        ctx.st(using ctx.k) match
          case Base(t, c) =>
            if (t == base.L.bot) || (c == ExpsLat.bot) then L.bot else
              Base(t, Set(EVar(ln,(),SVar(0))))
          case v => v
      case _ =>
        ctx.st(using ctx.k)


  override inline def node(nd: Node, b: VarCtx, k: VarKey)(using ctx: SolvCtxT): Dl =
    (nd,b,k) match
      case (n:LNode, Left(b),Left(k)) if n.is_start && n.methodInfo.mthd=="main" && project.mainClass==n.methodInfo.cls =>
        Base(base.node(nd,b,k)(using baseNCtx), Set(EVar(n,(),SVar(0))))
      case (n,Left(b),Left(k)) =>
        Base(base.node(n,b,k)(using baseNCtx), Set.empty)
      case (_, Right(b),Right(k)) =>
        Next(next.node(nd,b,k)(using nextNCtx))
      case _ => L.bot

  override inline def start(locals: Int, stack: Int, access: Int)(using ctx: JBCtxT): Dl =
    (ctx.c,ctx.k) match
      case (Left(b),Left(k)) =>
        ctx.st(using ctx.k) match {
          case Base(v,e) if v==base.L.bot && e==ExpsLat.bot => L.bot
          case Base(v,e) => Base(base.start(locals,stack,access)(using baseECtx),Set(Op(NOP)))
          case t => L.top
        }
      case (Right(b),Right(k)) =>
        Next(next.start(locals,stack,access)(using nextECtx))
      case _ => L.bot

  inline def isThreadRun(a:Option[(String,String,String)]): Boolean =
    a match {
      case Some((cls,mth,ty)) =>
        cls=="java.lang.Thread" && mth=="run" // todo: check for subclasses
      case None =>
        false
    }

  override def isBuiltinFun(instr: MethodInstr, typ: MethodType, static: Boolean): Boolean =
    base.isBuiltinFun(instr,typ, static) || next.isBuiltinFun(instr,typ, static)


  override def explain[jv](n: Node, c: VarCtx, k: VarKey, lod: Int)(using ctx: SolvCtxT, j: JsonF[jv]): jv =
    (c, k) match
      case (Left(c), Left(k)) => base.explain(n, c, k, lod)(using baseNCtx, j)
      case (Right(c), Right(k)) => next.explain(n, c, k, lod)(using nextNCtx, j)
      case _ => j.Str("<broken>")

  // hargnemise üleminekufunktsioon
  def prepend(instr: Instr, e:Exp): Exp =
    e match
      case Op(NOP) => Op(instr)
      case _ if instr==NOP => e
      case _ if instr.isInstanceOf[GOTO] => e
      case _ => Op(instr,e)

  def branch_exps(instr: JumpInstr, thn: Boolean, es: Exps)(using ctx: JBCtxT): Exps =
    val i = for e <- es yield prepend(instr,e)
    i

  override inline def branch(instr: JumpInstr, thn: Boolean)(using ctx: JBCtxT): Dl =
    ctx.k match
      case Left(k) =>
        val c =
          getState match
            case Base(b,_) if b==base.L.bot => ExpsLat.bot
            case Base(_,e) => branch_exps(instr, thn, e)
            case Next(_) => ExpsLat.top
        val b = base.branch(instr,thn)(using baseECtx)
        Base(b, c)
      case Right(k) =>
        Next(next.branch(instr, thn)(using nextECtx))



  inline def normal_exps(instrs: Iterable[Instr])(using ctx: JBCtxT): Exps =
    getState match {
      case Base(b, n) if base.L.bot==b&&next.L.bot==n => ExpsLat.bot
      case Base(_, ps) =>
        for p <- ps
            i <- instrs yield prepend(i,p)
      case _ => ExpsLat.top
    }

  def normal_def(instr: Instr)(using ctx: JBCtxT): Dl =
    ctx.k match
      case Left(k) =>
        val t: base.Dl = base.normal(instr)(using baseECtx)
        val c = normal_exps(List(instr))
        Base(t, c)
      case Right(k) =>
        Next(next.normal(instr)(using nextECtx))

  // erindi üleminekufunktsioon
  override inline def throws(instr: Instr, ex: Either[String, Set[String]])(using ctx: JBCtxT): Dl = L.bot

  // annotatsiooni ülemunekufunktsioon
  override def annot(ant: Annot)(using ctx: JBCtxT): Dl =
    (ctx.k, getState) match
      case (Left(k), Base(_, ps)) =>
        val t: base.Dl = base.annot(ant)(using baseECtx)
        Base(t, ps)
      case (Left(k), st) =>
        st
      case (Right(k),_) =>
        Next(next.annot(ant)(using nextECtx))

  // need on meetodiväljakutsete jaoks
  override inline def enter(instr: MethodInstr, typ: MethodType, static: Boolean)(using ctx: JBCtxT): Seq[(VarCtx, VarKey, VarKey => Dl,Dl)] =
    ctx.k match {
      case Left(k) =>
        getState(using ctx) match {
          case Base(b, c) =>
            val t = base.enter(instr, typ, static)(using baseECtx)
            def nst(f:base.VarKey=>base.Dl)(x:VarKey):Dl = x match
              case Left(k) => Base(f(k),c)
              case Right(k) => L.bot
            t.map((c,k,st,d) => (Left(c),Left(k),nst(st), Base(d,Set(Op(NOP)))))
          case _ => Seq.empty
        }
      case Right(k) =>
        def nst(f:next.VarKey=>next.Dl)(x:VarKey):Dl = x match
          case Left(k) => L.bot
          case Right(k) => Next(f(k))
        val t = next.enter(instr, typ, static)(using nextECtx)
        t.map((c,k,st,d) => (Right(c),Right(k),nst(st), Next(d)))
    }


  override inline def combine(instr: MethodInstr, typ: MethodType, static: Boolean, result: VarKey => Dl)(using ctx: JBCtxT): Dl =
    ctx.k match
      case Left(k) =>
        (getState, result(Left(k))) match {
          case (Base(b, c), Base(br, _)) =>
            val i = for e <- c yield prepend(instr,e)
            def nst(x:base.VarKey):base.Dl = result(Left(x)) match
              case Base(k,_) => k
              case Next(_) => base.L.top
            Base(base.combine(instr,typ,static,nst)(using baseECtx),i)
          case _ => L.bot
        }
      case Right(k) =>
        (getState, result(Right(k))) match {
          case (Next(b), Next(br)) =>
            def nst(x:next.VarKey):next.Dl = result(Right(x)) match
              case Next(k) => k
              case Base(_,_) => next.L.bot
            Next(next.combine(instr,typ,static,nst)(using nextECtx))
          case _ => L.bot
        }


//  def findDeps(es:Exp):Set[Node] =
//    es match
//      case Unknw => Set.empty
//      case Op(op, l@_*) => l.toSet.flatMap(findDeps)
//      case EVar(n, c, v) => Set(n)
//      case LFP(n, c, v, e) => findDeps(e) - n
//      case Join(as) => as.flatMap(findDeps)


  override def keyStage(k: VarKey): Int =
    k match
      case Left(k) => 0
      case Right(k) => 1 + next.keyStage(k)

  override def metaNode(node: Node, ct: VarCtx, key: VarKey)(using ctx: SolvCtxT): Option[Dl] =
    (key,ct) match {
      case (Left(k), Left(c)) =>
        base.metaNode(node,c,k)(using baseNCtx).map(Base(_,ExpsLat.bot))
      case (Right(k), Right(c)) =>
        next.metaNode(node,c,k)(using nextNCtx).map(Next(_))
      case _ => Some(L.bot)
    }

