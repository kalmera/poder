package põder.framework

class BreakTransferFunction extends Throwable:
  override def toString: String =
    val s = getClass.getSuperclass.getName
    val message = getLocalizedMessage
    if message != null then s + ": " + message
    else s

trait CSys:
  type Var
  type Dl >: Null
  lazy val L: Lattice[Dl] & WidenOpF[Dl] & NarrowOpF[Dl]
  def tf(using ctx: VarSolvCtx[Var,Dl])(v: Var, inVerification:Boolean = false): Dl
