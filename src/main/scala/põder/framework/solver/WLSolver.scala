package põder.framework.solver

import põder.framework._
import põder.log.Log
import põder.util.perfCount

import scala.collection.mutable

final class WLSolver[Sys <: CSys] (val s: Sys, val loadSol: s.Var => Option[(s.Dl,Boolean)]) extends SolverAddon[Sys]:
  import s._

  val sided = mutable.HashSet.empty[Var]
  val worklist = mutable.HashSet.empty[Var]
  val infl = mutable.HashMap.empty[Var, Set[Var]]
  val stbl = mutable.HashSet.empty[Var]
  val ord  = mutable.HashMap.empty[Var, Int]
  var ordmax = 10

  def eval(wait:Var=>Unit, x: Var)(y:Var): Dl =
    if !st.contains(y) then
      solve_one(wait)(y)
    infl(y) = infl.getOrElse(y, Set.empty) + x
    if (ord contains x) && (ord contains y) then
      if ord(y) >= ord(x) then
        widened += x
    else
      if ! (ord contains y) then
        ord(y) = ordmax
        ordmax += 1
        ord(x) = ordmax
        ordmax+= 1
    st(y)

  def interWaitLoop(v: Var): Unit =
    perfCount.timeout()
    currentNode = Some(v)

    if !run && !step then
      onWait()


    while !run && !step && !kill do
      Thread.sleep(100)

    if !kill then
      step = false

    perfCount.timein()

  def set(wait:Var=>Unit)(v:Var, d:Dl): Unit =
    if !st.contains(v) then
      solve_one(wait)(v)
    val old = st.getOrElseUpdate(v, getOldVal(v))
    val d1 =
      if widened contains v then box(old, d)
      else if sided contains(v) then L.join(old,d)
      else d
    val test = !L.equal(d1, old) // d != old
    if test then
//      Log.log("updating value ...")
      updated += v
      st(v) = d1
      wait(v)
      val vinfl = infl.getOrElse(v, Set()) ++ (if widened contains v then Set(v) else Set())
      infl(v) = Set()
      vinfl foreach (stbl -= _)
      worklist ++= vinfl
//    else
//      wait(v)

  def side(v:Var, d:Dl): Unit =
    sided += v
    set(_ => ())(v,d)

  def solve_one(wait:Var=>Unit)(v:Var):Unit =
    if stop then
      stop = false
      run  = false
      step = false
      interWaitLoop(v)

    if !(st contains v) then
      loadSol(v) match
        case Some((d,certain)) =>
          st(v) = d
          if certain then
            stbl += v
            return
        case None =>
          st(v) = L.bot

    if stbl contains v then return

    stbl += v

    onEnterNode(v)
    whileNoException(wait(v)) {
      val d = s.tf(using mkCtx(eval(wait,v),side))(v)
      set(wait)(v,d)
    }
    onExitNode(v)

  def solve(vs: Set[Var]): Map[Var, Dl] =
    worklist ++= vs
    while worklist.nonEmpty do
      val h = worklist.head
      worklist -= h
      solve_one(interWaitLoop)(h)

    if run then
      perfCount.timeout()
      onWait()
      perfCount.timein()

//    printf("valmis!\n")
    onFinished()
    st.toMap
