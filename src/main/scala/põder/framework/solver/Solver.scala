package põder.framework.solver

import põder.framework._
import põder.log.Log

import collection.mutable

trait Solver[+Sys <: CSys]:
  val s: Sys
  val loadSol: s.Var => Option[(s.Dl,Boolean)]

  def stopButton(): Unit
  def runButton() : Unit
  def stepButton(): Unit
  def killButton(): Unit
  def updatedSet  : Set[s.Var]
  def widenedSet  : Set[s.Var]
  def clearSets() : Unit
  def st          : collection.Map[s.Var, s.Dl]
  def currentNode : Option[s.Var]

  var onEnterNode : s.Var => Unit = _ => ()
  var onExitNode  : s.Var => Unit = _ => ()
  var onFinished  : () => Unit = () => ()
  var onWait      : () => Unit = () => ()

  def solve(vs: Set[s.Var]): Map[s.Var, s.Dl]


abstract class SolverAddon[Sys <: CSys] extends Solver[Sys]:
  import s._

  inline def whileNoException[G,F](inline wait: =>G)(inline f: =>F):F =
    def loop:F =
      try
        f
      catch
        case _: BreakTransferFunction =>
          wait
          loop
    loop

  def getOldVal(v:Var): Dl = loadSol(v).map(_._1).getOrElse(L.bot)

  var run = false
  var step = false
  var kill = false
  var stop = false

  def stopButton(): Unit =
    stop = true
    run  = false
    step = false
  def runButton(): Unit =
    stop = false
    run = true
  def stepButton(): Unit =
    stop = false
    step = true
  def killButton(): Unit = kill = true

  val updated = mutable.HashSet.empty[Var]
  override def updatedSet: Set[Var] = updated.toSet

  val widened = mutable.HashSet.empty[Var]
  override def widenedSet: Set[Var] = widened.toSet

  override def clearSets(): Unit =
    updated.clear()
//    widened.clear()

  var currentNode: Option[Var] = None

  override val st:  mutable.HashMap[Var,Dl] = mutable.HashMap.empty

  inline def box(x: Dl, y: Dl): Dl =
    if L.leq(y, x) then L.narrow(x, y) else L.widen(x, y)

  inline def mkCtx(get1: Var => Dl, set1: (Var,Dl) => Unit): VarSolvCtx[Var,Dl] =
    new VarSolvCtx[Var,Dl] {
      override def get(v: Var): Dl = get1(v)
      override def set(v: Var, d:Dl): Unit = set1(v,d)
    }
