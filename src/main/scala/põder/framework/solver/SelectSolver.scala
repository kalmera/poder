package põder.framework.solver

import põder.framework._
import põder.log.Log

import scala.collection.mutable



object SelectSolver:
  abstract class Slvr:
    def apply[Sys<:CSys](s:Sys, p:s.Var => Option[(s.Dl,Boolean)]): Solver[Sys]

  val allSol: Map[String, Slvr] = Map(
    "slr" -> new Slvr { override def apply[Sys<:CSys](s:Sys, p:s.Var => Option[(s.Dl,Boolean)]): Solver[Sys] = new WLSolver[Sys](s,p) },
    "td"  -> new Slvr { override def apply[Sys<:CSys](s:Sys, p:s.Var => Option[(s.Dl,Boolean)]): Solver[Sys] = new TDSolver[Sys](s,p) },
    "rec" -> new Slvr { override def apply[Sys<:CSys](s:Sys, p:s.Var => Option[(s.Dl,Boolean)]): Solver[Sys] = new RecSolver[Sys](s,p) }
  )

  def apply[Sys<:CSys](solverName:String, s:Sys, p:s.Var => Option[(s.Dl,Boolean)]): Solver[Sys] =
    def getSol(name:String) = SelectSolver.allSol(name)(s,p)
    if SelectSolver.allSol isDefinedAt solverName then
      Log.logCommon(s"Solving using '$solverName'")
      getSol(solverName)
    else
      getSol("slr")

object Verifyer:
  def verify[Sys <: CSys](s: Sys, loadedResult: collection.Map[s.Var,Boolean],ds: Map[s.Var, s.Dl]): Boolean =
    import s._
    var clean = true
    def checkLeq(v: Var, d: Dl, d1: Dl): Unit =
      if !L.leq(d, d1) then
        println(s"Verification failed for $v, value:\n$d\nnot subsubed by:\n$d1")
        clean = false

    def get(x: Var)(y: Var): Dl =
      ds.getOrElse(y, {
        println(s"Verification failed for $x, requires variable $y")
        clean = false
        L.bot
      })

    inline def mkCtx(get1: Var => Dl, set1: (Var,Dl) => Unit): VarSolvCtx[Var,Dl] =
      new VarSolvCtx[Var,Dl] {
        override def get(v: Var): Dl = get1(v)
        override def set(v: Var, d:Dl): Unit = set1(v,d)
      }
    def set(x: Var)(y: Var, d: Dl): Unit =
      val d1 = ds.getOrElse(y, {
        println(s"Verification failed for $x, sets variable $y")
        clean = false
        L.bot
      })
      checkLeq(y, d, d1)


    for (v, d) <- ds if !(loadedResult.get(v) contains true) do
      Log.pushNode(v)
      val d1 = s.tf(using mkCtx(get(v),set(v)))(v, inVerification = true)
      Log.popNode()
      checkLeq(v, d1, d)

    clean

  def verify_reachable[Sys <: CSys](s: Sys, loadedResult: collection.Map[s.Var,Boolean], previousSol: s.Var => Option[(s.Dl, Boolean)], startVars: Set[s.Var]): Boolean =
    import s._
    var clean = true
    val ds = mutable.Set.empty[Var]
    var vars = startVars

    def checkLeq(v: Var, d: Dl, d1: Dl): Unit =
      if !L.leq(d, d1) then
        println(s"Verification failed for $v, value:\n$d1\nnot subsubed by:\n$d")
        clean = false

    inline def mkCtx(get1: Var => Dl, set1: (Var,Dl) => Unit): VarSolvCtx[Var,Dl] =
      new VarSolvCtx[Var,Dl] {
        override def get(v: Var): Dl = get1(v)
        override def set(v: Var, d:Dl): Unit = set1(v,d)
      }

    def get(x: Var)(y: Var): Dl =
      solve_one(y)

    def set(x: Var)(y: Var, d: Dl): Unit =
      vars += y

    def solve_one(v: Var): Dl =
      Log.pushNode(v)
      var d1 = L.bot
      if !(ds contains v) then
        ds += v
        d1 = s.tf(using mkCtx(get(v),set(v)))(v, inVerification = true)
      val prevSol = previousSol(v)
      Log.popNode()
      if prevSol.isDefined then
        val d = prevSol.get._1
        checkLeq(v, d1, d)
        d
      else
        L.bot

    while vars.nonEmpty do
      val v = vars.head
      vars -= v
      if (loadedResult.get(v) contains true) then
        solve_one(v)
    clean
