package põder.framework.solver

import põder.framework._
import põder.log.Log
import põder.util.perfCount

import scala.collection.mutable

class TDSolver[Sys <: CSys](val s: Sys, val loadSol: s.Var => Option[(s.Dl,Boolean)]) extends SolverAddon[Sys]:
  import s._

  val called = mutable.Set.empty[Var]
  val wpoint = mutable.Set.empty[Var]
  val stable = mutable.Set.empty[Var]
  val sigma = mutable.Map.empty[Var, Dl]
  val infl  = mutable.Map.empty[Var, Set[Var]]

  def destabilize(x: Var): Unit =
    val w = infl(x)
    infl += x -> Set()
    for y <- w if stable contains y do
      stable -= y
      destabilize(y)

  def box(v:Var)(x: Dl, y: Dl): Dl =
    if L.leq(y, x) then L.narrow(x, y) else
      updated += v
      L.widen(x, y)

  def solve(x: Var): Unit =
      def interWaitLoop(v: Var): Unit =
        perfCount.timeout()
        currentNode = Some(v)

        if !run && !step then
          onWait()


        while !run && !step && !kill do
          Thread.sleep(100)

        if !kill then
          step = false

        perfCount.timein()
      def set(wait: Var => Unit)(y: Var,d: Dl): Unit =
        if sigma(x) != d then
          sigma += x -> d
          updated += x
          destabilize(x)
          solve(x)
      def eval(y: Var): Dl =
        if called contains y then wpoint += y
        solve(y)
        infl += y -> (infl(y) ++ Set(x))
        sigma(y)
      if stop then
        stop = false
        run  = false
        step = false
        interWaitLoop(x)
      loadSol(x) match
        case Some((d,certain)) =>
          sigma(x) = d
          if certain then
            stable += x
            return
        case _ => ()

      if (!(stable contains x)) || (called contains x) then
        called += x
        stable += x
        var tmp: Dl = null
        if wpoint contains x then
          wpoint -= x
          onEnterNode(x)
          tmp = whileNoException(interWaitLoop(x)){
            box(x)(sigma(x),s.tf(using mkCtx(eval,set(interWaitLoop)))(x))
          }
          onExitNode(x)
          infl += x -> (infl(x) ++ Set(x))
        else
          onEnterNode(x)
          tmp = s.tf(using mkCtx(eval, set(interWaitLoop)))(x)
          onExitNode(x)
        called -= x
        set(interWaitLoop)(x,tmp)

  override def solve(vs: Set[Var]): Map[Var, Dl] =
    vs.foreach(solve)
    if run then
      onWait()
    onFinished()
    sigma.toMap
