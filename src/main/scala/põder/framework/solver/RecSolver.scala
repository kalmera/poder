package põder.framework.solver

import põder.framework._
import põder.log.Log
import põder.util.perfCount

import scala.collection.mutable

class RecSolver[Sys <: CSys](final val s: Sys, val loadSol: s.Var => Option[(s.Dl,Boolean)]) extends SolverAddon[Sys]:
  import s._

  val infl = mutable.Map.empty[Var, Set[Var]]
  val stbl = mutable.Set.empty[Var]
  val ord  = mutable.Map.empty[Var, Int]
  var ordmax = 10

  def solve(vs: Set[Var]): Map[Var, Dl] =
    def eval(x: Var)(y:Var): Dl =
      infl(y) = infl.getOrElse(y,Set.empty) + x
      sol(y)
      if ord(y) <= ord(x) then
        widened += x
      st(y)

    def interWaitLoop(v: Var): Unit =
      perfCount.timeout()
      currentNode = Some(v)

      if !run && !step  then
        onWait()


      while !run && !step && !kill do
        Thread.sleep(100)

      if !kill then
        step = false
      perfCount.timein()

    def set(wait:Var=>Unit)(v:Var,d:Dl): Unit =
      sol(v)
      val old = st.getOrElseUpdate(v,getOldVal(v))
      val test = !(L.equal(d, old))// d != old
      if test then
        Log.log("updating value ...")
        updated += v
        if widened contains v then
          st(v) = box(old, d)
        else
          st(v) = d
        wait(v)
        val vinfl = infl.getOrElse(v,Set())
        infl(v) = Set()
        vinfl foreach (stbl -= _)
        vinfl foreach sol


    def sol(v:Var):Unit =
      if stop then
        stop = false
        run  = false
        step = false
        interWaitLoop(v)

      if !(ord contains v) then
        ord(v) = ordmax
        ordmax += 1

      if !(st contains v) then
        loadSol(v) match
          case Some((d,certain)) =>
            st(v) = d
            if certain then
              stbl += v
              return
          case None =>
            st(v) = L.bot

      if stbl contains v then return

      stbl += v

      onEnterNode(v)
      whileNoException(interWaitLoop(v)){
        set(interWaitLoop)(v, s.tf(using mkCtx(eval(v),set(_ => ())))(v))
      }
      onExitNode(v)

    vs foreach sol

    if run then
      onWait()

    onFinished()
    st.toMap
