package põder.framework

import põder.framework.cfg.{Diff, Relative}

trait VarSolvCtx[V, D]:
  def get(v:V): D
  def set(v:V, d:D): Unit

trait SolvCtx[N, C, K, D] extends VarSolvCtx[(N,C,K), D]:
  def get(n:N, c:C, k:K): D
  def set(n:N, c:C, k:K, d:D): Unit
  override def get(v: (N, C, K)): D = get(v._1, v._2, v._3)
  override def set(v: (N, C, K), d: D): Unit = set(v._1, v._2, v._3,d)

case class UpgradeCtx[N, C, K, D](vctx: VarSolvCtx[(N,C,K), D]) extends SolvCtx[N, C, K, D]:
  override def get(n:N, c:C, k:K): D = vctx.get((n,c,k))
  override def set(n:N, c:C, k:K, d:D): Unit = vctx.set((n,c,k),d)
  override def get(v: (N, C, K)): D = vctx.get(v)
  override def set(v: (N, C, K), d: D): Unit = vctx.set(v,d)

trait NodeCtx[N, C, K, D] extends SolvCtx[N, C, K, D]:
  val from: N
  val from_start: Boolean
  val to: N
  val to_final: Boolean
  val k: K
  val c: C
  def st(using k:K): D

trait JBCtx[N, C, K, D] extends NodeCtx[N, C, K, D]:
  val stackSize: Int
  val stackDiff: Diff = Relative(0)

class JBC[N, C, K, D](ctx: NodeCtx[N, C, K, D])(
  val stackSize: Int,
  override val stackDiff: Diff = Relative(0),
  override val k: K = ctx.k,
  st: K=>D = (k:K) => ctx.st(using k),
  override val from_start: Boolean = ctx.from_start,
  override val to_final: Boolean = ctx.to_final
) extends JBCtx[N, C, K, D]:
  override val c: C = ctx.c
  override val from: N = ctx.from
  private val st1 = st
  override def st(using k: K): D = st1(k)
  override val to: N = ctx.to
  override def get(n: N, c: C, k: K): D = ctx.get(n,c,k)
  override def set(n: N, c: C, k: K, d: D): Unit = ctx.set(n,c,k,d)

