//package põder.framework
//
//import java.io.File
//
//import org.mapdb.DBMaker
//
//import scala.collection.JavaConverters._
//import scala.collection.mutable
//
//
//class Store[node >: Serializable, dom >: Serializable](f:File) {
//  type key = (node,Map[String,Int])
//
//  private val db = DBMaker.fileDB(f).closeOnJvmShutdown().make()
//  private val store = mutable.Map.empty[String, mutable.Map[key,dom]]
//
//  def get(cl: String): (node, Map[String,Int]) => dom = {
//    val hm = store.getOrElseUpdate(cl, db.hashMap(cl).createOrOpen().asScala.asInstanceOf[mutable.Map[key, dom]])
//    (n, conf) => hm.get((n, conf))
//  }
//
//  def set(cl: String): (node, Map[String,Int], dom) => Unit = {
//    val hm = store.getOrElseUpdate(cl, db.hashMap(cl).createOrOpen().asScala.asInstanceOf[mutable.Map[key, dom]])
//    (n, conf, d) => hm.update((n, conf), d)
//  }
//}
