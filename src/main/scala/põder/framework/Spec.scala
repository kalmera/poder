package põder.framework

import scala.language.implicitConversions
import põder.analyses.NodeGVar
import põder.framework.cfg.*
import põder.framework.solver.{SelectSolver, Solver, Verifyer}
import põder.log.Log
import põder.util.{CellConv, perfCount, Project}
import põder.util.json.JsonF
import põder.util.typ.{ArrayT, MethodType, ObjectT, Parser, VoidT}

import scala.collection.mutable

abstract class MetaAnalysis[Node]:
  def forward: Boolean

  protected var inVerification = false
  def setVerificationMode(b: Boolean): Unit =
    inVerification = b

  type VarKey
  lazy val VarKey: JsonOpF[VarKey]
  def keyStage(k:VarKey): Int = 0

  type VarCtx
  lazy val VarCtx: JsonOpF[VarCtx]

  lazy val startCtxKey : Seq[(VarCtx,VarKey)]

  type Dl >: Null
  lazy val L: Lattice[Dl] & WidenOpF[Dl] & NarrowOpF[Dl]

  type NCtx[N1,C1,K1,D1] <: SolvCtx[N1,C1,K1,D1]
  final type SolvCtxT = NCtx[Node,VarCtx,VarKey,Dl]

  def explain[jv](n:Node, c:VarCtx, k:VarKey, lod: Int = 1)(using ctx: SolvCtxT, j: JsonF[jv]): jv =
    val v = ctx.get(n,c,k)
    j.Obj((n,c,k).toString() -> L.toJson(v))

  def genEdges(n:Node, ct:VarCtx, key: VarKey)(using ctx: SolvCtxT): Set[(Edge,(Node,VarKey))] = Set.empty
  def metaNode(node: Node, c: VarCtx, key: VarKey)(using ctx: SolvCtxT): Option[Dl] = None


trait CFGAnalysis[Node, Edge] extends MetaAnalysis[Node] :
  def node(nd: Node, b: VarCtx, k: VarKey)(using ctx: SolvCtxT):Dl

  def edge(e: Edge)(using ctx: NodeCtx[Node, VarCtx, VarKey, Dl]):Dl

abstract class JBCAnalysis extends MetaAnalysis[Node]:
  val at: AnalysisTask
  lazy val project: ProgramCFG = at.pcfg

  override type NCtx[N1,C1,K1,D1] <: SolvCtx[N1,C1,K1,D1]

  type ECtx[N2,C2,K2,D2] <: JBCtx[N2,C2,K2,D2]
  final type JBCtxT = ECtx[Node,VarCtx,VarKey,Dl]

  /// helper function that generates entry sequences using usings
  inline final def enterWith(d:Dl)(using inline ctx: ECtx[Node,VarCtx,VarKey,Dl], c:VarCtx, k:VarKey): Seq[(VarCtx,VarKey,VarKey=>Dl,Dl)] =
    Seq((c,k,(k:VarKey) => ctx.st(using k),d))

  def node(nd: Node, b: VarCtx, k: VarKey)(using ctx: SolvCtxT):Dl

  def branch(instr: JumpInstr, thn: Boolean)(using ctx:  ECtx[Node,VarCtx,VarKey,Dl]):Dl
  def normal(instr: Instr)(using ctx:  ECtx[Node,VarCtx,VarKey,Dl]):Dl
  def throws(instr: Instr, ex: Either[String, Set[String]])(using ctx:  ECtx[Node,VarCtx,VarKey,Dl]): Dl
  def annot(ant: Annot)(using ctx:  ECtx[Node,VarCtx,VarKey,Dl]): Dl
  def start(locals: Int, stack: Int, access: Int)(using ctx:  ECtx[Node,VarCtx,VarKey,Dl]): Dl

  def enter(instr: MethodInstr, typ: MethodType, static: Boolean)(using ctx:  ECtx[Node,VarCtx,VarKey,Dl]): Seq[(VarCtx,VarKey,VarKey=>Dl,Dl)]
  def combine(instr: MethodInstr, typ: MethodType, static: Boolean, result: VarKey => Dl)(using ctx:  ECtx[Node,VarCtx,VarKey,Dl]): Dl

  def isBuiltinFun(instr: MethodInstr, typ:MethodType, static:Boolean): Boolean = false


object UnitJsonOp extends JsonOpF[Unit]:
  override def toJson[jv](x: Unit)(using j: JsonF[jv]): jv = j.Null


trait SimpleJBCAnalysis extends JBCAnalysis :
  inline def forward: Boolean = true

  override type NCtx[A,B,C,D] = SolvCtx[A,B,C,D]
  override type ECtx[A,B,C,D] = JBCtx[A,B,C,D]

  given unit: Unit = ()

  type VarKey = Unit
  final lazy val VarKey: JsonOpF[VarKey] = UnitJsonOp

  type VarCtx = Unit
  final lazy val VarCtx: JsonOpF[VarCtx] = UnitJsonOp

  final lazy val startCtxKey: Seq[(VarCtx,VarKey)] = Seq(((),()))
  
  override inline def node(nd: Node, b: VarCtx, k: VarKey)(using ctx: SolvCtxT):Dl

  override inline def branch(instr: JumpInstr, thn: Boolean)(using ctx: JBCtxT):Dl
  override inline def normal(instr: Instr)(using ctx: JBCtxT):Dl
  override inline def throws(instr: Instr, ex: Either[String, Set[String]])(using ctx: JBCtxT): Dl
  override inline def annot(ant: Annot)(using ctx: JBCtxT): Dl
  override inline def start(locals: Int, stack: Int, access: Int)(using ctx: JBCtxT): Dl

  override inline def enter(instr: MethodInstr, typ: MethodType, static: Boolean)(using ctx: JBCtxT): Seq[(VarCtx,VarKey,VarKey=>Dl,Dl)]
  override inline def combine(instr: MethodInstr, typ: MethodType, static: Boolean, result: VarKey => Dl)(using ctx: JBCtxT): Dl

abstract class MetaJBCAnalysis extends JBCAnalysis:
  override def branch(instr: JumpInstr, thn: Boolean)(using ctx: JBCtxT): Dl = L.bot
  override def normal(instr: Instr)(using ctx: JBCtxT): Dl = L.bot
  override def throws(instr: Instr, ex: Either[String, Set[String]])(using ctx: JBCtxT): Dl = L.bot
  override def annot(ant: Annot)(using ctx: JBCtxT): Dl = L.bot
  override def start(locals: Int, stack: Int, access: Int)(using ctx: JBCtxT): Dl = L.bot
  override def enter(instr: MethodInstr, typ: MethodType, static: Boolean)(using ctx: JBCtxT): Seq[(VarCtx, VarKey, VarKey => Dl, Dl)] = Seq.empty
  override def combine(instr: MethodInstr, typ: MethodType, static: Boolean, result: VarKey => Dl)(using ctx: JBCtxT): Dl = L.bot

trait AnalysisCSys[n] extends CFGAnalysis[n,Edge] with CSys:
    override type NCtx[A,B,C,D] = SolvCtx[A,B,C,D]

    def prevs(n:n): Set[(Edge, n)]
    def newEdges(to:(n,VarKey), eds: Set[(Edge,(n,VarKey))], stage:Int): Unit

    private val edges: mutable.Map[n,Set[(VarKey,Edge,n,VarKey)]] = mutable.Map.empty

    private def prev(n:n,k:VarKey): Set[(Edge,n,VarKey)] =
      val pv = if keyStage(k)==0 then prevs(n).map((e,n) => (e,n,k)) else Set.empty
      val r = edges.get(n) match
        case None => pv
        case Some(es) => pv++es.filter(_._1==k).map((_,e,f,fk) => (e,f,fk))
      Log.log(s"prev($n,$k) = ${r.mkString("{",",","}").replace("\n",";")}")
      r

    def addEdge(t:n, tk:VarKey, eds: Set[(Edge,(n,VarKey))]):Unit =
      Log.log(s"addEdge($t,$tk,${eds.map{case (e,f) => (e.toString.replace("\n",";"),f)}.toString})")
      val es = edges.getOrElse(t,Set.empty)
      newEdges((t,tk), eds++es.view.filter(_._1!=tk).map{case (_,e,f,fk) => (e,(f,fk))}, keyStage(tk))
      edges += t -> (es.filter(_._1!=tk) ++ eds.map{case (e,(f,fk)) => (tk,e,f,fk)})

    final type Var = (n,VarCtx,VarKey)
    override inline def tf(using ctx:VarSolvCtx[Var,Dl])(v: Var, inVerification: Boolean): Dl =
      perfCount.sub("transfer-fun") {
        setVerificationMode(inVerification)
        given sctx: SolvCtx[n,VarCtx,VarKey,Dl] = UpgradeCtx(ctx)
        val (to_nd, to_ctx, to_key) = v
        val newEdges = genEdges(to_nd,to_ctx,to_key)
        addEdge(to_nd,to_key,newEdges)
        metaNode(to_nd, to_ctx, to_key) match
          case Some(value) => value
          case None =>
            val ncontr = node(to_nd, to_ctx, to_key)
            prev(to_nd,to_key).foldLeft(ncontr){
                case (l,(e, node, key)) =>
                given ctx1: NodeCtx[n,VarCtx,VarKey,Dl] =
                  new NodeCtx[n,VarCtx,VarKey,Dl] {
                    override val from: n = node
                    override val from_start: Boolean = true
                    override val to: n = to_nd
                    override val to_final: Boolean = true
                    override val k: VarKey = to_key
                    override val c: VarCtx = to_ctx
                    override def st(using k: VarKey): Dl = ctx.get((node,to_ctx,k))
                    override def get(n: n, c: VarCtx, k: VarKey): Dl = ctx.get((n,c,k))
                    override def set(n: n, c: VarCtx, k: VarKey, d: Dl): Unit = ctx.set((n,c,k),d)
                  }
                L.join(l, edge(e))
            }
      }


trait GeneralAnalysis extends JBCAnalysis with CFGAnalysis[Node,Edge]:
   override type NCtx[N1, C1, K1, D1] = SolvCtx[N1, C1, K1, D1]
   override type ECtx[N1, C1, K1, D1] = JBCtx[N1, C1, K1, D1]
   inline def getMethodStartNode(c:String, m:String, t:MethodType): cfg.Node = at.pcfg.startVarsFun(c,m,t)
   inline def getMethodReturnNode(c:String, m:String, t:MethodType): cfg.Node = at.pcfg.returnVarsFun(c,m,t)
   inline def getMethodThrowingNodes(c:String, m:String, t:MethodType): List[cfg.Node] = at.pcfg.getMethodThrowingNodes(c,m,t).toList

   type N = cfg.Node
   type E = Edge

   private inline def checkBuiltin(m:MethodInstr, t:MethodType, s:Boolean): Option[String] = m match
      case INVOKEDYNAMIC(_,_) => None
      case m if m.a.get.isDefined =>
        if m.a._1.startsWith("[") then
          if m.a._1 == "clone" then
            None
          else
            Some("java/lang/Object")
        else
          (if isBuiltinFun(m, t, s) then None else Some(m.a._1))
      case _ => None

   private inline def iterInstr(xs: Seq[Either[Instr, Annot]])(using ctx: NodeCtx[N, VarCtx, VarKey, Dl]): Dl =
      def getStackSize(n:Node):Int  =
        n match
          case node: MethodNode => node.stackSize
          case _ => 0
      if xs.isEmpty then
        ctx.st(using ctx.k)
      else if xs.length == 1 then
        xs.head match
          case Left(instr) => normal(instr)(using JBC(ctx)(getStackSize(ctx.from), instr.stackDiff))
          case Right(annot) => this.annot(annot)(using JBC(ctx)(getStackSize(ctx.from), Relative(0)))
      else
        var stackSize = getStackSize(ctx.from)
        val start_k: VarKey = ctx.k
        val d0 = xs.head match
          case Left(instr) => (k:VarKey) =>
            val r = normal(instr)(using new JBC(ctx)(stackSize,instr.stackDiff, k=k, from_start=true, to_final=false))
            instr.stackDiff match {
              case Absolute(s) => stackSize = s
              case Relative(s) => stackSize += s
            }
            r
          case Right(ann) => (k:VarKey) =>
            annot(ann)(using JBC(ctx)(stackSize,k = k, from_start=true, to_final=false))
        val d1 = xs.tail.init.foldLeft(d0) {
          case (f, Left(instr)) => (k:VarKey) =>
            val r = normal(instr)(using JBC(ctx)(stackSize, instr.stackDiff, k=k, st=f, from_start=false, to_final=false))
            instr.stackDiff match {
              case Absolute(s) => stackSize = s
              case Relative(s) => stackSize += s
            }
            r
          case (f, Right(ann)) => (k:VarKey) =>
            annot(ann)(using JBC(ctx)(stackSize, k=k, st=f, from_start=false, to_final=false))
        }
        xs.last match
          case Left(instr) =>
            val r = normal(instr)(using JBC(ctx)(stackSize, instr.stackDiff, st=d1, from_start=false, to_final=true))
            instr.stackDiff match {
              case Absolute(s) => stackSize = s
              case Relative(s) => stackSize += s
            }
            r
          case Right(ann) =>
            annot(ann)(using JBC(ctx)(stackSize, st=d1, from_start=false, to_final=true))

   private def tfEdges(e: E)(using ctx: NodeCtx[Node, VarCtx, VarKey, Dl]): Dl = e match
//      case _ if !ctx.from.isInstanceOf[MethodNode] => L.bot
      case BasicBlock(xs) if !forward => iterInstr(xs.reverse)
      case BasicBlock(xs) => iterInstr(xs)
      case Jump(instr) => branch(instr, thn = true)(using JBC(ctx)(ctx.from.asInstanceOf[LNode].stackSize,instr.stackDiff))
      case Fall(instr) => branch(instr, thn = false)(using JBC(ctx)(ctx.from.asInstanceOf[LNode].stackSize,instr.stackDiff))
      case Start(locals, stack, access) => start(locals, stack, access)(using JBC(ctx)(ctx.from.asInstanceOf[LNode].stackSize))
      case Returns(instr) =>
        if L.leq(ctx.st(using ctx.k), L.bot) then
          L.bot
        else if instr.a.get.isEmpty then
          L.top
        else
          val (_, m, t) = instr.a.apply
          val types = põder.util.typ.Parser.mpar(t)
          val isStatic = instr.isInstanceOf[INVOKESTATIC]
          checkBuiltin(instr, types, isStatic) match
            case None =>
              normal(instr)(using JBC(ctx)(ctx.from.asInstanceOf[LNode].stackSize,instr.stackDiff))
            case Some(k1) =>
              val startNode = getMethodStartNode(k1, m, Parser.mpar(t))
              val returnNode = getMethodReturnNode(k1, m, Parser.mpar(t))
              L.Join {
                for (c, k, q, r) <- enter(instr, types, isStatic)(using JBC(ctx)(ctx.from.asInstanceOf[LNode].stackSize,instr.stackDiff)) yield
                  ctx.set(startNode,c,k,r)
                  val ctx1 = JBC(ctx)(ctx.from.asInstanceOf[LNode].stackSize,instr.stackDiff, st=q)
                  combine(instr, types, isStatic, ctx.get(returnNode,c,_))(using ctx1)
              }
      case Throws(xs, exception) =>
        // xs = list of instructions that can throw this exception
        if xs.isEmpty then
          ctx.st(using ctx.k)
        else if xs.length == 1 then
          xs.head match
            case Left(instr) =>
              normal(instr)(using JBC(ctx)(ctx.from.asInstanceOf[LNode].stackSize,instr.stackDiff))
            case Right(annot) =>
              this.annot(annot)(using JBC(ctx)(ctx.from.asInstanceOf[LNode].stackSize))
        else
          val xs1 = if forward then xs else xs.reverse
          val start_k: VarKey = ctx.k
          var stackSize = ctx.from.asInstanceOf[LNode].stackSize
          val d0 = xs1.head match
            case Left(instr) => (k:VarKey) =>
              val r = throws(instr, exception)(using JBC(ctx)(stackSize,instr.stackDiff, k=k, from_start=true, to_final=false))
              instr.stackDiff match {
                case Absolute(s) => stackSize = s
                case Relative(s) => stackSize += s
              }
              r
            case Right(annot) => (k:VarKey) =>
              this.annot(annot)(using JBC(ctx)(stackSize, k=k, from_start=true, to_final=false))
          val d1 = xs1.tail.init.foldLeft(d0) {
            case (f, Left(instr)) => (k:VarKey) =>
              val r = throws(instr, exception)(using JBC(ctx)(stackSize,instr.stackDiff, k=k, st=f, from_start=false, to_final=false))
              instr.stackDiff match {
                case Absolute(s) => stackSize = s
                case Relative(s) => stackSize += s
              }
              r
            case (f, Right(annot)) => (k:VarKey) =>
              this.annot(annot)(using JBC(ctx)(stackSize, k=k, st=f, from_start=false, to_final=false))
          }
          xs1.last match
            case Left(instr) =>
              val r = throws(instr, exception)(using JBC(ctx)(stackSize, instr.stackDiff, st=d1, from_start=false, to_final=true))
              instr.stackDiff match {
                case Absolute(s) => stackSize = s
                case Relative(s) => stackSize += s
              }
              r
            case Right(annot) =>
              this.annot(annot)(using JBC(ctx)(stackSize, st=d1, from_start=false, to_final=true))

   final inline def edge(e: E)(using ctx: NodeCtx[Node, VarCtx, VarKey, Dl]): Dl =
     val task =
       e match
         case t: BasicBlock[Annot] => "normal"
         case t: Jump[Annot] => "jump"
         case t: Fall[Annot] => "fall"
         case t: Start[Annot] => "start"
         case t: Returns[Annot] => "returns"
         case t: Throws[Annot] => "throw"
     perfCount.sub(task)(tfEdges(e)(using ctx))

abstract class AnalysisTask:
  val pcfg: ProgramCFG
  val project: Project
  val solver: String

  def newCFG(gr: Graph): Unit
  def finished(cs: ConstrSys): Unit
  def update(cs: ConstrSys): Unit
  def storeNode(cs: ConstrSys): Unit
  def saveResults(cs: ConstrSys): Unit

  def newEdges(to:Node, es:Set[(Edge,Node)], stage:Int): Unit

trait ConstrSys extends CFGAnalysis[Node,Edge]:
  override type NCtx[N1, C1, K1, D1] = SolvCtx[N1, C1, K1, D1]
//  override type ECtx[N1, C1, K1, D1] = NodeCtx[N1, C1, K1, D1]
  val solver: Solver[AnalysisCSys[Node]]
  val st: Thread

trait CompiledAnalysis
  extends JBCAnalysis
     with GeneralAnalysis
     with AnalysisCSys[Node]
     with ConstrSys
  :
    override type NCtx[N1, C1, K1, D1] = SolvCtx[N1, C1, K1, D1]
    override type ECtx[N1, C1, K1, D1] = JBCtx[N1, C1, K1, D1]
    inline override def prevs(n:Node): Set[(Edge, Node)] = project.prev.withDefaultValue(Set())(n)

    override def newEdges(to: (N, VarKey), eds: Set[(Edge, (N, VarKey))], stage: Int): Unit =
      at.newEdges(to._1, eds.map{case (e,nk) => (e,nk._1)}, stage)

    private inline def varFun(c: String, m: String, t: MethodType) =
      if forward then at.pcfg.returnVarsFun(c, m, t) else at.pcfg.startVarsFun(c, m, t)

    at.pcfg.startMethods =
      if at.project.juliet then
        Set(("good", MethodType(List(), VoidT)), ("bad", MethodType(List(), VoidT)))
      else
        Set(("main", MethodType(List(ArrayT(ObjectT("java/lang/String"))), VoidT)))

    final def loadVal(v:Var): Option[(Dl,Boolean)] =
      v match
        case (n:LNode,c,k) =>
          at.project.loadResult[mutable.Buffer[(VarCtx, VarKey, Dl, Boolean)]](n) match {
            case Some(b) =>
              val q = b.iterator
              while (q.hasNext) do
                val (c1,k1,d,s) = q.next()
                if c==c1 && k==k1 then
                  return Some((d,s))
              return None
            case _ =>
              None
          }
        case _ => None

    private final val loadedResult: mutable.Map[Var,Boolean] = mutable.Map.empty
    private final def recordLoad(v: Var): Option[(Dl,Boolean)] =
      val p = loadVal(v)
      p.foreach(q => loadedResult += v -> q._2)
      p

    final val solver : Solver[AnalysisCSys[Node]] =
      val slvr = SelectSolver[this.type](at.solver,this,recordLoad)
      slvr.onWait = () => at.update(this)
      slvr.onFinished = () => at.finished(this)
      slvr.onEnterNode = (x:Var) => Log.pushNode(x._1)
      slvr.onExitNode = _ => Log.popNode()
      slvr

    private final val outer = this

    final val st = new Thread() {
      override def run(): Unit =
        val startVars: Set[(Node, solver.s.VarCtx, solver.s.VarKey)] =
          for  (m, t) <- at.pcfg.startMethods
               (c, k) <- solver.s.startCtxKey
            yield (varFun(at.pcfg.mainPath ++ at.pcfg.mainClass, m, t), c, k)

        Log.log(s"Analyzing '${at.pcfg.mainClass}' using analysis '${at.project.analysisName}'.")
        perfCount.sub("verify") {
          Verifyer.verify_reachable(solver.s, loadedResult.asInstanceOf, loadVal.asInstanceOf, startVars)
        }

        val result : Map[(Node,solver.s.VarCtx,solver.s.VarKey),solver.s.Dl] = perfCount.sub("solver") {
          solver.solve(startVars)
        }
        at.update(outer)
        perfCount.sub("verify") {
          Verifyer.verify(solver.s,loadedResult.asInstanceOf,result)
        }
        val toSave = mutable.Map.empty[Node, mutable.Buffer[(solver.s.VarCtx, solver.s.VarKey, solver.s.Dl, Boolean)]]
        for ((v, c, k), d) <- result if v.isInstanceOf[LNode] && at.project.isLibrary(v.asInstanceOf[LNode].methodInfo.cls) do
          toSave.getOrElseUpdate(v, mutable.Buffer.empty[(solver.s.VarCtx, solver.s.VarKey, solver.s.Dl, Boolean)]) += ((c, k, d, v.asInstanceOf[LNode].methodInfo.mthd != "main"))
        for (y, d) <- toSave if y.isInstanceOf[LNode] do at.project.saveResult(y.asInstanceOf, d)
    }

