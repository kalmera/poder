package põder.framework.gui

import javafx.application.Platform
import javafx.event.EventHandler
import javafx.scene.control.ScrollPane
import javafx.scene.input.ScrollEvent
import javafx.scene.text.FontSmoothingType
import javafx.concurrent.Worker
import põder.framework.cfg.{LNode, Node, MethodInfo, MethodNode}
import javafx.scene.web.WebView
import põder.framework.gui.JavaLexer.State.Normal
import põder.framework.solver.Solver

import java.io.{File, PrintWriter}
import java.nio.file.Files
import scala.annotation.tailrec
import scala.collection.immutable.Queue
import scala.io.Source
import scala.util.matching.Regex
import netscape.javascript.JSObject
import põder.framework.AnalysisCSys
import põder.framework.gui.DynGraph.fileLineMap

object JavaLexer:
  def lexTry(in:CharSequence, xs:List[(Regex, Kind)]): Option[(String, Kind, CharSequence)] = {
    xs match
      case Nil => if in.length()==0 then None else Some((in.toString, "unmatched",""))
      case (r,k)::tl =>
        r.findPrefixMatchOf(in) match
          case Some(v) => Some((v.matched, k, v.after))
          case None => lexTry(in, tl)
  }

  def simpleMatchPrefix(in:CharSequence, r:Regex): (String, CharSequence) =
    r.findPrefixMatchOf(in) match
      case Some(value) => (value.matched, value.after)
      case None => ("", in)

  val kw = Seq("abstract", "continue", "for", "new", "switch", "assert", "default", "goto", "package",
    "synchronized", "boolean", "do", "if", "private", "this", "break", "double", "implements",
    "protected", "throw", "byte", "else", "import", "public", "throws", "case", "enum", "instanceof",
    "return", "transient", "catch", "extends", "int", "short", "try", "char", "final", "interface",
    "static", "void", "class", "finally", "long", "strictfp", "volatile", "const", "float", "native",
    "super", "while")

  val whitePattern = s"[ \t\n\r\f]*".r

  val kwPattern = s"(${kw.mkString("|")})".r

  val lit = Seq("true", "false", "null")

  val litPattern = s"([0-9.]+|${lit.mkString("|")})".r

  val oper = Seq("+", "-", "*", "/", "%", "=", ":", ",", ";", "(", ")", "{", "}")

  val operPattern = s"(${oper.map(s => s"\\Q$s\\E").mkString("|")})".r

  val commentPattern = s"((\\Q//\\E.*)|(\\Q/*\\E.*\\Q*/\\E))".r

  val mlCommentStartPattern = s"(\\Q/*\\E([^\\*]|\\*[^/])*)$$".r
  val mlCommentEndPattern = s"([^\\*]|\\*[^/])*\\Q*/\\E".r

  val charPattern = s"'[^']+'".r

  val stringPattern = s"\"[^\"]+\"".r

  val mlStringStartPattern = s"(\"\"\"([^\"]|\"[^\"]|\"\"[^\"])*)$$".r
  val mlStringEndPattern = s"([^\"]|\"[^\"]|\"\"[^\"])*\"\"\"".r

  val otherPattern = s"[^ \t\n\r\f${oper.map(s => s"\\Q$s\\E").mkString("")}]+".r

  type Kind = String

  enum State:
    case Normal, MLComment, MLString

  @tailrec
  def lexLine(in: CharSequence, q: Queue[(String, Kind)] = Queue.empty, st: State = State.Normal): (Seq[(String, Kind)], State) =
    val (ws, in2) = simpleMatchPrefix(in, whitePattern)
    st match
      case State.Normal =>
        val ps = List(mlStringStartPattern -> "mlstr", kwPattern -> "kw", litPattern -> "lit", commentPattern -> "com",
          mlCommentStartPattern -> "mlcom", operPattern -> "op", charPattern -> "char", stringPattern -> "str",
          otherPattern -> "")
        lexTry(in2, ps) match
          case Some((m,k,r)) =>
            if k=="mlcom" && r.length()==0 then
              (q.appended((ws++m, k)), State.MLComment)
            else if k=="mlstr" && r.length()==0 then
              (q.appended((ws++m, k)), State.MLString)
            else
              lexLine(r, q.appended((ws++m, k)), st)
          case None => (q, State.Normal)
      case State.MLComment =>
        val (m,r) = simpleMatchPrefix(in, mlCommentEndPattern)
        if m.isEmpty then
          (q.appended((ws++in.toString, "mlcom")), State.MLComment)
        else
          lexLine(r, q.appended((ws++m, "mlcom")), State.Normal)
      case State.MLString =>
        val (m,r) = simpleMatchPrefix(in, mlStringEndPattern)
        if m.isEmpty then
          (q.appended((ws++in.toString, "mlstr")), State.MLString)
        else
          lexLine(r, q.appended((ws++m, "mlstr")), State.Normal)


object JavaConnector:
  var selector: Set[MethodNode] => Unit = _ => ()
  def select(file: String, ln: Int): Unit  =
    fileLineMap.get((file,ln)) match
      case Some(nd) => selector(nd)
      case None => ()

class SourceFileView(srcf: File, source:String) {
  val wv = new WebView

  wv.getEngine.getLoadWorker.stateProperty().addListener((observable, oldValue, newValue) => {
    if Worker.State.SUCCEEDED == newValue then
      val window = wv.getEngine.executeScript("window").asInstanceOf[JSObject]
      window.setMember("javaConnector", JavaConnector);
  })

  def populateHasData(s: Solver[AnalysisCSys[Node]]): Unit =
    val hasData =
      s.st.keys.map(_._1).flatMap {
        case k: MethodNode if k.methodInfo.sourceFile == source =>
            k.linenr.toSet
        case _ => Set.empty
      }.toSet
    changedLines(hasData)

  def hilightLine(l: Int): Unit =
    val str = s"for(let x of document.getElementsByClassName(\"line${l-1}\")) { x.classList.add(\"hilight\"); }"
    wv.getEngine.executeScript(str)

  def deHilight(): Unit =
    val str = s"for(let x of document.getElementsByClassName(\"hilight\")) { x.classList.remove(\"hilight\"); }"
    wv.getEngine.executeScript(str)

  def selectLines(ls: Seq[Int]): Unit = {
    val cs = ls.map(l => s"for(let x of document.getElementsByClassName(\"line${l-1}\")) { x.classList.add(\"selected\"); };").mkString("\n")
    val str = s"for(let x of document.getElementsByClassName(\"selected\")) { x.classList.remove(\"selected\");\n}; " + cs
    Platform.runLater { () =>
      wv.getEngine.executeScript(str)
    }
  }

  def changedLines(ls: Set[Int]): Unit = {
    val cs = ls.map(l => s"for(let x of document.getElementsByClassName(\"line${l-1}\")) { x.classList.add(\"changed\"); x.classList.add(\"hasData\"); };\n").mkString("\n")
    val str = s"for(let x of document.getElementsByClassName(\"changed\")) { x.classList.remove(\"changed\");\n}; " + cs
    Platform.runLater { () =>
      wv.getEngine.executeScript(str)
    }
  }

  val htmlFile: File = File.createTempFile("p6dr-",".html")
  htmlFile.deleteOnExit()

  val style: String =
    """
    table {
      white-space: break-spaces;
      font-family: ui-monospace,SFMono-Regular,SF Mono,Menlo,Consolas,Liberation Mono,monospace;
    }
    .blob-num::before {
      content: attr(data-line-number);
    }
    .blob-num {
      cursor: pointer;
      vertical-align: text-top;
      min-width: 40px;
    }
    .kw {
      color: rgb(207, 34, 46);
    }
    .lit {
      color: rgb(5, 80, 174);
    }
    .op {
      color: rgb(130, 80, 223);
    }
    .com, .mlcom, .str, .mlstr {
      color: rgb(110, 119, 129);
    }
    .lit {
      color: rgb(5, 80, 174);
    }
    .hasData {
      color: rgb(5, 80, 174);
    }
    .hasData:hover {
      text-decoration: underline;
    }
    .selected {
      background-color: rgb(187, 248, 255);
    }
    .hilight {
      background-color: rgb(255, 242, 197);
    }
    .changed .blob-num {
      background-color: rgb(255, 255, 174)
    }
    """.stripMargin


  val script =
    s"""
      |<script type="text/javascript">
      |  function select(ln) {
      |    javaConnector.select("${srcf.getName}", ln);
      |  };
      |</script>
    """.stripMargin

  private def prepareFile(): Unit =
    val src = Source.fromFile(srcf)
    val output = new PrintWriter(htmlFile)

    output.println("<html><head><meta charset=\"UTF-8\"><style>")
    output.println(style)
    output.println("</style></head><body>")

    output.println("<table><tbody>")
    var state = JavaLexer.State.Normal
    for (l,i) <- src.getLines().zipWithIndex do
      output.print(s"<tr class=\"line$i\" onclick=\"select(${i+1});\"><td class=\"blob-num\" data-line-number=\"${i+1}\"></td>")
      output.print("<td>")
      val (ll,s) = JavaLexer.lexLine(l,st = state)
      state = s
      for (s,k) <- ll do
        output.print(s"<span class=\"$k\">$s</span>")
      output.println("</td></tr>")
    output.println("</tbody></table>")
    output.println(script)
    output.println("</body>\n</html>")

    src.close()
    output.close()


  private def init():Unit =
    prepareFile()
//    println(htmlFile.getPath)
    wv.getEngine.load(s"file://${htmlFile.getPath}" ) //loadContent(processedFile, "text/html")
    wv.setMaxHeight(10000)
    wv.setMaxWidth(10000)

  init()
}
