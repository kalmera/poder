package põder.framework.gui
import com.mxgraph.layout.hierarchical.mxHierarchicalLayout
import com.mxgraph.model.mxCell
import com.mxgraph.util.{mxConstants, mxEvent, mxEventObject}
import com.mxgraph.util.mxEventSource.mxIEventListener
import com.mxgraph.view.*
import javafx.application.Platform
import põder.framework.cfg.{Edge, Graph, LNode, MethodInfo, MethodNode, Node}
import põder.framework.gui.DynGraph.fileLineMap
import scalafx.scene.control.Tab

import scala.collection.immutable.Set
import scala.collection.mutable
import scala.util.matching.Regex

def addStyle(s:String, n:String):String =
  if !s.eq(null) && s.endsWith(";") then
    s++n
  else if s.eq(null) then
    n
  else
    s++";"++n
def removeStyle(s:String, n:String*):String =
  n.foldLeft(s){
    case (s1,n1) =>
      s1.replaceAll(Regex.quote(n1)++"([^;])*;","")
  }
def changeStyle(s: String, n: (String, String)*): String =
  n.foldLeft(s) { case (s1,(n1,v1)) =>
    s1.replaceAll("("++Regex.quote(n1)++")=([^;])*;","$1="++v1++";")
  }

object DynGraph {
  private val gs: mutable.Map[MethodInfo, DynGraph] = mutable.Map.empty
  def graphFromMI(n:MethodInfo): Option[DynGraph] = gs.get(n)
  val fileLineMap: mutable.Map[(String,Int),Set[MethodNode]] = mutable.Map.empty
  def allGraphs(): Seq[DynGraph] = gs.values.toSeq
}

final class DynGraph(
    val mi:MethodInfo,
    select: Set[MethodNode] => Unit,
    selecte: (MethodNode, MethodNode, Edge) => Unit)
  extends mxGraph:
  var sourceTabComponent: Option[Tab] = None
  var graphTabComponent: Option[Tab] = None
  var registerControl: Option[Node => Unit] = None

  private val nodes: mutable.Map[MethodNode, mxCell] = mutable.Map.empty
  private val cellsNodes: mutable.Map[Any, MethodNode] = mutable.Map.empty

  private val nodeEdges: mutable.Map[MethodNode, Set[(MethodNode, MethodNode, Edge)]] = mutable.Map.empty

  private val edges: mutable.Map[(MethodNode, MethodNode, Edge), mxCell] = mutable.Map.empty
  private val cellEdges: mutable.Map[Any,(MethodNode, MethodNode, Edge)] = mutable.Map.empty

  def getNodes: Map[MethodNode,mxCell] = nodes.toMap

  override def isCellSelectable(cell: Any): Boolean =
    val state = view.getState(cell)
    val style = if state != null then state.getStyle else this.getCellStyle(cell)
    super.isCellSelectable(cell) && !isCellLocked(cell) && !style.containsKey("nonselectable")

  getSelectionModel.addListener(mxEvent.CHANGE, new mxIEventListener {
    var oldStyles = Set.empty[mxCell]

    override def invoke(sender: scala.Any, evt: mxEventObject): Unit = {
      getModel.beginUpdate()
      def removeSelectedStyle(k: mxCell): Unit = {
        val r = removeStyle(k.getStyle, "strokeColor", "strokeWidth")
        k.setStyle(addStyle(r, "strokeColor=white;"))
      }

      sender match {
        case s: mxGraphSelectionModel =>
          for cell <- s.getCells do {
            if cellsNodes contains cell then {
              oldStyles.foreach(removeSelectedStyle)
              val c = cell.asInstanceOf[mxCell]
              oldStyles = Set(c)
              c.setStyle(addStyle(c.getStyle, Graph.selectNodeStyle))
              select(Set(cellsNodes(cell)))
              refresh()
            } else cell match {
              case c: mxCell if cellEdges contains c =>
                oldStyles.foreach(removeSelectedStyle)
                oldStyles = Set(c)
                c.setStyle(addStyle(c.getStyle, Graph.selectNodeStyle))
                val (f,t,e) = cellEdges(c)
                selecte(f,t,e)
                refresh()
              case _ =>
            }
          }
      }
      getModel.endUpdate()
    }
  })

  getModel.beginUpdate()
  private val estyle = getStylesheet.getDefaultEdgeStyle
  private val vstyle = getStylesheet.getDefaultVertexStyle
  vstyle.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_RECTANGLE)
  vstyle.put(mxConstants.STYLE_PERIMETER, mxPerimeter.RectanglePerimeter)
  vstyle.put(mxConstants.STYLE_FONTCOLOR, "#111")
  vstyle.put(mxConstants.STYLE_FONTSIZE, Integer.valueOf(14))
  vstyle.put(mxConstants.STYLE_FILLCOLOR, "#fff")
  vstyle.put(mxConstants.STYLE_ROUNDED, Boolean.box(true))
  vstyle.put(mxConstants.STYLE_RESIZABLE, Boolean.box(false))
  vstyle.put(mxConstants.STYLE_AUTOSIZE, Boolean.box(true))

  estyle.put("nonselectable", Boolean.box(true))
  estyle.put(mxConstants.STYLE_AUTOSIZE, Boolean.box(true))
  estyle.put(mxConstants.STYLE_ROUNDED, Boolean.box(true))
  estyle.put(mxConstants.STYLE_EDGE, mxConstants.EDGESTYLE_ENTITY_RELATION)
  estyle.put(mxConstants.STYLE_STROKEWIDTH, Integer.valueOf(2))

  private val parent = getDefaultParent

  private val lo = new mxHierarchicalLayout(this)
  lo.setIntraCellSpacing(10.0)
  lo.setInterRankCellSpacing(25.0)
  lo.setInterHierarchySpacing(10.0)
  lo.setParallelEdgeSpacing(6.0)
  lo.setResizeParent(true)
  lo.setFineTuning(true)

  setCellsEditable(false)
  setCellsBendable(false)
  setCellsMovable(false)
  setCellsDeletable(false)
  setAllowDanglingEdges(false)
  setAllowLoops(false)
  setCellsCloneable(false)
  setCellsDisconnectable(false)
  setDropEnabled(false)
  setSplitEnabled(false)
  setCellsBendable(false)
  getModel.endUpdate()

  private val edgesToAdd: mutable.Map[MethodNode,Set[(Edge,MethodNode)]] = mutable.Map.empty

  def addEdges(mn:MethodNode, es:Set[(Edge,MethodNode)]): Unit =
    val os = edgesToAdd.getOrElse(mn, Set.empty)
    edgesToAdd.update(mn, os ++ es)

  private def addNode(n: MethodNode): Unit =
    if !(nodes contains n) then
      n.linenr match
        case Some(line) =>
          val file = n.methodInfo.sourceFile
          if !(fileLineMap contains ((file,line))) then
            fileLineMap.update((file,line),Set(n))
          else
            val oldns = fileLineMap((file,line))
            if oldns.map(_.id).min == n.id then
              fileLineMap.update((file,line),oldns + n)
            if oldns.map(_.id).min > n.id then
              fileLineMap.update((file,line),Set(n))
        case None => ()
      val dtext = n.toString
      val nwidth = dtext.length * 12 + 20
      val nheight = if dtext.isEmpty then 20.0 else 25.0
      val o = insertVertex(parent, null, dtext, 10, 20, nwidth, nheight, Graph.defaultNodeStyle)
      nodes += n -> o.asInstanceOf[mxCell]
      cellsNodes += o -> n
      registerControl.foreach(f => f(n))

  private def removeNodeOutput(n:MethodNode): Unit =
    if nodeEdges contains n then
      for e <- nodeEdges(n) if e._2==n do
        val c = edges.remove(e)
        c.foreach{ c =>
          c.removeFromParent()
          cellEdges.remove(c)
        }
      nodeEdges.remove(n)

  private def addEdge(fr: MethodNode, to:MethodNode, ed:Edge): Unit =
    if !(edges contains (fr,to,ed)) then
      nodeEdges += fr -> (nodeEdges.getOrElse(fr,Set.empty) + ((fr,to,ed)))
      nodeEdges += to -> (nodeEdges.getOrElse(to,Set.empty) + ((fr,to,ed)))
      val dtext = ed.toString.split('\n')
      val nwidth = dtext.map(_.length).max * 8 + 10
      val nheight = dtext.length * 15 + 8
      val en = insertVertex(parent, null, ed.toString, 0, 0, nwidth, nheight, Graph.defaultEdgeStyle)
      edges += (fr,to,ed) -> en.asInstanceOf[mxCell]
      cellEdges += en.asInstanceOf[mxCell] -> (fr,to,ed)
      insertEdge(parent, null, "", nodes(fr), en, "endArrow=none")
      if (!to.is_exn) then
        insertEdge(parent, null, "", en, nodes(to), "")

  def update(): Unit =
    Platform.runLater { () =>
      getModel.beginUpdate()
      edgesToAdd.keys.foreach(n =>
        addNode(n)
        removeNodeOutput(n)
      )
      edgesToAdd.values.foreach(_.foreach(n => addNode(n._2)))
      edgesToAdd.foreach {
        case (to, es) => es.foreach { case (e, from) => addEdge(from, to, e) }
      }
      getModel.endUpdate()
      lo.execute(getDefaultParent)
      refresh()

    }

  def registerMI(): Unit =
    DynGraph.gs += mi -> this

  registerMI()