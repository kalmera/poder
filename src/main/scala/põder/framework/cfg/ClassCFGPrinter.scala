package põder.framework.cfg

import põder.util.{Cell, Cell0, CellConv}
import põder.util.typ.{MethodType, Parser}
import org.objectweb.asm.*

import scala.collection.mutable
import scala.language.postfixOps
import scala.util.{Left, Right}


// Here, we read in the method `name` and return it to the class-visitor using `ret`.
class MethodCFGPrinter(mi: MethodInfo, ret: Graph => Unit, exceptions: Boolean)
  extends MethodVisitor(Opcodes.ASM9)
:
  // Nodes & edges
  type N = MethodNode

  // node sets & edge set
  private val es = mutable.Map.empty[N,Set[(Edge,N)]]
  def addEdge(fr:N, e:Edge, to: N): Unit =
    val os = es.getOrElse(to,Set.empty)
    es.update(to, os+((e,fr)))

  private var next_line: Option[Int] = None
  private var id_counter = 0
  // Creating new nodes.
  private def newNode(): N =
    if nextIsInvariant then
      cur.annot = List(lastLDC.asInstanceOf[String])
      nextIsInvariant = false
      cur
    else
      val node = new LNode(mi,id_counter)
      id_counter += 1
      node.linenr = next_line
      node

  private def newNode(s:String): N =
    val node = newNode()
    node.str = s
    node

  private def newNode(label:Label): N =
    val node =  newNode()
    node.labels += label.toString
    node

  // We have a map from our labels to nodes.
  // Although the labels are not strictly necessary,
  // we add them as they appear in the instructions.
  private val labels = mutable.Map.empty[Label, N]
  private def labelNode: Label => N = { l =>
    labels.getOrElseUpdate(l, newNode(l))
  }

  // special nodes for start end return
  var startNode  = newNode(s"start(${mi.mthd})")
  var secondNode = newNode("")
  var returnNode = newNode(s"return(${mi.mthd})")
  var exceptionNode = newNode(s"ex(${mi.mthd})")

  // a set to keep track of all ATHROW nodes
  var athrowNodes = mutable.Set.empty[N]

  // ex
  private val exTbl = mutable.Buffer.empty[(Label, Label, Label, String)]

  // This is the current or the previous node, depending on your perspective.
  private var cur = secondNode
  private var lab: Option[String] = None

  // A helper function that adds edges to the graph.
  var equeue = mutable.Queue.empty[(N, Either[Instr,Annot], N)]
  private def pushEdgesInQueue(): Unit =
    if equeue.nonEmpty then
      val first = equeue.head._1
      val last = equeue.last._3
      addEdge(first, BasicBlock(equeue.map(_._2).toList), last)
      // If the BasicBlock (treated as one label node) ends with ATHROW, add the preceding node as an ATHROW node
      if equeue.last._2 == Left(ATHROW) then
        athrowNodes += first
      equeue.clear()
  private def processEdge(from: N, to: N, s:Either[Instr,Annot], ft: Boolean = true): Unit =
    to.varNames = from.varNames
    s match
      case Left(instr) =>
        instr match
          case i: MethodInstr =>
            pushEdgesInQueue()
            addEdge(from, Returns(i), to)
          case i: JumpInstr =>
            pushEdgesInQueue()
            if ft then
              addEdge(from, Fall(i), to)
            else
              addEdge(from, Jump(i), to)
          case _ =>
            equeue.enqueue((from, s, to))
            if instr.isInstanceOf[RetInstr] then
              pushEdgesInQueue()
      case Right(annot) =>
        val _ = equeue.enqueue((from, s, to))


  // As labels come after instructions, we cannot push edges into the graph immediately.
  // First, we store the edge in `old` and then update the to node, if necessary.
  private var old: Option[(N,N,Either[Instr,Annot])] = None

  // This method pushes the `old` stored edge into the graph. It is used when we want to
  // clean up `old` to store a new edge.
  private def pushOld(): Unit =
    for (from,to,data) <- old do
      from.lastLabel = lab
      data match
        case Left(v) if v.isInstanceOf[RetInstr] =>
          processEdge(from, returnNode , data)
        case _ =>
          processEdge(from, to, data)
          cur = to
    old = None

  def propagateLineNr(start:N): Unit =
    val visited = mutable.Set.empty[Node]
    def prop(s: N, line: Option[Int]): Unit =
      if s.linenr.isDefined then s.near_linenr = s.linenr
      if s.near_linenr.isEmpty then s.near_linenr = line
      visited += s
      val newline = if s.linenr isDefined then s.linenr else line
      for (to,eds) <- es; (_,fr) <- eds if fr == s && !visited.contains(to) do
        prop(to, newline)
    prop(start,start.linenr)


  def propagateLocalVarName(i:Int, n:String, t:põder.util.typ.Type, start:Label, end: Label): Unit =
    val visited = mutable.Set.empty[N]
    def prop(s: N): Unit =
      visited += s
      s.varNames += i -> n
      for (to,eds) <- es; (ed,fr) <- eds if fr == s && !visited.contains(to) do
        to.stackSize = ed.calcStack(fr.stackSize)
        prop(to)
    val s = labelNode(start)
    if s == secondNode then
      prop(startNode)
    else
      prop(s)


  // Called at the end of the method.
  private def pushEnd(): Unit =
    if old.isEmpty && es.isEmpty && mi.cls  == "java/lang/Throwable" && mi.mthd == "fillInStackTrace" then
      // some library methods have no implementation (empty list of instructions)
      // these should be handeled using isBuiltinFunction or by adding code here
      val mid = newNode()
      addEdge(secondNode,BasicBlock(List(Left(LDC(Cell("Põder-stack-trace-object"))))),mid)
      old = Some((mid,returnNode,Left(xRETURN(A))))
    else
      old = old.map{case (x,y,z) => (x,returnNode,z)}
    pushOld()
    pushEdgesInQueue()
    addEdge(startNode, Start(mi.locals, mi.stackSize, mi.access), secondNode)

    es.mapValuesInPlace{ case (to,eds) => eds.filter{
      _._1  match
        case Fall(GOTO(_)) => false
        case BasicBlock(block) => {
          block.last match
            case Left(ATHROW) => false
            case _ => true
        }
        case _ => true
    }}
    es.filterInPlace{case (to,eds) => eds.nonEmpty}

    for (l,n) <- labels do
      n.labels += l.toString
    var ns: Set[N] = Set(exceptionNode) ++ (for (to,eds) <- es; (_,fr) <- eds; x <- Set(fr, to) yield x)

    startNode.is_start   = true
    returnNode.is_return = true
    exceptionNode.is_exn = true

    var n = 0
    val labelSeq: Map[String, Int] = labelSeqBuf.map{q => n+=1; q -> n}.toMap

    for (to,eds) <- es; (ed,fr) <- eds if !fr.is_start do
      val is = ed match
        case BasicBlock(x) => x
        case Jump(i) => Seq()
        case Fall(i) => Seq()
        case Start(_, _, _) => Seq()
        case Returns(i) => Seq(Left(i))
        case Throws(i, _) => Seq()
      if is nonEmpty then
        val exs = mutable.Set.empty[String]
        val lastLabel = fr.lastLabel
        val filteredExTbl = exTbl.filter{case (st, en, gt, ex) => labelSeq(lastLabel.get) >= labelSeq(st.toString) && labelSeq(lastLabel.get) < labelSeq(en.toString)}
        val handlers = filteredExTbl.map(_._3).distinct // ordered set of all exception handlers
        var exFromNode = fr
        for handler <- handlers do
          val toNode = labelNode(handler)
          // iterate over all the exceptions leading to this handler
          for exEntry <- filteredExTbl if exEntry._3 == handler do
            if exceptions || is.last == Left(ATHROW) || is.last == Left(MONITOREXIT) then
              addEdge(exFromNode, Throws[Annot](is.toList, Left(exEntry._4)), toNode)
              exs += exEntry._4
          if handler != handlers.last then
            // for all handlers but the last one, add a label node and a new mid-node
            val newExNode = newNode()
            newExNode.linenr = exFromNode.linenr
            ns += newExNode
            addEdge(exFromNode, Throws[Annot](is.toList, Right(exs.toSet)), newExNode)
            exFromNode = newExNode
            exs.clear()
          else
            // for the last handler, only add the label node, no new mid-node
            addEdge(exFromNode, Throws[Annot](is.toList, Right(exs.toSet)), exceptionNode)


        if (exceptions || is.last == Left(ATHROW)|| is.last == Left(MONITOREXIT)) && handlers.isEmpty then
          addEdge(fr, Throws[Annot](is.toList, Right(exs.toSet)), exceptionNode)

    local_annots foreach {
      case (l, e, i, m, t) =>
        propagateLocalVarName(i,m,t,l,e)
    }

    startNode.linenr = Some(min_line-1)
    secondNode.linenr = Some(min_line-1)
    returnNode.linenr = Some(max_line)

    propagateLineNr(startNode)

//    Node.registerMethod(cls,mthd,typ,startNode, returnNode)
    ret(new Graph(ns, es.toMap, startNode, returnNode, exceptionNode, athrowNodes.toSet, mi))

  // Called when reading in the instruction `s`.
  private def pushNextEdge(s:Instr): Unit =
    pushOld()
    old = Some(cur, newNode(""), Left(s))

  private val local_annots = mutable.Buffer.empty[(Label, Label, Int,String,põder.util.typ.Type)]
  private def addLocal(i:Int, n:String, t:põder.util.typ.Type, start:Label, end: Label) =
    local_annots += ((start, end, i, n, t))

  private val labelSeqBuf = mutable.Buffer.empty[String]
  // Called when reading in a label.
  private def pushLabel(l:Label): Unit =
    labelSeqBuf += l.toString
    old = old.map { case
      (from, to, d) =>
      d match
        case Left(v) if !v.isInstanceOf[RetInstr] =>
          val nto = labelNode(l)
          (from, nto, d)
        case _ =>
          (from, to, d)
    }


  // Visitor functions

  // start of method code
  override def visitCode(): Unit = {
  }

  // generic instructions
  override def visitInsn(opcode: Int): Unit =
    val instr = InstrUtil.INSTR(opcode)()
    if instr.isInstanceOf[RetInstr] then
      pushNextEdge(instr)
      old = old.map((f,_,i) => (f,returnNode,i))
    else
      pushNextEdge(instr)


  // ??? I do not know what this is
  override def visitFrame(`type`: Int, nLocal: Int, local: Array[AnyRef], nStack: Int, stack: Array[AnyRef]): Unit = {
//    printf("Frame(%d, %d, %s, %s, %s)\n", `type`, nLocal, local.mkString("[", ", ", "]"), nStack, stack.mkString("[",", ", "]"))
  }

  // field instructions
  override def visitFieldInsn(opcode: Int, owner: String, name: String, desc: String): Unit =
    val i = InstrUtil.INSTR(opcode)().asInstanceOf[Instr & FieldArg]
    i.a.set(owner, name, desc) // add the argument
    pushNextEdge(i)

  // increment instructions
  override def visitIincInsn(`var`: Int, increment: Int): Unit =
    pushNextEdge(IINC(Cell((`var`.toShort, increment.toShort))))

  // integer instructions
  override def visitIntInsn(opcode: Int, operand: Int): Unit =
    val i = InstrUtil.INSTR(opcode)().asInstanceOf[Instr & IntArg]
    i.a.set(operand) // add the argument
    pushNextEdge(i)

  // the INVOKEDYNAMIC instruction
  override def visitInvokeDynamicInsn(name: String, desc: String, bsm: Handle, bsmArgs: AnyRef*): Unit =
    pushNextEdge(INVOKEDYNAMIC(Cell(("", name,desc)),Cell((bsm.getOwner,bsm.getName,bsm.getDesc,bsm.isInterface,bsmArgs.map(_.toString).toList))))

  // jump instructions (arg is a label)
  override def visitJumpInsn(opcode: Int, label: Label): Unit =
    val i = InstrUtil.INSTR(opcode)().asInstanceOf[Instr & LabelArg]
//    val l = lableToString(label)
    i.a.set(label.toString)
//    println(s"$i(${i.a.get}) -> ${labelNode(label)}" )
    pushNextEdge(i)                           // add the fall-through edge
    processEdge(cur, labelNode(label), Left(i), ft = false) // add the jump edge

  var max_line = -1
  var min_line = Int.MaxValue
  override def visitLineNumber(line: Int, start: Label): Unit =
    max_line = Math.max(max_line,line)
    min_line = Math.min(min_line,line)
    next_line = Some(line)
    pushLabel(start)

  // label & line number
  override def visitLabel(start: Label): Unit =
    pushLabel(start)
    if cur == secondNode then
      labels += start -> cur
    else
      cur = labelNode(start)
    pushOld()
    lab = Some(start.toString)
    pushEdgesInQueue()

  var lastLDC: Any = scala.compiletime.uninitialized

  // big constants
  override def visitLdcInsn(cst: scala.Any): Unit =
    var size: Int = 1
    cst match
      case x: Type => {
        põder.util.typ.Type.fromAsm(x) match
          case Left(value)  => lastLDC = value
          case Right(value) => lastLDC = value
      }
      case (_: Long) | (_: Double) =>
        size = 2
        lastLDC = cst
      case (_: Float) | (_: Integer) | (_:String) =>
        lastLDC = cst
    pushNextEdge(LDC(Cell(lastLDC),size))

  // ???
  override def visitLocalVariable(name: String, desc: String, signature: String, start: Label, end: Label, index: Int): Unit =
    val _ = addLocal(index, name, Parser.tpar(desc)._1, start, end)
    // printf("LocalVariable(%s, %s, %s, %s, %s, %d)\n", name, desc, signature, lableToString(start), lableToString(end), index)

  def lableToString(l:Label): String =
    l.toString

  // lookup switch instruction
  override def visitLookupSwitchInsn(dflt: Label, keys: Array[Int], labels: Array[Label]): Unit =
    pushOld()
    val dflt_str = lableToString(dflt)
    val labs = labels.map(lableToString)
    for ((i, ls),l) <- keys.view.zip(labs).zip(labels) do
      val instr = LOOKUPSWITCH(Cell0((dflt_str, Array(i), Array(ls))))
      processEdge(cur, labelNode(l), Left(instr), ft = false)
    val instr = LOOKUPSWITCH(Cell0((dflt_str, keys, labs)))
    processEdge(cur, labelNode(dflt), Left(instr), ft = true)

  var nextIsInvariant = false

  // method instruction
  override def visitMethodInsn(opcode: Int, owner: String, name: String, desc: String, itf: Boolean): Unit =
    if owner=="P6der" && name=="invariant" then
      nextIsInvariant = true
      // TODO: Remove nop
      old = None // Some((old.get._1, old.get._2, Left[Instr,Annot](NOP)))
    else
      val i = InstrUtil.INSTR(opcode)().asInstanceOf[Instr & MethodArg]
      i.a.set(owner, name, desc)
      pushNextEdge(i)

  // instrcutions with variable arguments
  override def visitVarInsn(opcode: Int, `var`: Int): Unit =
    val i = InstrUtil.INSTR(opcode)().asInstanceOf[Instr & VarArg]
    i.a.set(`var`.toByte)
    pushNextEdge(i)

  // instructions with type arguments
  override def visitTypeInsn(opcode: Int, `type`: String): Unit =
    val i = InstrUtil.INSTR(opcode)().asInstanceOf[Instr & TypeArg]
    i.a.set(`type`)
    pushNextEdge(i)

  override def visitTryCatchBlock(start: Label, end: Label, handler: Label, `type`: String): Unit =
    exTbl += ((start, end, handler, `type`))

  // table switch instructions
  override def visitTableSwitchInsn(min: Int, max: Int, dflt: Label, labels: Label*): Unit =
    pushOld()
    val dflt_str = lableToString(dflt)
    val labs = labels.map(lableToString).toArray
    for (i,l) <- min.to(max).zip(labels) do
      val instr = TABLESWITCH(Cell0((i,i,dflt_str,labs)))
      processEdge(cur, labelNode(l), Left(instr), ft = false)
    val instr = TABLESWITCH(Cell0((min,max,dflt_str,labs)))
    processEdge(cur, labelNode(dflt), Left(instr), ft = true)

  override def visitParameter(name: String, access: Int): Unit = {
    //  printf("Parameter(%s, %d)\n", name, access)
  }

  // multianewarray instruction --- need an example
  override def visitMultiANewArrayInsn(desc: String, dims: Int): Unit =
    pushNextEdge(MULTIANEWARRAY(Cell((desc,dims.toByte))))

  override def visitMaxs(maxStack: Int, maxLocals: Int): Unit =
    mi.locals = maxLocals
    mi.stackSize = maxStack


  override def visitEnd(): Unit =
    pushEnd()

// Now we simply collect the CFG of methods. Later, we may also collect class shapes.
class ClassCFGPrinter(val exceptions: Boolean) extends ClassVisitor(Opcodes.ASM9):
  val methodGraphs = mutable.Map.empty[(String,String, MethodType), Graph]
  var packageName = ""
  var className = ""
  var superName = ""

  var sourceName = ""

  override def visit(version: Int, access: Int, name: String, signature: String, sup: String, interfaces: Array[String]): Unit =
    val classNamePat = "(.*/)([^/]*)".r
    name match
      case classNamePat(pkg, clss) => packageName = pkg
      case _ => ()
    superName = sup
    className = name

  override def visitField(access: Int, name: String, desc: String, signature: String, value: scala.Any): FieldVisitor =
//    printf("Field(%d, %s, %s, %s, %s, %s)\n", access, name, desc, signature, value)
    null

  override def visitMethod(access: Int, name: String, desc: String, signature: String, exc: Array[String]): MethodVisitor =
    val _ = Option(exc) match
      case Some(x) => x.mkString("[", ", ","]")
      case None    => "null"
//    printf("Method(%d, %s, %s, %s, %s)\n", access, name, desc, signature, ex)
    val static: Boolean = (access & Opcodes.ACC_STATIC) != 0
    val mi = MethodInfo(static,className,name,Parser.mpar(desc),sourceName,access)
    new MethodCFGPrinter(mi, methodGraphs.update((className,name, Parser.mpar(desc)),_),exceptions)

  override def visitSource(source: String, debug: String): Unit =
    sourceName = source

  override def visitEnd(): Unit = {
//    println("EndClass")
  }
