package põder.framework.cfg

import põder.util.typ.MethodType

import scala.collection.mutable

case class MethodInfo(
  var is_static: Boolean = false,
  var cls: String = "not set",
  var mthd: String = "not set",
  var mthdTyp: MethodType = null,
  var sourceFile: String = "not set",
  var access: Int = -1,
  var locals: Int = -1,
  var stackSize: Int = -1)
    extends Serializable

//sealed
trait Node extends Serializable
case class GNode(ty:String) extends Node
abstract class MethodNode extends Node:
  val id: Int
  val methodInfo: MethodInfo
  var str: String
  val labels: mutable.Set[String]
  var lastLabel: Option[String]
  var is_start: Boolean
  var is_return: Boolean
  var is_exn: Boolean
  var offset: Option[Int]
  var linenr: Option[Int]
  var near_linenr: Option[Int]
  var varNames: Map[Int,String]
  var annot: List[AnyRef]
  var stackSize: Int

case class MethodNodeWrapper(override val methodInfo: MethodInfo, n:Node) extends MethodNode:
  val id: Int = n.hashCode()
  var str: String = n.toString
  override def toString: String = str
  val labels: mutable.Set[String] = mutable.Set.empty
  var lastLabel: Option[String] = None
  var is_start: Boolean = false
  var is_return: Boolean = false
  var is_exn: Boolean = false
  var offset: Option[Int] = None
  var linenr: Option[Int] = None
  var near_linenr: Option[Int] = None
  var varNames: Map[Int, String] = Map.empty
  var annot: List[AnyRef] = List.empty
  var stackSize: Int = 0

case class LNode(val methodInfo: MethodInfo, val id: Int) extends MethodNode:
  var str: String = ""
  val labels: mutable.Set[String] = mutable.Set.empty
  var lastLabel: Option[String] = None
  var is_start: Boolean = false
  var is_return: Boolean = false
  var is_exn: Boolean = false
  var offset: Option[Int] = None
  var linenr: Option[Int] = None
  var near_linenr: Option[Int] = None
  var varNames: Map[Int,String] = Map()
  var annot: List[AnyRef] = List()
  var stackSize: Int = -1
  override def toString: String =
    id.toString
//    s"$id:${methodInfo.cls}:${methodInfo.mthd}:$near_linenr"
//    str++(if (annot.isEmpty) "" else annot.mkString("[",",","]"))
