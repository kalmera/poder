package põder.framework.cfg

import com.mxgraph.layout.hierarchical.mxHierarchicalLayout
import com.mxgraph.model.mxCell
import com.mxgraph.util.{mxConstants, mxEvent, mxEventObject}
import com.mxgraph.util.mxEventSource.mxIEventListener
import com.mxgraph.view.{mxGraph, mxGraphSelectionModel, mxPerimeter}
import javafx.application.Platform
import põder.framework
import põder.util.typ.MethodType
import põder.framework.cfg.Edge
import põder.framework.cfg.Graph.allGraphs
import põder.framework.gui.DynGraph

import scala.collection.mutable
import scala.util.matching.Regex
import scala.util.{Left, Right}


object Graph:
  val defaultEdgeStyle = "defaultVertex;autosize=true;fillColor=white;fontColor=black;strokeColor=white;"
  val defaultNodeStyle = "defaultVertex;autosize=true;fillColor=black;fontColor=white;"
  val selectNodeStyle = "strokeColor=orange;strokeWidth=6;"
  var allGraphs: Set[Graph] = Set.empty

class Graph
(val nodes: Set[MethodNode],
 val edges: Map[MethodNode, Set[(Edge, MethodNode)]],
 val start: MethodNode,
 val retrn: MethodNode,
 val exn:   MethodNode,
 val throwingNodes: Set[MethodNode],
 val mi: MethodInfo
) extends Serializable :

  allGraphs += this

  var current_pos: Option[MethodNode] = None
  var widen_points: Set[MethodNode] = Set.empty

  override def toString: String =
    val last = if current_pos.isDefined then " *" else ""
    mi.cls + "." + mi.mthd + mi.mthdTyp.toText + last

  def toMXG(select: Set[MethodNode] => Unit, selecte: (MethodNode, MethodNode, Edge) => Unit): DynGraph =
    val mi = nodes.head.methodInfo
    val graph = DynGraph(mi, select, selecte)
    for (to, es) <- edges do
      graph.addEdges(to, es)
    graph.update()
    Platform.runLater(() =>
      graph.getModel.beginUpdate()
      current_pos.foreach( n =>
        graph.getNodes.get(n).foreach( v =>
          v.setStyle(framework.gui.changeStyle(v.getStyle, "fillColor" -> "#FF0000", "fontColor" -> "white"))
        ))
      widen_points.foreach(n =>
        graph.getNodes.get(n).foreach(v =>
          v.setStyle(framework.gui.changeStyle(v.getStyle, "fillColor" -> "#0000FF", "fontColor" -> "white"))
        ))
      graph.getModel.endUpdate()
      graph.refresh()
    )
    graph


abstract class ProgramCFG {
  var startMethods: Set[(String, MethodType)] = Set.empty
  val next: mutable.Map[Node, Set[(Edge, Node)]]
  val prev: mutable.Map[Node, Set[(Edge, Node)]]
  val usedMeths: mutable.Set[(String, String, MethodType)]

  var basePath: String
  var mainPath: String
  var mainClass: String

  def startVarsFun(c: String, m: String, t: MethodType): MethodNode
  def returnVarsFun(c: String, m: String, t: MethodType): MethodNode
  def getMethodThrowingNodes(c: String, m: String, t: MethodType): Set[MethodNode]

  def getMethod(c: String, m: String, t: MethodType): Graph
}
