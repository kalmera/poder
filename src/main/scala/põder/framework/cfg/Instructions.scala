package põder.framework.cfg

import scala.language.implicitConversions
import põder.util.Cell
import põder.util.Cell0
import põder.util.CellConv
import põder.util.typ._

// Many instructions range over simple types.
// To collect those instructions together, lets define simple types.
sealed abstract class SimpleType extends Serializable:
  val size: Int = 1
sealed abstract class SimpleValType        extends SimpleType
sealed abstract class SimpleWholeType      extends SimpleValType
sealed abstract class SimpleSmallWholeType extends SimpleWholeType
sealed abstract class SimpleBigWholeType   extends SimpleWholeType
sealed abstract class SimpleFloatType      extends SimpleValType
case object I extends SimpleBigWholeType    // Int
case object L extends SimpleBigWholeType:   // Long
  override val size: Int = 2
case object B extends SimpleSmallWholeType  // Byte
case object S extends SimpleSmallWholeType  // Short
case object C extends SimpleSmallWholeType  // Char
case object F extends SimpleFloatType       // Float
case object D extends SimpleFloatType:      // Double
  override val size: Int = 2
case object A extends SimpleType            // Object (or Array?)

def type2SimpleType(t: TypeNV):SimpleType =
  t match {
    case BoolT   => B
    case CharT   => C
    case ByteT   => B
    case ShortT  => S
    case IntT    => I
    case FloatT  => F
    case LongT   => L
    case DoubleT => D
    case ObjectT(_) => A
    case ArrayT(_)  => A
  }

def type2size(t:Type): Int =
  t match {
    case VoidT   => 0
    case LongT   => 2
    case DoubleT => 2
    case _       => 1
  }

// The class reader visitor is arranged based instruction arguments.
// For int argumented instructions, there is one callback that
// gives you the opcode and the integer argument.
// We, therefore, define traits to provide instructions with their arguments.
//
trait Arg extends Serializable

trait IntArg extends Arg:
  var a: Cell[Int]

trait FieldArg extends Arg:
  var a: Cell[(String,String,String)]
  def fieldType: Option[TypeNV] = a.get.map(a => Parser.tnvpar(a._3)._1)
  def fieldSize: Int = fieldType.fold(0)(type2size(_))

trait MethodArg extends Arg:
  var a: Cell[(String,String,String)]
  override def toString: String =
    if a.get.isDefined then
      s"${this.getClass.getSimpleName}(${a._1},${a._2})"
    else
      super.toString
  def methodType: Option[MethodType] = a.get.map(a => Parser.mpar(a._3))
  def argsSize: Int =
    methodType.fold(0)(_.args.map(type2size(_)).sum)

trait TypeArg extends Arg:
  var a: Cell[String]

trait VarArg extends Arg:
  var a: Cell[Byte]

trait LabelArg extends Arg:
  var a: Cell0[String]

trait ArithType:
  val x: SimpleValType
  override def toString: String =
    s"$x${this.getClass.getSimpleName.drop(1)}"

trait SimpleTypeArg:
  val x: SimpleType
  override def toString: String =
    s"$x${this.getClass.getSimpleName.drop(1)}"

trait OrderArg:
  val x: Order
  override def toString: String =
    s"${this.getClass.getSimpleName.dropRight(1)}$x"

sealed abstract class Diff extends Serializable:
  def apply(s:Int): Int
case class Absolute(i:Int) extends Diff:
  override def apply(d: Int): Int = i
case class Relative(i:Int) extends Diff:
  override def apply(d: Int): Int = i+d


// The base-class for instructions (with or without instructions).
sealed abstract class Instr extends Serializable:
  def stackDiff: Diff

// Categorization based on the ASM documentation
sealed abstract class ArrayInstr extends Instr
sealed abstract class FieldInstr extends Instr with FieldArg
sealed abstract class StackInstr (override val stackDiff: Diff) extends Instr
sealed abstract class ConstInstr (override val stackDiff: Diff) extends Instr
sealed abstract class CastInstr  (override val stackDiff: Diff) extends Instr
sealed abstract class ObjInstr   (override val stackDiff: Diff) extends Instr
sealed abstract class JumpInstr  (override val stackDiff: Diff) extends Instr
sealed abstract class RetInstr   (override val stackDiff: Diff) extends Instr
sealed abstract class ArithInstr (override val stackDiff: Diff) extends Instr
sealed abstract class MethodInstr(val isStatic: Boolean) extends Instr with MethodArg:
  final override def stackDiff: Diff =
    val mt = methodType.get
    Relative(type2size(mt.ret) - mt.args.map(type2size(_)).sum - (if isStatic then 0 else 1))


case class  xLOAD(x:SimpleType, var a: Cell[Byte] = Cell())   extends StackInstr(Relative(1)) with VarArg:
  override def toString: String =
    s"${x}LOAD(${a.apply})"
case class  xSTORE(x:SimpleType, var a: Cell[Byte] = Cell())  extends StackInstr(Relative(-1)) with VarArg:
  override def toString: String =
    s"${x}STORE(${a.apply})"
case class  xALOAD(x:SimpleType)                 extends StackInstr(Relative(-1*x.size)) with SimpleTypeArg
case class  xASTORE(x:SimpleType)                extends StackInstr(Relative(-2-x.size)) with SimpleTypeArg
case object POP                                  extends StackInstr(Relative(-1))
case object POP2                                 extends StackInstr(Relative(-2))
case object DUP                                  extends StackInstr(Relative(1))
case object DUP_X1                               extends StackInstr(Relative(1))
case object DUP_X2                               extends StackInstr(Relative(1))
case object DUP2                                 extends StackInstr(Relative(2))
case object DUP2_X1                              extends StackInstr(Relative(2))
case object DUP2_X2                              extends StackInstr(Relative(2))
case object SWAP                                 extends StackInstr(Relative(0))
case class  IINC(a:Cell[(Short,Short)])          extends StackInstr(Relative(0))
case object NOP                                  extends StackInstr(Relative(0))

// xConst instructions push small values onto the operand stack
// We provide a couple of layers: based on type ...
sealed abstract class xCONST(x:SimpleType) extends ConstInstr(Relative(x.size))

// ... and value
sealed abstract class ICONST(val n:Int)    extends xCONST(I)
sealed abstract class LCONST(val n:Long)   extends xCONST(L)
sealed abstract class FCONST(val n:Float)  extends xCONST(F)
sealed abstract class DCONST(val n:Double) extends xCONST(D)
case object           ACONST_NULL      extends xCONST(A)

case object ICONST_M1 extends ICONST(-1)
case object ICONST_0  extends ICONST(0)
case object ICONST_1  extends ICONST(1)
case object ICONST_2  extends ICONST(2)
case object ICONST_3  extends ICONST(3)
case object ICONST_4  extends ICONST(4)
case object ICONST_5  extends ICONST(5)
case object LCONST_0  extends LCONST(0)
case object LCONST_1  extends LCONST(1)
case object FCONST_0  extends FCONST(0.0f)
case object FCONST_1  extends FCONST(1.0f)
case object FCONST_2  extends FCONST(2.0f)
case object DCONST_0  extends DCONST(0.0)
case object DCONST_1  extends DCONST(1.0)

// For larger constants we have these instructions
case class BIPUSH(var a: Cell[Int] = Cell()) extends ConstInstr(Relative(1)) with IntArg
case class SIPUSH(var a: Cell[Int] = Cell()) extends ConstInstr(Relative(1)) with IntArg
case class LDC(a: Cell[Any] = Cell(), size: Int = 1) extends ConstInstr(Relative(size))

case class xADD(x:SimpleValType) extends ArithInstr(Relative(-1*x.size)) with ArithType
case class xSUB(x:SimpleValType) extends ArithInstr(Relative(-1*x.size)) with ArithType
case class xMUL(x:SimpleValType) extends ArithInstr(Relative(-1*x.size)) with ArithType
case class xDIV(x:SimpleValType) extends ArithInstr(Relative(-1*x.size)) with ArithType
case class xREM(x:SimpleValType) extends ArithInstr(Relative(-1*x.size)) with ArithType
case class xNEG(x:SimpleValType) extends ArithInstr(Relative(0)) with ArithType

case class xSHL(x:SimpleBigWholeType)  extends ArithInstr(Relative(-1*x.size)) with ArithType
case class xSHR(x:SimpleBigWholeType)  extends ArithInstr(Relative(-1*x.size)) with ArithType
case class xUSHR(x:SimpleBigWholeType) extends ArithInstr(Relative(-1*x.size)) with ArithType
case class xAND(x:SimpleBigWholeType)  extends ArithInstr(Relative(-1*x.size)) with ArithType
case class xOR(x:SimpleBigWholeType)   extends ArithInstr(Relative(-1*x.size)) with ArithType
case class xXOR(x:SimpleBigWholeType)  extends ArithInstr(Relative(-1*x.size)) with ArithType
case class xCMPL(x:SimpleFloatType)    extends ArithInstr(Relative(1-2*x.size)) with ArithType
case class xCMPG(x:SimpleFloatType)    extends ArithInstr(Relative(1-2*x.size)) with ArithType
case object LCMP                       extends ArithInstr(Relative(-3)) with ArithType {val x=L}

case class  xRETURN(x:SimpleType) extends RetInstr(Absolute(x.size))  with SimpleTypeArg
case object RETURN                extends RetInstr(Absolute(0))
case object ATHROW                extends RetInstr(Absolute(1)) // abs 1

case class x2y(x: SimpleValType, y: SimpleValType)  extends CastInstr(Relative(y.size-x.size))
case class CHECKCAST(var a:Cell[String]= Cell())    extends CastInstr(Relative(0)) with TypeArg

// Instructions like IFx vary on the branch condition
sealed abstract class Order { val str: String }
case object EQ extends Order { val str = "="  }
case object NE extends Order { val str = "!=" }
case object LT extends Order { val str = "<"  }
case object GE extends Order { val str = ">=" }
case object GT extends Order { val str = ">"  }
case object LE extends Order { val str = "<=" }


case class IFx(x: Order, var a:Cell0[String]= Cell0())                           extends JumpInstr(Relative(-1)) with LabelArg with OrderArg
case class IF_ICMPx(x: Order, var a:Cell0[String]= Cell0())                      extends JumpInstr(Relative(-2)) with LabelArg with OrderArg
case class IF_ACMPx(x: Order, var a:Cell0[String]= Cell0())                      extends JumpInstr(Relative(-2)) with LabelArg with OrderArg
case class GOTO(var a:Cell0[String]= Cell0())                                    extends JumpInstr(Relative(0)) with LabelArg:
  override def toString: String = s"GOTO"
case class IFNULL(var a:Cell0[String]= Cell0())                                  extends JumpInstr(Relative(-1)) with LabelArg
case class IFNONNULL(var a:Cell0[String]= Cell0())                               extends JumpInstr(Relative(-1)) with LabelArg
case class TABLESWITCH(var a:Cell0[(Int,Int,String,Array[String])]= Cell0())     extends JumpInstr(Relative(-1)):
  override def toString: String = s"TABLESWITCH(${a.apply._1}, ${a.apply._2})"
case class LOOKUPSWITCH(var a:Cell0[(String,Array[Int],Array[String])]= Cell0()) extends JumpInstr(Relative(-1)):
  override def toString: String = s"LOOKUPSWITCH(${a.apply._2.mkString("[",",","]")})"


case class GETSTATIC(var a: Cell[(String,String,String)]= Cell()) extends FieldInstr:
  override def stackDiff: Diff = Relative(fieldSize)
case class PUTSTATIC(var a: Cell[(String,String,String)]= Cell()) extends FieldInstr:
  override def stackDiff: Diff = Relative(-fieldSize)
case class GETFIELD(var a: Cell[(String,String,String)]= Cell())  extends FieldInstr:
  override def stackDiff: Diff = Relative(fieldSize-1)
case class PUTFIELD(var a: Cell[(String,String,String)]= Cell())  extends FieldInstr:
  override def stackDiff: Diff = Relative(-1-fieldSize)


case class INVOKEVIRTUAL(var a: Cell[(String,String,String)]= Cell())          extends MethodInstr(false)
case class INVOKESPECIAL(var a: Cell[(String,String,String)]= Cell())          extends MethodInstr(false)
case class INVOKESTATIC(var a: Cell[(String,String,String)]= Cell())           extends MethodInstr(true)
case class INVOKEINTERFACE(var a: Cell[(String,String,String)]= Cell())        extends MethodInstr(false)
case class INVOKEDYNAMIC(var a: Cell[(String,String,String)]= Cell(), var c:Cell[(String, String, String, Boolean,List[AnyRef])]=Cell()) extends MethodInstr(true):
  override def toString: String =
    s"INVOKEDYNAMIC(${a._2},${a._3})"

case class NEW(var a:Cell[(String)]= Cell())          extends ObjInstr(Relative(1)) with TypeArg
case class INSTANCEOF(var a: Cell[String]= Cell())    extends ObjInstr(Relative(0)) with TypeArg
case object MONITORENTER                              extends ObjInstr(Relative(-1))
case object MONITOREXIT                               extends ObjInstr(Relative(-1))

case class NEWARRAY(var a:Cell[Int]= Cell())             extends ArrayInstr with IntArg:
  final override def stackDiff: Diff = Relative(0)
case class ANEWARRAY(var a:Cell[String]= Cell())         extends ArrayInstr with TypeArg:
  final override def stackDiff: Diff = Relative(0)
case object ARRAYLENGTH                                  extends ArrayInstr:
  final override def stackDiff: Diff = Relative(0)
case class MULTIANEWARRAY(a:Cell[(String,Byte)]= Cell()) extends ArrayInstr:
  final override def stackDiff: Diff = Relative(a._2)

// RET and JSR are deprecated
case class DEPRECATED(n:String) extends Instr:
  override def stackDiff: Diff =
    n.toLowerCase match {
      case "ret" => Relative(0)
      case "jsr" => Relative(1)
    }

// Some instruction codes are unused (or unused in ASM)
case object UNUSED extends Instr:
  override def stackDiff: Diff = throw new NoSuchMethodException()

object InstrUtil:
  val INSTR: Array[() => Instr] =
    Array(
        () => NOP
      , () => ACONST_NULL
      , () => ICONST_M1
      , () => ICONST_0
      , () => ICONST_1
      , () => ICONST_2
      , () => ICONST_3
      , () => ICONST_4
      , () => ICONST_5
      , () => LCONST_0
      , () => LCONST_1
      , () => FCONST_0
      , () => FCONST_1
      , () => FCONST_2
      , () => DCONST_0
      , () => DCONST_1
      , () => BIPUSH()
      , () => SIPUSH()
      , () => LDC()
      , () => UNUSED // ldc_w
      , () => UNUSED // ldc_w2
      , () => xLOAD(I)//ILOAD
      , () => xLOAD(L)//LLOAD
      , () => xLOAD(F)//FLOAD
      , () => xLOAD(D)//DLOAD
      , () => xLOAD(A)//ALOAD
      , () => UNUSED//ILOAD_0
      , () => UNUSED//ILOAD_1
      , () => UNUSED//ILOAD_2
      , () => UNUSED//ILOAD_3
      , () => UNUSED//LLOAD_0
      , () => UNUSED//LLOAD_1
      , () => UNUSED//LLOAD_2
      , () => UNUSED//LLOAD_3
      , () => UNUSED//FLOAD_0
      , () => UNUSED//FLOAD_1
      , () => UNUSED//FLOAD_2
      , () => UNUSED//FLOAD_3
      , () => UNUSED//DLOAD_0
      , () => UNUSED//DLOAD_1
      , () => UNUSED//DLOAD_2
      , () => UNUSED//DLOAD_3
      , () => UNUSED//ALOAD_0
      , () => UNUSED//ALOAD_1
      , () => UNUSED//ALOAD_2
      , () => UNUSED//ALOAD_3
      , () => xALOAD(I)//IALOAD
      , () => xALOAD(L)//LALOAD
      , () => xALOAD(F)//FALOAD
      , () => xALOAD(D)//DALOAD
      , () => xALOAD(A)//AALOAD
      , () => xALOAD(B)//BALOAD
      , () => xALOAD(C)//CALOAD
      , () => xALOAD(S)//SALOAD
      , () => xSTORE(I)//ISTORE
      , () => xSTORE(L)//LSTORE
      , () => xSTORE(F)//FSTORE
      , () => xSTORE(D)//DSTORE
      , () => xSTORE(A)//ASTORE
      , () => UNUSED//xSTORE_c
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => UNUSED
      , () => xASTORE(I)//IASTORE
      , () => xASTORE(L)//LASTORE
      , () => xASTORE(F)//FASTORE
      , () => xASTORE(D)//DASTORE
      , () => xASTORE(A)//AASTORE
      , () => xASTORE(B)//BASTORE
      , () => xASTORE(C)//CASTORE
      , () => xASTORE(S)//SASTORE
      , () => POP
      , () => POP2
      , () => DUP
      , () => DUP_X1
      , () => DUP_X2
      , () => DUP2
      , () => DUP2_X1
      , () => DUP2_X2
      , () => SWAP
      , () => xADD(I)//IADD
      , () => xADD(L)//LADD
      , () => xADD(F)//FADD
      , () => xADD(D)//DADD
      , () => xSUB(I)//ISUB
      , () => xSUB(L)//LSUB
      , () => xSUB(F)//FSUB
      , () => xSUB(D)//DSUB
      , () => xMUL(I)//IMUL
      , () => xMUL(L)//LMUL
      , () => xMUL(F)//FMUL
      , () => xMUL(D)//DMUL
      , () => xDIV(I)//IDIV
      , () => xDIV(L)//LDIV
      , () => xDIV(F)//FDIV
      , () => xDIV(D)//DDIV
      , () => xREM(I)//IREM
      , () => xREM(L)//LREM
      , () => xREM(F)//FREM
      , () => xREM(D)//DREM
      , () => xNEG(I)//INEG
      , () => xNEG(L)//LNEG
      , () => xNEG(F)//FNEG
      , () => xNEG(D)//DNEG
      , () => xSHL(I)//ISHL
      , () => xSHL(L)//LSHL
      , () => xSHR(I)//ISHR
      , () => xSHR(L)//LSHR
      , () => xUSHR(I)//IUSHR
      , () => xUSHR(L)//LUSHR
      , () => xAND(I)//IAND
      , () => xAND(L)//LAND
      , () => xOR(I)//IOR
      , () => xOR(L)//LOR
      , () => xXOR(I)//IXOR
      , () => xXOR(L)//LXOR
      , () => IINC(Cell())
      , () => x2y(I,L)//I2L
      , () => x2y(I,F)//I2F
      , () => x2y(I,D)//I2D
      , () => x2y(L,I)//L2I
      , () => x2y(L,F)//L2F
      , () => x2y(L,D)//L2D
      , () => x2y(F,I)//F2I
      , () => x2y(F,L)//F2L
      , () => x2y(F,D)//F2D
      , () => x2y(D,I)//D2I
      , () => x2y(D,L)//D2L
      , () => x2y(D,F)//D2F
      , () => x2y(I,B)//I2B
      , () => x2y(I,C)//I2C
      , () => x2y(I,S)//I2S
      , () => LCMP
      , () => xCMPL(F)//FCMPL
      , () => xCMPG(F)//FCMPG
      , () => xCMPL(D)//DCMPL
      , () => xCMPG(D)//DCMPG
      , () => IFx(EQ)//IFEQ
      , () => IFx(NE)//IFNE
      , () => IFx(LT)//IFLT
      , () => IFx(GE)//IFGE
      , () => IFx(GT)//IFGT
      , () => IFx(LE)//IFLE
      , () => IF_ICMPx(EQ)//IF_ICMPEQ
      , () => IF_ICMPx(NE)//IF_ICMPNE
      , () => IF_ICMPx(LT)//IF_ICMPLT
      , () => IF_ICMPx(GE)//IF_ICMPGE
      , () => IF_ICMPx(GT)//IF_ICMPGT
      , () => IF_ICMPx(LE)//IF_ICMPLE
      , () => IF_ACMPx(EQ)//IF_ACMPEQ
      , () => IF_ACMPx(NE)//IF_ACMPNE
      , () => GOTO()
      , () => DEPRECATED("JSR")
      , () => DEPRECATED("RET")
      , () => TABLESWITCH()
      , () => LOOKUPSWITCH()
      , () => xRETURN(I)//IRETURN
      , () => xRETURN(L)//LRETURN
      , () => xRETURN(F)//FRETURN
      , () => xRETURN(D)//DRETURN
      , () => xRETURN(A)//ARETURN
      , () => RETURN
      , () => GETSTATIC()
      , () => PUTSTATIC()
      , () => GETFIELD()
      , () => PUTFIELD()
      , () => INVOKEVIRTUAL()
      , () => INVOKESPECIAL()
      , () => INVOKESTATIC()
      , () => INVOKEINTERFACE()
      , () => INVOKEDYNAMIC()
      , () => NEW()
      , () => NEWARRAY()
      , () => ANEWARRAY()
      , () => ARRAYLENGTH
      , () => ATHROW
      , () => CHECKCAST()
      , () => INSTANCEOF()
      , () => MONITORENTER
      , () => MONITOREXIT
      , () => UNUSED//wide(load/store/ret)
      , () => MULTIANEWARRAY()
      , () => IFNULL()
      , () => IFNONNULL()
    )
