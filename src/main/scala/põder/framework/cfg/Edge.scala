package põder.framework.cfg

import põder.framework.cfg.{Annot, Instr, JumpInstr, MethodInstr}

import scala.util.{Left, Right}

// For modularity reasons, the type of annotations is given as
// a parameter. Thus, different analysese may use different annotations in the future.
sealed abstract class TFType[Annot] extends Serializable:
  def calcStack(s:Int): Int

// Usually we use cfg.Annot (at this point there are no annotations defined)
type Edge = TFType[Annot]

final case class BasicBlock[Annot](var x: List[Either[Instr,Annot]]) extends TFType[Annot]:
  override def calcStack(s:Int): Int = x.foldLeft(s){
    case (s,Left(i)) => i.stackDiff(s)
    case (s,Right(_)) => s
  }
  override def toString: String =
    x.map{
      case Left(y)  => y.toString
      case Right(y) => y.toString
    }.mkString("\n")
final case class Jump[Annot](i: JumpInstr) extends TFType[Annot]:
  override def calcStack(s:Int): Int = i.stackDiff(s)
final case class Fall[Annot](i: JumpInstr) extends TFType[Annot]:
  override def calcStack(s:Int): Int = i.stackDiff(s)
final case class Start[Annot](locals: Int, stack: Int, access: Int) extends TFType[Annot]:
  override def calcStack(s:Int): Int = 0
  def accStr: String =
    var s = Set[String]()
    if (access & 0x0001) == 0x0001 then
      s += "public"
    if (access & 0x0010) == 0x0010 then
      s += "final"
    if (access & 0x0020) == 0x0020 then
      s += "synchronized"
    if (access & 0x0200) == 0x0200 then
      s += "interface"
    if (access & 0x0400) == 0x0400 then
      s += "abstract"
    if (access & 0x1000) == 0x1000 then
      s += "synthetic"
    if (access & 0x2000) == 0x2000 then
      s += "annotation"
    if (access & 0x4000) == 0x4000 then
      s += "enum"
    s.mkString(" ")

  override def toString: String =
    s"Start(locals = $locals, stack = $stack, $accStr)"
final case class Returns[Annot](i:MethodInstr) extends TFType[Annot]:
  override def calcStack(s:Int): Int = i.stackDiff(s)
  override def toString: String = /*"Returns:\n" ++ */ i.toString
final case class Throws[Annot](var i: List[Either[Instr,Annot]], e:Either[String,Set[String]]) extends TFType[Annot]:
  override def calcStack(s:Int): Int = i.foldLeft(s){
    case (s,Left(i)) => i.stackDiff(s)
    case (s,Right(_)) => s
  }
  override def toString: String =
    val b = i.map {
      case Left(y) => y.toString
      case Right(y) => y.toString
    }.mkString("\n")

    e match
      case Left(value) => s"Throws($value):\n$b"
      case Right(value) => s"Throws(¬${value.mkString("{", ", ", "}")}):\n$b"
