package põder.util

import smtlib.trees.Commands._
import smtlib.trees.CommandsResponses._
import smtlib.trees.Terms._
//
//import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.Duration
import scala.concurrent.{Await,Promise,Future,ExecutionContext, ExecutionContextExecutor}

// This is for handling the Z3 SMT solver.
object SMTHelper:
  var monitor = Option.empty[() => Unit]

  var done = false
  private var z3interpreter: smtlib.interpreters.Z3Interpreter = null
  private def z3: smtlib.interpreters.Z3Interpreter =
    if z3interpreter == null then
      z3interpreter = smtlib.interpreters.Z3Interpreter.buildDefault
    z3interpreter

  def eval(s:SExpr): SExpr = z3.eval(s)
  def reset(): Boolean =
    z3.eval(Reset()).isInstanceOf[SuccessfulResponse]
  def push(): Boolean =
    z3.eval(Push(1)).isInstanceOf[SuccessfulResponse]
  def pop(): Boolean =
    z3.eval(Pop(1)).isInstanceOf[SuccessfulResponse]
  def assert(t: Term): Boolean =
    val q = z3.eval(Assert(t))
//    val out = new PrintWriter(System.out, true)
//    SMTHelper.z3.printer.printSExpr(q, out)
//    out.println()
//    out.flush()
    q.isInstanceOf[SuccessfulResponse]

  given executionContext: ExecutionContextExecutor = ExecutionContext.global

  def sat(): Option[Boolean] =
    perfCount.sub("sat",0.5){
      Await.result(sat_future(),Duration.Inf)
    }

  private def sat_future():Future[Option[Boolean]] =
    val p = Promise[Option[Boolean]]()
    val f = Future(sat_inner())
    f.onComplete(p.tryComplete)
    ExecutionContext.global.execute(() => {
      def loop(): Unit = {
        if !p.isCompleted || !f.isCompleted then {
          monitor match {
            case Some(g) => {
              try {
                g()
                Thread.sleep(200)
                loop()
              } catch {
                case ex: Throwable =>
                  p.tryFailure(ex)
                  z3.kill()
                  z3interpreter = smtlib.interpreters.Z3Interpreter.buildDefault
                  val _ = push()
              }
            }
            case None =>
              Thread.sleep(1000)
              loop()
          }
        }
      }
      loop()
    })
    p.future

  private def sat_inner():Option[Boolean] =
    val z = z3.eval(CheckSat())
    z match
      case status: CheckSatStatus =>
        if status.status == SatStatus then
          Some(true)
        else if status.status == UnsatStatus then
          Some(false)
        else
          None
      case _ => None

  def getAssignment(vars: Seq[String]): Seq[(String,Term)] =
    def l(x:String): Term = QualifiedIdentifier(Identifier(SSymbol(x)))
    if vars.isEmpty then
      Seq.empty
    else
      val r = z3.eval(GetValue(l(vars.head), vars.tail.map(l)))
      r match
        case GetValueResponseSuccess(x) =>
          x.flatMap {
            case (QualifiedIdentifier(Identifier(SSymbol(z), _), _), y) => Seq((z, y))
            case _ => Seq.empty
          }
        case q => Seq.empty
  def declareConst(x:String, k:String): Boolean =
    z3.eval(DeclareConst(SSymbol(x),Sort(Identifier(SSymbol(k))))).isInstanceOf[SuccessfulResponse]

  def declareFun(x:String, as:Seq[String], r:String): Boolean =
    val args = as.map(k => Sort(Identifier(SSymbol(k))))
    z3.eval(DeclareFun(SSymbol(x), args, Sort(Identifier(SSymbol(r))))).isInstanceOf[SuccessfulResponse]

  def simplify(t: Term): Term =
    z3.in.write("(simplify ")
    z3.printer.printSExpr(t, z3.in)
    z3.in.write(")\n")
    z3.in.flush()
    val r = z3.parser.parseTerm
    r

