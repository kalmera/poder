package põder.util

import põder.analyses.NodeGVar

import java.io.{FileInputStream, FileOutputStream, ObjectInputStream, ObjectOutputStream}
import põder.framework.cfg.*
import põder.util.typ.MethodType

import scala.collection.mutable
import java.io.File
import java.nio.file.*

object Project:
  def apply(proj_path: Path): Option[Project] =
    val proj = proj_path.toFile
    if !proj_path.toFile.exists then
      Files.createDirectory(proj_path)

    if !proj.exists then
      return None
    val cf = Paths.get(proj_path.toString,"conf")
    if !cf.toFile.exists then
      return None
    val cfs = new FileInputStream(cf.toFile)
    val ip = new ObjectInputStream(cfs)
    val (path,lib, poder_ver, java_ver, source_base, mainClass, analysisName, exceptions, juliet, stable) =
      ip.readObject().asInstanceOf[(String, String, String, String, String, String, String, Boolean, Boolean, Boolean)]
    ip.close()
    val p = new Project(path, lib)
    if p.java_ver == java_ver then
      p.poder_ver = poder_ver
      p.java_ver = java_ver
      p.source_base = source_base
      p.mainClass = mainClass
      p.analysisName = analysisName
      p.exceptions = exceptions
      p.juliet = juliet
      p.stable = stable
      Some(p)
    else
      None

// The configuration for the analysis, serialization 
final class Project
  (val path: String,
   var lib_path : String,
   val no_store: Boolean = false)
  extends Serializable
:
  var poder_ver: String                = "1.0"
  var java_ver : String                = System.getProperty("java.version")
  val graphs  : mutable.Map[(String,String,MethodType),Graph] = mutable.Map.empty
  val superMap: mutable.Map[String, String] = mutable.Map.empty

  var source_base  = ""

  var mainClass    = "Main"
  var analysisName = "modular"
  var exceptions   = false
  var juliet       = false
  var stable       = false

  val libPrefixes = Set("java/", "javax/","org/ietf/jgss", "org/omg", "org/w3c/dom", "org/xml", "javafx/", "jdk/", "sun/nio/")
  def isLibrary(cls:String): Boolean =
    libPrefixes.exists(cls.startsWith)

  def fullMainPath: String =
    source_base ++ File.separator ++ mainClass.replace(".",File.separator) ++ ".class"

  def versionPath: String =
    File.separator ++ "poder_v" ++ poder_ver ++ "_" ++ java_ver

  def analysisPath: String =
     versionPath ++ File.separator ++ analysisName

  def createOnDisk(): Unit =
    if no_store then
      return
    val proj = Paths.get(path,analysisPath)
    if !Files.exists(proj) then Files.createDirectories(proj)
    if proj.toFile.exists then
      val cf = Paths.get(proj.toString,"conf")
      if !Files.exists(cf) then Files.createFile(cf)
      val cfs = new FileOutputStream(cf.toFile)
      val op = new ObjectOutputStream(cfs)
      val data: (String, String, String, String, String, String, String, Boolean, Boolean, Boolean) =
        (path, lib_path, poder_ver, java_ver, source_base, mainClass, analysisName, exceptions, juliet, stable)
      op.writeObject(data)
      op.close()

  def loadGraph(cls:String, mthd: String, typ: MethodType): Option[Graph] =
    if cls==null then
      return None
    def loadFile: Option[Graph] =
      val dirname = cls.replace("/", ".")++"."++mthd++typ.toShort.replace("/", ".")
      val base = if isLibrary(cls) then lib_path else path
      val d = Paths.get(base,analysisPath,dirname,"graph")
      if !d.toFile.exists then
        return None
      val cfs = new FileInputStream(d.toFile)
      val ip = new ObjectInputStream(cfs)
      val cfg = ip.readObject().asInstanceOf[Graph]
      ip.close()
      graphs += (cls,mthd,typ) -> cfg
      Some(cfg)
    if no_store then
      graphs.get((cls,mthd,typ))
    else
      loadSuper(cls)
      graphs.get((cls,mthd,typ)).orElse(loadFile)

  def storeGraph(cls:String, mthd: String, typ: MethodType, cfg: Graph): Unit =
    graphs += (cls, mthd, typ) -> cfg
    if no_store then
      return
    val dirname = cls.replace("/", ".") ++ "." ++ mthd ++ typ.toShort.replace("/", ".")
    val base = if isLibrary(cls) then lib_path else path
    val d = Paths.get(base,analysisPath,dirname)
    if !Files.exists(d) then Files.createDirectories(d)
    val gfpath = Paths.get(d.toString,"graph")
    if gfpath.toFile.exists then
      return
    val gf = Files.createFile(gfpath)
    val cfs = new FileOutputStream(gf.toFile)
    val op = new ObjectOutputStream(cfs)
    op.writeObject(cfg)
    op.close()
    storeSuper(cls, superMap(cls))

  private def loadSuper(cls:String): Unit =
    if (!no_store && cls != null) || !(superMap.keySet contains cls) then
      val base = if isLibrary(cls) then lib_path else path
      val gfpath = Paths.get(base,analysisPath, cls.replace("/", ".")++".super")
      if gfpath.toFile.exists then
        val cfs = new FileInputStream(gfpath.toFile)
        val ip = new ObjectInputStream(cfs)
        val sup = ip.readObject().asInstanceOf[String]
        ip.close()
        superMap += cls -> sup

  private def storeSuper(cls:String, sup: String): Unit =
    if no_store then
      return
    val base = if isLibrary(cls) then lib_path else path
    val gfpath = Paths.get(base,analysisPath,cls.replace("/", ".")++".super")
    if gfpath.toFile.exists then
      return
    val gf = Files.createFile(gfpath)
    val cfs = new FileOutputStream(gf.toFile)
    val op = new ObjectOutputStream(cfs)
    op.writeObject(sup)
    op.close()

  def saveResult[T](node:Node, value: T): Unit = {
    if no_store then return
    val gfpath =
      node match
        case node: MethodNode =>
          val dirname = node.methodInfo.cls.replace("/", ".") ++ "." ++ node.methodInfo.mthd ++ node.methodInfo.mthdTyp.toShort.replace("/", ".")
          val base = if isLibrary(node.methodInfo.cls) then lib_path else path
          val d = Paths.get(base, analysisPath, dirname)
          try Files.createDirectory(d) catch
            case e: FileAlreadyExistsException => ()
          Paths.get(d.toString, node.id.toString)
        case GNode(ty) =>
          val dirname = "gnodes"
          val base = if isLibrary(ty) then lib_path else path
          val d = Paths.get(base, analysisPath, dirname)
          try Files.createDirectory(d) catch
            case e: FileAlreadyExistsException => ()
          Paths.get(d.toString, ty.replace("/", "."))
    if gfpath.toFile.exists then
      Files.delete(gfpath)

    val gf = Files.createFile(gfpath)
    val cfs = new FileOutputStream(gf.toFile)
    val op = new ObjectOutputStream(cfs)
    op.writeObject(value)
    op.close()
  }

  def loadResult[T](node:Node): Option[T] =
    if no_store then None else
      val d = node match
          case GNode(ty) =>
            val dirname = "gnodes"
            val base = if isLibrary(ty) then lib_path else path
            Paths.get(base, analysisPath, dirname, ty.replace("/","."))
          case node: MethodNode =>
            val dirname = node.methodInfo.cls.replace("/", ".") ++ "." ++ node.methodInfo.mthd ++ node.methodInfo.mthdTyp.toShort.replace("/", ".")
            val base = if isLibrary(node.methodInfo.cls) then lib_path else path
            Paths.get(base, analysisPath, dirname, node.id.toString)
      if !d.toFile.exists then
        return None
      val cfs = new FileInputStream(d.toFile)
      val ip = new ObjectInputStream(cfs)

      val value = ip.readObject()
      ip.close()
      Some(value.asInstanceOf[T])

