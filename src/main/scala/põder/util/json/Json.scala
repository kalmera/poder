package põder.util.json

import javafx.event.{Event, EventHandler, EventType}
import scalafx.scene.control.TreeItem

import javax.swing.JLabel
import scala.annotation.tailrec

trait JsonF[jval]:
  def Label(s:String, n:Int, v:jval): jval
  def Obj(ms: (String,jval)*): jval
  def Obj(m: Iterable[(String,jval)]): jval
  def Arr(a: jval*): jval
  def Arr(as: Iterable[jval]): jval
  def Bool(b: Boolean): jval
  def Str(s:String): jval
  def Num(i:BigDecimal): jval
  def Null: jval

  def fromVal[T](x:T): jval

sealed abstract class JVal:
  def toText: String

class JPTreeItem(s:String, var lab:Option[(String,Int)]) extends javafx.scene.control.TreeItem[String](s)

class PTreeItem(s:String, lab:Option[(String,Int)] = None) extends TreeItem[String](JPTreeItem(s,lab))

object Json extends JsonF[JVal]:
  case class JLabel(src:String, n:Int, v:JVal) extends JVal:
    override def toText: String = v.toText

  case class JObj(m: Map[String, JVal]) extends JVal:
    override def toText: String =
      if m.isEmpty then
        "{}"
      else if m.toSeq.length == 1 then
        m.map { case (k, v) => k ++ ": " ++ v.toText }
          .mkString("{", "," , "}")
      else
        m.map { case (k, v) => k ++ ": " ++ v.toText }
          .mkString("{\n", ",\n", "}")

  case class JArr(a: Array[JVal]) extends JVal:
    override def toText: String =
      a.map(_.toText).mkString("[", "\n, ", "]")

  case class JBool(b: Boolean) extends JVal:
    override def toText: String =
      b.toString

  case class JStr(s: String) extends JVal:
    override def toText: String = s

  case class JNum(i: BigDecimal) extends JVal:
    override def toText: String = i.toString

  case object JNull extends JVal:
    override def toText: String = "null"

  def fromVal[T](y: T): JVal = y match
    case null            => JNull
    case m:Map[_,_] => JObj(m.map{case (k,v) => k.toString -> fromVal(v)})
    case q:Array[_]      => JArr(q.map(fromVal))
    case b:Boolean       => JBool(b)
    case s:String        => JStr(s)
    case i:Int           => JNum(BigDecimal(i))
    case i:BigDecimal    => JNum(i)
    case _               => JStr(y.toString)

  override def Label(src:String, n:Int, v:JVal): JVal = JLabel(src,n,v)
  override def Obj(m: (String, JVal)*): JVal = JObj(m.toMap)
  override def Obj(m: Iterable[(String, JVal)]): JVal = JObj(m.toMap)
  override def Arr(a: JVal*): JVal = JArr(a.toArray)
  override def Arr(as: Iterable[JVal]): JVal = JArr(as.toArray)
  override def Bool(b: Boolean): JVal = JBool(b)
  override def Str(s: String): JVal = JStr(s)
  override def Num(i: BigDecimal): JVal = JNum(i)
  override def Null: JVal = JNull

  @tailrec
  def isSimple(v: JVal): Boolean =
    v match {
      case JObj(o) => false
      case JArr(ar) => ar.length <= 1
      case JLabel(_,_,v) => isSimple(v)
      case _ => true
    }

  def getSimpleRef(v:JVal):(String, Option[(String,Int)]) = {
    v match
      case JLabel(s,n,v) => (v.toText, Some((s,n)))
      case JArr(a) if a.length==1 => getSimpleRef(a.head)
      case _ => (v.toText, None)
  }


  def toTreeItems(v: JVal)(using level: Int = 5): Seq[PTreeItem] =
    given Int = Math.max(0,level-1)
    v match
      case JLabel(s,n,v) =>
        val ts = toTreeItems(v)
        ts.map(_.delegate).foreach{
          case t:JPTreeItem => t.lab = Some((s,n))
          case _ => ()
        }
        ts
      case JObj(m)  =>
        var pa = List.empty[PTreeItem]
        for (n,o) <- m do
          if n.length<15 && isSimple(o) then
            val (txt, ref) = getSimpleRef(o)
            val ti = new PTreeItem(n++": "++txt, ref)
            pa :+= ti
          else
            val ti = new PTreeItem(n)
            ti.expanded = level > 0
            toTreeItems(o).foreach(ti.children.append(_))
            pa :+= ti
        pa
      case JArr(a)  =>
        a.toIndexedSeq.flatMap(toTreeItems)
//        val pa = new TreeItem("array")
//        pa.expanded = true
//        for {o <- a} {
//          toTreeItems(o).foreach(pa.children.append(_))
//        }
//        Seq(pa)
      case JBool(b) => Seq(new PTreeItem(b.toString))
      case JStr(s)  => Seq(new PTreeItem(s))
      case JNum(i)  => Seq(new PTreeItem(i.toString))
      case JNull    => Seq(new PTreeItem("null"))



object TreeItemJson extends JsonF[Seq[PTreeItem]]:
  override def Null: Seq[PTreeItem] = Seq(new PTreeItem("null"))
  override def Num(i: BigDecimal): Seq[PTreeItem] = Seq(new PTreeItem(i.toString))
  override def Str(s: String): Seq[PTreeItem] = Seq(new PTreeItem(s))
  override def Bool(b: Boolean): Seq[PTreeItem] = Seq(new PTreeItem(b.toString))
  override def Arr(a: Seq[PTreeItem]*): Seq[PTreeItem] = a.flatten
  override def Label(s: String, n: Int, v: Seq[PTreeItem]): Seq[PTreeItem] =
    v.map(_.delegate).foreach{
      case t:JPTreeItem => t.lab = Some((s,n))
      case _ => ()
    }
    v

  override def Arr(as: Iterable[Seq[PTreeItem]]): Seq[PTreeItem] = as.flatten.toSeq

  override def fromVal[T](x: T): Seq[PTreeItem] = x match
    case null            => Null
    case m:Map[_,_]      => Obj(m.map{case (k,v) => k.toString -> fromVal(v)})
    case q:Array[_]      => Arr(q.map(fromVal))
    case b:Boolean       => Bool(b)
    case s:String        => Str(s)
    case i:Int           => Num(BigDecimal(i))
    case i:BigDecimal    => Num(i)
    case _               => Str(x.toString)


  override def Obj(ms: (String, Seq[PTreeItem])*): Seq[PTreeItem] =
    Obj(ms)

  override def Obj(ms: Iterable[(String, Seq[PTreeItem])]): Seq[PTreeItem] =
    var pa = Seq.empty[PTreeItem]
    for (n,tis) <- ms do
      val ti = new PTreeItem(n)
      tis.foreach(ti.children.append(_))
      pa :+= ti
    pa
