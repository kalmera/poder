package põder.util

import põder.log.Log

import scala.Product2
import scala.collection.mutable
import scala.reflect.ClassTag

// Type-safe and serializable data cells.
// Used for Bytecode instruction arguments.
sealed trait CellT[T : ClassTag] extends Serializable:
  protected val tag: ClassTag[T] = implicitly
  protected var data: Option[T]  = None
  def set(t: T): Unit =
    if data.isEmpty then data = Some(t) else assert(false)
  def get: Option[T] = data
  def apply: T = data.get

// A cell is convertable to its value
given CellConv[T]: Conversion[CellT[T], T] with
  def apply(c: CellT[T]): T = c.apply

// Cell is for data that is expected to contain a value
final class Cell[T : ClassTag] extends CellT[T]:
  override def toString: String =
    data.fold("?")(_.toString)
  override def equals(obj: Any): Boolean =
    obj match
      case c:Cell[_] =>
        c.tag==tag &&
        c.data.asInstanceOf[Option[T]] == data
      case _ => false
  override def hashCode(): Int = data.hashCode()

object Cell:
  def apply[T:ClassTag](): Cell[T] = new Cell[T]
  def apply[T:ClassTag](t:T): Cell[T] =
    val c = new Cell[T]
    c.set(t)
    c

// Cell0 is for data that is NOT expected to contain a value
final class Cell0[T : ClassTag] extends CellT[T]:
  override def toString: String =
    data.fold("?")(_.toString)
  override def equals(obj: Any): Boolean =
    obj match
      case c:Cell0[_] =>
        c.tag==tag
      case _ => false
  override val hashCode: Int = tag.hashCode()

object Cell0:
  def apply[T:ClassTag](): Cell0[T] = new Cell0[T]
  def apply[T:ClassTag](t:T): Cell0[T] =
    val c = new Cell0[T]
    c.set(t)
    c

// Measure the performance of a sub-computation
sealed trait PerfCount:
  def sub[F](n: String, reportTime: Double = Double.MaxValue)(f: =>F): F
  def timeout(): Unit
  def timein(): Unit
  def status: String
  def statusFlat: String

// Measure the performance using nanoTime
final class NanoPerfCount extends PerfCount:
  private inline def tab(s:String): String =
    if s.isEmpty then s
    else "\t" ++ s.replaceAll("\n","\n\t")

  final class PerfTreeNode(var name: String, var parent: Option[PerfTreeNode] = None):
    val children: mutable.Map[String, PerfTreeNode] = mutable.Map.empty
    var timeNs : Long = 0
    var started: Option[Long] = None

    def status: String =
      if children.isEmpty then
        f"{ $name%s: ${timeNs/1000000000d}%.2f s }"
      else
        f"{ $name%s: ${timeNs / 1000000000d}%.2f s:\n" ++
          tab(children.values.map(_.status).mkString("\n")) ++ "}"
  private val r = new PerfTreeNode("total")
  private var c = r

  private def inTree(n:String, p: PerfTreeNode = c): Boolean =
    p.name == n || p.parent.exists(inTree(n,_))


  final inline def sub[F](n: String, reportTime: Double = Double.MaxValue)(f: =>F): F = synchronized {
    if !inTree(n) then
      c = c.children.getOrElseUpdate(n, new PerfTreeNode(n, Some(c)))
      c.started = Some(System.nanoTime())
      val r: F = f
      val t = System.nanoTime()
      c.started match
        case Some(v) =>
          val nt = t - v
          c.timeNs += nt
          if reportTime < nt / 1000000000d then
            Log.logCommon(f"Subtask $n%s took ${nt / 1000000000d}%.2f s.")
          c.started = None
        case None =>
      c = c.parent.get
      r
    else
      f
  }

  def timeout(): Unit =
    val t = System.nanoTime()
    def f(p:PerfTreeNode): Unit =
      p.started match
        case Some(v) =>
          p.timeNs += t - v
          p.started = None
        case None =>
      p.parent.foreach(f)
    f(c)

  def timein(): Unit =
    val t = System.nanoTime()
    def f(p:PerfTreeNode): Unit =
      if p.started.isEmpty then
        p.started = Some(t)
      p.parent.foreach(f)
    f(c)

  def status: String =
    r.children.values.map(_.status).mkString("\n")

  def statusFlat: String =
    val m: mutable.Map[String, Long] = mutable.Map.empty
    def add(p: PerfTreeNode): Unit =
      m.update(p.name, m.getOrElse(p.name,0L)+p.timeNs)
      p.children.values.foreach(add)
    r.children.values.foreach(add)
    m.map(p => f"${p._1}%s: ${p._2/1000000000d}%.2f s").mkString("\n")

// Do not measure the performance.
final class NoPerfCount extends PerfCount:
  override def sub[F](n: String, reportTime: Double)(f: => F): F = f
  override def timein(): Unit = ()
  override def timeout(): Unit = ()
  override def status: String = "Performance counters disabled"
  override def statusFlat: String = "Performance counters disabled"

// Static setting
val perfCount : PerfCount = NoPerfCount()
