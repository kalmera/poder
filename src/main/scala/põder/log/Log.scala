package põder.log

import javafx.application.Platform
import põder.framework.cfg.{LNode, MethodInfo, MethodNode, Node}
import scalafx.beans.property.StringProperty

import scala.collection.mutable

object Log:
  var simple_log = false // will be set by command line parameter
  var regression = false // will be set by command line parameter

  // This is for displaying a log.
  class SubLog:
    val property: StringProperty = StringProperty("")

    def enqueue(elems: String*): Unit =
      Platform.runLater(() => {
        val v = property.value
        if v.isEmpty then
          property.value = elems.mkString("\n")
        else
          property.value = v ++ "\n" ++ elems.mkString("\n")
      })

  // None is the common log and Some(n)-s are for solver unknown n
  private val logs = mutable.Map.empty[Option[Any], SubLog]
  private var currentKey: Option[Any] = None
  private var currentLog: SubLog = logs.getOrElseUpdate(currentKey, new SubLog)

  // GUI needs these properties
  def getLogCommon: StringProperty =
    if simple_log then
      return null
    logs.getOrElseUpdate(None, new SubLog).property

  // GUI needs these properties
  def getLog(n: Any): StringProperty =
    if simple_log then
      return null
    if logs contains Some(n) then
      logs(Some(n)).property
    else
      new StringProperty("(empty)")

  // This will be the call-stack of the solver so that we will know the current unknown
  private var nodes: List[Option[Any]] = List(None)

  // Call this when the solver enters the right-hand side of unknown n
  def pushNode(n: Any): Unit =
    if !simple_log then
      nodes +:= Some(n)
      if !currentKey.contains(n) then
        currentKey = Some(n)
        currentLog = logs.getOrElseUpdate(currentKey, new SubLog)

  // Call this when the solver exits the right-hand side of unknown n
  def popNode(): Unit =
    if !simple_log then
      nodes = nodes.tail
      currentKey = nodes.head
      currentLog = logs.getOrElseUpdate(currentKey, new SubLog)

  // This is for node-specific log.
  // You need to click on a node to see it.
  def log(xs: String*): Unit =
    if !simple_log then
      currentLog.enqueue(xs*)

  // The common log is usually displayed on the bottom-right.
  def logCommon(xs: String*): Unit =
    if regression then ()
    else if simple_log then
      println(xs.mkString(""))
    else if !regression then
      logs.getOrElseUpdate(None, new SubLog).enqueue(xs*)

  // This is used for regression test checks. The syntax needs to play along with regression.sh.
  // Value in msg should appear in source code on some line associated with node n.
  def logAssert(msg:String, n:Node): Unit =
    n match
      case ln: MethodNode if regression =>
        println(s"$msg;${ln.methodInfo.cls};${ln.methodInfo.mthd};${ln.near_linenr.mkString}")
      case _ =>
        ()