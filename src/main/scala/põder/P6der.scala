package põder

import java.io.{File, IOException, PrintStream}
import com.mxgraph.model.mxCell
import com.mxgraph.swing.mxGraphComponent
import javafx.application.Platform
import javafx.collections.ListChangeListener
import javafx.event
import javafx.event.EventHandler
import javafx.scene.control
import org.objectweb.asm.ClassReader
import põder.analyses.Registry
import põder.framework.{AnalysisTask, CFGAnalysis, ConstrSys, JBCAnalysis, SimpleJBCAnalysis, cfg}
import põder.framework.*
import põder.framework.cfg.*
import põder.framework.gui.{DynGraph, SourceFileView}
import põder.framework.solver.SelectSolver
import põder.log.Log
import põder.util.json.Json.JObj
import põder.util.json.{JPTreeItem, JVal, Json, JsonF, PTreeItem, TreeItemJson}
import põder.util.typ.{ArrayT, MethodType, ObjectT, VoidT}
import põder.util.{Project, SMTHelper, perfCount}
import scalafx.Includes.*
import scalafx.application.JFXApp3
import scalafx.application.JFXApp3.PrimaryStage
import scalafx.beans.property.{BooleanProperty, ObjectProperty}
import scalafx.beans.value.ObservableValue
import scalafx.embed.swing.SwingNode
import scalafx.event.ActionEvent
import scalafx.geometry.Orientation.Vertical
import scalafx.geometry.{Insets, Pos}
import scalafx.scene.Scene
import scalafx.scene.control.Alert.AlertType
import scalafx.scene.control.*
import scalafx.scene.input.KeyEvent
import scalafx.scene.layout.*
import scalafx.scene.text.{Font, Text}
import scalafx.stage.{DirectoryChooser, Stage, WindowEvent}

import scala.collection.mutable
import scala.io.Source
import java.nio.file.*
import javax.swing.event.{ChangeEvent, ChangeListener}
import scala.util.matching.Regex
import sys.process.*

import scalafx.Includes._
import scalafx.event.ActionEvent

object P6der:
  var gui = false
  var analysisName = "modular"
  var solverName = "slr"
  var exceptions = false
  var juliet = false
  var libpath: Option[String] = None

  def printUsage(): Unit = {
  }

  def processArgs: List[String] => (String,Option[String],String) =
    case "--regression"::xs =>
      Log.regression = true
      processArgs(xs)
    case "--lib"::p::xs =>
      libpath = Some(p)
      processArgs(xs)
    case "--exceptions"::xs =>
      exceptions = true
      processArgs(xs)
    case "--analysis"::x::xs =>
      analysisName = x
      processArgs(xs)
    case "--solver"::x::xs =>
      solverName = x
      processArgs(xs)
    case "--juliet"::xs =>
      juliet = true
      processArgs(xs)
    case "--gui"::xs =>
      gui = true
      processArgs(xs)
    case List(base,cls) =>
      (base,libpath,cls)
    case _ =>
      printUsage()
      sys.exit()

  object pl extends ProcessLogger:
    val files: mutable.Set[String] = mutable.Set.empty
    def deleteFiles(path: Path): Unit =
      files.foreach(s => Paths.get(path.toString, s).toFile.delete())
      files.clear()
    private val reg = "\\[wrote (.*)\\]".r
    override def out(s: => String): Unit = ()
    override def err(s: => String): Unit =
      reg.findFirstMatchIn(s).foreach(s => files += s.group(1))

    override def buffer[T](f: => T): T = f

  private object DevNull extends java.io.OutputStream:
    override def write(b: Int): Unit = ()
    override def write(b: Array[Byte]): Unit = ()
    override def write(b: Array[Byte], off: Int, len: Int): Unit = ()
    override def flush(): Unit = ()
    override def close(): Unit = ()

  private val printDevNull = PrintStream(DevNull)

  def main(args: Array[String]): Unit =
    if args.isEmpty then
      val myOut = System.out
      val myErr = System.err
      System.setErr(printDevNull) // silence JavaFX module warning
      System.setOut(printDevNull)
      val m = new P6derGuiApp {
        override def start(): Unit =
          System.setErr(myErr)
          System.setOut(myOut)
          super.start()
      }
      m.main(args)
      System.setErr(myErr)
      System.setOut(myOut)
    else
      val (b,l,c) = processArgs(args.toList)
      val file = c.replace(".",File.separator)
      val path = Paths.get(b)
      if Paths.get(path.toString, file++".java").toFile.exists then
        val cmd = s"javac -verbose -g ${file}.java "
        Log.logCommon("Build command: "++cmd)
        val r = Process(cmd, path.toAbsolutePath.toFile) ! pl
        if r==0 && (Paths.get(path.toString, file++".class")).toFile.exists then
          val p = new Project(b, l getOrElse b,true)
          p.exceptions = exceptions
          p.analysisName = analysisName
          p.juliet = juliet
          p.source_base = path.toString()
          p.mainClass = c
          if gui then
            val app = new JFXApp3 {
              override def start(): Unit =
                stage = new JFXApp3.PrimaryStage {
                  opacity = 0
                }
                val pdr = new P6derGui(p)
                pdr.stage.show()
                pdr.stage.onCloseRequest = _ => {
                  pl.deleteFiles(path)
                }
                Platform.runLater(() => stage.close())
            }
            app.main(Array())
          else
            Log.simple_log = true
            val app = new P6derCmdLine(p, solverName)
            app.main(Array())
            pl.deleteFiles(path)

abstract class CFGProgram (
  val project: Project
) extends ProgramCFG
:

  def addCfg(c: String, m: String, t: MethodType): Unit

  val next = mutable.Map.empty[Node, Set[(Edge, Node)]]
  val prev = mutable.Map.empty[Node, Set[(Edge, Node)]]
  val usedMeths = mutable.Set.empty[(String, String, MethodType)]

  var (basePath, mainPath, mainClass, _) = loadClassFullPath(project.fullMainPath)


  private def loadClass(reader: ClassReader): (String,String,Map[(String, String, MethodType), Graph]) =
    val classPrinter = new ClassCFGPrinter(project.exceptions)
    reader.accept(classPrinter, 0)

    project.superMap.update(classPrinter.className, classPrinter.superName)

    if mainClass != null && classPrinter.className.endsWith(mainClass) && mainPath != classPrinter.packageName then
      val pquoted = Regex.quote(classPrinter.packageName)
      val pathPat = s"(.*)($pquoted)".r
      mainPath match
        case pathPat(b, p) =>
          mainPath = p
          basePath = basePath ++ b
        case _ =>
          println(s"Warning: Cannot establish base path for class ${classPrinter.className}.")
    for cfg <- classPrinter.methodGraphs.values
        (to, eds) <- cfg.edges
        (ed, fr) <- eds do
      prev.update(to, prev.getOrElse(to, Set()) + ((ed, fr)))
      next.update(fr, next.getOrElse(fr, Set()) + ((ed, to)))
    classPrinter.methodGraphs.foreach{ case ((c,m,t),cfg) => project.storeGraph(c,m,t,cfg)}
    (classPrinter.packageName, classPrinter.className, classPrinter.methodGraphs.toMap)


  def loadClassFullPath(fullPath: String): (String,String,String,Map[(String, String, MethodType), Graph]) =
    val file = new java.io.File(fullPath)
    if (file.exists && file.isFile) then
      val fileData = Source.fromFile(file, "ISO8859-1")
      val fileDataBytes = fileData.map(_.toByte).toArray
      val (pn, cn, gs) = loadClass(new ClassReader(fileDataBytes))
      var pns = pn.split(Regex.quote("/")).toSeq
      val cpp = file.getCanonicalPath
      var cp = cpp.split(Regex.quote(File.separator)).toSeq.init
      while pns.nonEmpty && cp.last == pns.last do
        cp = cp.init
        pns = pns.init
      (cp.mkString("",File.separator,File.separator), pn, cn, gs)
    else
      println(s"Error: Failed to load main class ${fullPath}.")
      sys.exit()

  def loadClassFile(classFile: String): Unit =
    val fullPath =
      if java.nio.file.Files.isRegularFile(java.nio.file.Paths.get(basePath++classFile)) then
        basePath++classFile else basePath++mainPath++classFile
    val fileData = Source.fromFile(fullPath, "ISO8859-1")
    val fileDataBytes = fileData.map(_.toByte).toArray
    val (pn,cn, gs) = loadClass(new ClassReader(fileDataBytes))

  private val loadedClasses = mutable.Set.empty[String]
  private def loadClassName(className: String): Unit =
    if !(loadedClasses contains className) then
      loadedClasses += className
      var cr: ClassReader = null
      try
        cr = new ClassReader(className)
      catch
        case a: IOException =>
  //        printf("cannot load resource %s: %s\n",className,a.toString)
          loadClassFile(className++".class")
          return
      val _ = loadClass(cr)

  private def setPrevNextMap(cfg: Graph): Unit =
    for (to,eds) <- cfg.edges
        (ed,fr) <- eds do
      if (!prev.getOrElse(to, Set()).contains((ed, fr))) then
        prev.update(to, prev.getOrElse(to, Set()) + ((ed, fr)))
      if (!next.getOrElse(fr, Set()).contains((ed, fr))) then
        next.update(fr, next.getOrElse(fr, Set()) + ((ed, to)))


  def graphFun(c:String, m:String, t:MethodType): Graph =
    project.loadGraph(c, m, t) match
      case Some(cfg) =>
        setPrevNextMap(cfg)
        addCfg(c, m, t)
        cfg
      case None =>
        loadClassName(c)
        project.loadGraph(c, m, t) match
          case Some(cfg) =>
            addCfg(c, m, t)
            cfg
          case None =>
            graphFun(project.superMap(c), m, t)

  def startVarsFun(c: String, m: String, t: MethodType): MethodNode =
    graphFun(c, m, t).start

  def returnVarsFun(c:String, m:String, t:MethodType): MethodNode =
    graphFun(c, m, t).retrn

  def getMethodThrowingNodes(c: String, m: String, t: MethodType): Set[MethodNode] =
    graphFun(c, m, t).throwingNodes

  def getMethod(c:String, m:String, t:MethodType): Graph =
    try
      graphFun(c,m,t)
    catch
      case e: NullPointerException =>
        println(s"Cannot load class: $c")
        throw e
      case e:NoSuchElementException =>
        println(s"Cannot find the method: $c.$m")
        throw e



class P6derCmdLine(val project: Project, val solver: String)
  extends App
:
  private val cfgp = new CFGProgram(project) {
    override def addCfg(c: String, m: String, t: MethodType): Unit = ()
  }
  private val outer = this
  private val at: AnalysisTask = new AnalysisTask {
    val pcfg: ProgramCFG = outer.cfgp
    val project: Project = outer.project
    val solver: String = outer.solver
    def newCFG(gr: Graph): Unit = ()
    def finished(cs: ConstrSys): Unit = ()
    def saveResults(cs: ConstrSys): Unit = ()
    def storeNode(cs: ConstrSys): Unit = ()
    def update(cs: ConstrSys): Unit = ()
    def newEdges(to: Node, es: Set[(Edge, Node)], stage: Int): Unit = ()
    def removeNode(ft: Node, stage: Int): Unit = ()
  }

  private val cs: ConstrSys = Registry.getCSys(project.analysisName, at)

  val (bp,mp,mc,cfg) = cfgp.loadClassFullPath(project.fullMainPath)
  cfgp.startMethods.foreach{case (m,t) =>
    cfgp.addCfg(cfgp.mainClass,m,t)
  }

  cs.solver.runButton()
  cs.st.run()

class P6derGui(val project: Project):
  val globItems  = TreeItem("")
  val gs = mutable.Map.empty[Any, TreeItem[String]]

  val nt = mutable.Map.empty[Node, Tab]
  val comps = mutable.Set.empty[mxGraphComponent]

  var current_cs : Option[(AnalysisTask,ConstrSys)] = None

  def isStandard(c:String, m:String): Boolean =
     c.startsWith("java") || c.startsWith("scala")

  val stdMethods = new CheckBox("Std. Mthds")
  def skipStandard: Boolean = !stdMethods.selected.value

  val folCfg = new CheckBox("Follow CFG "){
    selected = true
  }
  def followCfg: Boolean = folCfg.selected.value

  def selectE(fr: Node, to: Node, ed: Edge): Unit = {
  }
  var nd: Option[Set[MethodNode]] = None

  def select(ns: Set[MethodNode]): Unit =
    nd = Some(ns)
    current_cs match
      case Some((at,cs)) =>
        given j: JsonF[Seq[PTreeItem]] = TreeItemJson
        given ctx: cs.solver.s.SolvCtxT = new SolvCtx:
          override def set(n: Node, c: cs.solver.s.VarCtx, k: cs.solver.s.VarKey, d: cs.solver.s.Dl): Unit = ()
          override def get(n: Node, c: cs.solver.s.VarCtx, k: cs.solver.s.VarKey): cs.solver.s.Dl =
            cs.solver.st.getOrElse((n,c,k),cs.solver.s.L.bot)
        val ks = cs.solver.st.keys.filter(q => q._1.isInstanceOf[MethodNode] && ns.contains(q._1.asInstanceOf[MethodNode]))
        ks.map(_._1.asInstanceOf[MethodNode]).groupBy(_.methodInfo.sourceFile).foreach{
          case (sf,mns) =>
            val lines = mns.flatMap(_.linenr.toList)
            loadedSources.get(sf).foreach(_.foreach(_.selectLines(lines.toSeq)))
        }
        val jv = ks.flatMap {
          case (MethodNodeWrapper(_,n), c, k) => cs.solver.s.explain(n, c, k)
          case (n: Node, c, k) => cs.solver.s.explain(n, c, k)
        }
        def reveal[T](level:Int)(ti: javafx.scene.control.TreeItem[T]):Unit =
          ti.expanded = true
          if level>1 then
            ti.children.foreach(reveal(level-1))
        jv.foreach(x => reveal(3)(x.delegate))
//        println(jv.toText)
        Platform.runLater(() => {
          log.text <== Log.getLog(ns.head)
          val ti = new TreeItem[String]("data")
          jv.foreach(ti.children.append(_))
          tv.root = ti
          tv.showRoot = false
        })
      case None =>
  framework.gui.JavaConnector.selector = select

  def solverStart(aName: String, sName: String): Unit =
    val at: AnalysisTask = new GuiTask(cfgp,project,aName, sName)
    val csc: ConstrSys = Registry.getCSys(aName,at)
    current_cs = Some((at,csc))
    cfgp.startMethods.foreach { case (m, t) =>
      cfgp.addCfg(cfgp.mainPath++cfgp.mainClass, m, t)
    }
    sb.disable = false
    rb.disable = false
//    sv.disable = false
    analysisSelector.disable = true
    csc.st.start()

  var loadedSources: Map[String, Option[SourceFileView]] = Map.empty
  def tryLoadSource(gr:DynGraph,cs:ConstrSys): Unit =
    if (loadedSources contains gr.mi.sourceFile) then
      return
    gr.sourceTabComponent match
      case Some(tab) =>
        Platform.runLater(() => {
          val _ = graphs.tabs += tab
        })
      case None =>
        loadedSources += gr.mi.sourceFile -> None
        val srcf = File(project.source_base ++ File.separator ++ gr.mi.sourceFile)
        if srcf.exists() && srcf.isFile then
          val tab = new Tab()
          val sv = new SourceFileView(srcf, gr.mi.sourceFile)
          loadedSources += gr.mi.sourceFile -> Some(sv)
          tab.text = gr.mi.sourceFile
          tab.content = sv.wv
          Platform.runLater(() => {
            val _ = graphs.tabs += tab
            sv.populateHasData(cs.solver)
          })

  def registerGraph(gr:DynGraph): Unit =
    val tab = gr.graphTabComponent.getOrElse {
        val node = new SwingNode()
        val comp = new mxGraphComponent(gr)
        comps += comp
        comp.setConnectable(false)
        comp.setDragEnabled(false)
        comp.setAutoScroll(true)
        comp.refresh()
        node.content = comp
        val tab = new Tab()
        tab.text = gr.mi.cls ++ "." ++ gr.mi.mthd
        tab.content = node
        gr.graphTabComponent = Some(tab)
        gr.registerControl = Some { n => nt += n -> tab }
        for n <- gr.getNodes.keys do
          nt += n -> tab
        tab
      }

    Platform.runLater(() => {
      val _ = graphs.tabs += tab
    })

  lazy private val cfgp = new CFGProgram(project) {
    def addCfg(c:String, m:String, t:MethodType): Unit =
      if !(usedMeths contains (c,m,t)) && !(skipStandard && isStandard(c,m)) then
        usedMeths.add((c,m,t))
        val rgr = getMethod(c,m,t)
        if current_cs.isDefined then
          current_cs.get._1.newCFG(rgr)
          if followCfg then
            val gr = rgr.toMXG(select,selectE)
            tryLoadSource(gr,current_cs.get._2)
            registerGraph(gr)
  }

  val globs = new TreeView[String]()
  globs.root = globItems
  globs.showRoot = false

  val graphNameBuffer: scalafx.collections.ObservableBuffer[Graph] =
    scalafx.collections.ObservableBuffer[Graph]()

  val graphsComboBox: ComboBox[Graph] = new ComboBox[Graph] {
    maxWidth = 800
    promptText = "Load method ..."
    editable = false
    items = graphNameBuffer

    onAction = (e:ActionEvent) =>
      if !this.getSelectionModel.isEmpty then
        val rgr = this.getSelectionModel.getSelectedItem
        val gr = DynGraph.graphFromMI(rgr.mi).getOrElse {
          rgr.toMXG(select, selectE)
        }
        this.getSelectionModel.clearSelection()
        if current_cs.isDefined then
          tryLoadSource(gr,current_cs.get._2)
        registerGraph(gr)
        current_cs.foreach{case (at,cs) => at.update(cs) }
        // treeview onchanged
  }
  val graphs = new TabPane()
  graphs.tabClosingPolicy = TabPane.TabClosingPolicy.SelectedTab
  graphs.prefWidth = 5000
  graphs.prefHeight = 5000
  graphs.minHeight = 50
  graphs.minWidth = 50

  val vbox: VBox = new VBox{
    padding = Insets(0)
    spacing = 0
    alignment = Pos.TopLeft
    prefHeight = 5000
    children = Seq(graphsComboBox,graphs)
  }

  class GuiTask(
      val pcfg: põder.framework.cfg.ProgramCFG,
      val project: põder.util.Project,
      analysis: String,
      val solver: String) extends AnalysisTask:
    private var done = false

    def newCFG(gr: Graph): Unit =
      graphNameBuffer.add(gr)
    def finished(cs: ConstrSys): Unit =
      done = true
      Log.logCommon(s"Analysis finished.")
      Platform.runLater(() => {
        loadedSources.values.foreach(_.foreach(_.changedLines(Set.empty)))
        pi.progress = 1
        tb.disable = true
        sb.disable = true
        rb.disable = true
        folCfg.disable = true
//        graphs.visible = true
        sv.setText("save results")
      })

    def saveResults(cs: ConstrSys): Unit =
      def notMain(n: Node): Boolean =
        n match
          case mn: MethodNode => mn.methodInfo.mthd != "main"
          case _ => false

      val toSave = mutable.Map.empty[Node, mutable.Buffer[(cs.solver.s.VarCtx, cs.solver.s.VarKey, cs.solver.s.Dl, Boolean)]]
      for ((v, c, k), d) <- cs.solver.st.toMap do
        toSave.getOrElseUpdate(v, mutable.Buffer.empty[(cs.solver.s.VarCtx, cs.solver.s.VarKey, cs.solver.s.Dl, Boolean)]) +=
          ((c, k, d, notMain(v)))
      for (y, d) <- toSave do project.saveResult(y, d)
      if done then
        sv.disable = true

    def storeNode(cs: ConstrSys): Unit = {
      //        for{node <- nd
      //            v <- cs.solver.st.get(node)}{
      //          val confidence = done && node.methodInfo.mthd != "main" // high confidence if not main method
      //          project.saveResult(node,(v,confidence))
      //        }
    }

    def newEdges(to: Node, es: Set[(Edge, Node)], stage: Int): Unit =
      to match
        case t: MethodNode =>
          val gr =
            DynGraph.graphFromMI(t.methodInfo) match
              case Some(gr) => gr
              case _ => return
          val es1 = es.map {
            case (e, n: MethodNode) => (e, n)
            case (e, n) => (e, MethodNodeWrapper(gr.mi, n))
          }
          gr.addEdges(t, es1)
        case t =>
          for gr <- DynGraph.allGraphs() do
            val es1 = es.map {
              case (e, n: MethodNode) => (e, n)
              case (e, n) => (e, MethodNodeWrapper(gr.mi, n))
            }
            gr.addEdges(MethodNodeWrapper(gr.mi, t), es1)

    var clearOld: () => Unit = () => ()

    var stepOverStd = false

    def update(cs: ConstrSys): Unit =
      val solver = cs.solver
      lazy val ctx: SolvCtx[Node, solver.s.VarCtx, solver.s.VarKey, solver.s.Dl] = new SolvCtx {
        override def get(n: Node, c: solver.s.VarCtx, k: solver.s.VarKey): solver.s.Dl = solver.st.getOrElse((n, c, k), solver.s.L.bot)
        override def set(n: Node, c: solver.s.VarCtx, k: solver.s.VarKey, d: solver.s.Dl): Unit = ??? // maybe add side-effect list to solver
      }
      if skipStandard && solver.currentNode.exists(x =>
        x._1.isInstanceOf[MethodNode] && isStandard(x._1.asInstanceOf[MethodNode].methodInfo.cls, x._1.asInstanceOf[MethodNode].methodInfo.mthd)) then
        if !stepOverStd then
          Log.logCommon(s"Stepping over std. method call.")
          stepOverStd = true
        solver.stepButton()
      else
        stepOverStd = false
        Log.logCommon(s"Analysis paused.")
        Platform.runLater(() => {
          pi.progress = 1
          tb.disable = true
        })

        def addOrUpdateGlob(n: solver.s.Var): Unit =
          gs.get(n) match
            case Some(x) =>
              val tv = Json.toTreeItems(solver.s.explain(n._1, n._2, n._3, 0)(using ctx, Json))
              x.children.clear()
              tv.flatMap(_.children).foreach(x.children += _)
            case None =>
              val tv = Json.toTreeItems(solver.s.explain(n._1, n._2, n._3, 0)(using ctx, Json))
              tv.foreach(tk => gs += n -> tk)
              tv.foreach(globItems.children.append(_))

        def setColor[A, B](v: Option[(Node, A, B)], cs: (String, String)*): Unit =
          v match
            case Some((n: MethodNode, _, _)) =>
              DynGraph.graphFromMI(n.methodInfo).foreach {
                _.getNodes.get(n) match
                  case Some(v) =>
                    v.setStyle(framework.gui.changeStyle(v.getStyle, cs*))
                  case _ => ()
              }
            case _ => ()

        if comps != null then
          clearOld()

          val nodesToClear = solver.updatedSet ++ solver.widenedSet ++ solver.currentNode.toSet

          nodesToClear.filterNot(_._1.isInstanceOf[MethodNode]).foreach(addOrUpdateGlob)

          solver.updatedSet.foreach(x => setColor(Some(x), "fillColor" -> "#00BB00", "fontColor" -> "white"))
          solver.widenedSet.foreach(x => setColor(Some(x), "fillColor" -> "#0000FF", "fontColor" -> "white"))

          val mns = nodesToClear.filter(_._1.isInstanceOf[MethodNode]).map(_._1.asInstanceOf[MethodNode])

          mns.groupBy(_.methodInfo.sourceFile).view.mapValues(_.flatMap(_.linenr.toSeq)).foreach {
            case (sv: String, xs) =>
              loadedSources.get(sv).foreach {
                _.foreach { sv => sv.changedLines(xs) }
              }
          }

          clearOld = () =>
            for x <- nodesToClear do
              setColor(Some(x), "fillColor" -> "#000000", "fontColor" -> "white")
          
          solver.currentNode match
            case Some((x:MethodNode,_,_)) =>
              for g <- Graph.allGraphs do
                g.current_pos = (if g.mi==x.methodInfo then Some(x) else None)
            case _ => ()

          for g <- Graph.allGraphs do
            g.widen_points =
              for v <- solver.widenedSet
                  if v._1.isInstanceOf[MethodNode]
                  vmn = v._1.asInstanceOf[MethodNode]
                  if g.nodes.contains(vmn)
                   yield vmn

          solver.clearSets()

          if followCfg then
              solver.currentNode.foreach(x => nt.get(x._1).foreach(graphs.getSelectionModel.select(_)))

          Platform.runLater { () =>
            DynGraph.allGraphs().foreach(_.update())
            graphsComboBox.setItems(graphNameBuffer)
            comps.foreach(_.refresh())
          }

    tv.root = new TreeItem("<tyhi>")

  val tv = new TreeView[String]()
  tv.getSelectionModel.getSelectedItems.addListener(new ListChangeListener[javafx.scene.control.TreeItem[String]] {
    var oldSrc: Option[SourceFileView] = None
    override def onChanged(change: ListChangeListener.Change[? <: control.TreeItem[String]]): Unit =
      oldSrc.foreach(_.deHilight())
      if !tv.getSelectionModel.getSelectedItems.isEmpty then
        tv.getSelectionModel.getSelectedItems.get(0) match
          case t: JPTreeItem =>
            for (src,line) <- t.lab do
              loadedSources.get(src).foreach{_.foreach{ sv =>
                oldSrc = Some(sv)
                sv.hilightLine(line)}}
          case _ => ()
  })


  val log = new TextArea()
  val info = new TabPane()
  info.prefHeight = 5000
  val commonlog = new TextArea {
    text <== Log.getLogCommon
    def of: (ObservableValue[String, String], String, String) => Unit = (_,_,l) => {
      Platform.runLater( () =>
        this.delegate.positionCaret(l.length)
      )
    }
    text.onChange[String](of)
  }

  val result = new Tab()
  result.text = "result"
  result.content = tv
  val globals = new Tab()
  globals.text = "globals"
  globals.content = globs
  val logTab = new Tab()
  logTab.text = "log"
  logTab.content = log
  info.tabs += result
  info.tabs += logTab
  info.tabs += globals

  val sb = new Button("step")
  val rb = new Button("run")
  val tb = new Button("stop")
  val sv = new Button("save progress")

  val analysisSelector: GridPane = new GridPane() {
    val solverChoice = new ChoiceBox[String]()
    val startButton = new Button("Start!")

    solverChoice.getItems.addAll(SelectSolver.allSol.keys.toSeq*)
    solverChoice.selectionModel.value.clearAndSelect(0)
    startButton.onAction = { (_: ActionEvent) =>
      Log.logCommon(s"Start button pushed.")
      solverStart(project.analysisName,
        solverChoice.selectionModel.value.getSelectedItem)
    }
    margin = Insets(4)
    hgap = 10
    vgap = 10
//    val atext = new Text("Analysis:"){
//      opacity <== when(disabled) choose 0.4 otherwise 1
//    }
    val stext = new Text("Solver:") {
      opacity <== when(disabled) choose 0.4 otherwise 1
    }

    this.add(stext,1,0)
    this.add(solverChoice,2,0)
    this.add(stdMethods,3,0)
    this.add(startButton,4,0)
  }

  val pi = new ProgressIndicator{
    maxHeight = 20
    maxWidth = 20
    progress = 1
  }

  val analysisControl: GridPane = new GridPane() {
    sb.prefWidth = 53
    rb.prefWidth = 53
    tb.prefWidth = 53
    sv.prefWidth = 103
    tb.disable = true
    sb.disable = true
    rb.disable = true
    sv.disable = true
    sb.onAction = { (_: ActionEvent) =>
      Log.logCommon(s"Step button pushed.")
      SMTHelper.monitor = Some(() => ())
//      graphs.visible = false
      pi.progress = -1
      tb.disable = false
      current_cs.foreach(_._2.solver.stepButton())
    }
    rb.onAction = { (_: ActionEvent) =>
      Log.logCommon(s"Run button pushed.")
      SMTHelper.monitor = Some { () => () }
//      graphs.visible = false
      pi.progress = -1
      tb.disable = false
      current_cs.foreach(_._2.solver.runButton())
    }
    tb.onAction = { (_: ActionEvent) =>
      Log.logCommon(s"Stop button pushed.")
      SMTHelper.monitor = Some(() => throw new põder.framework.BreakTransferFunction)
      current_cs.foreach(_._2.solver.stopButton())
      tb.disable = true
//      graphs.visible = true
    }
    sv.onAction = { (_: ActionEvent) =>
      Log.logCommon(s"Save button pushed.")
      SMTHelper.monitor = Some(() => ())
      current_cs.foreach( (at,cs) => at.saveResults(cs))
    }
    padding = Insets(4)
    hgap <== 4
    this.addRow(0, sb, folCfg, pi, rb, tb, sv)
  }

  private val infos = new SplitPane {
    dividerPositions = 0.8
    orientation = Vertical
    items.add(info)
    items.add(commonlog)
  }


  val stage : Stage = new Stage {
    title = "P6der"
    scene = new Scene {
      root = new SplitPane {
        private val grapControls = new BorderPane() {
          this.center = vbox
          this.bottom = new TilePane() {
            private var currentZoom = 100
            val zm = new Spinner[Integer](5, 400, currentZoom, 5)
            zm.prefWidth = 85
            zm.styleClass += Spinner.StyleClassSplitArrowsHorizontal
            zm.editable = true
            zm.value.onChange {
              val z = zm.value.value
              comps foreach {
                _.zoom((z / 100.0) / (currentZoom / 100.0))
              }
              currentZoom = z
            }
//            val store = new Button("store")
//            store.onAction = _ => {
//              current_cs.foreach( (at,cs) => at.storeNode(cs))
//            }
            hgap <== 10
            children += new Text("Zoom:")
            children += zm
//            children += store
          }
        }
        val aControls: VBox = new VBox {
          children += analysisSelector
          children += new Separator()
          children += analysisControl
          children += infos
        }
        items.add(grapControls)
        items.add(aControls)
        maxWidth = 600
        maxHeight = 600
        maximized = true
      }
      onKeyPressed = { (e:KeyEvent) =>
        if e.controlDown && e.code.name.toLowerCase == "l" then
          Log.logCommon(perfCount.status)
        else if e.controlDown && e.code.name.toLowerCase == "k" then
          Log.logCommon(perfCount.statusFlat)
        else if e.controlDown && e.code.name.toLowerCase == "p" then
          Log.logCommon(perfCount.statusFlat)
      }
    }
    onCloseRequest = { (e:WindowEvent) =>
      Log.logCommon(s"Closing ...")
      current_cs.foreach(_._2.solver.killButton())
      current_cs.foreach(_._2.solver.stopButton())
      SMTHelper.monitor = Some(() => throw new põder.framework.BreakTransferFunction)
      System.exit(0)
    }
  }
class DirChooserField(stage: Stage) extends HBox:
  private val self = this
  val button = new Button("...") {
    onAction = _ => {
      chooseDir(stage)
    }
  }
  val field: TextField = new TextField {
    prefWidth  <== self.prefWidth-button.width
    prefHeight <== self.prefHeight
  }
  val directoryChooser: DirectoryChooser = new DirectoryChooser(){
    title = "Choose source directory ..."
  }
  private def chooseDir(stage:Stage):Unit =
    var folder = new File(field.text.value)
    if !folder.exists() then folder = new File(".")
    directoryChooser.initialDirectory = folder
    val f = directoryChooser.showDialog(stage)
    if f != null && f.isDirectory then
      field.text = f.getCanonicalPath
      field.fireEvent(new ActionEvent)
  children = Seq(field, button)
  field.disable <==> button.disable

object CreateProject:
  def createProject(src: String, cls: String, ana:String, ex:Boolean, jul:Boolean, prj:String, libp:String): Project =
    val dpath = Paths.get(libp)
    if !Files.exists(dpath) then Files.createDirectory(dpath)
    val p = new Project(prj,libp)
    p.analysisName = ana
    p.exceptions   = !ex
    p.mainClass    = cls
    p.juliet       = jul
    p.source_base  = src
    p.createOnDisk()
    p
  def findAllClasses(path:String):Seq[String] =
    val pl = (if path.last==File.separatorChar then -1 else 0) + path.length
    def pathToPackage(s:String): String =
      s.substring(pl+1,s.length-6).replace(File.separator,".")
    val f = new File(path)
    def r(d:Int)(f:File):Seq[String] =
      if d == 7 then
        Seq()
      else if f.isFile && f.toString.split("\\.").last == "class" then
        Seq(pathToPackage(f.toString))
      else if f.isDirectory then
        f.listFiles().toIndexedSeq.flatMap(r(d+1))
      else
        Seq()
    r(0)(f)
  val stage: Stage = new Stage {
    val stage: Stage = this
    title = "New Project"
    scene = new Scene {
      val fH = 20
      val fW = 900
      val lblW = 150
      private val projLbl = new Label("User code project:") {
        minWidth = lblW
      }
      object projPath extends DirChooserField(stage):
        var overridden = false
        val unavaliable : BooleanProperty = BooleanProperty(true)
        prefWidth = fW
        prefHeight = fH
        field.text.onChange((_,_,newv) => {
          val p = Paths.get(newv)
          unavaliable.set(p.toFile.exists || !p.toFile.getParentFile.exists)
        })
        field.onAction = _ => {
          overridden = true
        }
      object libPath extends DirChooserField(stage):
        var overridden = false
        val unavaliable : BooleanProperty = BooleanProperty(true)
        prefWidth = fW
        prefHeight = fH
        field.text.onChange((_,_,newv) => {
          val p = Paths.get(newv)
          unavaliable.set(!p.toFile.exists && !p.toFile.getParentFile.exists)
        })
        field.onAction = _ => {
          overridden = true
        }
      private val libChk = new CheckBox("Separate std. library:"){
        minWidth = lblW
        selected.onChange((_,_,v) =>
          if v then {
            libPath.field.disable = false
            libPath.field.text.unbind()
          } else {
            libPath.field.disable = true
            libPath.field.text <== projPath.field.text
          }
        )
        libPath.field.disable = true
        libPath.field.text <== projPath.field.text
      }
      private val mainLbl = new Label("Main class:") {
        minWidth = lblW
      }
      private val mainCls = new ComboBox[String] {
        prefWidth = fW
        prefHeight = fH
        onAction = _ => {
          if !projPath.overridden then
            val base = Paths.get(sourcePath.field.text.value)
            if base != null && !selectionModel.value.isEmpty then
              val path = Paths.get(base.toString, "analysis_" ++ selectionModel.value.getSelectedItem)
              projPath.field.text = path.toString
        }
      }
      private val sourceLbl = new Label("Source dir.:") {
        minWidth = lblW
      }
      private val sourcePath : DirChooserField = new DirChooserField(stage) {
        prefWidth = fW
        prefHeight = fH
        private var last = 0
        field.onAction = _ => {
          mainCls.getItems.clear()
          mainCls.selectionModel.value.clearSelection()
          new Thread {
            override def run(): Unit =
              last += 1;
              val l = last
              val cs = findAllClasses(field.text.value)
              if last == l then
                Platform.runLater { () =>
                  val _ = mainCls.getItems.addAll(cs*)
                }
          }.start()
        }
        field.text = new File(".").getCanonicalPath
        field.fireEvent(new ActionEvent)
      }
      private val anaLbl = new Label("Analysis:"){
        minWidth = lblW
      }
      private val anaFld = new ChoiceBox[String] {
        prefWidth = fW
        prefHeight = fH
        items.get().addAll(Registry.getNames*)
        selectionModel.value.clearAndSelect(0)
      }
      private val ignEx = new Label("Ignore exceptions:"){
        minWidth = lblW
      }
      private val ignExBx = new CheckBox {
        selected = true
        prefWidth = fH+3
        prefHeight = fH+3
      }
      private val jul = new Label("Juliet:"){
        minWidth = lblW
      }
      private val julBx = new CheckBox {
        prefWidth = fH+3
        prefHeight = fH+3
      }
      private val cncl = new Button("Cancel") {
        onAction = _ => {
          stage.hide()
          StartPage.stage.show()
        }
        margin = Insets(25,25,10,25)
      }
      private val crt = new Button("Create project") {
        disable <== projPath.unavaliable && libPath.unavaliable && mainCls.selectionModel.value.selectedIndexProperty() < 0
        onAction = _ => {
          val p = createProject(
            sourcePath.field.text.value,
            mainCls.selectionModel.value.getSelectedItem,
            anaFld.selectionModel.value.getSelectedItem,
            ignExBx.selected.value,
            julBx.selected.value,
            projPath.field.text.value,
            libPath.field.text.value)
          stage.hide()
          val w = new P6derGui(p)
          w.stage.show()
        }
        margin = Insets(25,25,10,0)
      }

      root = new BorderPane {
          top   =  new BorderPane { prefHeight = 10 }
          left  =  new BorderPane { prefWidth = 25 }
          right =  new BorderPane { prefWidth = 25 }
          center = new VBox {
            children = Seq(
              new HBox {alignment = Pos.CenterLeft; children = Seq(sourceLbl, sourcePath)},
              new HBox {alignment = Pos.CenterLeft; children = Seq(mainLbl  , mainCls)},
              new HBox {alignment = Pos.CenterLeft; children = Seq(anaLbl   , anaFld)},
              new HBox {alignment = Pos.CenterLeft; children = Seq(ignEx    , ignExBx)},
              new HBox {alignment = Pos.CenterLeft; children = Seq(jul      , julBx)},
              new BorderPane {prefHeight = 15},
              new HBox {alignment = Pos.CenterLeft; children = Seq(projLbl  , projPath)},
              new HBox {alignment = Pos.CenterLeft; children = Seq(libChk   , libPath)}
            )
          }
          bottom = new HBox {
            alignment = Pos.CenterRight
            children = Seq(cncl,crt)
          }
      }
    }

    width = 500
    minWidth = 210

    height = 300
    minHeight = 220
  }

object OpenProject:
  var pField: TextField = new TextField()

  def getWorkingVersions(directoryName: String): (Seq[String], mutable.Map[String, Seq[String]]) =
    var workingVrs = Seq.empty[String]
    val verToAn = mutable.Map.empty[String, Seq[String]]

    val vrs = getListOfSubDirectories(directoryName, "poder_v")
    for  vr <- vrs do
      val ans = getListOfSubDirectories(directoryName ++ File.separator ++ vr, "", Registry.getNames)
      var workingAns = Seq.empty[String]
      for  an <- ans do
        val path = Paths.get(directoryName ++ File.separator ++ vr ++ File.separator ++ an)
        Project(path) match
          case Some(proj) =>
            workingAns = workingAns :+ an
          case None => throw new Error("OpenProject.getWorkingVersions: project not found in "++path.toString())
      if workingAns.nonEmpty then
        verToAn(vr) = workingAns
        workingVrs = workingVrs :+ vr
    (workingVrs, verToAn)
  def getListOfSubDirectories(directoryName: String, startsWith: String = "", inList: Seq[String] = Seq.empty): Seq[String] =
    if new File(directoryName).exists() then
      new File(directoryName)
        .listFiles.toIndexedSeq
        .filter(_.isDirectory)
        .filter(_.getName.startsWith(startsWith) || startsWith == "")
        .filter(fn => inList.contains(fn.getName) || inList.isEmpty)
        .map(_.getName)
    else
      Seq.empty
  def getCurrentVersionIndx(versionList: Seq[String]): Int =
    val dummy_prj = new Project("", "")
    versionList.indexOf("poder_v" ++ dummy_prj.poder_ver ++ "_" ++ dummy_prj.java_ver)
  def getPath(str: String): Path =
    Paths.get(str)
  def updateProjPath(projPath: String): Unit =
    pField.text.value = projPath
    pField.fireEvent(new ActionEvent)

  val stage: Stage = new Stage {
    val stage: Stage = this
    var workingVrs: Seq[String] = Seq.empty
    var verToAn: mutable.Map[String, Seq[String]] = mutable.Map.empty[String, Seq[String]]

    title = "Open project"
    scene = new Scene {

      val fH = 20
      val fW = 900
      val lblW = 150
      private val anaLbl = new Label("Analysis:"){
        minWidth = lblW
      }
      private val anaFld: ChoiceBox[String] = new ChoiceBox[String] {
        prefWidth = fW
        prefHeight = fH
      }
      private val verLbl = new Label("Version:"){
        minWidth = lblW
      }
      private val verFld: ChoiceBox[String] = new ChoiceBox[String] {
        prefWidth = fW
        prefHeight = fH
        onAction = _ => {
          anaFld.getItems.clear()
          anaFld.getItems.addAll(verToAn(selectionModel.value.getSelectedItem)*)
          anaFld.selectionModel.value.clearAndSelect(0)
        }
      }
      private val projLbl = new Label("User code project:") {
        minWidth = lblW
      }
      private val projPath: DirChooserField = new DirChooserField(stage) {
        pField = field
        prefWidth = fW
        prefHeight = fH
        field.onAction = _ => {
          verFld.getItems.clear()
          val vrs = getWorkingVersions(field.text.value)
          workingVrs = vrs._1
          verToAn = vrs._2
          val current_vr_indx = getCurrentVersionIndx(workingVrs)
          if current_vr_indx >= 0 then
            workingVrs = Seq(workingVrs(current_vr_indx))
          else if workingVrs.length <= 0 then
            new Alert(AlertType.Information) {
              initOwner(OpenProject.stage)
              title = "Probleem"
              headerText = "Projekti laadimine ebaõnnestus"
              contentText = "Etteantud asukohast polnud võimalik leida 'conf' faili."
            }.showAndWait()
          else
            new Alert(AlertType.Information) {
              initOwner(OpenProject.stage)
              title = "Probleem"
              headerText = "Hoiatus! Projektis polnud õiges versioonis analüüsi."
              contentText = "Võid valida teise versiooni, mis ei pruugi töötada"
            }.showAndWait()
          verFld.getItems.addAll(workingVrs*)
          verFld.selectionModel.value.clearAndSelect(0)
        }
      }
      private val cncl = new Button("Cancel") {
        onAction = _ => {
          stage.hide()
          StartPage.stage.show()
        }
        margin = Insets(25,25,10,25)
      }
      private val crt = new Button("Open project") {
        disable <== verFld.selectionModel.value.selectedIndexProperty() < 0 && anaFld.selectionModel.value.selectedIndexProperty() < 0
        onAction = _ => {
          val path = Paths.get(projPath.field.text.value ++
                          File.separator ++ verFld.selectionModel.value.getSelectedItem ++
                          File.separator ++ anaFld.selectionModel.value.getSelectedItem)
          Project(path) match
            case Some(proj) =>
              val w = new P6derGui(proj)
              stage.hide()
              w.stage.show()
            case None =>
              val _ = new Alert(AlertType.Information) {
                initOwner(stage)
                title = "Probleem"
                headerText = "Projekti laadimine ebaõnnestus."
                contentText = "Etteantud asukohast polnud võimalik laadida 'conf' faili."
              }.showAndWait()
        }
        margin = Insets(25,25,10,0)
      }
      root = new BorderPane {
        top   =  new BorderPane { prefHeight = 10 }
        left  =  new BorderPane { prefWidth = 25 }
        right =  new BorderPane { prefWidth = 25 }
        center = new VBox {
          children = Seq(
            new HBox {alignment = Pos.CenterLeft; children = Seq(projLbl  , projPath)},
            new BorderPane {prefHeight = 15},
            new HBox {alignment = Pos.CenterLeft; children = Seq(verLbl   , verFld)},
            new HBox {alignment = Pos.CenterLeft; children = Seq(anaLbl   , anaFld)},
          )
        }
        bottom = new HBox {
          alignment = Pos.CenterRight
          children = Seq(cncl,crt)
        }
      }
    }
    width = 500
    height = 200
  }

object StartPage:
  def handleOpenProject(fPath: String): Any =
    val vrs = OpenProject.getWorkingVersions(fPath)
    val workingVrs = vrs._1
    val verToAn = vrs._2
    val currentVrIndx = OpenProject.getCurrentVersionIndx(workingVrs)
    if currentVrIndx >= 0 && verToAn(workingVrs(currentVrIndx)).length == 1 then
      val currentVr = workingVrs(currentVrIndx)
      val currentVrAn = verToAn(workingVrs(currentVrIndx)).head
      val path = OpenProject.getPath(fPath ++ File.separator ++ currentVr ++ File.separator ++ currentVrAn)
      Project(path) match
        case Some(proj) =>
          val w = new P6derGui(proj)
          stage.hide()
          w.stage.show()
        case None => throw new Error("OpenProject.getWorkingVersions: project not found in "++path.toString())
    else if workingVrs.length <= 0 then
      new Alert(AlertType.Information) {
        initOwner(stage)
        title = "Probleem"
        headerText = "Projekti laadimine ebaõnnestus"
        contentText = "Etteantud asukohast polnud võimalik laadida 'conf' faili."
      }.showAndWait()
    else
      stage.hide()
      OpenProject.updateProjPath(fPath)
      OpenProject.stage.show()
  val stage: PrimaryStage = new PrimaryStage {
    private val stage = this
    title = "Põder"
    scene = new Scene {
      val newProj: Button = new Button("New Project"){
        prefWidth = 200
        prefHeight = 60
        onAction = _ => {
          stage.hide()
          CreateProject.stage.show()
        }
      }
      val directoryChooser: DirectoryChooser = new DirectoryChooser(){
        title = "Choose a directory ..."
      }
      val opnProj = new Button("Open Project"){
        disable = true
        prefWidth = 200
        prefHeight = 60
        onAction = _ => {
          val f = directoryChooser.showDialog(stage)
          if f != null && f.isDirectory then
            Project(Paths.get(f.toString)) match
              case Some(proj) =>
                val w = new P6derGui(proj)
                stage.hide()
                w.stage.show()
              case None =>
                val _ = handleOpenProject(f.getPath)
        }
      }
      val close   = new Button("Close"){
        prefWidth = 200
        prefHeight = 60
        onAction = _ => {
          stage.close()
        }
      }
      val lbl = new HBox {
        alignment = Pos.BottomCenter
        val pdr = new Text("\uD83E\uDD8C") {
          font =  new Font(40)
        }
        children = Seq(new Label("©Tartu Ülikool  "), pdr)
      }
      root = new BorderPane {
        center = new HBox {
          alignment = Pos.Center
          children = new VBox {
            children = Seq(newProj, opnProj, close)
          }
        }
        bottom = lbl
      }
    }

    width = 250
    height = 300
  }

class P6derGuiApp extends JFXApp3 {
  override def start() : Unit =
    stage = StartPage.stage
}
