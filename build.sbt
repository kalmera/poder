name := "põder"
version := "1.0"

ThisBuild / scalaVersion := "3.6.3"

scalacOptions := Seq("-deprecation")

ThisBuild / libraryDependencies += "org.ow2.asm" % "asm" % "9.7.1"
ThisBuild / libraryDependencies += "com.github.vlsi.mxgraph" % "jgraphx" % "4.2.2"
ThisBuild / libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.19" % Test

// Add dependency on ScalaFX library
ThisBuild / libraryDependencies += "org.scalafx" %% "scalafx" % "23.0.1-R34"

val platf = Seq("linux", "mac", "mac-aarch64", "win")
val javafx_packages = Seq("javafx-base", "javafx-controls", "javafx-fxml", "javafx-graphics", "javafx-media", "javafx-swing", "javafx-web")

ThisBuild / libraryDependencies ++= {
  for {pkg <- javafx_packages
       pla <- platf} yield "org.openjfx" % pkg % "23.0.2" classifier pla
}

ThisBuild / assemblyJarName := name.value++"-"++version.value++".jar"

ThisBuild / mainClass := Some(name.value++".P6der")

// META-INF discarding
ThisBuild / assemblyMergeStrategy := {
  case PathList("META-INF", _*) => MergeStrategy.discard
  case _ => MergeStrategy.first
}