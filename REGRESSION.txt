* tests are in regression/<analysis name>/*.java

* tests may be run using 'make regression'

* regression testing runs 

java -jar <jar> --regression --analysis <analysis> <directory> <java file> 

for example 

java -jar target/scala-3.*/põder-*.jar --regression --analysis modular regression/modular Test1

That prints out something like this:
Check true;Test1;main;14
Check true;Test1;main;10
...

* Output may be generated in Põder by calling "Log.logAssert".

* Output must be marked in the java file. For example: on line 14 of 
  Test1.java (class Test1, method main) is a comment: 
  
  "Põder: Check true"
  
* Warnings are issued when output and "Põder: " comments do not match.


  