sources=$(wildcard src/main/scala/põder/*.scala) $(wildcard src/main/scala/põder/*/*.scala) $(wildcard src/main/scala/põder/*/*/*.scala) $(wildcard src/main/scala/põder/*/*/*/*.scala)

all: target/scala-3.0.0/põder-1.0.jar

run-gui:
	java -jar target/scala-3.*/põder-*.jar

clean:
	rm -rf target

target/scala-3.0.0/põder-1.0.jar: $(sources)
	sbt assembly

regression: all
	./scripts/regression.sh