# The Põder Static Analysis Framework

Põder is a static analysis framework is developed by Kalmer Apinis and others at the University of Tartu, Laboratory for Software Science. 

This work is supported by the Estonian Research Council grant PSG61 "High-Assurance Software Development With Sound Interactive Static Analysis".

### Building & Running

##### Download the Release Jar

1. Download most recent release Jar from: https://bitbucket.org/kalmera/poder/downloads/
2. Download the test file https://bitbucket.org/kalmera/poder/downloads/OInt.java
3. Compile the test file using 'javac -g OInt.java'
4. Run the Jar -> New Project -> Select the source dir. & main class -> Create Project

Short demo:
![Demo](demo.gif)

##### Build using SBT

    git clone https://bitbucket.org/kalmera/poder.git
    cd poder
    sbt assembly
    java -jar target/scala-*/põder-*.jar  

Alternatively you may use 'make all' and 'make run-gui' instead of the last two commands.

Optionally you may run regression tests using 'make regression' or './scripts/regression.sh'.

##### Build using IntelliJ IDEA
 
1. Clone `git clone https://bitbucket.org/kalmera/poder.git`
2. In IntelliJ IDEA, select "Import Project" or File->New->Project from Existing Sources...
3. From "import project from external model" select "sbt", then click "next" and "finish"
4. We suggest to "enable auto-import" of sbt.
5. Open P6der.scala and run the põder.P6der object using "Run 'P6der'".
