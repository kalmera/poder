class P6der {
  public static void check(boolean b) {
    assert(b);
    // Analüüs peaks meetodi `P6der.check` kutsel väljastama hoiatuse, kui argument pole true.
  }
}
class Test1 {
  public static void main(String[] args) {
    int x = 1;
    P6der.check(x==1);
    x = 3;
    P6der.check(x!=1);
    P6der.check(x==3);
    int y = x + 1;
    P6der.check(y==4);
    P6der.check(y>x);
    x = (3*y)/2;
    P6der.check(x==6);
  }
}
class Test2 {
  public static void main(String[] args) {
    int x = 0;
    if (Math.random() <= 0.5) {
      x = 10;
      P6der.check(x==10);
    } else {
      x = 20;
      P6der.check(x==20);
    }
    P6der.check(x>=10);
    P6der.check(x<=20);
  }
}

class Test2b {
  public static void main(String[] args) {
    int x = 0;
    if (Math.random() <= 0.5)
      x = 10;
    else
      x += 20;
    P6der.check(x>=10);
    P6der.check(x<=20);
  }
}

class Test3 {
  public static void main(String[] args) {
    int x = 0;
    if (Math.random() <= 0.5) {
      x = 10;
      P6der.check(x==10);
    } else {
      x = 20;
      P6der.check(x==20);
    }
    P6der.check(x>=10);
    P6der.check(x<=20);
  }
}

class Test4 {
  public static void main(String[] args) {
    int x = 0;
    for (;Math.random() * 2 <= 1; x++) { }
    P6der.check(x>=0);
    P6der.check(!(x<1000));

    for (x = 0; x < 100; x++) { }
    P6der.check(x==100);
  }
}

class Test4b {
  public static void main(String[] args) {
    int x = 0;
    for (x = 5; x < 100; x++) { }
    P6der.check(x==100);
    int y = x + 3;
  }
}

class Test4c {
  public static void main(String[] args) {
    int x = 0;
    while (x < 100) {
      x += 1;
    }
    P6der.check(x == 100);
  }
}

class Test5 {
  private static int incr(int i) {
    return i+1;
  }
  private static int viis() {
    return 5;
  }
  private static void m(int q, int k) {
    P6der.check(q==20);
    // selle töötamine sõltub muudest otsustest
    // aga tõenäoliselt me tahame, et see töötaks
  }
  public static void main(String[] args) {
    int x = 1 + viis();
    P6der.check(x==6); // see peaks töötama

    x = 20;
    int y = 5;
    m(x, y);

    x = 30;
    x = incr(incr(x));
    P6der.check(x==32); // selle töötamine sõltub muudest otsustest
  }
}

class Test6 {
  public static void main(String[] args) {
    int y = 2;
    int x = 0;
    P6der.check(!(x==2));
    for (x = 0; x < 100; x++) { }
    //P6der.check(x==100);
  }
}