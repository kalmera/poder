class P6der {
  public static void check(boolean b) {
    assert(b);
  }
}
class Test1 {
  public static void main(String[] args) {
    int x = 1;
    P6der.check(x!=1); // Põder: Check false
    P6der.check(x==1); // Põder: Check true
    P6der.check(x!=3); // Põder: Check true
    P6der.check(x==3); // Põder: Check false
    x = 3;
    P6der.check(x!=1); // Põder: Check true
    P6der.check(x==3); // Põder: Check true
  }
}
