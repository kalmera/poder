class P6der {
  public static void check(boolean b) {
    assert(b);
  }
}
class Test10 {
  static int one(){
    return 1;
  }
  static int two(){
    return one()+one();
  }
  public static void main(String[] args) {
    int x = one();
    P6der.check(x==1); // Põder: Check true
    int y = two();
    P6der.check(x==1); // Põder: Check true
    P6der.check(y==2); // Põder: Check true
  }
}
