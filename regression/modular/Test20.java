class P6der {
  public static void check(boolean b) {
    assert(b);
  }
}
class Test20 {
  static void f(int x){
    P6der.check(x<100); // Põder: Check true
  }
  public static void main(String[] args) {
    f(10);
    f(20);
  }
}
