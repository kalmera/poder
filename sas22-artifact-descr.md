
# Context-Sensitive Meta-Constraint Systems for Explainable Program Analysis

This the artifact description file for the SAS 2022 paper "Context-Sensitive Meta-Constraint Systems for Explainable Program Analysis".

The artifact consists of an OVA virtual machine running Linux which contains the source of the Põder static analysis framework, the compiled põder-1.0.jar file, the test code OInt.java, and compiled .class files.

(The machine should not require credentials. If needed, though, the user name in the VM is "user" and the password "pass".)


## Running the examples in the paper

Step-by-step explanation how to analyze the running example of the paper.

![demo](demo.gif)

1. Run Põder using "java -jar ~/poder/target/scala-3.1.1/põder-1.0.jar", or "./runp6der.sh", or click "Põder" in the Activities menu. A window will open with three buttons.

2. Click "New Project". The window "New Project" replaces the initial window.

3. Click on the "Source dir." path, make sure it says "/home/user/test" and press enter.

4. Choose "OInt" as the Main class. Make sure that Analysis is "expr".

5. Press "Create project". A window titled P6der will replace the previous window.

6. Click "Start!" to make Põder load the main class.

7. Uncheck "Follow CFG" and click "run". After a few seconds, the analysis finishes and prints "Analysis finished." on the lower right part of the window.

8. Make sure that "OInt.java" tab is selected on the left. Search and click on it if necessary. 

Instead of the running example OInt you can analyze other examples. Follow the steps 1 to 8 but select a different main class. Then click on the line of interest and look at the results.

* Remember to recompile classes using, e.g., "javac -g OInt.java" if you change the source code. Do NOT forget "-g"! The analyzer requires debug information to work. Note that you may need to delete the analysis_OInt directory.
* As it is Java ecosystem, some NullPointerExceptions may occur in the user interface code. It has only visual effect.

The following examples can be tried:

* Class OInt is the running example.
* OIntBad is the running example where the main object has escaped to other threads.
* OIntSimple is the running example with x and y being local variables instead of fields (such that the helper analysis is not needed).
* OIntMethod is an example to show how method calls appear in explanations.

## Replicating the explanations from the paper

**Explanation (for OInt) of x==100:**

The explanation from Figure 7:
```
    value 100 due to condition “at least 100” on line 15 
      range [0,100] due to a loop on line 15 on field x
        starting with: value 0 due to constant on line 13 
        cycle with:
          range [1, 100] due to operation IADD 
            parameter 1:
              range [0, 99] due to condition “at most 99” 
                range [0, 100] due to field x at line 15
            parameter 2: value 1 due to constant on line 16
```

To see this in the analyzer:

1. Follow the above instruction to analyze OInt.

2. Scroll down in the source code listing to find the main class OInt. Click on line with "P6der.evalInt(x);" to display information about that line on the right. The line will become selected (cyan color).

3. Note that the value of 1st operand on line 17 is {[100,100]}. This means that x has value 100.

4. Note the explanation in "value 100 due to ...". To see the full explanation, click on the right-pointing triangles to uncover further nodes.


**Explanation (for OInt) of y==0:**

The paper claims the following explanation (based on the loop eliminated in Fig. 6):

```
    value 0 due to constant on line 14
```

To see this in the analyzer:

1. Follow the above instruction to analyze OInt.

2. In the source code display, click on line with "P6der.evalInt(y);". The selected line will become selected (cyan color) and information on the right is updated.

3. Note that the value of 1st operand on line 18 is {[0,0]}. This means that y is 0.

4. Note the explanation in "value 0 due to ...". 


**For OIntBad, after the loop, x>0.**

The explanation from Fig. 8:

```
    range [0, +inf] due to loop in class OIntBad on field x. 
      starting with: value 0 due to constant
      cycle with:
        range [1, +inf] due to operation IADD
          parameter 1: range [0,+inf] due to field x at the head of the loop in the class OIntBad. 
          parameter 2: value 1 due to constant.
```

To see this in the analyzer:

1. Follow the above instruction, but select OIntBad instead of OInt.

2. Scroll down in the source code listing to find the class OIntBad (in the OInt.java file). In the source code display, click on line with "P6der.evalInt(x);".

3. Note that the the value of 1st operand on line 36 is now {[0,+inf]}, as escaped variables are computed flow-insensitively.

4. Note the explanation in "range [0,+inf] due to ...". 

 
## Modifying the Põder analyzer 

There is a tutorial for writing a simple constant propagation analysis here:

* ConstProp.scala

Here is a template for a simple analysis:

* Value.scala

The analysis described in the paper is implemented mainly in the following files:

* ObjectEscape.scala -- the helper analysis to decide if 'this' has escaped;
* Expr.scala         -- what expressions to generate;
* IntSetExpr.scala   -- using the two above; adds interval evaluation and explanations.

Note that the mentioned files are in ~/poder/src/main/scala/põder/analyses/ .

After modifications, recompile with 'make' or 'sbt assembly' in ~/poder to generate a fresh JAR file ~/poder/target/scala-3.1.1/põder-1.0.jar .



## Description of the environment

A virtual machine might not be the optimal way for software development. Refer to https://bitbucket.org/kalmera/poder to get an up-to-date development environment.

Here we describe the steps performed for setting up an development environment for the virtual machine.

1. Java+Scala ecosystem was installed via sdkman from https://sdkman.io/ . Installed Java using "sdk install java" and SBT via "sdk install sbt".

2. The source code of Põder is available under ~/poder. This has been achieved using the command "git clone https://bitbucket.org/kalmera/poder.git".

3. Põder has been compiled into JAR file: ~/poder/target/scala-3.1.1/põder-1.0.jar .  This has been acheieved by running "sbt assembly" in ~/poder .

4. The test code is in file ~/test/OInt.java. This is the same file as https://bitbucket.org/kalmera/poder/downloads/OInt.java

5. The test code is compiled into class files using "javac -g OInt.java" in ~/test. 
